<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('login','LoginApiController@Login');
Route::get('project/list','ProjectApiController@list');
Route::post('project/list/detail','ProjectApiController@ProjectDetail');
Route::post('project/save','ProjectApiController@store');
Route::get('project/generate/location','ProjectApiController@getAllLocation');
Route::get('project/province','ProjectApiController@province');
Route::get('project/district/{id}','ProjectApiController@district');
Route::get('project/vdc/{id}','ProjectApiController@vdc');
Route::get('project/others','ProjectApiController@formOthers');
Route::post('project/store','ProjectApiController@store');
Route::get('household/cast_master','HouseholdApiController@casts_master');
Route::get('household/income_source','HouseholdApiController@incomeSource');
Route::get('household/cast/{id}','HouseholdApiController@cast');
Route::get('household/occupation','HouseholdApiController@occupation');
Route::post('household/save','HouseholdApiController@store');
Route::post('household/water_fetching','HouseholdApiController@waterFetching');
Route::post('household/store_hygiene','HouseholdApiController@storeHygiene');
Route::post('household/store_incidence','HouseholdApiController@storeIncidence');
Route::post('household/store_health','HouseholdApiController@storeHealthBehavior');
Route::post('household/store_inclusion','HouseholdApiController@storeInclusionData');
Route::post('wusc_store','WuscApiController@storeWusc');
Route::any('home', function () {
    return response('Hello World', 200)
                  ->header('Content-Type', 'text/plain');
});