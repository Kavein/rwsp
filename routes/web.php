<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/stats','AgeGroupController@ageGroupChart')->name('dashboard.stats');

Route::post('store_user','HomeController@storeUser')->name('user.store');
// Route::group(array('module'=>'Admin'), function() {
//     //Your routes belong to this module.
//     Route::get('admin/dashboard','AdminController@index');
// });

Route::group(['prefix'=>'admin','middleware'=>'auth'],function(){

    Route::get('project/agegroup/export/{id}','ProjectReportController@exportAgeGroup')->name('admin.ageGroup.export');
    Route::get('project/hygiene/export/{id}','ProjectReportController@exportHygiene')->name('admin.project.hygiene.export');
    Route::get('project/improved_hygiene/export/{id}','ProjectReportController@exportImprovedHygiene')->name('admin.project.improvedhygiene.export');
    Route::get('project/handwash/export/{id}','ProjectReportController@exportHandwash')->name('admin.project.handwash.export');
    Route::get('project/social_economic_status/export/{id}','ProjectReportController@SocialEconomicExport')->name('admin.project.socialeconomic.export');
    Route::get('project/water_borne/export/{id}','ProjectReportController@exportWaterBorne')->name('admin.project.waterBorne.export');
    Route::get('project/women_status/export/{id}','ProjectReportController@womenHousehold')->name('admin.project.womenStatus.export');
    Route::get('project/water_fetcching/export/{id}','ProjectReportController@waterFetching')->name('admin.project.waterFetching.export');
    Route::get('project/cast_wise/export/{id}','ProjectReportController@CastWise')->name('admin.project.castWise.export');
    Route::get('project/average_cost/export/{id}','ProjectReportController@AverageCost')->name('admin.project.average_cost.export');
    Route::get('project/logframe/export/{id}','ProjectReportController@logframe')->name('admin.project.logframe.export');
});