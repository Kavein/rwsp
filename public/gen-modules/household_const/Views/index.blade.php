@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Household_consts		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Household_consts</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.household_consts.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="household_const-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >House_no</th>
<th >Project_id</th>
<th >Initial_id</th>
<th >Head_name</th>
<th >Head_gender</th>
<th >Tole_name</th>
<th >Total_male</th>
<th >Total_female</th>
<th >Total_member</th>
<th >Cast_id</th>
<th >Cast_division</th>
<th >Total_female_widow</th>
<th >Litrate_no</th>
<th >Illitrate_no</th>
<th >Income_pension</th>
<th >Other_income_source</th>
<th >Religion_minority</th>
<th >Status</th>
<th >Del_flag</th>
<th >Created_by</th>
<th >Updated_by</th>
<th >Created_at</th>
<th >Updated_at</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#household_const-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.household_consts.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "id",name: "id"},
            { data: "house_no",name: "house_no"},
            { data: "project_id",name: "project_id"},
            { data: "initial_id",name: "initial_id"},
            { data: "head_name",name: "head_name"},
            { data: "head_gender",name: "head_gender"},
            { data: "tole_name",name: "tole_name"},
            { data: "total_male",name: "total_male"},
            { data: "total_female",name: "total_female"},
            { data: "total_member",name: "total_member"},
            { data: "cast_id",name: "cast_id"},
            { data: "cast_division",name: "cast_division"},
            { data: "total_female_widow",name: "total_female_widow"},
            { data: "litrate_no",name: "litrate_no"},
            { data: "illitrate_no",name: "illitrate_no"},
            { data: "income_pension",name: "income_pension"},
            { data: "other_income_source",name: "other_income_source"},
            { data: "religion_minority",name: "religion_minority"},
            { data: "status",name: "status"},
            { data: "del_flag",name: "del_flag"},
            { data: "created_by",name: "created_by"},
            { data: "updated_by",name: "updated_by"},
            { data: "created_at",name: "created_at"},
            { data: "updated_at",name: "updated_at"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
