<?php



Route::group(array('prefix'=>'admin/','module'=>'Household_const','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Household_const\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_consts/','AdminHousehold_constController@index')->name('admin.household_consts');
    Route::post('household_consts/gethousehold_constsJson','AdminHousehold_constController@gethousehold_constsJson')->name('admin.household_consts.getdatajson');
    Route::get('household_consts/create','AdminHousehold_constController@create')->name('admin.household_consts.create');
    Route::post('household_consts/store','AdminHousehold_constController@store')->name('admin.household_consts.store');
    Route::get('household_consts/show/{id}','AdminHousehold_constController@show')->name('admin.household_consts.show');
    Route::get('household_consts/edit/{id}','AdminHousehold_constController@edit')->name('admin.household_consts.edit');
    Route::match(['put', 'patch'], 'household_consts/update/{id}','AdminHousehold_constController@update')->name('admin.household_consts.update');
    Route::get('household_consts/delete/{id}', 'AdminHousehold_constController@destroy')->name('admin.household_consts.edit');
});




Route::group(array('module'=>'Household_const','namespace' => 'App\Modules\Household_const\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_consts/','Household_constController@index')->name('household_consts');
    
});