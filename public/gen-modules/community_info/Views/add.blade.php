@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Community_infos   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.community_infos') }}">prj_community_infos</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.community_infos.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="community_name">Community_name</label><input type="text" name="community_name" id="community_name" class="form-control" ></div><div class="form-group">
                                    <label for="male_students">Male_students</label><input type="text" name="male_students" id="male_students" class="form-control" ></div><div class="form-group">
                                    <label for="female_students">Female_students</label><input type="text" name="female_students" id="female_students" class="form-control" ></div><div class="form-group">
                                    <label for="total_students">Total_students</label><input type="text" name="total_students" id="total_students" class="form-control" ></div><div class="form-group">
                                    <label for="total_staff">Total_staff</label><input type="text" name="total_staff" id="total_staff" class="form-control" ></div><div class="form-group">
                                    <label for="existing_latrines">Existing_latrines</label><input type="text" name="existing_latrines" id="existing_latrines" class="form-control" ></div><div class="form-group">
                                    <label for="tapstand_required">Tapstand_required</label><input type="text" name="tapstand_required" id="tapstand_required" class="form-control" ></div><div class="form-group">
                                    <label for="other_existing_latrines">Other_existing_latrines</label><input type="text" name="other_existing_latrines" id="other_existing_latrines" class="form-control" ></div><div class="form-group">
                                    <label for="other_tapstand_required">Other_tapstand_required</label><input type="text" name="other_tapstand_required" id="other_tapstand_required" class="form-control" ></div><div class="form-group">
                                    <label for="diarrhoea_male">Diarrhoea_male</label><input type="text" name="diarrhoea_male" id="diarrhoea_male" class="form-control" ></div><div class="form-group">
                                    <label for="diarrhoea_female">Diarrhoea_female</label><input type="text" name="diarrhoea_female" id="diarrhoea_female" class="form-control" ></div><div class="form-group">
                                    <label for="diarrhoea_total">Diarrhoea_total</label><input type="text" name="diarrhoea_total" id="diarrhoea_total" class="form-control" ></div><div class="form-group">
                                    <label for="dysentery_male">Dysentery_male</label><input type="text" name="dysentery_male" id="dysentery_male" class="form-control" ></div><div class="form-group">
                                    <label for="dysentery_female">Dysentery_female</label><input type="text" name="dysentery_female" id="dysentery_female" class="form-control" ></div><div class="form-group">
                                    <label for="dysentery_total">Dysentery_total</label><input type="text" name="dysentery_total" id="dysentery_total" class="form-control" ></div><div class="form-group">
                                    <label for="jaundice_male">Jaundice_male</label><input type="text" name="jaundice_male" id="jaundice_male" class="form-control" ></div><div class="form-group">
                                    <label for="jaundice_female">Jaundice_female</label><input type="text" name="jaundice_female" id="jaundice_female" class="form-control" ></div><div class="form-group">
                                    <label for="jaundice_total">Jaundice_total</label><input type="text" name="jaundice_total" id="jaundice_total" class="form-control" ></div><div class="form-group">
                                    <label for="colera_male">Colera_male</label><input type="text" name="colera_male" id="colera_male" class="form-control" ></div><div class="form-group">
                                    <label for="colera_female">Colera_female</label><input type="text" name="colera_female" id="colera_female" class="form-control" ></div><div class="form-group">
                                    <label for="colera_total">Colera_total</label><input type="text" name="colera_total" id="colera_total" class="form-control" ></div><div class="form-group">
                                    <label for="worms_male">Worms_male</label><input type="text" name="worms_male" id="worms_male" class="form-control" ></div><div class="form-group">
                                    <label for="worms_female">Worms_female</label><input type="text" name="worms_female" id="worms_female" class="form-control" ></div><div class="form-group">
                                    <label for="worms_total">Worms_total</label><input type="text" name="worms_total" id="worms_total" class="form-control" ></div><div class="form-group">
                                    <label for="scabies_male">Scabies_male</label><input type="text" name="scabies_male" id="scabies_male" class="form-control" ></div><div class="form-group">
                                    <label for="scabies_female">Scabies_female</label><input type="text" name="scabies_female" id="scabies_female" class="form-control" ></div><div class="form-group">
                                    <label for="scabies_total">Scabies_total</label><input type="text" name="scabies_total" id="scabies_total" class="form-control" ></div><div class="form-group">
                                    <label for="profiling_date">Profiling_date</label><input type="text" name="profiling_date" id="profiling_date" class="form-control" ></div><div class="form-group">
                                    <label for="disabled_male">Disabled_male</label><input type="text" name="disabled_male" id="disabled_male" class="form-control" ></div><div class="form-group">
                                    <label for="disabled_female">Disabled_female</label><input type="text" name="disabled_female" id="disabled_female" class="form-control" ></div><div class="form-group">
                                    <label for="blind_male">Blind_male</label><input type="text" name="blind_male" id="blind_male" class="form-control" ></div><div class="form-group">
                                    <label for="blind_female">Blind_female</label><input type="text" name="blind_female" id="blind_female" class="form-control" ></div><div class="form-group">
                                    <label for="deaf_male">Deaf_male</label><input type="text" name="deaf_male" id="deaf_male" class="form-control" ></div><div class="form-group">
                                    <label for="deaf_female">Deaf_female</label><input type="text" name="deaf_female" id="deaf_female" class="form-control" ></div><div class="form-group">
                                    <label for="blind_deaf_male">Blind_deaf_male</label><input type="text" name="blind_deaf_male" id="blind_deaf_male" class="form-control" ></div><div class="form-group">
                                    <label for="blind_deaf_female">Blind_deaf_female</label><input type="text" name="blind_deaf_female" id="blind_deaf_female" class="form-control" ></div><div class="form-group">
                                    <label for="speech_male">Speech_male</label><input type="text" name="speech_male" id="speech_male" class="form-control" ></div><div class="form-group">
                                    <label for="speech_female">Speech_female</label><input type="text" name="speech_female" id="speech_female" class="form-control" ></div><div class="form-group">
                                    <label for="mental_male">Mental_male</label><input type="text" name="mental_male" id="mental_male" class="form-control" ></div><div class="form-group">
                                    <label for="mental_female">Mental_female</label><input type="text" name="mental_female" id="mental_female" class="form-control" ></div><div class="form-group">
                                    <label for="intel_male">Intel_male</label><input type="text" name="intel_male" id="intel_male" class="form-control" ></div><div class="form-group">
                                    <label for="intel_female">Intel_female</label><input type="text" name="intel_female" id="intel_female" class="form-control" ></div><div class="form-group">
                                    <label for="multiple_male">Multiple_male</label><input type="text" name="multiple_male" id="multiple_male" class="form-control" ></div><div class="form-group">
                                    <label for="multiple_female">Multiple_female</label><input type="text" name="multiple_female" id="multiple_female" class="form-control" ></div><div class="form-group">
                                    <label for="community_mapping">Community_mapping</label><input type="text" name="community_mapping" id="community_mapping" class="form-control" ></div><div class="form-group">
                                    <label for="project_id">Project_id</label><input type="text" name="project_id" id="project_id" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.community_infos') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
