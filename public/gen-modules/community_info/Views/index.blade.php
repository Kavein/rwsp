@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Community_infos		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Community_infos</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.community_infos.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="community_info-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Community_name</th>
<th >Male_students</th>
<th >Female_students</th>
<th >Total_students</th>
<th >Total_staff</th>
<th >Existing_latrines</th>
<th >Tapstand_required</th>
<th >Other_existing_latrines</th>
<th >Other_tapstand_required</th>
<th >Diarrhoea_male</th>
<th >Diarrhoea_female</th>
<th >Diarrhoea_total</th>
<th >Dysentery_male</th>
<th >Dysentery_female</th>
<th >Dysentery_total</th>
<th >Jaundice_male</th>
<th >Jaundice_female</th>
<th >Jaundice_total</th>
<th >Colera_male</th>
<th >Colera_female</th>
<th >Colera_total</th>
<th >Worms_male</th>
<th >Worms_female</th>
<th >Worms_total</th>
<th >Scabies_male</th>
<th >Scabies_female</th>
<th >Scabies_total</th>
<th >Profiling_date</th>
<th >Disabled_male</th>
<th >Disabled_female</th>
<th >Blind_male</th>
<th >Blind_female</th>
<th >Deaf_male</th>
<th >Deaf_female</th>
<th >Blind_deaf_male</th>
<th >Blind_deaf_female</th>
<th >Speech_male</th>
<th >Speech_female</th>
<th >Mental_male</th>
<th >Mental_female</th>
<th >Intel_male</th>
<th >Intel_female</th>
<th >Multiple_male</th>
<th >Multiple_female</th>
<th >Community_mapping</th>
<th >Project_id</th>

								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#community_info-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.community_infos.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "id",name: "id"},
            { data: "community_name",name: "community_name"},
            { data: "male_students",name: "male_students"},
            { data: "female_students",name: "female_students"},
            { data: "total_students",name: "total_students"},
            { data: "total_staff",name: "total_staff"},
            { data: "existing_latrines",name: "existing_latrines"},
            { data: "tapstand_required",name: "tapstand_required"},
            { data: "other_existing_latrines",name: "other_existing_latrines"},
            { data: "other_tapstand_required",name: "other_tapstand_required"},
            { data: "diarrhoea_male",name: "diarrhoea_male"},
            { data: "diarrhoea_female",name: "diarrhoea_female"},
            { data: "diarrhoea_total",name: "diarrhoea_total"},
            { data: "dysentery_male",name: "dysentery_male"},
            { data: "dysentery_female",name: "dysentery_female"},
            { data: "dysentery_total",name: "dysentery_total"},
            { data: "jaundice_male",name: "jaundice_male"},
            { data: "jaundice_female",name: "jaundice_female"},
            { data: "jaundice_total",name: "jaundice_total"},
            { data: "colera_male",name: "colera_male"},
            { data: "colera_female",name: "colera_female"},
            { data: "colera_total",name: "colera_total"},
            { data: "worms_male",name: "worms_male"},
            { data: "worms_female",name: "worms_female"},
            { data: "worms_total",name: "worms_total"},
            { data: "scabies_male",name: "scabies_male"},
            { data: "scabies_female",name: "scabies_female"},
            { data: "scabies_total",name: "scabies_total"},
            { data: "profiling_date",name: "profiling_date"},
            { data: "disabled_male",name: "disabled_male"},
            { data: "disabled_female",name: "disabled_female"},
            { data: "blind_male",name: "blind_male"},
            { data: "blind_female",name: "blind_female"},
            { data: "deaf_male",name: "deaf_male"},
            { data: "deaf_female",name: "deaf_female"},
            { data: "blind_deaf_male",name: "blind_deaf_male"},
            { data: "blind_deaf_female",name: "blind_deaf_female"},
            { data: "speech_male",name: "speech_male"},
            { data: "speech_female",name: "speech_female"},
            { data: "mental_male",name: "mental_male"},
            { data: "mental_female",name: "mental_female"},
            { data: "intel_male",name: "intel_male"},
            { data: "intel_female",name: "intel_female"},
            { data: "multiple_male",name: "multiple_male"},
            { data: "multiple_female",name: "multiple_female"},
            { data: "community_mapping",name: "community_mapping"},
            { data: "project_id",name: "project_id"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-red waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
