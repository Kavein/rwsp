<?php

namespace App\Modules\Community_info\Model;


use Illuminate\Database\Eloquent\Model;

class Community_info extends Model
{
    public  $table = 'prj_community_infos';

    protected $fillable = ['id','community_name','male_students','female_students','total_students','total_staff','existing_latrines','tapstand_required','other_existing_latrines','other_tapstand_required','diarrhoea_male','diarrhoea_female','diarrhoea_total','dysentery_male','dysentery_female','dysentery_total','jaundice_male','jaundice_female','jaundice_total','colera_male','colera_female','colera_total','worms_male','worms_female','worms_total','scabies_male','scabies_female','scabies_total','profiling_date','disabled_male','disabled_female','blind_male','blind_female','deaf_male','deaf_female','blind_deaf_male','blind_deaf_female','speech_male','speech_female','mental_male','mental_female','intel_male','intel_female','multiple_male','multiple_female','community_mapping','project_id',];
}
