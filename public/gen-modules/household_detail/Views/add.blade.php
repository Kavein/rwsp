@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Household_details   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.household_details') }}">tbl_household_details</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.household_details.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="house_no">House_no</label><input type="text" name="house_no" id="house_no" class="form-control" ></div><div class="form-group">
                                    <label for="project_id">Project_id</label><input type="text" name="project_id" id="project_id" class="form-control" ></div><div class="form-group">
                                    <label for="head_name">Head_name</label><input type="text" name="head_name" id="head_name" class="form-control" ></div><div class="form-group">
                                    <label for="head_gender">Head_gender</label><input type="text" name="head_gender" id="head_gender" class="form-control" ></div><div class="form-group">
                                    <label for="tole_name">Tole_name</label><input type="text" name="tole_name" id="tole_name" class="form-control" ></div><div class="form-group">
                                    <label for="total_male">Total_male</label><input type="text" name="total_male" id="total_male" class="form-control" ></div><div class="form-group">
                                    <label for="total_female">Total_female</label><input type="text" name="total_female" id="total_female" class="form-control" ></div><div class="form-group">
                                    <label for="total_member">Total_member</label><input type="text" name="total_member" id="total_member" class="form-control" ></div><div class="form-group">
                                    <label for="cast_id">Cast_id</label><input type="text" name="cast_id" id="cast_id" class="form-control" ></div><div class="form-group">
                                    <label for="religion_id">Religion_id</label><input type="text" name="religion_id" id="religion_id" class="form-control" ></div><div class="form-group">
                                    <label for="religion_minority">Religion_minority</label><input type="text" name="religion_minority" id="religion_minority" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" name="status" id="status" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="updated_by">Updated_by</label><input type="text" name="updated_by" id="updated_by" class="form-control" ></div><div class="form-group">
                                    <label for="created_at">Created_at</label><input type="text" name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" name="updated_at" id="updated_at" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.household_details') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
