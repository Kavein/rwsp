<?php

namespace App\Modules\Household_detail\Model;


use Illuminate\Database\Eloquent\Model;

class Household_detail extends Model
{
    public  $table = 'tbl_household_details';

    protected $fillable = ['id','house_no','project_id','head_name','head_gender','tole_name','total_male','total_female','total_member','cast_id','religion_id','religion_minority','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
