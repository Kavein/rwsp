<?php



Route::group(array('prefix'=>'admin/','module'=>'Household_detail','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_details/','AdminHousehold_detailController@index')->name('admin.household_details');
    Route::post('household_details/gethousehold_detailsJson','AdminHousehold_detailController@gethousehold_detailsJson')->name('admin.household_details.getdatajson');
    Route::get('household_details/create','AdminHousehold_detailController@create')->name('admin.household_details.create');
    Route::post('household_details/store','AdminHousehold_detailController@store')->name('admin.household_details.store');
    Route::get('household_details/show/{id}','AdminHousehold_detailController@show')->name('admin.household_details.show');
    Route::get('household_details/edit/{id}','AdminHousehold_detailController@edit')->name('admin.household_details.edit');
    Route::match(['put', 'patch'], 'household_details/update/{id}','AdminHousehold_detailController@update')->name('admin.household_details.update');
    Route::get('household_details/delete/{id}', 'AdminHousehold_detailController@destroy')->name('admin.household_details.edit');
});




Route::group(array('module'=>'Household_detail','namespace' => 'App\Modules\Household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_details/','Household_detailController@index')->name('household_details');
    
});