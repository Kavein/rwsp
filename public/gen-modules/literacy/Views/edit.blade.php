@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Literacies   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.literacies') }}">mst_literacies</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.literacies.update') }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                                    <label for="name">Name</label><input type="text" value = "{{$literacy->name}}"  name="name" id="name" class="form-control" ></div><div class="form-group">
                                    <label for="status">Status</label><input type="text" value = "{{$literacy->status}}"  name="status" id="status" class="form-control" ></div><div class="form-group">
                                    <label for="del_flag">Del_flag</label><input type="text" value = "{{$literacy->del_flag}}"  name="del_flag" id="del_flag" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" value = "{{$literacy->created_by}}"  name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="updated_by">Updated_by</label><input type="text" value = "{{$literacy->updated_by}}"  name="updated_by" id="updated_by" class="form-control" ></div><div class="form-group">
                                    <label for="created_at">Created_at</label><input type="text" value = "{{$literacy->created_at}}"  name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" value = "{{$literacy->updated_at}}"  name="updated_at" id="updated_at" class="form-control" ></div>
<input type="hidden" name="id" id="id" value = "{{$literacy->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.literacies') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
