<?php



Route::group(array('prefix'=>'admin/','module'=>'Literacy','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Literacy\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('literacies/','AdminLiteracyController@index')->name('admin.literacies');
    Route::post('literacies/getliteraciesJson','AdminLiteracyController@getliteraciesJson')->name('admin.literacies.getdatajson');
    Route::get('literacies/create','AdminLiteracyController@create')->name('admin.literacies.create');
    Route::post('literacies/store','AdminLiteracyController@store')->name('admin.literacies.store');
    Route::get('literacies/show/{id}','AdminLiteracyController@show')->name('admin.literacies.show');
    Route::get('literacies/edit/{id}','AdminLiteracyController@edit')->name('admin.literacies.edit');
    Route::match(['put', 'patch'], 'literacies/update/{id}','AdminLiteracyController@update')->name('admin.literacies.update');
    Route::get('literacies/delete/{id}', 'AdminLiteracyController@destroy')->name('admin.literacies.edit');
});




Route::group(array('module'=>'Literacy','namespace' => 'App\Modules\Literacy\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('literacies/','LiteracyController@index')->name('literacies');
    
});