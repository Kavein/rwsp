<?php



Route::group(array('prefix'=>'admin/','module'=>'Income_source','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Income_source\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('income_sources/','AdminIncome_sourceController@index')->name('admin.income_sources');
    Route::post('income_sources/getincome_sourcesJson','AdminIncome_sourceController@getincome_sourcesJson')->name('admin.income_sources.getdatajson');
    Route::get('income_sources/create','AdminIncome_sourceController@create')->name('admin.income_sources.create');
    Route::post('income_sources/store','AdminIncome_sourceController@store')->name('admin.income_sources.store');
    Route::get('income_sources/show/{id}','AdminIncome_sourceController@show')->name('admin.income_sources.show');
    Route::get('income_sources/edit/{id}','AdminIncome_sourceController@edit')->name('admin.income_sources.edit');
    Route::match(['put', 'patch'], 'income_sources/update/{id}','AdminIncome_sourceController@update')->name('admin.income_sources.update');
    Route::get('income_sources/delete/{id}', 'AdminIncome_sourceController@destroy')->name('admin.income_sources.edit');
});




Route::group(array('module'=>'Income_source','namespace' => 'App\Modules\Income_source\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('income_sources/','Income_sourceController@index')->name('income_sources');
    
});