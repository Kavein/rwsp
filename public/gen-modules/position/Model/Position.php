<?php

namespace App\Modules\Position\Model;


use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    public  $table = 'mst_positions';

    protected $fillable = ['id','name','status','del_flag','created_by','created_at','updated_by','updated_at',];
}
