<?php



Route::group(array('prefix'=>'admin/','module'=>'Prep_household_detail','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Prep_household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('prep_household_details/','AdminPrep_household_detailController@index')->name('admin.prep_household_details');
    Route::post('prep_household_details/getprep_household_detailsJson','AdminPrep_household_detailController@getprep_household_detailsJson')->name('admin.prep_household_details.getdatajson');
    Route::get('prep_household_details/create','AdminPrep_household_detailController@create')->name('admin.prep_household_details.create');
    Route::post('prep_household_details/store','AdminPrep_household_detailController@store')->name('admin.prep_household_details.store');
    Route::get('prep_household_details/show/{id}','AdminPrep_household_detailController@show')->name('admin.prep_household_details.show');
    Route::get('prep_household_details/edit/{id}','AdminPrep_household_detailController@edit')->name('admin.prep_household_details.edit');
    Route::match(['put', 'patch'], 'prep_household_details/update/{id}','AdminPrep_household_detailController@update')->name('admin.prep_household_details.update');
    Route::get('prep_household_details/delete/{id}', 'AdminPrep_household_detailController@destroy')->name('admin.prep_household_details.edit');
});




Route::group(array('module'=>'Prep_household_detail','namespace' => 'App\Modules\Prep_household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('prep_household_details/','Prep_household_detailController@index')->name('prep_household_details');
    
});