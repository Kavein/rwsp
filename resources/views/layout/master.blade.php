<html lang="en">
<head>
    <title>{{ $page['title'] }}</title>
    <meta name="description" content="@yield('page_description')">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width">
    <link rel="shortcut icon" type="image/x-icon" href="img/site_logo.png">
    @include('layout.ogfile')
    
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:400,600" rel="stylesheet">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <!-- Alertify CSS -->
    <link rel="stylesheet" href="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/css/alertify.min.css"/>
    <link rel="stylesheet" href="{{asset('css/bowercomponent/slick-theme.css')}}">
    <link rel="stylesheet" href="{{asset('css/bowercomponent/slick.css')}}">
    <link rel="stylesheet" href="{{asset('css/bowercomponent/photoswipe.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bowercomponent/default-skin.min.css')}}">

    <link rel="stylesheet" href="{{asset('css/bowercomponent/jquery.mCustomScrollbar.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/bowercomponent/lightbox.min.css')}}">
    <link rel="stylesheet" href="{{asset('css/app.css?ver=4')}}">
    <link rel="stylesheet" href="{{asset('css/global.css?ver=4')}}">
    <link rel="stylesheet" href="{{asset('css/thestyles.css?ver=4')}}">
    <link rel="stylesheet" href="{{asset('css/responsive.css?ver=4')}}">

    {{--{!! Html::style('css/bowercomponent/slick-theme.css') !!}--}}
    {{--{!! Html::style('css/bowercomponent/slick.css') !!}--}}
    {{--{!! Html::style('css/bowercomponent/jquery.mCustomScrollbar.min.css') !!}--}}
    {{--{!! Html::style('css/bowercomponent/lightbox.min.css') !!}--}}

    {{--{!! Html::style('css/app.css') !!}--}}
    {{--{!! Html::style('css/global.css') !!}--}}
    {{--{!! Html::style('css/thestyles.css') !!}--}}
    {{--{!! Html::style('css/responsive.css') !!}--}}
    <script type="text/javascript" src="{{ asset('js/jquery.min.js') }}"></script>
</head>

<body class = "{{ (isset($page['class']) && $page['class'])?$page['class']:'' }}">
<div id="wrapper">
    @include('layout.header')
    <!-------------------------------- Content Wrapper Starts ---------------------------------->
    <div id="content-wrapper">
         @yield('content') 
    </div>
    <!--------------------------------- Content Wrapper Ends ----------------------------------->
    @include('layout.footer')
</div>

<!--------------------- Maintenance Start -------------------->
<div class="modal fade" id="theMaintenanceModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i class="fa fa-times"></i>
                </button>
            </div>
            <div class="modal-body">
                <div class="modal-loading-area">

                    <div class="image-container">
                        <img src="{{ asset('img/maintanance.png') }}">
                    </div>

                    <div class="text-container">
                        <h1>Website under maintenance</h1>
                        <p>We are doing some housekeeping – pardon our appearance.</p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
<!--------------------- Maintenance Start -------------------->


@include('layout.script')
@yield('custom_script')

<script src="//cdn.jsdelivr.net/npm/alertifyjs@1.11.2/build/alertify.min.js"></script>
</body>
</html>