<html><head>
	<title>WaterBorne Disease Report</title>
</head>
<body>
<table border="1">
		<tbody>
			<tr>
				<td colspan="49">WaterBorne Disease F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="49"> <h3>WaterBorne Disease Status</h3></th>
			</tr>
			<tr>
				<td colspan="6" rowspan="2"></td>
				<td colspan="4">Diarrhoea</td>
				<td colspan="4">Dysentry</td>
				<td colspan="4">Jaundice</td>
				<td colspan="4">Cholera </td>
				<td colspan="4">Scabies</td>
				<td colspan="4">Worms</td>
				<td rowspan="3">Number of days absent form school due to sickness</td>
				<td rowspan="3">Number of max sick days</td>
				<td rowspan="3">Death Cause</td>
				<td colspan="3" rowspan="2">Health Seeking Behaviour</td>
				<td rowspan="3">Annual Expennse</td>

			</tr>

			<tr>

				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
				<td colspan="2">0-5 year</td>
				<td colspan="2">6 year above</td>
				
			</tr>

			<tr>
				<td>S.No</td>
				<td>Project No.</td>
				<td>Project Name</td>
				<td>District</td>
				<td>VDC</td>
				<td>Ward No</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Total</td>
				<td>Frequency</td>

				<td>Prefering Health Post</td>
				<td>Prefering Traditional Healer</td>
				<td>Prefering Homemade Medicines</td>
			</tr>

			<tr>
				<td></td>
				<td>New/Rebuild</td>
				<td>West(Post-Monsoon)</td>
				<td colspan="46"></td>
			</tr>
			@php
				$total=array(
					'diarrhoea'=>0,
					'dysentry'=>0,
					'jaundice'=>0,
					'cholera'=>0,
					'scabies'=>0,
					'worms'=>0
				);
			@endphp
			@php
			

			$sum_diarrhoea_between0and5_total_post = 0;
			$sum_diarrhoea_between0and5_frequency_post = 0;
			$sum_diarrhoea_above6_total_post = 0;
			$sum_diarrhoea_above6_frequency_post = 0;

			$sum_dysentry_between0and5_total_post = 0;
			$sum_dysentry_between0and5_frequency_post = 0;
			$sum_dysentry_above6_total_post = 0;
			$sum_dysentry_above6_frequency_post = 0;

			$sum_jaundice_between0and5_total_post = 0;
			$sum_jaundice_between0and5_frequency_post = 0;
			$sum_jaundice_above6_total_post = 0;
			$sum_jaundice_above6_frequency_post = 0;

			$sum_colera_between0and5_total_post = 0;
			$sum_colera_between0and5_frequency_post = 0;
			$sum_colera_above6_total_post = 0;
			$sum_colera_above6_frequency_post = 0;

			$sum_scabies_between0and5_total_post = 0;
			$sum_scabies_between0and5_frequency_post = 0;
			$sum_scabies_above6_total_post = 0;
			$sum_scabies_above6_frequency_post = 0;

			
			$sum_worms_between0and5_total_post = 0;
			$sum_worms_between0and5_frequency_post = 0;
			$sum_worms_above6_total_post = 0;
			$sum_worms_above6_frequency_post = 0;

			$sum_max_absence_post=0;
			$sum_max_sick_post=0;
			$sum_healthpost_preference_post=0;
			$sum_traditional_preference_post=0;
			$sum_home_medicine_preference_post=0;
			$sum_max_annual_expense_post=0;

			@endphp

			@foreach($data as $key => $value)
			<tr>
				<td>{{@$key+1}}</td>
				<td>{{@$value->project_no}}</td>
				<td>{{@$value->project_name}}</td>
				<td>{{@$value->district}}</td>
				<td>{{@$value->vdc}}</td>
				<td>{{@$value->ward_no}}</td>
				<td>{{@$value->diarrhoea_between0and5_total}}</td>
				<td>{{@$value->diarrhoea_between0and5_frequency}}</td>
				<td>{{@$value->diarrhoea_above6_total}}</td>
				<td>{{@$value->diarrhoea_above6_frequency}}</td>
				<td>{{@$value->dysentry_between0and5_total}}</td>
				<td>{{@$value->dysentry_between0and5_frequency}}</td>
				<td>{{@$value->dysentry_above6_total}}</td>
				<td>{{@$value->dysentry_above6_frequency}}</td>
				<td>{{@$value->jaundice_between0and5_total}}</td>
				<td>{{@$value->jaundice_between0and5_frequency}}</td>
				<td>{{@$value->jaundice_above6_total}}</td>
				<td>{{@$value->jaundice_above6_frequency}}</td> 
				<td>{{@$value->colera_between0and5_total}}</td>
				<td>{{@$value->colera_between0and5_frequency}}</td>
				<td>{{@$value->colera_above6_total}}</td>
				<td>{{@$value->colera_above6_frequency}}</td>
				<td>{{@$value->scabies_between0and5_total}}</td>
				<td>{{@$value->scabies_between0and5_frequency}}</td>
				<td>{{@$value->scabies_above6_total}}</td>
				<td>{{@$value->scabies_above6_frequency}}</td>
				<td>{{@$value->worms_between0and5_total}}</td>
				<td>{{@$value->worms_between0and5_frequency}}</td>
				<td>{{@$value->worms_above6_total}}</td>
				<td>{{@$value->worms_above6_frequency}}</td>
				<td>{{@$value->max_absence}}</td>
				<td>{{@$value->max_sick}}</td>
				<td>{{@$value->max}}</td>

				<td>{{@$value->healthpost_preference}}</td>
				<td>{{@$value->traditional_preference}}</td>
				<td>{{@$value->home_medecine_preference}}</td>
				<td>{{@$value->max_annual_expense}}</td>
				


				@php 
				
				$sum_diarrhoea_between0and5_total_post += $value->diarrhoea_between0and5_total;
			$sum_diarrhoea_between0and5_frequency_post += $value->diarrhoea_between0and5_frequency;
			$sum_diarrhoea_above6_total_post +=$value->diarrhoea_above6_total ;
			$sum_diarrhoea_above6_frequency_post +=$value->diarrhoea_above6_frequency ;

			$sum_dysentry_between0and5_total_post += $value->dysentry_between0and5_total;
			$sum_dysentry_between0and5_frequency_post +=$value->dysentry_between0and5_frequency ;
			$sum_dysentry_above6_total_post += $value->dysentry_above6_total;
			$sum_dysentry_above6_frequency_post += $value->dysentry_above6_frequency;

			$sum_jaundice_between0and5_total_post += $value->jaundice_between0and5_total;
			$sum_jaundice_between0and5_frequency_post +=$value->jaundice_between0and5_frequency ;
			$sum_jaundice_above6_total_post += $value->jaundice_above6_total;
			$sum_jaundice_above6_frequency_post += $value->jaundice_above6_frequency;

			$sum_colera_between0and5_total_post += $value->colera_between0and5_total;
			$sum_colera_between0and5_frequency_post +=$value->colera_between0and5_frequency ;
			$sum_colera_above6_total_post += $value->colera_above6_total;
			$sum_colera_above6_frequency_post += $value->colera_above6_frequency;

			$sum_scabies_between0and5_total_post += $value->scabies_between0and5_total;
			$sum_scabies_between0and5_frequency_post +=$value->scabies_between0and5_frequency ;
			$sum_scabies_above6_total_post += $value->scabies_above6_total;
			$sum_scabies_above6_frequency_post += $value->scabies_above6_frequency;

			
			$sum_worms_between0and5_total_post += $value->worms_between0and5_total;
			$sum_worms_between0and5_frequency_post +=$value->worms_between0and5_frequency ;
			$sum_worms_above6_total_post += $value->worms_above6_total;
			$sum_worms_above6_frequency_post += $value->worms_above6_frequency;

			$sum_max_absence_post +=$value->max_absence;
			$sum_max_sick_post+=$value->max_sick;
			$sum_healthpost_preference_post +=$value->healthpost_preference;
			$sum_traditional_preference_post +=$value->traditional_preference;
			$sum_home_medicine_preference_post+=$value->home_medecine_preference;
			$sum_max_annual_expense_post +=$value->max_annual_expense;
			@endphp
			</tr>
			 
			
		
			
			@if($value->max=="scabies")
				@php $total['scabies']+=1; @endphp
			@elseif($value->max=="diarrhoea")
				@php $total['diarrhoea']+=1; @endphp
			@elseif($value->max=="dysentry")
				@php $total['dysentry']+=1; @endphp
			@elseif($value->max=="jaundice")
				@php $total['jaundice']+=1; @endphp
			@elseif($value->max=="cholera")
				@php $total['cholera']+=1; @endphp	
			@else
				@php $total['worms']+=1; @endphp	
			@endif
			@endforeach
			<tr>
				
				<td colspan="4"></td>
				<td colspan="2">Total </td>
				<td>{{@$sum_diarrhoea_between0and5_total_post }}</td>
				<td>{{@$sum_diarrhoea_between0and5_frequency_post }}</td>
				<td>{{@$sum_diarrhoea_above6_total_post }}</td>
				<td>{{@$sum_diarrhoea_above6_frequency_post }}</td>
				
				<td>{{@$sum_dysentry_between0and5_total_post }}</td>
				<td>{{@$sum_dysentry_between0and5_frequency_post }}</td>
				<td>{{@$sum_dysentry_above6_total_post }}</td>
				<td>{{ @$sum_dysentry_above6_frequency_post }}</td>
					
				<td>{{@$sum_jaundice_between0and5_total_post }}</td>
				<td>{{@$sum_jaundice_between0and5_frequency_post }}</td>
				<td>{{@$sum_jaundice_above6_total_post }}</td>
				<td>{{ @$sum_jaundice_above6_frequency_post }}</td>
					
				<td>{{@$sum_colera_between0and5_total_post }}</td>
				<td>{{@$sum_colera_between0and5_frequency_post }}</td>
				<td>{{@$sum_colera_above6_total_post }}</td>
				<td>{{@$sum_colera_above6_frequency_post }}</td>
					
				<td>{{@$sum_scabies_between0and5_total_post }}</td>
				<td>{{@$sum_scabies_between0and5_frequency_post }}</td>
				<td>{{@$sum_scabies_above6_total_post }}</td>
				<td>{{@$sum_scabies_above6_frequency_post }}</td>
					
				<td>{{@$sum_worms_between0and5_total_post }}</td>
				<td>{{@$sum_worms_between0and5_frequency_post }}</td>
				<td>{{@$sum_worms_above6_total_post }}</td>
				<td>{{@$sum_worms_above6_frequency_post }}</td>
				<td>{{@$sum_max_absence_post }}</td>
				<td>{{@$sum_max_sick_post }}</td>
				<td> - </td>
				<td>{{@$sum_healthpost_preference_post }}</td>
				<td>{{@$sum_traditional_preference_post }}</td>
				<td>{{@$sum_home_medicine_preference_post }}</td>
				<td>{{@$sum_max_annual_expense_post }}</td>
					
			</tr>		

		</tbody>
		
	</table>
</body></html>