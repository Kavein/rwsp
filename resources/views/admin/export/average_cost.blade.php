<html><head>
	<title>women_household Report</title>
</head>
	<table border="1">

		<tbody>
			<tr>
				<td colspan="7">Average Cost F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="7"> <h3>Women Household's Status</h3></th>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No.</td>
				<td>Project Name</td>
				<td>District</td>
				<td>G.P/N.P</td>
				<td>Ward No.</td>
				<td>Average Cost</td>
			</tr>
			<tr>
				<td colspan="7">New/Re-Build</td>
			</tr>
			@php
				$total_cost = 0;
			@endphp
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->district}}</td>
				<td>{{$value->vdc}}</td>
				<td>{{$value->ward_no}}</td>
				<td>{{$value->average_cost}}</td>
			</tr>

			@php
				$total_cost += $value->average_cost;
			@endphp
			@endforeach
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td>Total</td>
				<td>{{@$total_cost}}</td>
			</tr>


			
			
		</tbody>

	</table>
