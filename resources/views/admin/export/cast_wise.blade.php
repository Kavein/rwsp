
	<table border="1">
		<tbody>
			<tr>
				<td colspan="129">Cast Wise F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="129"> <h3>Cast Wise Status</h3></th>
			</tr>
			<tr>
				<td colspan="6" rowspan="3"></td>
					<?php $casts = ''; ?>
					<?php $cast_hh_pop = ''; ?>
					<?php $cast_m_f = ''; ?>
				@foreach($data->cast_divisions as $value)
					<td colspan="{{ count($value->casts)* 3}}">{{$value->name}}</td>
						@foreach($value->casts as $k => $v)
							<?php $casts .= '<td colspan="3">'.$v.'</td>'  ?> 
							<?php $cast_hh_pop .= '<td rowspan="2">HH</td><td colspan="2">POP</td>'  ?> 
							<?php $cast_m_f .= '<td>M</td><td>F</td>'  ?> 
						@endforeach
				@endforeach
			</tr>
			<tr>
				<?php echo $casts ?>
			</tr>
			<tr>
				<?php echo $cast_hh_pop ?>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>District</td>
				<td>VDC/Muncipality</td>
				<td>Ward No</td>
				<?php echo $cast_m_f ?>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->district}}</td>
				<td>{{$value->vdc}}</td>
				<td>{{$value->ward_no}}</td>
				@foreach($value->finals as $k => $v)
					<td>{{ ($v)?$v:'-' }}</td>
				@endforeach
			</tr>
			@endforeach

			
			
		</tbody>
		
	</table>
