<html><head>
	<title>women_household Report</title>
</head>
	<table border="1">

		<tbody>
			<tr>
				<td colspan="8">Women Household's F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="8"> <h3>Women Household's Status</h3></th>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No.</td>
				<td>Project Name</td>
				<td>District</td>
				<td>G.P/N.P</td>
				<td>Ward No.</td>
				<td>Women Household Head</td>
				<td>Widow</td>
			</tr>

			<tr>
				<td></td>
				<td>New/Rebuild</td>
				<td></td>
			</tr>
			@php
				$sum_female_head_number=0;
				$sum_widow_number=0;

				$sum_female_head_number_pre=0;
				$sum_widow_number_pre=0;
				$sum_female_head_number_post=0;
				$sum_widow_number_post=0;
			@endphp
			@foreach($data as $key => $value)
				<tr>
					<td>{{@$key+1}}</td>
					<td>{{@$value->project_no}}</td>
					<td>{{@$value->project_name}}</td>
					<td>{{@$value->district}}</td>
					<td>{{@$value->vdc}}</td>
					<td>{{@$value->ward_no}}</td>
					<td>{{@$value->female_head_number}}</td>
					<td>{{@$value->widow_number}}</td>
				</tr>
				@php
					$sum_female_head_number_post+=$value->female_head_number;
					$sum_widow_number_post+=$value->widow_number ;
				@endphp
			@endforeach
			<tr>
				<td colspan="4"></td>
				<td colspan="2">Total</td>
				<td>{{$sum_female_head_number_post}}</td>
				<td>{{$sum_widow_number_post}}</td>
			</tr>
		</tbody>
	</table>
