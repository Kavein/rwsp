<!DOCTYPE html>
<html>
<head>
	<title>Logframe Report</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="21">Logframe Report</td>
			</tr>
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">IMPACT</td>
			</tr>
			<tr>
				<td colspan="20">Improved health and quality of life, and reduced vulnerability of the population of Nepal, who benefit from a more disaster-resilient economy</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project Number</td>
				<td>Project Name</td>
				<td>(Impact Indicator 1) Infants with medium to severe stunting prevalence’s as recorded by height for age at local health post</td>
				<td>(Impact Indicator 1) Children under 5 years with reported diarrhoea through local health post</td>
				<td>(Impact Indicator 2) The amount of time saving in water collection by household</td>
				<td>(Impact Indicator 3) Reduced death toll / economic output remains stable in face of disasters</td>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->report_below_5}}</td>
				<td>{{$value->infants_height}}</td>
				<td>{{$value->total_second}}</td>
				<td>{{$value->death_toll}}</td>
			</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">OUTCOMES</td>
			</tr>
			
			<tr>
				<td></td>
				<td colspan="19">Local governments, communities and the private sector have the capacity to enable and sustain resilient WASH services in X Palikas</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="19">X People use safe water from resilient and sustainable water supply schemes, and practice improved hygiene and safe sanitation</td>
			</tr>
			<tr>
				<td></td>
				<td colspan="19">Communities and local governments in X Palikas are ready and able to respond to disasters, and are actively taking DRR measures </td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project Number</td>
				<td>Project Name</td>
				<td>(Outcome Indicator 1) Average functionality rates of water supply schemes across supported Palikas</td>
				<td>(Outcome Indicator 2) Average % of schemes across the Palikas achieving or exceeding service standards (see note) at least 80% of the time</td>
				<td>(Outcome Indicator 3) % households in lowest wealth quintile in Palika with (affordable) using safe water, practicing hand washing with soap and safe sanitation</td>
				<td>(Outcome Indicator 4) No. Palikas declared for Total Sanitation </td>
				<td>(Outcome Indicator 5) No. Palikas with DRR capacity in place </td>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->supply_scheme}}</td>
				<td>{{$value->exceeding_service}}</td>
				<td>{{$value->affordable_wealth}}</td>
				<td>{{$value->total_sanitation}}</td>
				<td>{{$value->ddr}}</td>
			</tr>
			@endforeach
			
			<tr>
				<td colspan="20">OUTPUT 1</td>
			</tr>
			<tr>
				<td colspan="20">Local governments capacities strengthened to enable and sustain resilient and improved WASH services in x Palikas</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project Number</td>
				<td>Project Name</td>
				<td>Number of Palkas have updated, publicly available life-cycle costed GESI informed plans to achieve and sustain universal access to WASH services</td>
				<td>Number of Palikas that have GESI informed local policy on tariff  setting and community contributions for water supply scheme</td>
				<td>Number of schemes joint funded between LG and GWT </td>
				<td>X Palikas with GESI informed guidelines on community selection and resilient water and sanitation infrastructure for different contexts</td>
				<td>X Palikas annually awards cleanest communities and best performing water supply service providers, based on robust monitoring data </td>
				<td>X Palikas undertaking social and public audits annually at community / ward levels</td>
				<td>X Palikas with robust WASH MIS in place and used in planning process</td>
				<td>X Palikas receive additional resources for successful completion of Minimum Condition and Performance Measure Assessment</td>
			</tr>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->project_no}}</td>
					<td>{{$value->project_name}}</td>
					<td>{{$value->gesi_wash}}</td>
					<td>{{$value->gesi_supply_schemes}}</td>
					<td>{{$value->gwt_fund}}</td>
					<td>{{$value->esi_informed}}</td>
					<td>{{$value->award}}</td>
					<td>{{$value->annual_social}}</td>
					<td>{{$value->wash_water}}</td>
					<td>{{$value->condition_success}}</td>
				</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">OUTPUT 2</td>
			</tr>
			<tr>
				<td colspan="20">
					Local artisans and small businesses provide improved, regulated WASH-related services and products, to meet the local demand in x Palikas
				</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project Number</td>
				<td>Project Name</td>
				<td>% WSUCs with performance-based contract with trained and equipped maintenance service provider</td>
				<td>No. manual drillers/ handpump installers trained on improving safe water services and mitigation / emergency response measures</td>
				<td>Number of toilet upgraded or newly installed by trained providers to the agreed GESI informed sanitation standard</td>
				<td>No. of private or informal sector gaining or increasing income from WASH-related activities</td>
				<td>Number of approved small businesses providing safe FSM services for HH toilets</td>
			</tr>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->project_no}}</td>
					<td>{{$value->project_name}}</td>
					<td>{{$value->wusc_contract}}</td>
					<td>{{$value->manual_drillers}}</td>
					<td>{{$value->gesi_toilets}}</td>
					<td>{{$value->private_wash}}</td>
					<td>{{$value->fsm_business}}</td>
				</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">Output 3</td>
			</tr>
			<tr>
				<td colspan="20">Communities have strengthened capacity to operate, enable and sustain resilient and inclusive water supply services, and maintain sanitation and hygiene standards in X Palikas</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>No of WSUCs across the Palikas which score >70% in pre-defined WSUC competency checklist</td>
				<td>No of WSUCs, FCHVs and Health Posts with HH information on simple sanitation and hygiene indicators based on cleanliness competition criteria</td>
				<td>No of communities have capacity building of WSP</td>
			</tr>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->project_no}}</td>
					<td>{{$value->project_name}}</td>
					<td>{{$value->wusc_checklist}}</td>
					<td>{{$value->fchv_clean}}</td>
					<td>{{$value->wsp_water}}</td>
				</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">Output 4</td>
			</tr>
			<tr>
				<td colspan="20">Number of people gain improved access to safe water from resilient and sustainable water supply systems </td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>Number of people with access to safely managed water supply to the premises (yard tap) at least 12 hrs/day </td>
				<td>Number of people from poor and marginalised groups with access to safely managed water supply service at least 8 hrs/day </td>
				<td>% water schemes that deliver water within national water quality standards at point of collection</td>
				<td>Number of schools, clinics and public institutions with access to safely managed water supply service within their grounds at least 12 hrs / day, specifically DRR designed for those selected as safe havens</td>
			</tr>
			@foreach($data as $key => $value)
				<tr>
					<td>{{$key+1}}</td>
					<td>{{$value->project_no}}</td>
					<td>{{$value->project_name}}</td>
					<td>{{$value->total_safety_management_12}}</td>
					<td>{{$value->total_safety_management_8}}</td>
					<td>{{$value->total_project_scheme}}</td>
					<td>{{$value->total_school_safety_12}}</td>
				</tr>
			@endforeach
			
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">Output 5</td>
			</tr>
			<tr>
				<td colspan="20">Number of people with Improved and sustained hygiene and sanitation practices, and increased access to improved sanitation facilities </td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>Number of communities actively competing in ward level cleanliness competitions</td>
				<td>Number of people in target Palikas have handwashing facility with water and soap within 1-2 m of toilets</td>
				<td>Number of people in target Palikas with clean toilet meeting local  cleanliness competitions criteria</td>
				<td>Number of schools trained and equipped and running participatory BCC and outreach sessions</td>
				<td>Number of schools, clinics and public institutions with access to safe sanitation and hand washing facilities, specifically DRR designed for those selected as safe havens.</td>
				<td>X% of HHs performing grey water separation to use in kitchen garden (improving resilience) (SDI)</td>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->total_cleanliness_competitions}}</td>
				<td>{{$value->total_handwashing_facility}}</td>
				<td>{{$value->total_clean_toilet}}</td>
				<td>{{$value->total_fchv_trained}}</td>
				<td>{{$value->total_school_safe_haven}}</td>
				<td>{{$value->total_grey_water}}</td>
			</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">Output 6</td>
			</tr>
			<tr>
				<td colspan="20">Strengthened capacities of x community, x Plalikas and x district to prepare for and respond to emergency</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>Number District and Palika representatives and staff trained in leadership for emergency preparedness response </td>
				<td>Number of Palikas with established EOC and DPRP</td>
				<td>Number of CDMC task force members trained on CBSAR, First Aid, EWS, assessment, logistic management and E-WASH</td>
				<td>Number of most vulnerable CDMC prepositioned with hazard specific search and rescue material, relief materials at most vulnerable  </td>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->fchv_staff}}</td>
				<td>{{$value->eoc_dprp}}</td>
				<td>{{$value->cmdc_task}}</td>
				<td>{{$value->cdmc_search}}</td>
			</tr>
			@endforeach
			<tr>
				<td></td>
			</tr>
			<tr>
				<td colspan="20">Output 7</td>
			</tr>
			<tr>
				<td colspan="20">Knowledge and learnings from the programme effectively captured and disseminated at the provincial and national level, to maximise the impact of the programme </td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>Project No</td>
				<td>Project Name</td>
				<td>GWT participate in X% of relevant sector forums (related to DRR and WASH sustainability technical working groups)</td>
				<td>Number of exchange visits to the project for key sector stakeholders</td>
				<td>Number of trainings delivered by GWT/Palikas to other agencies</td>
				<td>Number of knowledge products (lesson learned, case studies, voices from field) developed and shared with wider sector </td>
			</tr>
			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->project_no}}</td>
				<td>{{$value->project_name}}</td>
				<td>{{$value->drr_wash}}</td>
				<td>{{$value->visit_project}}</td>
				<td>{{$value->train_gwt}}</td>
				<td>{{$value->product}}</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
</body>
</html>