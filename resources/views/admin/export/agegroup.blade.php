<html><head>
	<title>Agegroup Report</title>
</head>
	<table border="1">
	<tbody>
		
		<tr>
			<td colspan="3">Benefited Population By Age-Group_FY_2018/19</td>
			<td rowspan="2"></td>
			<td rowspan="2"></td>
			<td rowspan="2"></td>
			<td rowspan="2"></td>
			<td rowspan="2"></td>
			<td colspan="11"> Age Groups (Years)</td>
		</tr>
		<tr>
			<td colspan="3">Data is taken during 2nd community remap</td>
			<td colspan="2">0-5</td>
			<td colspan="2">6-15</td>
			<td colspan="2">16-65</td>
			<td colspan="2">66 Above</td>
			<td colspan="2">Sub Total</td>
			<td rowspan="2">Total</td>
		</tr>
		
		<tr>
			<td >S.No</td>
			<td >Project Number</td>
			<td >Project Name</td>
			<td >Ward No</td>
			<td >G.P / N.P</td>
			<td >District</td>
			<td >HH</td>
			<td >Population</td>
			<td >F</td>
			<td >M</td>
			<td >F</td>
			<td >M</td>
			<td >F</td>
			<td >M</td>
			<td >F</td>
			<td >M</td>
			<td >F</td>
			<td >M</td>
			
			
		</tr>
		<tr>
			<td></td>
			<td>New / Rebuild</td>
			<td></td>
		</tr>
		@php
			$sum_hh_post = 0;
			$sum_pop_post = 0;
			$sum_0_5male_post = 0;
			$sum_0_5female_post = 0;
			$sum_6_15male_post = 0;
			$sum_6_15female_post = 0;
			$sum_16_65male_post = 0;
			$sum_16_65female_post = 0;
			$sum_65above_male_post = 0;
			$sum_65above_female_post = 0;
			$sum_sub_female_post = 0;
			$sum_sub_male_post = 0;
			$sno = 0;

		@endphp
		@foreach($data as $key => $value)
			<tr>
				<td>{{ @$sno+1 }}</td>
				<td>{{ @$value->project_no }}</td>
				<td>{{ @$value->project_name }}</td>
				<td>{{ @$value->ward_no }}</td>
				<td>{{ @$value->vdc }}</td>
				<td>{{ @$value->district }}</td>
				<td>{{ @$value->hh }}</td>
				<td>{{ @$value->pop }}</td>
				<td>{{ @$value->agegrp0to5female }}</td>
				<td>{{ @$value->agegrp0to5male }}</td>
				<td>{{ @$value->agegrp6to15female }}</td>
				<td>{{ @$value->agegrp6to15male }}</td>
				<td>{{ @$value->agegrp16to65female }}</td>
				<td>{{ @$value->agegrp16to65male }}</td>
				<td>{{ @$value->agegrpabove65female }}</td>
				<td>{{ @$value->agegrpabove65male }}</td>
				<td>{{ @$value->sub_female }}</td>
				<td>{{ @$value->sub_male }}</td>
				<td>{{ @$value->pop }}</td>
				
			</tr>
			@php
				$sum_hh_post += $value->hh;
				$sum_pop_post += $value->pop;
				
				$sum_0_5female_post += $value->agegrp0to5female;
				$sum_0_5male_post += $value->agegrp0to5male;
				$sum_6_15female_post += $value->agegrp6to15female;
				$sum_6_15male_post += $value->agegrp6to15male;
				$sum_16_65female_post += $value->agegrp16to65female;
				$sum_16_65male_post += $value->agegrp16to65male;
				$sum_65above_female_post += $value->agegrpabove65female;
				$sum_65above_male_post += $value->agegrpabove65male;
				$sum_sub_female_post += $value->sub_female;
				$sum_sub_male_post += $value->sub_male;
				
			@endphp
		@endforeach
		<tr>
			<td colspan="6"><p style="float: right">Total:</p></td>
			<!-- <td colspan="2"></td> -->
			<td>{{ @$sum_hh_post}}</td>
			<td>{{ @$sum_pop_post}}</td>
			<td>{{ @$sum_0_5female_post}}</td>
			<td>{{ @$sum_0_5male_post}}</td>
			<td>{{ @$sum_6_15female_post}}</td>
			<td>{{ @$sum_6_15male_post}}</td>
			<td>{{ @$sum_16_65female_post}}</td>
			<td>{{ @$sum_16_65male_post}}</td>
			<td>{{ @$sum_65above_female_post}}</td>
			<td>{{ @$sum_65above_male_post}}</td>
			<td>{{ @$sum_sub_female_post}}</td>
			<td>{{ @$sum_sub_male_post}}</td>
			<td>{{ $sum_pop_post}}</td>
		</tr>
		 <tr>
			<td colspan="19"></td>
		</tr>
		
	</tbody>
</table>	
