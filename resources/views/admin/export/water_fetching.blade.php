<head>
	<title>WaterFetching Report</title>
</head>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="19">Water Fetching F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="19"> <h3>Water Fetching Status</h3></th>
			</tr>
			<tr>
				<td colspan="8"></td>
				<td rowspan="2">Source Name</td>
				<td rowspan="2">Type</td>
				<td rowspan="2">Water Collection pot in liter</td>
				<td colspan="5">Time consumed in one trip</td>
				<td colspan="3">Time consumed in 8 times</td>
			</tr>

			<tr>
				<td>S.No</td>
				<td>Project No.</td>
				<td>Project Name</td>
				<td>District</td>
				<td>G.P/N.P</td>
				<td>Ward No.</td>
				<td>Resp. SS</td>
				<td>HHs in Total</td>
				<td>Going in second</td>
				<td>Waiting in second</td>
				<td>Filling in second</td>
				<td>Return in second</td>
				<td>Total expended time in second</td>

				<td>Total expended time in Second</td>
				<td>Total expended time in Minute</td>
				<td>Total expended time in Hours</td>
			</tr>

			<tr>
				<td></td>
				<td>New/Rebulid</td>
				<td>West(Post-monsoon)</td>
				<td colspan="17"></td>
			</tr>

			@php
			$sum_hh=0;
			$sum_total_water_colletion_pot=0;
			$sum_total_going_second=0;
			$sum_total_waiting_second=0;
			$sum_total_filling_second=0;
			$sum_total_return_second=0;
			$sum_total_second=0;
			$sum_total_second_8round=0;
			$sum_total_minute_8round=0;
			$sum_total_hour_8round=0;

			$sum_hh_pre=0;
			$sum_hh_post=0;
			 @endphp
			
		
			@foreach($data as $key => $value)
			<tr>
				<td>{{@$key+1}}</td>
				<td>{{@$value->project_no}}</td>
				<td>{{@$value->project_name}}</td>
				<td>{{@$value->district}}</td>
				<td>{{@$value->vdc}}</td>
				<td>{{@$value->ward_no}}</td>
				<td>sdf</td>
				<td>{{@$value->hh}}</td>
				{{-- <td>Source Name</td> --}}
				<td>@foreach($value->source as $sources)
						{{ $sources->name }}
				@endforeach</td>
				<td>Type</td>
				<td>{{@$value->total_water_colletion_pot}}</td>
				<td>{{@$value->total_going_second}}</td>
				<td>{{@$value->total_waiting_second}}</td>
				<td>{{@$value->total_filling_second}}</td>
				<td>{{@$value->total_return_second}}</td>
				<td>{{@$value->total_second}}</td>
				<td>{{@$value->total_second_8round}}</td>
				<td>{{@$value->total_minute_8round}}</td>
				<td>{{@$value->total_hour_8round}}</td>
			</tr>
			@php
			$sum_hh_post +=$value->hh;
			// $sum_total_water_colletion_pot += $value->total_water_colletion_pot;    
			// $sum_total_going_second += $value->total_going_second;
			// $sum_total_waiting_second += $value->total_waiting_second;
			// $sum_total_filling_second += $value->total_filling_second;
			// $sum_total_return_second += $value->total_return_second;
			// $sum_total_second += $value->total_second;
			// $sum_total_second_8round += $value->total_second_8round; 
			// $sum_total_minute_8round += $value->total_minute_8round;
			// $sum_total_hour_8round += $value->total_hour_8round;

			 @endphp
			@endforeach

			<tr>
				<td colspan="4"></td>
				<td colspan="3">Total:</td>
			<td>	{{$sum_hh_post}}</td>
				{{-- <td></td>
				<td></td>
				<td> {{ $sum_total_water_colletion_pot }} </td>
				
				<td> {{ $sum_total_going_second }} </td>
				<td> {{ $sum_total_waiting_second }} </td>
				<td> {{ $sum_total_filling_second }} </td>
				<td> {{ $sum_total_return_second }} </td>
				<td> {{ $sum_total_second }} </td>
				<td> {{ $sum_total_second_8round }} </td>
				<td> {{ $sum_total_minute_8round }} </td>
				<td> {{ $sum_total_hour_8round }} </td> --}}
			</tr>
		</tbody>
	</table>

