<html><head>
	<title>SocialEconomic Report</title>
</head>
<body>
		@php 
		
		$total_arr=array();
		@endphp
	
	<table border="1">
	<tbody>
		<tr>
			<td colspan="29">SocialEconomic Staus F/Y 18/19</td>
		</tr>
		<tr>
			<th colspan="29"> <h3>Baseline SocialEconomic Status</h3></th>
		</tr>
		<tr>
			<td colspan="9"></td>
			<td colspan="20">Initial Phase</td>
		</tr>		
		
		<tr>
			<td rowspan="2">Sno</td>
			<td rowspan="2">Project Number</td>
			<td rowspan="2">Project Name</td>
			<td rowspan="2">District</td>
			<td rowspan="2">VDC</td>
			<td rowspan="2">Ward No</td>
			<td rowspan="2">HH</td>
			<td rowspan="2">Population</td>
			<th colspan="5">Income Source And Pension </th>
			<th colspan="2">Litrate(Above age of 6)</th>
			<th colspan="2">Children going to School</th>
			<th colspan="4">Water Fetching</th>
			<th colspan="2">Homestead Kitchen Gardening</th>
			<th colspan="2">Affects on Children Due to Water Fetching</th>
			<th rowspan="2">Women With Women As Lead</th>
			<th rowspan="2">Widow Women Number</th>
			<th rowspan="2">Disable People Number</th>
		</tr>
		<tr>
			@foreach($data->income_sources as $value)
				<th>{{$value->name}}</th>
			@endforeach
			<th >Litrate</th>
			<th >Illitrate</th>
			<th >Boys</th>
			<th >Girls</th>
			<th >Men</th>
			<th >Women</th>
			<th >Boys</th>
			<th >Girls</th>
			<th >Have</th>
			<th >No Have</th>
			<th >No time for Homework</th>
			<th >No time for School</th>
		</tr>
		@php	
			$sum_hh=0;
			$sum_pop=0;
			$sum_hh_litrate=0;
			$sum_hh_illitrate=0;
			$sum_hh_school_male=0;
			$sum_hh_school_female=0;
			$sum_hh_fetching_male=0;
			$sum_hh_fetching_female=0;
			$sum_hh_fetching_boys=0;
			$sum_hh_fetching_girls=0;
			$sum_hh_kitchen_gardening_yes=0;
			$sum_hh_kitchen_gardening_no=0;
			$sum_hh_homework=0;
			$sum_hh_absent_school=0;
			$sum_hh_female_head=0;
			$sum_hh_female_widow=0;
			$sum_hh_disabled=0;

			$sum_hh_post=0;
			$sum_pop_post=0;
			$sum_hh_litrate_post=0;
			$sum_hh_illitrate_post=0;
			$sum_hh_school_male_post=0;
			$sum_hh_school_female_post=0;
			$sum_hh_fetching_male_post=0;
			$sum_hh_fetching_female_post=0;
			$sum_hh_fetching_boys_post=0;
			$sum_hh_fetching_girls_post=0;
			$sum_hh_kitchen_gardening_yes_post=0;
			$sum_hh_kitchen_gardening_no_post=0;
			$sum_hh_homework_post=0;
			$sum_hh_absent_school_post=0;
			$sum_hh_female_head_post=0;
			$sum_hh_female_widow_post=0;
			$sum_hh_disabled_post=0;

			$sum_hh_pre=0;
			$sum_pop_pre=0;
			$sum_hh_litrate_pre=0;
			$sum_hh_illitrate_pre=0;
			$sum_hh_school_male_pre=0;
			$sum_hh_school_female_pre=0;
			$sum_hh_fetching_male_pre=0;
			$sum_hh_fetching_female_pre=0;
			$sum_hh_fetching_boys_pre=0;
			$sum_hh_fetching_girls_pre=0;
			$sum_hh_kitchen_gardening_yes_pre=0;
			$sum_hh_kitchen_gardening_no_pre=0;
			$sum_hh_homework_pre=0;
			$sum_hh_absent_school_pre=0;
			$sum_hh_female_head_pre=0;
			$sum_hh_female_widow_pre=0;
			$sum_hh_disabled_pre=0;
		@endphp
		
		@foreach($data as $key => $value)
		<tr>
			<td>{{@$key+1}}</td>
			 <td>{{@$value->project_no}}</td>
			<td>{{@$value->project_name}}</td>
			<td>{{@$value->district}}</td>
			<td>{{@$value->vdc}}</td>
			<td>{{@$value->ward_no}}</td>
			<td>{{@$value->hh}}</td>
			<td>{{@$value->population}}</td>
			@php $count=0; @endphp
			@foreach($value->income_ssource as $k=>$v)
		
			@php 
				if(isset($total_arr[$k]))
				{
					$total_arr[$k]=$total_arr[$k]+$v->count;
				}
				else {
					$total_arr[$k]=$v->count;
				}
				
			@endphp
			@if($v!=null)
				<td>{{@$v->count}}</td>
			@else
			<td>0</td>

			@endif
			@php $count+=1; @endphp
			@endforeach
			
			@for($i=$count;$i<5;$i++)
				@php 
				if(!isset($total_arr[$i]))
				{
					$total_arr[$i]=0;
				}
				
				@endphp
			<td>0</td>
			@endfor

			<td>{{@$value->hh_litrate}}</td>
			<td>{{@$value->hh_illitrate}}</td>
			<td>{{@$value->hh_school_male}}</td>
			<td>{{@$value->hh_school_female}}</td>
			<td>{{@$value->hh_fetching_male}}</td>
			<td>{{@$value->hh_fetching_female}}</td>
			<td>{{@$value->hh_fetching_boys}}</td>
			<td>{{@$value->hh_fetching_girls}}</td>
			<td>{{@$value->hh_kitchen_gardening_yes}}</td>
			<td>{{@$value->hh_kitchen_gardening_no}}</td>
			<td>{{@$value->hh_homework}}</td>
			<td>{{@$value->hh_absent_school}}</td>
			<td>{{@$value->hh_female_head}}</td>
			<td>{{@$value->hh_female_widow}}</td>
			<td>{{@$value->hh_disabled}}</td>

			@php
			$sum_hh_post += $value->hh;
			$sum_pop_post += $value->population;
			$sum_hh_litrate_post += $value->hh_litrate;
			$sum_hh_illitrate_post += $value->hh_illitrate;
			$sum_hh_school_male_post += $value->hh_school_male;
			$sum_hh_school_female_post += $value->hh_school_female;
			$sum_hh_fetching_male_post += $value->hh_fetching_male;
			$sum_hh_fetching_female_post += $value->hh_fetching_female;
			$sum_hh_fetching_boys_post += $value->hh_fetching_boys;
			$sum_hh_fetching_girls_post += $value->hh_fetching_girls;
			$sum_hh_kitchen_gardening_yes_post += $value->hh_kitchen_gardening_yes;
			$sum_hh_kitchen_gardening_no_post += $value->hh_kitchen_gardening_no;
			$sum_hh_homework_post += $value->hh_homework;
			$sum_hh_absent_school_post += $value->hh_absent_school;
			$sum_hh_female_head_post += $value->hh_female_head;
			$sum_hh_female_widow_post +=$value->hh_female_widow;
			$sum_hh_disabled_post += $value->hh_disabled;
			@endphp
		</tr>
		@endforeach
		<tr>
			<td colspan="5"></td>
			<td>Total</td>
			<td>{{@$sum_hh_post}}</td>
			<td>{{@$sum_pop_post }}</td>
			@foreach($total_arr as $a)
				<td> {{ @$a }} </td>
			@endforeach
			<td>{{@$sum_hh_litrate_post}}</td>
			<td>{{@$sum_hh_illitrate_post}}</td>
			<td>{{@$sum_hh_school_male_post}}</td>
			<td>{{@$sum_hh_school_female_post}}</td>
			<td>{{@$sum_hh_fetching_male_post}}</td>
			<td>{{@$sum_hh_fetching_female_post}}</td>
			<td>{{@$sum_hh_fetching_boys_post}}</td>
			<td>{{@$sum_hh_fetching_girls_post}}</td>
			<td>{{@$sum_hh_kitchen_gardening_yes_post}}</td>
			<td>{{@$sum_hh_kitchen_gardening_no_post}}</td>
			<td>{{@$sum_hh_homework_post}}</td>
			<td>{{@$sum_hh_absent_school_post}}</td>
			<td>{{@$sum_hh_female_head_post}}</td>
			<td>{{@$sum_hh_female_widow_post }}</td>
			<td>{{@$sum_hh_disabled_post}}</td>
		</tr>
	</tbody>
</table>	

</body></html>