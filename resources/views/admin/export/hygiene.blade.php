
	<table border="1">
	<tbody>
		<tr>
			<td colspan="23">House HoldHygiene F/Y 18/19</td>
		</tr>
		<tr>
			<th colspan="23"> <h3>Household Hygiene Report of all Projects</h3></th>
		</tr>
		<tr>
			<td colspan="6" rowspan="2"></td>
			<td colspan="16">Initial Phase</td>
		</tr>		
		<tr>
			<td rowspan="2">HH</td>
			<td colspan="2">Surrounding Cleanliness</td>
			<td colspan="2">Domestic Aminal Management</td>
			<td colspan="2">Drinking Water Management</td>
			<td colspan="2">Food Cover Management</td>
			<td colspan="2">Utensils Cleanliness Management</td>
			<td colspan="2">Wastage Management</td>
			<td>Cleansing Agent In Latrine</td>
			<td>Water Jar in Latrine</td>
			<td>Cleaning Brush in Latrine</td>
		</tr>
		<tr>
			<td>Sno</td>
			<td>Project Number</td>
			<td>Project Name</td>
			<td>VDC</td>
			<td>Ward No</td>
			<td>District</td>
			<td>Clean</td>
			<td>Not Clean</td>
			<td>Managed</td>
			<td>Not Managed</td>
			<td>Clean</td>
			<td>Not Clean</td>
			<td>Covered</td>
			<td>Not Covered</td>
			<td>Managed</td>
			<td>Not Managed</td>
			<td>Managed</td>
			<td>Not Managed</td>
		</tr>
		@php
			$total_hh = 0;
			$hh_clean_house = 0;
			$hh_not_clean_house = 0;
			$hh_animal_managed = 0;
			$hh_animal_not_managed = 0;
			$drinking_water_managed = 0;
			$drinking_water_not_managed = 0;
			$food_covered = 0;
			$food_not_covered = 0;
			$utensils_cleaned = 0;
			$utensils_not_cleaned = 0;
			$garbage_managed = 0;
			$garbage_not_managed = 0;
			$cleansing_agent_available = 0;
			$latrine_water_available = 0;
			$latrine_cleaning_brush = 0;
		@endphp

		@foreach($data as $key => $value)
		<tr>
			<td>{{ $key+1 }}</td>
			<td>{{ $value->project_no }}</td>
			<td>{{ $value->project_name }}</td>
			<td>{{ $value->vdc }}</td>
			<td>{{ $value->ward_no }}</td>
			<td>{{ $value->district }}</td>
			<td>{{ $value->hh }}</td>
			<td>{{ $value->hh_clean_house }}</td>
			<td>{{ $value->hh_not_clean_house }}</td>
			<td>{{ $value->hh_animal_managed }}</td>
			<td>{{ $value->hh_animal_not_managed }}</td>
			<td>{{ $value->drinking_water_managed }}</td>
			<td>{{ $value->drinking_water_not_managed }}</td>
			<td>{{ $value->food_covered }}</td>
			<td>{{ $value->food_not_covered }}</td>
			<td>{{ $value->utensils_cleaned }}</td>
			<td>{{ $value->utensils_not_cleaned }}</td>
			<td>{{ $value->garbage_managed }}</td>
			<td>{{ $value->garbage_not_managed }}</td>
			<td>{{ $value->cleansing_agent_available }}</td>
			<td>{{ $value->latrine_water_available }}</td>
			<td>{{ $value->latrine_cleaning_brush }}</td>
		</tr>

		@php
			$total_hh += $value->hh;
			$hh_clean_house += $value->hh_clean_house;
			$hh_not_clean_house += $value->hh_not_clean_house;
			$hh_animal_managed += $value->hh_animal_managed;
			$hh_animal_not_managed += $value->hh_animal_not_managed;
			$drinking_water_managed += $value->drinking_water_managed;
			$drinking_water_not_managed += $value->drinking_water_not_managed;
			$food_covered += $value->food_covered;
			$food_not_covered += $value->food_not_covered;
			$utensils_cleaned += $value->utensils_cleaned;
			$utensils_not_cleaned += $value->utensils_not_cleaned;
			$garbage_managed += $value->garbage_managed;
			$garbage_not_managed += $value->garbage_not_managed;
			$cleansing_agent_available += $value->cleansing_agent_available;
			$latrine_water_available += $value->latrine_water_available;
			$latrine_cleaning_brush += $value->latrine_cleaning_brush;
	
		@endphp
		@endforeach

		<tr>
			<td colspan="5"></td>
			<td>Total</td>
			<td>{{ @$total_hh }}</td>
			<td>{{ @$hh_clean_house }}</td>
			<td>{{ @$hh_not_clean_house }}</td>
			<td>{{ @$hh_animal_managed }}</td>
			<td>{{ @$hh_animal_not_managed }}</td>
			<td>{{ @$drinking_water_managed }}</td>
			<td>{{ @$drinking_water_not_managed }}</td>
			<td>{{ @$food_covered }}</td>
			<td>{{ @$food_not_covered }}</td>
			<td>{{ @$utensils_cleaned }}</td>
			<td>{{ @$utensils_not_cleaned }}</td>
			<td>{{ @$garbage_managed }}</td>
			<td>{{ @$garbage_not_managed }}</td>
			<td>{{ @$cleansing_agent_available }}</td>
			<td>{{ @$latrine_water_available }}</td>
			<td>{{ @$latrine_cleaning_brush }}</td>
		</tr>
	</tbody>
</table>	
