
		<table border="1">
		<tbody>
			<tr>
				<td colspan="10">House HoldHygiene F/Y 18/19</td>
			</tr>
			<tr>
				<th colspan="10"> <h3>Improved Household Hygiene Report</h3></th>
			</tr>
			<tr>
				<td colspan="6" rowspan="2">Household Hygiene Report not Based on Population</td>
				<td colspan="4">Initial Phase</td>
			</tr>		
			<tr>
				<td colspan="4">Improved Hygiene Latrine Practices in HH Latrine</td>
			</tr>
			<tr>
				<td>Sno</td>
				<td>Project Number</td>
				<td>Project Name</td>
				<td>VDC</td>
				<td>Ward No</td>
				<td>District</td>
				<td>Total HH</td>
				<td>Soap</td>
				<td>Water Jar</td>
				<td>Brush/Broom</td>
			</tr>
			@foreach($data as $key => $value)
			@endforeach
			<tr>
				<td>{{ $key+1 }}</td>
				<td>{{ $value->project_no }}</td>
				<td>{{ $value->project_name }}</td>
				<td>{{ $value->vdc }}</td>
				<td>{{ $value->wand_no }}</td>
				<td>{{ $value->district }}</td>
				<td>{{ $value->hh }}</td>
				<td>{{ $value->cleansing_agent_available }}</td>
				<td>{{ $value->latrine_water_available }}</td>
				<td>{{ $value->latrine_cleaning_brush }}</td>
			</tr>
		</tbody>
	</table>	
