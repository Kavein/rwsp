@php
$total=array(
	'daily'=>0,
	'twice'=>0,
	'once'=>0
);
$total_cloth=array(
	'once'=>0,
	'more'=>0,
	'monthly'=>0
);	
@endphp
<html>
	<head>
		<title>Sanitary Handwash Report</title>
	</head>
	<body>
		<table border="1">
			<tbody>
				<tr>
					<td colspan="17">House Hold Handwash F/Y 18/19</td>
				</tr>
				<tr>
					<th colspan="17"> <h3>Baseline Sanitary Habit Handwash</h3></th>
				</tr>
				<tr>
					<td colspan="3" rowspan="2"></td>
					<td colspan="14">Initial Phase</td>
				</tr>		
				<tr>
					<td rowspan="2">Total HH</td>
					<td rowspan="2">Population </td>
					<td colspan="5">Handwashing Before Eating</td>
					<td colspan="3">Handwashing After Defecation</td>
					<td rowspan="2">Infant Children No wash Hand</td>
					<td rowspan="2">Total</td>
					<td rowspan="2">Bath Frequency</td>
					<td rowspan="2">Cloth Washing Frequency Frequency</td>
				</tr>
					<tr>
						<td>Sno</td>
						<td>Project Number</td>
						<td>Project Name</td>
						<td>With Water</td>
						<td>With Ash</td>
						<td>Soap And Water</td>
						<td>Others</td>
						<td>Total</td>
						<td>With Water</td>
						<td>With Ash</td>
						<td>Soap And Water</td>
					</tr>
				@php
					$sum_hh_post=0;
					$sum_hh_population_post=0;
					$sum_hh_handwash_water_post=0;
					$sum_hh_handwash_ash_post=0;	
					$sum_hh_handwash_water_soap_post=0;
					$sum_hh_handwash_others_post=0;
					$sum_hh_handwash_post=0;
					$sum_hh_defecate_water_post=0;
					$sum_hh_defecate_ash_post=0;
					$sum_hh_defecate_water_soap_post=0;
					$sum_hh_handwash_infant_post=0;
					$sum_hh_defecate_post=0;
				@endphp
				@foreach($data as $key => $value)
					<tr>
						<td>{{$key+1}}</td>
						<td>{{$value->project_no}}</td>
						<td>{{$value->project_name}}</td>
						{{-- <td>{{$value->district}}</td> --}}
						<td >{{$value->hh}}</td>
						<td >{{$value->hh_population}}</td>
						<td>{{$value->hh_handwash_water}}</td>
						<td>{{$value->hh_handwash_ash}}</td>
						<td>{{$value->hh_handwash_water_soap}}</td>
						<td>{{$value->hh_handwash_others}}</td>
						<td>{{$value->hh_handwash}}</td>
						<td>{{$value->hh_defecate_water}}</td>
						<td>{{$value->hh_defecate_ash}}</td>
						<td>{{$value->hh_defecate_water_soap}}</td>
						<td>{{$value->hh_handwash_infant}}</td>
						<td>{{$value->hh_defecate}}</td>
						<td>
					

						@if((($value->hygiene[0]->daily_bath)>=($value->hygiene[0]->twice_bath)) && (($value->hygiene[0]->daily_bath)>=($value->hygiene[0]->once_bath)))
							{{ "daily" }}
							@php $total['daily']+=1; @endphp
						@elseif((($value->hygiene[0]->twice_bath)>($value->hygiene[0]->daily_bath)) && (($value->hygiene[0]->twice_bath)>($value->hygiene[0]->once_bath)))
							{{"twice"}}
							@php $total['twice']+=1; @endphp
						@else
							{{"once"}}
							@php $total['once']+=1; @endphp
						@endif


						</td>
						<td>
							@if((($value->hygiene[0]->once_cloth)>=($value->hygiene[0]->more_cloth)) && (($value->hygiene[0]->once_cloth)>=($value->hygiene[0]->monthly_cloth)))
								{{ "once" }}
								@php $total_cloth['once']+=1; @endphp
							@elseif((($value->hygiene[0]->more_cloth)>=($value->hygiene[0]->once_cloth)) && (($value->hygiene[0]->more_cloth)>($value->hygiene[0]->monthly_cloth)))
								{{"more"}}
								@php $total_cloth['more']+=1; @endphp
							@else
								{{"monthly"}}
								@php $total_cloth['monthly']+=1; @endphp
							@endif
						</td>
					</tr>
					@php
					$sum_hh_post+=$value->hh;
					$sum_hh_population_post+=$value->hh_population;
					$sum_hh_handwash_water_post+=$value->hh_handwash_water;
					$sum_hh_handwash_ash_post+=$value->hh_handwash_ash;
					$sum_hh_handwash_water_soap_post+=$value->hh_handwash_water_soap;
					$sum_hh_handwash_others_post+=$value->hh_handwash_others;
					$sum_hh_handwash_post+=$value->hh_handwash;
					$sum_hh_defecate_water_post+=$value->hh_defecate_water;
					$sum_hh_defecate_ash_post+=$value->hh_defecate_ash;
					$sum_hh_defecate_water_soap_post+=$value->hh_defecate_water_soap;
					$sum_hh_handwash_infant_post+=$value->hh_handwash_infant;
					$sum_hh_defecate_post+=$value->hh_defecate;
					@endphp
					@endforeach
					<tr>
						<td colspan="2"></td><td>Total</td>
						<td> {{ $sum_hh_post }}</td>
						<td> {{ $sum_hh_population_post }}</td>
						<td> {{ $sum_hh_handwash_water_post }}</td>
						<td> {{ $sum_hh_handwash_ash_post }}</td>	
						<td> {{ $sum_hh_handwash_water_soap_post }}</td>
						<td> {{ $sum_hh_handwash_others_post }}</td>
						<td> {{ $sum_hh_handwash_post }}</td>
						<td> {{ $sum_hh_defecate_water_post }}</td>
						<td> {{ $sum_hh_defecate_ash_post }}</td>
						<td> {{ $sum_hh_defecate_water_soap_post }}</td>
						<td> {{ $sum_hh_handwash_infant_post }}</td>
						<td> {{ $sum_hh_defecate_post }}</td>
						@php $max_bath = array_keys($total, max($total));
							$max_cloth = array_keys($total_cloth, max($total_cloth));	 @endphp
						{{--<td>{{ $max_bath[0] }}</td>
						<td>{{$max_cloth[0] }}</td>--}}
						<td>-</td>
						<td>-</td>
					</tr>
		
			</tbody>
		</table>	
	</body>
</html>