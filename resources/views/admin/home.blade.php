@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Dashboard
			<small>Control panel</small>
		</h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Dashboard</li>
		</ol>
	</section>
    <section class="content">
      	<div class="row">

			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Total Projects</span>
						<span class="info-box-number">90<small>%</small></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Total HouseHold</span>
						<span class="info-box-number">90<small>%</small></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">Total Population</span>
						<span class="info-box-number">90<small>%</small></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>
			<div class="col-md-3 col-sm-6 col-xs-12">
				<div class="info-box">
					<span class="info-box-icon bg-aqua"><i class="ion ion-ios-gear-outline"></i></span>

					<div class="info-box-content">
						<span class="info-box-text">CPU Traffic</span>
						<span class="info-box-number">90<small>%</small></span>
					</div>
				<!-- /.info-box-content -->
				</div>
				<!-- /.info-box -->
			</div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Age Group Bar Chart</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="age_group" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>

          		<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Population By Age Group </h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="age_total" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>

	        </div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Number of Project (District Wise)</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="district_wise" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>

          		<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Benefited HouseHold and Population (District Wise)</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="district_pop" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>

	        </div>
	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Differently abled population and their characteristics (gender wise) </h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="disabled_pop" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>
	        </div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Number of Differently Abled Population</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="disabled_gender" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>
	        </div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Population And HouseHold (Geographical Region)</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="geo_pop" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>
	        </div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Number of Projects By Provinces</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="provience_data" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>
	        </div>

	        <div class="col-md-6">
	        	<div class="box box-success">
		            <div class="box-header with-border">
		            	<h3 class="box-title">Population and Household (Province Wise)</h3>

		            	<div class="box-tools pull-right">
		                	<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>
		                	</button>
		                	<!-- <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button> -->
		            	</div>
		            </div>
		            <div class="box-body chart-responsive">
		            	<div class="x_content">
		                    <canvas id="provience_pop" height="280" width="600"></canvas>
		                </div>
		            </div>
		            <!-- /.box-body -->
          		</div>
	        </div>
	        


	        
      	</div>
    </section>
   
    <script>
    	function _getage(data)
    	{
			var year = ['0-5','6-15','16-65', '65 above','Total'];
			var data_click = data.male;
			var data_viewer = data.female;

		    var barChartData = {
		        labels: year,
		        datasets: [{
		            label: 'Male',
		            backgroundColor: "rgba(1,175,79,1)",
		            data: data_click
		        }, {
		            label: 'Female',
		            backgroundColor: "rgba(79,129,189,1)",
		            data: data_viewer
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("age_group").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Age Group and Gender'
	                }
	            }
	        });   
    	}	

    	function _getdistrict(data)
    	{
    		// console.log(data.district_wise_name);
    		// console.log(data.male);
			var year = data.district_wise_name;
			var data_click = data.district_wise_project;
			// var data_click = data.male;

		    var barChartData = {
		        labels: year,
		        datasets: [{
                    label: 'Projects',
                    data: data_click,
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)',
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(153, 102, 255, 0.2)',
                        'rgba(255, 159, 64, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)',
                        'rgba(255,99,132,1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(153, 102, 255, 1)',
                        'rgba(255, 159, 64, 1)'
                    ],
                    borderWidth: 1
                }]
		    };	

		   	
	    	var ctx = document.getElementById("district_wise").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	            	legend:false,
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Number of Water Supply Projects (District wise)'
	                }
	            }
	        });   
           
    	}
		
		function _get_total_age(data)
    	{
    			var year = ['Age (0-5)','Age (6-15)','Age (16-65)', 'Age (65 Above)'];
    			var data_click = data.age_total;

			    var barChartData = {
			        labels: year,
			        datasets: [{
			            label: ['Male','asffsd','sdsdff','asd'],
			            backgroundColor: [
                                '#5cb85c',
                                '#f0ad4e',
                                '#d9534f',
                                '#d8744f',
                            ],
			            data: data_click
			        }, ]
			    };			    
			   	
		    	var ctx = document.getElementById("age_total").getContext("2d");		       
		        window.myBar = new Chart(ctx, {
		            type: 'doughnut',
		            data: barChartData,
		            options: {
		                elements: {
		                    rectangle: {
		                        borderWidth: 2,
		                        borderColor: false,
		                        borderSkipped: 'bottom'
		                    }
		                },
		                
		                responsive: true,
		                title: {
		                    display: true,
		                    text: 'Population By Age Group'
		                }
		            }
		        });   
           
    	}

    	function _getdistrictpop(data)
    	{
			var year = data.district_pop_name;
			var data_click = data.district_hh;
			var data_viewer = data.district_pop;

		    var barChartData = {
		        labels: year,
		        datasets: [{
		            label: 'HouseHold',
		            backgroundColor: "rgba(180,19,0,1)",
		            data: data_click
		        }, {
		            label: 'Population',
		            backgroundColor: "rgba(78,173,70,1)",
		            data: data_viewer
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("district_pop").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'HouseHold and Population (District Wise)'
	                }
	            }
	        });   
    	}

    	
    	function _getdisablepeople(data)
    	{
			var disable = ['Physical Disable','Blindness/Low vision','Deaf/hard to hearing','Deaf-blind','Speech problem','Mental disability','Intellectual disability','Multiple disability','Total'];
			var data_click = data.disable_male;
			var data_viewer = data.disable_female;

		    var barChartData = {
		        labels: disable,
		        datasets: [{
		            label: 'Male',
		            backgroundColor: "rgba(81,131,192,1)",
		            data: data_click
		        }, {
		            label: 'Female',
		            backgroundColor: "rgba(77,172,69,1)",
		            data: data_viewer
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("disabled_pop").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Disabled population and their characteristics (gender wise)'
	                }
	            }
	        });   
    	}
    	function _getdisablegender(data)
    	{
    			var year = ['Male','Female'];
    			var data_click = data.disable_gender;

			    var barChartData = {
			        labels: year,
			        datasets: [{
			            backgroundColor: [
                                '#5cb85c',
                                '#f0ad4e',
                            ],
			            data: data_click
			        }, ]
			    };			    
			   	
		    	var ctx = document.getElementById("disabled_gender").getContext("2d");		       
		        window.myBar = new Chart(ctx, {
		            type: 'doughnut',
		            data: barChartData,
		            options: {
		                elements: {
		                    rectangle: {
		                        borderWidth: 2,
		                        borderColor: false,
		                        borderSkipped: 'bottom'
		                    }
		                },
		                
		                responsive: true,
		                title: {
		                    display: true,
		                    text: 'Number of Disabled Population (GENDER WISE)'
		                }
		            }
		        });   
           
    	}

    	function _getgeopop(data)
    	{
			var geo = data.geo_region;
			var data_click = data.geo_hh;
			var data_viewer = data.geo_pop;

		    var barChartData = {
		        labels: geo,
		        datasets: [{
		            label: 'HouseHold',
		            backgroundColor: "rgba(81,131,192,1)",
		            data: data_click
		        }, {
		            label: 'Population',
		            backgroundColor: "rgba(77,172,69,1)",
		            data: data_viewer
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("geo_pop").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Population And HouseHold (Geographical Region)'
	                }
	            }
	        });   
    	}
    	

    	function _getprovience(data)
    	{
			var geo = data.provience_name;
			var data_click = data.provience_project;

		    var barChartData = {
		        labels: geo,
		        datasets: [{
		            label: 'Projects',
		            backgroundColor: "rgba(77,172,69,1)",
		            data: data_click
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("provience_data").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	            	legend:false,
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Number of Projects By Provinces'
	                }
	            }
	        });   
    	}

		function _getprovpop(data)
    	{
			var prov_name = data.prov_name;
			var data_click = data.prov_hh;
			var data_viewer = data.prov_pop;

		    var barChartData = {
		        labels: prov_name,
		        datasets: [{
		            label: 'HouseHold',
		            backgroundColor: "rgba(81,131,192,1)",
		            data: data_click
		        }, {
		            label: 'Population',
		            backgroundColor: "rgba(77,172,69,1)",
		            data: data_viewer
		        }]
		    };			    
		   	
	    	var ctx = document.getElementById("provience_pop").getContext("2d");		       
	        window.myBar = new Chart(ctx, {
	            type: 'bar',
	            data: barChartData,
	            options: {
	                elements: {
	                    rectangle: {
	                        borderWidth: 2,
	                        borderColor: false,
	                        borderSkipped: 'bottom'
	                    }
	                },
	                scales: {
                        yAxes: [{
                            ticks: {
                                beginAtZero:true
                            }
                        }]
                    },
	                responsive: true,
	                title: {
	                    display: true,
	                    text: 'Population and Household (Province Wise)'
	                }
	            }
	        });   
    	}
    	

        $( document ).ready(function() {        	
            $.get( '{{route('dashboard.stats')}}',{_token:'{{ csrf_token()  }}'}, function( data ) {  
            }).done(function(data){            	
            	// console.log(data.district_wise); 
            	 _getage(data);
            	 _get_total_age(data);
            	 _getdistrict(data);
            	 _getdistrictpop(data);
            	 _getdisablepeople(data);
            	 _getdisablegender(data);
            	 _getgeopop(data);
            	 _getprovience(data);
            	 _getprovpop(data);
 			});
        });


        
    </script>
@endsection