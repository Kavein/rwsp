<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepHhHealthBehaviorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_hh_health_behaviors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('healthpost_preference')->nullable();
            $table->string('traditional_preference')->nullable();
            $table->string('home_medecine_preference')->nullable();
            $table->string('annual_expenses')->nullable();
            $table->string('latrine_knowledge')->nullable();
            $table->string('disease_knowledge')->nullable();
            $table->string('safewater_knowledge')->nullable();
            $table->string('handwashing_knowledge')->nullable();
            $table->string('house_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prep_hh_health_behaviors');
    }
}
