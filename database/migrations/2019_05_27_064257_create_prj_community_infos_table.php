<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrjCommunityInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prj_community_infos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('community_name')->nullable();
            $table->integer('male_students')->nullable();
            $table->integer('female_students')->nullable();
            $table->integer('total_students')->nullable();
            $table->integer('total_staff')->nullable();
            $table->integer('existing_latrines')->nullable();
            $table->integer('tapstand_required')->nullable();
            $table->integer('other_existing_latrines')->nullable();
            $table->integer('other_tapstand_required')->nullable();
            $table->integer('diarrhoea_male')->nullable();
            $table->integer('diarrhoea_female')->nullable();
            $table->integer('diarrhoea_total')->nullable();
            $table->integer('dysentery_male')->nullable();
            $table->integer('dysentery_female')->nullable();
            $table->integer('dysentery_total')->nullable();
            $table->integer('jaundice_male')->nullable();
            $table->integer('jaundice_female')->nullable();
            $table->integer('jaundice_total')->nullable();
            $table->integer('colera_male')->nullable();
            $table->integer('colera_female')->nullable();
            $table->integer('colera_total')->nullable();
            $table->integer('worms_male')->nullable();
            $table->integer('worms_female')->nullable();
            $table->integer('worms_total')->nullable();
            $table->integer('scabies_male')->nullable();
            $table->integer('scabies_female')->nullable();
            $table->integer('scabies_total')->nullable();
            $table->date('profiling_date')->nullable();
            $table->integer('disabled_male')->nullable();
            $table->integer('disabled_female')->nullable();
            $table->integer('blind_male')->nullable();
            $table->integer('blind_female')->nullable();
            $table->integer('deaf_male')->nullable();
            $table->integer('deaf_female')->nullable();
            $table->integer('blind_deaf_male')->nullable();
            $table->integer('blind_deaf_female')->nullable();
            $table->integer('speech_male')->nullable();
            $table->integer('speech_female')->nullable();
            $table->integer('mental_male')->nullable();
            $table->integer('mental_female')->nullable();
            $table->integer('intel_male')->nullable();
            $table->integer('intel_female')->nullable();
            $table->integer('multiple_male')->nullable();
            $table->integer('multiple_female')->nullable();
            $table->string('community_mapping')->nullable();
            $table->integer('project_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prj_community_infos');
    }
}
