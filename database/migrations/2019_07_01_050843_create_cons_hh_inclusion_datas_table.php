<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsHhInclusionDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cons_hh_inclusion_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('hh_male')->nullable();
            $table->integer('hh_female')->nullable();
            $table->integer('hh_boys')->nullable();
            $table->integer('hh_girls')->nullable();
            $table->integer('wf_male')->nullable();
            $table->integer('wf_female')->nullable();
            $table->integer('wf_boys')->nullable();
            $table->integer('wf_girls')->nullable();
            $table->integer('pw_male')->nullable();
            $table->integer('pw_female')->nullable();
            $table->integer('pw_boys')->nullable();
            $table->integer('pw_girls')->nullable();
            $table->integer('cw_male')->nullable();
            $table->integer('cw_female')->nullable();
            $table->integer('cw_boys')->nullable();
            $table->integer('cw_girls')->nullable();
            $table->string('effect_on_women')->nullable();
            $table->text('other_incident_women')->nullable();
            $table->string('effect_on_children')->nullable();
            $table->text('other_effect_on_children')->nullable();
            $table->string('kitchen_gardening_practice')->nullable();
            $table->integer('out_migrating')->nullable();
            $table->integer('school_going_children_male')->nullable();
            $table->integer('school_going_children_female')->nullable();
            $table->integer('house_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cons_hh_inclusion_datas');
    }
}
