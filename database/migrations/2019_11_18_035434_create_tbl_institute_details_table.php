<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblInstituteDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_institute_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->integer('fiscal_year_id')->nullable();
            $table->string('institute_name')->nullable();
            $table->integer('male_number')->nullable();
            $table->integer('female_number')->nullable();
            $table->integer('total_staff')->nullable();
            $table->integer('total_latrine')->nullable();
            $table->integer('tapstand_required')->nullable();
            $table->integer('latrine_required')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_institute_details');
    }
}
