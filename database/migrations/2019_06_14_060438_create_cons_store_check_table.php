<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsStoreCheckTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cons_store_check', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('construction_id')->nullable();
            $table->date('store_check_Date')->nullable();
            $table->tinyinteger('store_check')->nullable();
            $table->timestamp('created_at')->useCurrent();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cons_store_check');
    }
}
