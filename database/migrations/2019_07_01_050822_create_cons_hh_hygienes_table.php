<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsHhHygienesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cons_hh_hygienes', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('latrine_available')->nullable();
            $table->integer('latrine_cost')->nullable();
            $table->integer('total_latrine')->nullable();
            $table->tinyInteger('latrine_cleansing_agent')->nullable();
            $table->tinyInteger('latrine_water_available')->nullable();
            $table->tinyInteger('latrine_cleaning_brush')->nullable();
            $table->string('defecate_latrine')->nullable();
            $table->string('defecate_open')->nullable();
            $table->tinyInteger('home_cleaning')->nullable();
            $table->tinyInteger('domestic_animal')->nullable();
            $table->tinyInteger('drinking_water')->nullable();
            $table->tinyInteger('food_covered')->nullable();
            $table->tinyInteger('utensils_clean')->nullable();
            $table->tinyInteger('garbage_disposel_pit')->nullable();
            $table->integer('handwash_water_soap')->nullable();
            $table->integer('handwash_water')->nullable();
            $table->integer('handwash_ash')->nullable();
            $table->integer('handwash_others')->nullable();
            $table->integer('defecate_water_soap')->nullable();
            $table->integer('defecate_water')->nullable();
            $table->integer('defecate_ash')->nullable();
            $table->integer('handwash_infant')->nullable();
            $table->string('bath_frequency')->nullable();
            $table->string('cloth_wash_frequency')->nullable();
            $table->string('menstrual_knowledge')->nullable();
            $table->integer('house_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cons_hh_hygienes');
    }
}
