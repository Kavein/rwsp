<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepHhWaterFetchingTimeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_hh_water_fetching_time', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_id')->nullable();
            $table->integer('water_collection_liter')->nullable();
            $table->integer('going_second')->nullable();
            $table->integer('waiting_second')->nullable();
            $table->integer('filling_second')->nullable();
            $table->integer('return_second')->nullable();
            $table->integer('total_second')->nullable();
            $table->integer('total_second_8round')->nullable();
            $table->integer('total_minute_8round')->nullable();
            $table->integer('total_hour_8round')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prep_hh_water_fetching_time');
    }
}
