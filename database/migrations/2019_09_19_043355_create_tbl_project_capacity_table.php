<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProjectCapacityTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_project_capacity', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->string('designated_wash')->nullable();
            $table->string('gesi_wash')->nullable();
            $table->string('esi_informed')->nullable();
            $table->string('investment')->nullable();
            $table->string('award')->nullable();
            $table->string('gwt_fund')->nullable();
            $table->string('fund_water')->nullable();
            $table->string('annual_social')->nullable();
            $table->string('wash_water')->nullable();
            $table->string('public_plan')->nullable();
            $table->string('condition_success')->nullable();
            $table->string('private_wash')->nullable();
            $table->string('wusc_contract')->nullable();
            $table->string('manual_drillers')->nullable();
            $table->string('gesi_toilets')->nullable();
            $table->string('fsm_business')->nullable();
            $table->string('managed_fs')->nullable();
            $table->string('trained_women')->nullable();
            $table->string('wusc_checklist')->nullable();
            $table->string('gesi_informed')->nullable();
            $table->string('ndwqs_update')->nullable();
            $table->string('wsp_water')->nullable();
            $table->string('fchv_clean')->nullable();
            $table->string('wsuc_restore')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_project_capacity');
    }
}
