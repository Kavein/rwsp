<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrepSurvey extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_survey', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->date('survey_date')->nullable();
            $table->date('design_date')->nullable();
            $table->text('source_assessment')->nullable();
            $table->text('water_structure')->nullable();
            $table->string('pipeline_length')->nullable();
            $table->string('gps_data')->nullable();
            $table->string('design_pic')->nullable();
            $table->date('design_agreement_date')->nullable();
            $table->string('water_source_record')->nullable();
            $table->integer('variation_taps')->nullable();
            $table->integer('variation_households')->nullable();
            $table->integer('variation_popn')->nullable();
        });
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
