<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProjectDisasterTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_project_disaster', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->string('death_toll')->nullable();
            $table->string('ddr')->nullable();
            $table->string('gwt_staff')->nullable();
            $table->string('fchv_staff')->nullable();
            $table->string('awc_stock')->nullable();
            $table->string('eoc_dprp')->nullable();
            $table->string('cdmc_wsuc')->nullable();
            $table->string('cdmc_plan')->nullable();
            $table->string('cdmc_wash')->nullable();
            $table->string('cmdc_task')->nullable();
            $table->string('cdmc_search')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_project_disaster');
    }
}
