<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepHhHealthDatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_hh_health_datas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('house_id');
            $table->integer('diarrhoea_0to5_male')->nullable();
            $table->integer('diarrhoea_0to5_female')->nullable();
            $table->integer('diarrhoea_0to5_total')->nullable();
            $table->integer('diarrhoea_0to5_frequency')->nullable();
            $table->integer('diarrhoea_above6_male')->nullable();
            $table->integer('diarrhoea_above6_female')->nullable();
            $table->integer('diarrhoea_above6_total')->nullable();
            $table->integer('diarrhoea_above6_frequency')->nullable();

            $table->integer('dysentry_0to5_male')->nullable();
            $table->integer('dysentry_0to5_female')->nullable();
            $table->integer('dysentry_0to5_total')->nullable();
            $table->integer('dysentry_0to5_frequency')->nullable();
            $table->integer('dysentry_above6_male')->nullable();
            $table->integer('dysentry_above6_female')->nullable();
            $table->integer('dysentry_above6_total')->nullable();
            $table->integer('dysentry_above6_frequency')->nullable();

            $table->integer('jaundice_0to5_male')->nullable();
            $table->integer('jaundice_0to5_female')->nullable();
            $table->integer('jaundice_0to5_total')->nullable();
            $table->integer('jaundice_0to5_frequency')->nullable();
            $table->integer('jaundice_above6_male')->nullable();
            $table->integer('jaundice_above6_female')->nullable();
            $table->integer('jaundice_above6_total')->nullable();
            $table->integer('jaundice_above6_frequency')->nullable();

            $table->integer('colera_0to5_male')->nullable();
            $table->integer('colera_0to5_female')->nullable();
            $table->integer('colera_0to5_total')->nullable();
            $table->integer('colera_0to5_frequency')->nullable();
            $table->integer('colera_above6_male')->nullable();
            $table->integer('colera_above6_female')->nullable();
            $table->integer('colera_above6_total')->nullable();
            $table->integer('colera_above6_frequency')->nullable();
                
            $table->integer('worm_infection_0to5_male')->nullable();
            $table->integer('worm_infection_0to5_female')->nullable();
            $table->integer('worm_infection_0to5_total')->nullable();
            $table->integer('worm_infection_0to5_frequency')->nullable();
            $table->integer('worm_infection_above6_male')->nullable();
            $table->integer('worm_infection_above6_female')->nullable();
            $table->integer('worm_infection_above6_total')->nullable();
            $table->integer('worm_infection_above6_frequency')->nullable();

            $table->integer('scabies_0to5_male')->nullable();
            $table->integer('scabies_0to5_female')->nullable();
            $table->integer('scabies_0to5_total')->nullable();
            $table->integer('scabies_0to5_frequency')->nullable();
            $table->integer('scabies_above6_male')->nullable();
            $table->integer('scabies_above6_female')->nullable();
            $table->integer('scabies_above6_total')->nullable();
            $table->integer('scabies_above6_frequency')->nullable();

            $table->integer('sickness_absence_school')->nullable();
            $table->integer('max_sick_days')->nullable();
            $table->string('death_cause')->nullable();
            $table->string('bedrest_adult')->nullable();
            $table->string('bedrest_days')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prep_hh_health_datas');
    }
}
