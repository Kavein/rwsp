<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class PrepConstruction extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_construction', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id');
            $table->date('quantity_bill_record_date')->nullable();
            $table->integer('rough_estimation_cost')->nullable();
            $table->integer('amended_estimation_cost')->nullable();
            $table->date('MOU_agreement_date')->nullable();
            // $table->date('store_check_date')->nullable();
            // $table->tinyInteger('store_check')->nullable();
            $table->date('program_implementation')->nullable();
            $table->date('vmw_training_date')->nullable();
            $table->integer('vmw_participants_male')->nullable();
            $table->integer('vmw_participants_female')->nullable();
            $table->string('supervisor_name')->nullable();
            $table->integer('supervisor_age')->nullable();
            $table->string('sex')->nullable();
            $table->date('mid_visit_date')->nullable();
            $table->tinyInteger('structure_monitor')->nullable();
            $table->date('wsp_completion')->nullable();
            $table->date('wst_date')->nullable();
            $table->integer('wst_participants')->nullable();
            $table->integer('om_fund')->nullable();
            $table->integer('variation')->nullable();
            $table->integer('variation_households')->nullable();
            $table->integer('variation_population')->nullable();
            $table->date('comissioning_date')->nullable();
            $table->date('post_construction_date')->nullable();
            $table->integer('post_construction_participants_male')->nullable();
            $table->integer('post_construction_participants_female')->nullable();
            $table->date('wsp_training_date')->nullable();
            $table->integer('wsp_training_participation_male')->nullable();
            $table->integer('wsp_training_participation_female')->nullable();
            $table->date('sanitary_funtion_survey')->nullable();
            $table->tinyInteger('created_by')->nullable();
            $table->date('created_at')->nullable();
            $table->tinyInteger('updated_by')->nullable();
            $table->date('updated_at')->nullable();
            $table->string('del_flag')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
