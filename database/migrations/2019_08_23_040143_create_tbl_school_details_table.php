<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSchoolDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_school_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->integer('community_id')->nullable();
            $table->integer('fiscal_year_id')->nullable();
            $table->string('school_name')->nullable();
            $table->integer('male_number')->nullable();
            $table->integer('female_number')->nullable();
            $table->integer('total_student')->nullable();
            $table->integer('total_staff')->nullable();
            $table->integer('total_latrine')->nullable();
            $table->integer('tapstand_required')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_school_details');
    }
}
