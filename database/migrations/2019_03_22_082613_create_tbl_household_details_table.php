<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblHouseholdDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_household_details', function (Blueprint $table) {
            $table->increments('id');
            $table->string('house_no')->nullable();
            $table->integer('project_id')->nullable();
            $table->string('head_name')->nullable();
            $table->string('head_gender')->nullable();
            $table->string('tole_name')->nullable();
            $table->integer('total_male')->nullable();
            $table->integer('total_female')->nullable();
            $table->integer('total_member')->nullable();
            $table->integer('cast_id')->nullable();
            $table->integer('cast_division')->nullable();
            $table->integer('total_female_widow')->nullable();
            $table->integer('litrate_no')->nullable();
            $table->integer('illitrate_no')->nullable();
            $table->tinyInteger('income_pension')->nullable();
            $table->text('other_income_source')->nullable();
            $table->tinyInteger('religion_minority')->nullable();
            $table->tinyInteger('status')->nullable();
            $table->tinyInteger('del_flag')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('created_at')->nullable();
            $table->date('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_household_details');
    }
}
