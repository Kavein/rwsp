<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblProjectKnowledgeTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_project_knowledge', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->string('wo_pres')->nullable();
            $table->string('drr_wash')->nullable();
            $table->string('visit_project')->nullable();
            $table->string('train_gwt')->nullable();
            $table->string('product')->nullable();
            $table->string('pre_learn')->nullable();
            $table->string('radio_message')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_project_knowledge');
    }
}
