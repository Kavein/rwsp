<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePhaseThreeYearsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('phase_three_years', function (Blueprint $table) {
            $table->increments('id');
            $table->string('time_fetching')->nullable();
            $table->string('odf_declaration')->nullable();
            $table->string('used_second_time')->nullable();
            $table->string('handwashing')->nullable();
            $table->string('health_data_survey')->nullable();
            $table->string('health_data_health_post')->nullable();
            $table->string('water_quality_test_env')->nullable();
            $table->string('wsp_implementaion')->nullable();
            $table->string('disaster')->nullable();
            $table->string('toilet')->nullable();
            $table->string('hygiene_practice')->nullable();
            $table->string('sanitation')->nullable();
            $table->string('dish_drying')->nullable();
            $table->string('om_fund')->nullable();
            $table->string('sanitary_inspection')->nullable();
            $table->string('water_quality_test_fs')->nullable();
            $table->string('effectiveness')->nullable();
            $table->string('status_tap')->nullable();
            $table->string('triparte_agreement')->nullable();
            $table->string('awareness_program')->nullable();
            $table->string('gender_wusc')->nullable();
            $table->string('key_decision')->nullable();
            $table->string('wmv_female')->nullable();
            $table->string('gender_analysis')->nullable();
            $table->integer('project_id')->nullable();
            $table->integer('created_by')->nullable();
            $table->integer('updated_by')->nullable();
            $table->date('created_at')->nullable();
            $table->date('updated_at')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('phase_three_years');
    }
}
