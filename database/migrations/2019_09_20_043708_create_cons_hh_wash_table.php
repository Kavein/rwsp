<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConsHhWashTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('cons_hh_wash', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('project_id')->nullable();
            $table->integer('house_id')->nullable();
            $table->string('safety_management_12')->nullable();
            $table->string('safety_management_8')->nullable();
            $table->string('uptake')->nullable();
            $table->string('school_safety_12')->nullable();
            $table->string('project_scheme')->nullable();
            $table->string('supply_scheme')->nullable();
            $table->string('exceeding_service')->nullable();
            $table->string('cleanliness_competitions')->nullable();
            $table->string('handwashing_facility')->nullable();
            $table->string('clean_toilet')->nullable();
            $table->string('fchv_trained')->nullable();
            $table->string('school_trained')->nullable();
            $table->string('school_safe_haven')->nullable();
            $table->string('indicator_clean')->nullable();
            $table->string('grey_water')->nullable();
            $table->string('affordable_wealth')->nullable();
            $table->string('total_sanitation')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cons_hh_wash');
    }
}
