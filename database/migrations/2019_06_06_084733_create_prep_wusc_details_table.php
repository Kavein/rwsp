<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepWuscDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_wusc_details', function (Blueprint $table) {
            $table->increments('id');
            $table->date('toilet_monitoring_date')->nullable();
            $table->date('dish_drying_demonstration_date')->nullable();
            $table->string('dish_drying_demonstration_type')->nullable();
            $table->date('dwrc_registration')->nullable();
            $table->date('tripartie_agreement_date')->nullable();
            $table->date('agreement_date')->nullable();
            $table->date('letter_of_intent')->nullable();
            $table->date('pre_construction')->nullable();
            $table->string('pre_construction_participants')->nullable();
            $table->string('average_cost')->nullable();
            $table->string('number_dish_racks')->nullable();
            $table->string('number_washing_platforms')->nullable();
            $table->string('number_waste_disposal')->nullable();
            $table->date('water_supply_plan_date')->nullable();
            $table->string('om_fund')->nullable();

            $table->string('chairman_name')->nullable();
            $table->string('chairman_gender')->nullable();
            $table->string('chairman_cast')->nullable();

            $table->string('vice_chairman_name')->nullable();
            $table->string('vice_chairman_gender')->nullable();
            $table->string('vice_chairman_cast')->nullable();

            $table->string('secretary_name')->nullable();
            $table->string('secretary_gender')->nullable();
            $table->string('secretary_cast')->nullable();

            $table->string('vice_secretary_name')->nullable();
            $table->string('vice_secretary_gender')->nullable();
            $table->string('vice_secretary_cast')->nullable();

            $table->string('treasurer_name')->nullable();
            $table->string('treasurer_gender')->nullable();
            $table->string('treasurer_cast')->nullable();

            $table->string('member_vhp_name')->nullable();
            $table->string('member_vhp_gender')->nullable();
            $table->string('member_vhp_cast')->nullable();

            // $table->string('member_name')->nullable();
            // $table->string('member_gender')->nullable();
            // $table->string('member_cast')->nullable();

            $table->string('vmw_name')->nullable();
            $table->string('vmw_gender')->nullable();
            $table->string('vmw_cast')->nullable();

            $table->string('css_name')->nullable();
            $table->string('css_gender')->nullable();
            $table->string('css_cast')->nullable();
            
            $table->string('wsp_name')->nullable();
            $table->string('wsp_gender')->nullable();
            $table->string('wsp_cast')->nullable();
            $table->string('water_analysis_report')->nullable();
            
            $table->tinyInteger('agreement_water_structure')->nullable();
            $table->tinyInteger('agreement_pipeline')->nullable();
            $table->tinyInteger('agreement_mpc_tap')->nullable();
            $table->tinyInteger('agreement_other_tap')->nullable();
            $table->tinyInteger('agreement_contribution')->nullable();
            $table->date('agreement_date')->nullable();
            $table->tinyInteger('agreement_pic')->nullable();

            $table->integer('project_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prep_wusc_details');
    }
}
