<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrepHhAgeGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('prep_hh_age_groups', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('between_0to5_male')->nullable();
            $table->integer('between_0to5_female')->nullable();
            $table->integer('between_0to5_total')->nullable();
            $table->integer('between_6to15_male')->nullable();
            $table->integer('between_6to15_female')->nullable();
            $table->integer('between_6to15_total')->nullable();
            $table->integer('between_16to65_male')->nullable();
            $table->integer('between_16to65_female')->nullable();
            $table->integer('between_16to65_total')->nullable();
            $table->integer('above65_male')->nullable();
            $table->integer('above65_female')->nullable();
            $table->integer('above65_total')->nullable();
            $table->integer('disabled_male')->nullable();
            $table->integer('disabled_female')->nullable();
            $table->integer('disabled_total')->nullable();
            $table->integer('house_id')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('prep_hh_age_groups');
    }
}
