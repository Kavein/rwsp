<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrepInclusionData extends Model
{
    public $table = 'prep_hh_inclusion_datas';
    public $timestamps = false;

    protected $fillable = ['id','hh_male','hh_female','hh_boys','hh_girls','wf_male','wf_female','wf_boys','wf_girls','pw_male','pw_female','pw_boys','pw_girls','cw_male','cw_female','cw_boys','cw_girls','effect_on_women','other_incident_women','effect_on_children','other_effect_on_children','kitchen_gardening_practice','out_migrating','school_going_children_male','school_going_children_female','house_id','project_id','fiscal_year_id'];
}
