<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrepHealthData extends Model
{
    public $table = 'prep_hh_health_datas';
    public $timestamps = false;

    protected $fillable = ['id','house_id','diarrhoea_0to5_male','diarrhoea_0to5_female','diarrhoea_0to5_total','diarrhoea_0to5_frequency','diarrhoea_above6_male','diarrhoea_above6_female','diarrhoea_above6_total','diarrhoea_above6_frequency','dysentry_0to5_male','dysentry_0to5_female','dysentry_0to5_total','dysentry_0to5_frequency','dysentry_above6_male','dysentry_above6_female','dysentry_above6_total','dysentry_above6_frequency','jaundice_0to5_male','jaundice_0to5_female','jaundice_0to5_total','jaundice_0to5_frequency','jaundice_above6_male','jaundice_above6_female','jaundice_above6_total','jaundice_above6_frequency','colera_0to5_male','colera_0to5_female','colera_0to5_total','colera_0to5_frequency','colera_above6_male','colera_above6_female','colera_above6_total','colera_above6_frequency','worm_infection_0to5_male','worm_infection_0to5_female','worm_infection_0to5_total','worm_infection_0to5_frequency','worm_infection_above6_male','worm_infection_above6_female','worm_infection_above6_total','worm_infection_above6_frequency','scabies_0to5_male','scabies_0to5_female','scabies_0to5_total','scabies_0to5_frequency','scabies_above6_male','scabies_above6_female','scabies_above6_total','scabies_above6_frequency','sickness_absence_school','max_sick_days','death_cause','bedrest_adult','bedrest_days','project_id','fiscal_year_id'];
}
