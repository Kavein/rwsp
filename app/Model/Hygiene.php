<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Hygiene extends Model
{
    public $table = 'hh_hygiene';
    public $timestamps = false;

    protected $fillable = ['id','latrine_available','latrine_cost','total_latrine','latrine_cleansing_agent','latrine_water_available','latrine_cleaning_brush','defecate_latrine','defecate_open','home_cleaning','domestic_animal','drinking_water','food_covered','utensils_clean','garbage_disposel_pit','handwash_water_soap','handwash_water','handwash_ash','handwash_others','defecate_water_soap','defecate_water','defecate_ash','handwash_infant','bath_frequency','cloth_wash_frequency','menstrual_knowledge','house_id','project_id','fiscal_year_id'];
}
