<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class PrepAgeGroup extends Model
{
    public $table = 'prep_hh_age_groups';
    public $timestamps = false;

    protected $fillable = ['id','between_0to5_male','between_0to5_female','between_0to5_total','between_6to15_male','between_6to15_female','between_6to15_total','between_16to65_male','between_16to65_female','between_16to65_total','above65_male','above65_female','above65_total','disabled_male','disabled_female','disabled_total','house_id','fiscal_year_id','project_id'];
}
