<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WaterFetch extends Model
{
    public $table = 'hh_water_fetching_time';
    public $timestamps = false;

    protected $fillable = ['id','house_id','water_collection_liter','going_second','waiting_second','filling_second','return_second','total_second','total_second_8round','total_minute_8round','total_hour_8round','project_id','fiscal_year_id'];
}
