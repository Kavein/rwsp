<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class CommunityInfo extends Model
{
    public $table = 'prj_community_infos';
    public $timestamps = false;

    protected $fillable = ['id','community_name','other_existing_latrines','other_tapstand_required','diarrhoea_male','diarrhoea_female','diarrhoea_total','dysentery_male','dysentery_female','dysentery_total','jaundice_male','jaundice_female','jaundice_total','colera_male','colera_female','colera_total','worms_male','worms_female','worms_total','scabies_male','scabies_female','scabies_total','profiling_date','disabled_male','disabled_female','blind_male','blind_female','deaf_male','deaf_female','blind_deaf_male','blind_deaf_female','speech_male','speech_female','mental_male','mental_female','intel_male','intel_female','multiple_male','multiple_female','community_mapping','project_id','fiscal_year'];
}
