<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class ConsHealthBehaviour extends Model
{
    public $table = 'cons_hh_health_behaviors';
    public $timestamps = false;

    protected $fillable = ['id','healthpost_preference','traditional_preference','home_medecine_preference','annual_expenses','latrine_knowledge','disease_knowledge','safewater_knowledge','handwashing_knowledge','house_id','project_id','fiscal_year_id'];
}
