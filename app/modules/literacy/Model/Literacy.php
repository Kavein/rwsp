<?php

namespace App\Modules\Literacy\Model;


use Illuminate\Database\Eloquent\Model;

class Literacy extends Model
{
    public  $table = 'mst_literacies';

    protected $fillable = ['id','name','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
