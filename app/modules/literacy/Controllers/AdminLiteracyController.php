<?php

namespace App\Modules\Literacy\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Literacy\Model\Literacy;

class AdminLiteracyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Literacy';
        return view("literacy::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getliteraciesJson(Request $request)
    {
        $literacy = new Literacy;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $literacy->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $literacy->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $literacy->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Literacy | Create';
        return view("literacy::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required',
                        ],
                        [
                            'name.required' => 'Literacy Cannot Be Empty!!',
                        ]);
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        $data['status'] = checkValue($request->status);
        $data['del_flag'] = 0;
        $success = Literacy::Create($data);
        return redirect()->route('admin.literacies')->with('success','Successfully Added !!');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $literacy = Literacy::findOrFail($id);
        $page['title'] = 'Literacy | Update';
        return view("literacy::edit",compact('page','literacy'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required',
                        ],
                        [
                            'name.required' => 'Income Source Cannot Be Empty!!',
                        ]);
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;        
        $data['status'] = checkValue($request->status);
        $data['del_flag'] = 0;
        $success = Literacy::where('id', $id)->update($data);
        return redirect()->route('admin.literacies')->with('success','Updated Successfully !!');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 0;
        $success = Literacy::where('id', $id)->update($data);
        return redirect()->route('admin.literacies');

        //
    }
}
