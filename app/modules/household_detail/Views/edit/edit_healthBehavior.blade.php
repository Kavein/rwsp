<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><label>Health Seeking Behaviors</label></strong></h3 >
    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Prefering Health Post</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="healthpost_preference" value="Yes" name="behavior[healthpost_preference]" {{$healthbehaviors->healthpost_preference == "Yes" ? 'checked':''}} >
                <label class="form-check-label" for="healthpost_preference">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="healthpost_preference" value="No" name="behavior[healthpost_preference]" {{$healthbehaviors->healthpost_preference == "No" ? 'checked':''}}>
                <label class="form-check-label" for="healthpost_preference">No</label>
            </div>
        </div>  
    </div>
    

    <div class="col-md-12 form-group row">
        <label for="traditional_preference" class="col-sm-4 col-form-label">Prefering Traditional Healer</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="traditional_preference" value="Yes" name="behavior[traditional_preference]" {{$healthbehaviors->traditional_preference == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="traditional_preference">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="traditional_preference" value="No" name="behavior[traditional_preference]" {{$healthbehaviors->traditional_preference == "No" ? 'checked':''}}>
                <label class="form-check-label" for="traditional_preference">No</label>
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Prefering Homemade Medicines</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="home_medecine_preference" value="Yes" name="behavior[home_medecine_preference]" {{$healthbehaviors->home_medecine_preference == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="home_medecine_preference">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="home_medecine_preference" value="No" name="behavior[home_medecine_preference]" {{$healthbehaviors->home_medecine_preference == "No" ? 'checked':''}}>
                <label class="form-check-label" for="home_medecine_preference">No</label>
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Annual Expenses On Treatment Of Disease</label>
        
        <div class="col-sm-6">
            <input type="number" name="behavior[annual_expenses]" class="form-control" placeholder="Annual Expenses" value="{{$healthbehaviors->annual_expenses}}">
            @if ($errors->has('behavior.annual_expenses'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('behavior.annual_expenses') }}</strong>
                </span>
            @endif
        </div>  
    </div>
    <div class="col-md-12 form-group row">
        <label for="latrine_knowledge" class="col-sm-4 col-form-label">Knowledge About Importance Of Latrine</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="latrine_knowledge" value="Yes" name="behavior[latrine_knowledge]" {{$healthbehaviors->latrine_knowledge == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="latrine_knowledge">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="latrine_knowledge" value="No" name="behavior[latrine_knowledge]" {{$healthbehaviors->latrine_knowledge == "No" ? 'checked':''}}>
                <label class="form-check-label" for="latrine_knowledge">No</label>
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="disease_knowledge" class="col-sm-4 col-form-label">Knowledge About Reasons Of Diseases</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="disease_knowledge" value="Yes" name="behavior[disease_knowledge]" {{$healthbehaviors->disease_knowledge == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="disease_knowledge">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="disease_knowledge" value="No" name="behavior[disease_knowledge]" {{$healthbehaviors->disease_knowledge == "No" ? 'checked':''}}>
                <label class="form-check-label" for="disease_knowledge">No</label>
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="safewater_knowledge" class="col-sm-4 col-form-label">Perception About Safe Water</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="safewater_knowledge" value="Yes" name="behavior[safewater_knowledge]" {{$healthbehaviors->safewater_knowledge == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="safewater_knowledge">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="safewater_knowledge" value="No" name="behavior[safewater_knowledge]" {{$healthbehaviors->safewater_knowledge == "No" ? 'checked':''}}>
                <label class="form-check-label" for="safewater_knowledge">No</label>
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="handwashing_knowledge" class="col-sm-4 col-form-label">Knowledge About Importance Of Hand Washing</label>
        
        <div class="col-sm-6">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input" type="radio" id="handwashing_knowledge" value="Yes" name="behavior[handwashing_knowledge]" {{$healthbehaviors->handwashing_knowledge == "Yes" ? 'checked':''}}>
                <label class="form-check-label" for="handwashing_knowledge">Yes</label>
            </div>
            <div class=" form-check-inline col-sm-4">
                <input class="form-check-input" type="radio" id="handwashing_knowledge" value="No" name="behavior[handwashing_knowledge]" {{$healthbehaviors->handwashing_knowledge == "No" ? 'checked':''}}>
                <label class="form-check-label" for="handwashing_knowledge">No</label>
            </div>
        </div>  
    </div>
</div>