
<table border="1">
	<thead>
		<tr>
			<th>House Number</th>
			<th>ProjectId</th>
			<th>Head Name</th>
			<th>Between Zero to Five Male</th>
			<th>Between Zero to Five FeMale</th>
			<th>Between Zero to Five TOtal</th>
			
		</tr>
	</thead>
	@foreach($data as $value)
	<tr>
		<td>{{$value->house_no}}</td>
		<td>{{$value->project_id}}</td>
		<td>{{$value->head_name}}</td>
		<td>{{ $value->agegrp->between_0to5_male }}</td>
		<td>{{ $value->agegrp->between_0to5_female }}</td>
		<td>{{ $value->agegrp->between_0to5_total }}</td>
	</tr>
	@endforeach
</table>