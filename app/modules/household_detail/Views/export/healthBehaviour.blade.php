@php 
$datas=$data[0];

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>House Detail</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				@if(@$datas->checkphase == "initial")
					<td colspan="21">Initial Phase Health Behaviour Report</td>
				@elseif(@$datas->checkphase == "preparation")
					<td colspan="21">Preparation Health Behaviour Report</td>
				@elseif(@$datas->checkphase == "construction")
					<td colspan="21">Construction Health Behaviour Report</td>
				@endif
				
			</tr>
			<tr>
				<td>Project Name</td>
				<td>{{  @$datas->projectname }}</td>
			</tr>
			<tr>
				<td>Project Number</td>
				<td>{{  @$datas->project_no }}</td>
			</tr>
			<tr>
				<td>Provience</td>
				<td>{{  @$datas->province->name }}</td>
			</tr>
			<tr>
				<td>District</td>
				<td>{{ @$datas->district->name }}</td>
			</tr>
			<tr>
				<td>Gaupalika/Nagarpalika</td>
				<td>{{ @$datas->vdc }}</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>House Number</td>
				<td>Family Head Name</td>
				<td>Family Head Gender</td>
				<td>Prefering Health Post</td>
				<td>Prefering Traditional Healer</td>
				<td>Prefering Homemade Medicines</td>
				<td>Annual Expenses On Treatment Of Disease</td>
				<td>Knowledge About Importance Of Latrine</td>
				<td>Knowledge About Reasons Of Diseases</td>
				<td>Perception About Safe Water</td>
				<td>Knowledge About Importance Of Hand Washing</td>
				
			</tr>

			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->house_no}}</td>
				<td>{{$value->head_name}}</td>
				<td>{{$value->head_gender}}</td>
				<td>{{$value->health[0]->healthpost_preference}}</td>
				<td>{{$value->health[0]->traditional_preference}}</td>
				<td>{{$value->health[0]->home_medecine_preference}}</td>
				<td>{{$value->health[0]->annual_expenses}}</td>
				<td>{{$value->health[0]->latrine_knowledge}}</td>
				<td>{{$value->health[0]->disease_knowledge}}</td>
				<td>{{$value->health[0]->safewater_knowledge}}</td>
				<td>{{$value->health[0]->handwashing_knowledge}}</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
</body>
</html>