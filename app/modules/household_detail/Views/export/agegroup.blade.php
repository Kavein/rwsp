@php 
$datas=$data[0];

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>House Detail</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="21">HouseDetail  Report</td>
			</tr>
			<tr>
				<td>Project Name</td>
				<td>{{  @$datas->projectname }}</td>
			</tr>
			<tr>
				<td>Project Number</td>
				<td>{{  @$datas->project_no }}</td>
			</tr>
			<tr>
				<td>Provience</td>
				<td>{{  @$datas->province->name }}</td>
			</tr>
			<tr>
				<td>District</td>
				<td>{{ @$datas->district->name }}</td>
			</tr>
			<tr>
				<td>Gaupalika/Nagarpalika</td>
				<td>{{ @$datas->vdc }}</td>
			</tr>
			<tr>
				<td rowspan="2">S.No</td>
				<td rowspan="2">House Number</td>
				<td rowspan="2">Family Head Name</td>
				<td rowspan="2">Family Head Gender</td>
				<td colspan="3">Age Group ( 0 - 5 )</td>
				<td colspan="3">Age Group ( 6 - 15 )</td>
				<td colspan="3">Age Group ( 16 - 65 )</td>
				<td colspan="3">Age Group ( Above 65 )</td>
				
			</tr>
			<tr>
				<td>Male</td>
				<td>Female</td>
				<td>Total</td>
				<td>Male</td>
				<td>Female</td>
				<td>Total</td>
				<td>Male</td>
				<td>Female</td>
				<td>Total</td>
				<td>Male</td>
				<td>Female</td>
				<td>Total</td>
			</tr>

			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->house_no}}</td>
				<td>{{$value->head_name}}</td>
				<td>{{$value->head_gender}}</td>
				<td>{{$value->age[0]->between_0to5_male}}</td>
				<td>{{$value->age[0]->between_0to5_female}}</td>
				<td>{{$value->age[0]->between_0to5_total}}</td>
				<td>{{$value->age[0]->between_6to15_male}}</td>
				<td>{{$value->age[0]->between_6to15_female}}</td>
				<td>{{$value->age[0]->between_6to15_total}}</td>
				<td>{{$value->age[0]->between_16to65_male}}</td>
				<td>{{$value->age[0]->between_16to65_female}}</td>
				<td>{{$value->age[0]->between_16to65_total}}</td>
				<td>{{$value->age[0]->above65_male}}</td>
				<td>{{$value->age[0]->above65_female}}</td>
				<td>{{$value->age[0]->above65_total}}</td>
			</tr>
			@endforeach
			
			
			
			
		
			
			
			
			
		</tbody>
	</table>
</body>
</html>