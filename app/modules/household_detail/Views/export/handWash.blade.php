@php 
$datas=$data[0];
//  dd($data);
@endphp
<table border="1">
	<tbody>
		<tr>
			<th colspan="16">
				@if(@$datas->checkphase == "initial")
					<h3>Initial Phase Hand Wash Detail</h3>
				@elseif(@$datas->checkphase == "preparation")
					<h3>Preparation Phase Hand Wash Detail</h3>
				@elseif(@$datas->checkphase == "construction")
					<h3>Construction Phase Hand Wash Detail</h3>
				@endif
			</th>
		</tr>
		<tr>
			<td>Project Name</td>
			<td>{{  @$datas->projectname }}</td>
		</tr>
		<tr>
			<td>Project Number</td>
			<td>{{  @$datas->project_no }}</td>
		</tr>
		<tr>
			<td>Provience</td>
			<td>{{  @$datas->province->name }}</td>
		</tr>
		<tr>
			<td>District Name</td>
			
			<td>{{ @$datas->district->name }}</td>
			
		</tr>
	
		<tr>
			<td>VDC Name</td>
			<td>{{ @$datas->vdc}}</td>
		</tr>
		
		<tr>
			<td>Ward No</td>
			<td>{{ @$datas->province->ward_no }}</td>
		</tr>

		<tr>
			<td rowspan="2">House No</td>
			<td rowspan="2">Community Name</td>
			<td rowspan="2">Head Name</td>
			<td rowspan="2">Total Members</td>
			<th colspan="4">Hand Wash Before Eating </th>
			<th colspan="3">Hand Wash After Defecation  </th>
			<th rowspan="2">Infant Children no wash Hand</th>
			<th rowspan="2">Bath Frequency</th>
			<th rowspan="2">Cloth Washing Frequency</th>
		</tr>
		<tr>
			<th >With Water And Soap </th>
			<th >With Water Only</th>
			<th >With Ash Only</th>
			<th >No Wash</th>
			<th >With Water And Soap </th>
			<th >With Water Only</th>
			<th >With Ash Only</th>
		
		</tr>
		@php 
		
		$sum_handwash_water_soap=0;
		$sum_handwash_water=0;
		$sum_handwash_ash=0;
		$sum_handwash_others=0;
		$sum_defecate_water_soap=0;
		$sum_defecate_water=0;
		$sum_defecate_ash=0;
		$sum_handwash_infant=0;
		$sum_total_member=0;
	
		@endphp
		@foreach($data as $value)
		<tr>
			<td>{{@$value->house_no}}</td>
			<td>{{@$value->community->community_name}}</td>
			<td>{{@$value->head_name}}</td>
			<td>{{@$value->total_member}}</td>
			
			<td>{{@$value->handWash->handwash_water_soap}}</td>
			<td>{{@$value->handWash->handwash_water}}</td>
			<td>{{@$value->handWash->handwash_ash}}</td>
			<td>{{@$value->handWash->handwash_others}}</td>
			<td>{{@$value->handWash->defecate_water_soap}}</td>
			<td>{{@$value->handWash->defecate_water}}</td>
			<td>{{@$value->handWash->defecate_ash}}</td>
		
			<td>{{@$value->handWash->handwash_infant}}</td>
			<td>{{@$value->handWash->bath_frequency}}</td>
			<td>{{@$value->handWash->cloth_wash_frequency}}</td> 
		</tr>

		@php 
		$sum_total_member+=$value->total_member;
		$sum_handwash_water_soap+=$value->handWash->handwash_water_soap;
		$sum_handwash_water+=$value->handWash->handwash_water;
		$sum_handwash_ash+=$value->handWash->handwash_ash;
		$sum_handwash_others+=$value->handWash->handwash_others;
		$sum_defecate_water_soap+=$value->handWash->defecate_water_soap;
		$sum_defecate_water+=$value->handWash->defecate_water;
		$sum_defecate_ash+=$value->handWash->defecate_ash;
		$sum_handwash_infant+=$value->handWash->handwash_infant;
	@endphp
		@endforeach
<tr><td colspan="3">total </td>
	<td>{{$sum_total_member}}</td>
	<td> {{ $sum_handwash_water_soap }} </td>
	<td> {{ $sum_handwash_water }} </td>
	<td> {{ $sum_handwash_ash }} </td>
	<td> {{ $sum_handwash_others }} </td>
	<td> {{ $sum_defecate_water_soap }} </td>
	<td> {{ $sum_defecate_water }} </td>
	<td> {{ $sum_defecate_ash }} </td>
	<td> {{ $sum_handwash_infant }} </td>
	
</tr>
	</tbody>
</table>	