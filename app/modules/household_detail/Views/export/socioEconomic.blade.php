@php 
$datas=$data[0];
//  dd($data);
@endphp
<table border="1">
<tbody>
	<tr>
		<th colspan="23"> 
			@if(@$datas->checkphase == "initial")
				<h3>Initial Social Economic Detail</h3>
			@elseif(@$datas->checkphase == "preparation")
				<h3>Preparation Social Economic Detail</h3>
			@elseif(@$datas->checkphase == "construction")
				<h3>Construction Social Economic Detail</h3>
			@endif</th>
	</tr>
	<tr>
		<td>Project Name</td>
		<td>{{  @$datas->projectname }}</td>
	</tr>
	<tr>
		<td>Project Number</td>
		<td>{{  @$datas->project_no }}</td>
	</tr>
	<tr>
		<td>Provience</td>
		<td>{{  @$datas->province->name }}</td>
	</tr>
		
		<tr>
			<td>District Name</td>
			<td>{{ @$datas->district->name }}</td>
		</tr>
		<tr>
			<td>VDC Name</td>
			<td>{{ @$datas->vdc}}</td>
		</tr>
		<tr>
			<td>Ward No</td>
			<td>{{ @$datas->province->ward_no }}</td>
		</tr>
	<tr>
		<td rowspan="2">House No</td>
		<td rowspan="2">Community Name</td>
		<td rowspan="2">Head Name</td>
		<td rowspan="2">Total Members</td>
		<td rowspan="2">Occupation</td>
		<th colspan="5">Income Source And Pension </th>
		<th colspan="2">Litrate(Above age of 6)</th>
		<th colspan="2">Children going to School</th>
		<th colspan="4">Water Fetching</th>
		<th >Homestead Kitchen Gardening</th>
		<th>Affects on Children Due to Water Fetching</th>
		<th rowspan="2">Women With Women As Lead</th>
		<th rowspan="2">Widow Women Number</th>
		<th rowspan="2">Disable People Number</th>
	</tr>
	<tr>
		@php
		$sum_total_member=0;
	$sum_litrate_no=0;
	$sum_illitrate_no=0;
	$sum_school_going_children_male=0;
	$sum_school_going_children_female=0;
	$sum_hh_male=0;
	$sum_hh_female=0;
	$sum_hh_boys=0;
	$sum_hh_girls=0;

	
	$sum_head_gender=0;
	$sum_total_female_widow=0;
	$sum_disabled_total=0;	
		@endphp
		@foreach($data->income_sources as $value)
			<th>{{@$value->name}}</th>
		@endforeach
		<th >Litrate</th>
		<th >Illitrate</th>
		<th >Boys</th>
		<th >Girls</th>
		<th >Men</th>
		<th >Women</th>
		<th >Boys</th>
		<th >Girls</th>
		<th >Have/No Have</th>
		<th >Effect</th>
	</tr>

	@foreach($data as $key => $items)
	
	<tr>
		<td>{{@$items->house_no}}</td>
		<td>{{@$items->community->community_name}}</td>
		<td>{{@$items->head_name}}</td>
		<td>{{@$items->total_member}}</td>
		<td>{{@$items->occupations}}</td>
		@foreach($data->income_sources as $value)
			@if(array_key_exists(@$value->id,$items->income_sources))
				<td>Yes</td>
			@else
				<td>No</td>
			@endif
		@endforeach
		<td>{{@$items->litrate_no}}</td>
		<td>{{@$items->illitrate_no}}</td>
		<td>{{@$items->inclusionData->school_going_children_male}}</td>
		<td>{{@$items->inclusionData->school_going_children_female}}</td>
		<td>{{@$items->inclusionData->hh_male}}</td>
		<td>{{@$items->inclusionData->hh_female}}</td>
		<td>{{@$items->inclusionData->hh_boys}}</td>
		<td>{{@$items->inclusionData->hh_girls}}</td>
		<td>{{@$items->inclusionData->kitchen_gardening_practice}}</td>
		<td>{{@$items->inclusionData->effect_on_children}}</td>
		<td>{{@$items->head_gender}}</td>
		<td>{{@$items->total_female_widow}}</td>
		<td>{{@$items->ageGroup->disabled_total}}</td>
	</tr>
	

	@php 

$sum_total_member+= @$items->total_member;
	$sum_litrate_no+=@$items->litrate_no;
	$sum_illitrate_no+=@$items->illitrate_no;
	$sum_school_going_children_male+=@$items->inclusionData->school_going_children_male;
	$sum_school_going_children_female+=@$items->inclusionData->school_going_children_female;
	$sum_hh_male+=	@$items->inclusionData->hh_male;
	$sum_hh_female+=@$items->inclusionData->hh_female;
	$sum_hh_boys+=@$items->inclusionData->hh_boys;
	$sum_hh_girls+=@$items->inclusionData->hh_girls;
	

	
	$sum_total_female_widow+=@$items->total_female_widow;
	$sum_disabled_total+=	@$items->ageGroup->disabled_total;
	@endphp
	@endforeach
	<tr><td colspan="3"> total</td>
		<td>{{ $sum_total_member}} </td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
		<td></td>
	<td>{{ $sum_litrate_no}} </td>
	<td>{{ $sum_illitrate_no}} </td>
	<td>{{ $sum_school_going_children_male}} </td>
	<td>{{ $sum_school_going_children_female}} </td>
	<td>{{ $sum_hh_male}} </td>
	<td>{{ $sum_hh_female}} </td>
	<td>{{ $sum_hh_boys}} </td>
	<td>{{ $sum_hh_girls}} </td>
	<td></td>
	<td></td>
	<td></td>
	<td>{{ $sum_total_female_widow}} </td>
	<td>{{ $sum_disabled_total}} </td></tr>
</tbody>