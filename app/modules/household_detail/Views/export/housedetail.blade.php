@php 
$datas=$data[0];

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>House Detail</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="21">HouseDetail  Report</td>
			</tr>
			<tr>
				<td>Project Name</td>
				<td>{{  @$datas->projectname }}</td>
			</tr>
			<tr>
				<td>Project Number</td>
				<td>{{  @$datas->project_no }}</td>
			</tr>
			<tr>
				<td>Provience</td>
				<td>{{  @$datas->province->name }}</td>
			</tr>
			<tr>
				<td>District</td>
				<td>{{ @$datas->district->name }}</td>
			</tr>
			<tr>
				<td>Gaupalika/Nagarpalika</td>
				<td>{{ @$datas->vdc }}</td>
			</tr>
			<tr>
				<td rowspan="2">S.No</td>
				<td rowspan="2">House Number</td>
				<td rowspan="2">Family Head Name</td>
				<td rowspan="2">Family Head Gender</td>
				<td rowspan="2">Tole Name</td>
				<td rowspan="2">Cast Division</td>
				<td rowspan="2">Cast</td>
				<td colspan="2">Total Member</td>
				<td rowspan="2">Widow Women</td>
				<td rowspan="2">Litrate Number</td>
				<td rowspan="2">Illitrate Number</td>
				<td rowspan="2">Occupation</td>
				<td rowspan="2">Income Source</td>
			</tr>
			<tr>
				<td>Male</td>
				<td>Female</td>
			</tr>

			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->house_no}}</td>
				<td>{{$value->head_name}}</td>
				<td>{{$value->head_gender}}</td>
				<td>{{$value->tole_name}}</td>
				<td>{{$value->get_cast_division}}</td>
				<td>{{$value->get_cast}}</td>
				<td>{{$value->total_male}}</td>
				<td>{{$value->total_female}}</td>
				<td>{{$value->total_female_widow}}</td>
				<td>{{$value->litrate_no}}</td>
				<td>{{$value->illitrate_no}}</td>
				<td>{{$value->occ}}</td>
				<td>{{$value->incos}}</td>
			</tr>
			@endforeach
			
			
			
			
		
			
			
			
			
		</tbody>
	</table>
</body>
</html>