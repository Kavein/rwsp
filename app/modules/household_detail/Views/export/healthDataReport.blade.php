@php 
$datas=$data[0];
//  dd($data);
@endphp
<table border="1">
	<tbody>
		<tr>
			<th colspan="59">
			@if(@$datas->checkphase == "initial")
			 	<h3>Initial Phase Data</h3>
			 @elseif(@$datas->checkphase == "preparation")
			 	<h3>Preparation Phase Data</h3>
			 @elseif(@$datas->checkphase == "construction")
			 	<h3>Construction Phase Data</h3>
			 @endif 
			</th>
		</tr>
		<tr>
			<td>Project Name</td>
			<td>{{  @$datas->projectname }}</td>
		</tr>
		<tr>
			<td>Project Number</td>
			<td>{{  @$datas->project_no }}</td>
		</tr>
		<tr>
			<td>Provience</td>
			<td>{{  @$datas->province->name }}</td>
		</tr>
		<tr>
			<td>District Name</td>
			<td>{{ @$datas->district->name }}</td>
		</tr>
		<tr>
			<td>VDC Name</td>
			<td>{{ @$datas->vdc}}</td>
		</tr>
		<tr>
			<td>Ward No</td>
			<td>{{ @$datas->province->ward_no }}</td>
		</tr>
		<tr>
			<td colspan="4"></td>
			<th colspan="8">INCIDENCE BY DIARRHOEA </th>
			<th colspan="8">INCIDENCE BY DYSENTERY  </th>
			<th colspan="8">INCIDENCE BY JAUNDICE  </th>
			<th colspan="8">INCIDENCE BY COLERA  </th>
			<th colspan="8">INCIDENCE BY WORMS INFESTATION </th>
			<th colspan="8">INCIDENCE BY SCABIES  </th>
			<th rowspan="3">Leave From School Due To Sickness  </th>
			<th rowspan="3">Total Days of Sickness  </th>
			<th rowspan="3">Death Cause And Count  </th>
			<th rowspan="2" colspan="3">Death Cause And Count  </th>
			<th rowspan="3">Annual Expenses On Treatment Of Disease </th>
		</tr>
		<tr>
			<td colspan="4"></td>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>
			<th colspan="4">Below Age 5 </th>
			<th colspan="4">Above 6 </th>

			{{-- <th colspan="2">Below Age 5 </th>
			<th colspan="2">Between 6-15 </th>
			<th colspan="2">Above 16 </th>
			<th colspan="2">Below Age 5 </th>
			<th colspan="2">Between 6-15 </th>
			<th colspan="2">Above 16 </th>
			<th colspan="2">Below Age 5 </th>
			<th colspan="2">Between 6-15 </th>
			<th colspan="2">Above 16 </th>
			<th colspan="2">Below Age 5 </th>
			<th colspan="2">Between 6-15 </th>
			<th colspan="2">Above 16 </th>
			<th colspan="2">Below Age 5 </th>
			<th colspan="2">Between 6-15 </th>
			<th colspan="2">Above 16 </th> --}}
		</tr>
		
		<tr>
			<td>House Number</td>
			<td>Community Name</td>
			<td>Head Name</td>
			<td>Total Member</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>

			<td>Male</td>
			<td>Female</td>
			<td>Total Sick</td>
			<td>Frequency of Sickness</td>
			
			
			<td>Health Post</td>
			<td>Traditional Healer</td>
			<td>Homemade Medicines</td>
			
		</tr>
		@php
		
					$sum_total_member =0;
			       $sum_diarrhoea_0to5_male =0;
			       $sum_diarrhoea_0to5_female =0;
			       $sum_diarrhoea_0to5_total =0;
			       $sum_diarrhoea_0to5_frequency =0;

			       $sum_diarrhoea_above6_male =0;
			       $sum_diarrhoea_above6_female =0;
			       $sum_diarrhoea_above6_total =0;
			       $sum_diarrhoea_above6_frequency =0;

			       $sum_dysentry_0to5_male =0;
			       $sum_dysentry_0to5_female =0;
			       $sum_dysentry_0to5_total =0;
			       $sum_dysentry_0to5_frequency =0;

			       $sum_dysentry_above6_male =0;
			       $sum_dysentry_above6_female =0;
			       $sum_dysentry_above6_total =0;
			       $sum_dysentry_above6_frequency =0;

			       $sum_jaundice_0to5_male =0;
			       $sum_jaundice_0to5_female =0;
			       $sum_jaundice_0to5_total =0;
			       $sum_jaundice_0to5_frequency =0;

			       $sum_jaundice_above6_male =0;
			       $sum_jaundice_above6_female =0;
			       $sum_jaundice_above6_total =0;
			       $sum_jaundice_above6_frequency =0;

			       $sum_colera_0to5_male =0;
			       $sum_colera_0to5_female =0;
			       $sum_colera_0to5_total =0;
			       $sum_colera_0to5_frequency =0;

			       $sum_colera_above6_male =0;
			       $sum_colera_above6_female =0;
			       $sum_colera_above6_total =0;
			       $sum_colera_above6_frequency =0;

			       $sum_worm_infection_0to5_male =0;
			       $sum_worm_infection_0to5_female =0;
			       $sum_worm_infection_0to5_total =0;
			       $sum_worm_infection_0to5_frequency =0;

			       $sum_worm_infection_above6_male =0;
			       $sum_worm_infection_above6_female =0;
			       $sum_worm_infection_above6_total =0;
			       $sum_worm_infection_above6_frequency =0;
			
			       $sum_scabies_0to5_male =0;
			       $sum_scabies_0to5_female =0;
			       $sum_scabies_0to5_total =0;
			       $sum_scabies_0to5_frequency =0;

			       $sum_scabies_above6_male =0; 
			       $sum_scabies_above6_female =0; 
			       $sum_scabies_above6_total =0;  
			       $sum_scabies_above6_frequency =0; 

			       $sum_sickness_absence_school =0;
			       $sum_max_sick_days =0;
			  
			     	$sum_healthpost_preference =0;
			     	$sum_traditional_preference =0;
			   		$sum_home_medecine_preference =0;
			      	$sum_annual_expenses =0;
		
		@endphp
		@foreach($data as $value)
		<tr>
			<td>{{@$value->house_no}}</td>
			<td>{{@$value->community->community_name}}</td>
			<td>{{@$value->head_name}}</td>
			<td>{{@$value->total_member}}</td>
			<td>{{@$value->healthData->diarrhoea_0to5_male}}</td>
			<td>{{@$value->healthData->diarrhoea_0to5_female}}</td>
			<td>{{@$value->healthData->diarrhoea_0to5_total}}</td>
			<td>{{@$value->healthData->diarrhoea_0to5_frequency}}</td>

			<td>{{@$value->healthData->diarrhoea_above6_male}}</td>
			<td>{{@$value->healthData->diarrhoea_above6_female}}</td>
			<td>{{@$value->healthData->diarrhoea_above6_total}}</td>
			<td>{{@$value->healthData->diarrhoea_above6_frequency}}</td>

			<td>{{@$value->healthData->dysentry_0to5_male}}</td>
			<td>{{@$value->healthData->dysentry_0to5_female}}</td>
			<td>{{@$value->healthData->dysentry_0to5_total}}</td>
			<td>{{@$value->healthData->dysentry_0to5_frequency}}</td>

			<td>{{@$value->healthData->dysentry_above6_male}}</td>
			<td>{{@$value->healthData->dysentry_above6_female}}</td>
			<td>{{@$value->healthData->dysentry_above6_total}}</td>
			<td>{{@$value->healthData->dysentry_above6_frequency}}</td>

			<td>{{@$value->healthData->jaundice_0to5_male}}</td>
			<td>{{@$value->healthData->jaundice_0to5_female}}</td>
			<td>{{@$value->healthData->jaundice_0to5_total}}</td>
			<td>{{@$value->healthData->jaundice_0to5_frequency}}</td>

			<td>{{@$value->healthData->jaundice_above6_male}}</td>
			<td>{{@$value->healthData->jaundice_above6_female}}</td>
			<td>{{@$value->healthData->jaundice_above6_total}}</td>
			<td>{{@$value->healthData->jaundice_above6_frequency}}</td>

			<td>{{@$value->healthData->colera_0to5_male}}</td>
			<td>{{@$value->healthData->colera_0to5_female}}</td>
			<td>{{@$value->healthData->colera_0to5_total}}</td>
			<td>{{@$value->healthData->colera_0to5_frequency}}</td>

			<td>{{@$value->healthData->colera_above6_male}}</td>
			<td>{{@$value->healthData->colera_above6_female}}</td>
			<td>{{@$value->healthData->colera_above6_total}}</td>
			<td>{{@$value->healthData->colera_above6_frequency}}</td>

			<td>{{@$value->healthData->worm_infection_0to5_male}}</td>
			<td>{{@$value->healthData->worm_infection_0to5_female}}</td>
			<td>{{@$value->healthData->worm_infection_0to5_total}}</td>
			<td>{{@$value->healthData->worm_infection_0to5_frequency}}</td>

			<td>{{@$value->healthData->worm_infection_above6_male}}</td>
			<td>{{@$value->healthData->worm_infection_above6_female}}</td>
			<td>{{@$value->healthData->worm_infection_above6_total}}</td>
			<td>{{@$value->healthData->worm_infection_above6_frequency}}</td>
			
			<td>{{@$value->healthData->scabies_0to5_male}}</td>
			<td>{{@$value->healthData->scabies_0to5_female}}</td>
			<td>{{@$value->healthData->scabies_0to5_total}}</td>
			<td>{{@$value->healthData->scabies_0to5_frequency}}</td>

			<td>{{@$value->healthData->scabies_above6_male}}</td>{{-- column nai --}}
			<td>{{@$value->healthData->scabies_above6_female}}</td>{{-- column nai --}}
			<td>{{@$value->healthData->scabies_above6_total}}</td>{{-- column nai --}}
			<td>{{@$value->healthData->scabies_above6_frequency}}</td>{{-- column nai --}}

			<td>{{@$value->healthData->sickness_absence_school}}</td>
			<td>{{@$value->healthData->max_sick_days}}</td>
			<td>{{strip_tags(@$value->healthData->death_cause)}}</td>
			<td>{{@$value->healthBehavior->healthpost_preference}}</td>
			<td>{{@$value->healthBehavior->traditional_preference}}</td>
			<td>{{@$value->healthBehavior->home_medecine_preference}}</td>
			<td>{{@$value->healthBehavior->annual_expenses}}</td>
		</tr>

@php

$sum_total_member +=$value->total_member;
			       $sum_diarrhoea_0to5_male +=$value->healthData->diarrhoea_0to5_male;
			       $sum_diarrhoea_0to5_female +=$value->healthData->diarrhoea_0to5_female;
			       $sum_diarrhoea_0to5_total +=$value->healthData->diarrhoea_0to5_total;
			       $sum_diarrhoea_0to5_frequency +=$value->healthData->diarrhoea_0to5_frequency;

			       $sum_diarrhoea_above6_male +=$value->healthData->diarrhoea_above6_male;
			       $sum_diarrhoea_above6_female +=$value->healthData->diarrhoea_above6_female;
			       $sum_diarrhoea_above6_total +=$value->healthData->diarrhoea_above6_total;
			       $sum_diarrhoea_above6_frequency +=$value->healthData->diarrhoea_above6_frequency;

			       $sum_dysentry_0to5_male +=$value->healthData->dysentry_0to5_male;
			       $sum_dysentry_0to5_female +=$value->healthData->dysentry_0to5_female;
			       $sum_dysentry_0to5_total +=$value->healthData->dysentry_0to5_total;
			       $sum_dysentry_0to5_frequency +=$value->healthData->dysentry_0to5_frequency;

			       $sum_dysentry_above6_male +=$value->healthData->dysentry_above6_male;
			       $sum_dysentry_above6_female +=$value->healthData->dysentry_above6_female;
			       $sum_dysentry_above6_total +=$value->healthData->dysentry_above6_total;
			       $sum_dysentry_above6_frequency +=$value->healthData->dysentry_above6_frequency;

			       $sum_jaundice_0to5_male +=$value->healthData->jaundice_0to5_male;
			       $sum_jaundice_0to5_female +=$value->healthData->jaundice_0to5_female;
			       $sum_jaundice_0to5_total +=$value->healthData->jaundice_0to5_total;
			       $sum_jaundice_0to5_frequency +=$value->healthData->jaundice_0to5_frequency;

			       $sum_jaundice_above6_male +=$value->healthData->jaundice_above6_male;
			       $sum_jaundice_above6_female +=$value->healthData->jaundice_above6_female;
			       $sum_jaundice_above6_total +=$value->healthData->jaundice_above6_total;
			       $sum_jaundice_above6_frequency +=$value->healthData->jaundice_above6_frequency;

			       $sum_colera_0to5_male +=$value->healthData->colera_0to5_male;
			       $sum_colera_0to5_female +=$value->healthData->colera_0to5_female;
			       $sum_colera_0to5_total +=$value->healthData->colera_0to5_total;
			       $sum_colera_0to5_frequency +=$value->healthData->colera_0to5_frequency;

			       $sum_colera_above6_male +=$value->healthData->colera_above6_male;
			       $sum_colera_above6_female +=$value->healthData->colera_above6_female;
			       $sum_colera_above6_total +=$value->healthData->colera_above6_total;
			       $sum_colera_above6_frequency +=$value->healthData->colera_above6_frequency;

			       $sum_worm_infection_0to5_male +=$value->healthData->worm_infection_0to5_male;
			       $sum_worm_infection_0to5_female +=$value->healthData->worm_infection_0to5_female;
			       $sum_worm_infection_0to5_total +=$value->healthData->worm_infection_0to5_total;
			       $sum_worm_infection_0to5_frequency +=$value->healthData->worm_infection_0to5_frequency;

			       $sum_worm_infection_above6_male +=$value->healthData->worm_infection_above6_male;
			       $sum_worm_infection_above6_female +=$value->healthData->worm_infection_above6_female;
			       $sum_worm_infection_above6_total +=$value->healthData->worm_infection_above6_total;
			       $sum_worm_infection_above6_frequency +=$value->healthData->worm_infection_above6_frequency;
			
			       $sum_scabies_0to5_male +=$value->healthData->scabies_0to5_male;
			       $sum_scabies_0to5_female +=$value->healthData->scabies_0to5_female;
			       $sum_scabies_0to5_total +=$value->healthData->scabies_0to5_total;
			       $sum_scabies_0to5_frequency +=$value->healthData->scabies_0to5_frequency;

			       $sum_scabies_above6_male +=$value->healthData->scabies_above6_male;
			       $sum_scabies_above6_female +=$value->healthData->scabies_above6_female;  
			       $sum_scabies_above6_total +=$value->healthData->scabies_above6_total;
			       $sum_scabies_above6_frequency +=$value->healthData->scabies_above6_frequency;

			       $sum_sickness_absence_school +=$value->healthData->sickness_absence_school;
			       $sum_max_sick_days +=$value->healthData->max_sick_days;

			  
			    //   $sum_annual_expenses +=$value->healthBehavior->annual_expenses;	
@endphp

		@endforeach

		<tr><td colspan="3">total</td>
			<td> {{ $sum_total_member  }} </td>
			<td> {{ $sum_diarrhoea_0to5_male  }} </td>
			<td> {{ $sum_diarrhoea_0to5_female  }} </td>
			<td> {{ $sum_diarrhoea_0to5_total  }} </td>
			<td> {{ $sum_diarrhoea_0to5_frequency  }} </td>

			<td> {{ $sum_diarrhoea_above6_male  }} </td>
			<td> {{ $sum_diarrhoea_above6_female  }} </td>
			<td> {{ $sum_diarrhoea_above6_total  }} </td>
			<td> {{ $sum_diarrhoea_above6_frequency  }} </td>

			<td> {{ $sum_dysentry_0to5_male  }} </td>
			<td> {{ $sum_dysentry_0to5_female  }} </td>
			<td> {{ $sum_dysentry_0to5_total  }} </td>
			<td> {{ $sum_dysentry_0to5_frequency  }} </td>

			<td> {{ $sum_dysentry_above6_male  }} </td>
			<td> {{ $sum_dysentry_above6_female  }} </td>
			<td> {{ $sum_dysentry_above6_total  }} </td>
			<td> {{ $sum_dysentry_above6_frequency  }} </td>

			<td> {{ $sum_jaundice_0to5_male  }} </td>
			<td> {{ $sum_jaundice_0to5_female  }} </td>
			<td> {{ $sum_jaundice_0to5_total  }} </td>
			<td> {{ $sum_jaundice_0to5_frequency  }} </td>

			<td> {{ $sum_jaundice_above6_male  }} </td>
			<td> {{ $sum_jaundice_above6_female  }} </td>
			<td> {{ $sum_jaundice_above6_total  }} </td>
			<td> {{ $sum_jaundice_above6_frequency  }} </td>

			<td> {{ $sum_colera_0to5_male  }} </td>
			<td> {{ $sum_colera_0to5_female  }} </td>
			<td> {{ $sum_colera_0to5_total  }} </td>
			<td> {{ $sum_colera_0to5_frequency  }} </td>

			<td> {{ $sum_colera_above6_male  }} </td>
			<td> {{ $sum_colera_above6_female  }} </td>
			<td> {{ $sum_colera_above6_total  }} </td>
			<td> {{ $sum_colera_above6_frequency  }} </td>

			<td> {{ $sum_worm_infection_0to5_male  }} </td>
			<td> {{ $sum_worm_infection_0to5_female  }} </td>
			<td> {{ $sum_worm_infection_0to5_total  }} </td>
			<td> {{ $sum_worm_infection_0to5_frequency  }} </td>

			<td> {{ $sum_worm_infection_above6_male  }} </td>
			<td> {{ $sum_worm_infection_above6_female  }} </td>
			<td> {{ $sum_worm_infection_above6_total  }} </td>
			<td> {{ $sum_worm_infection_above6_frequency  }} </td>
	 
			<td> {{ $sum_scabies_0to5_male  }} </td>
			<td> {{ $sum_scabies_0to5_female  }} </td>
			<td> {{ $sum_scabies_0to5_total  }} </td>
			<td> {{ $sum_scabies_0to5_frequency  }} </td>

			<td> {{ $sum_scabies_above6_male  }} </td> 
			<td> {{ $sum_scabies_above6_female  }} </td> 
			<td> {{ $sum_scabies_above6_total  }} </td>  
			<td> {{ $sum_scabies_above6_frequency  }} </td> 

			<td> {{ $sum_sickness_absence_school  }} </td>
			<td> {{ $sum_max_sick_days  }} </td>
	   
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			
			   <td> {{ $sum_annual_expenses  }} </td></tr>
	</tbody>
</table>
