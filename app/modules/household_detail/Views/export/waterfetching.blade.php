@php 
$datas=$data[0];

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>House Detail</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="21">HouseDetail  Report</td>
			</tr>
			<tr>
				<td>Project Name</td>
				<td>{{  @$datas->projectname }}</td>
			</tr>
			<tr>
				<td>Project Number</td>
				<td>{{  @$datas->project_no }}</td>
			</tr>
			<tr>
				<td>Provience</td>
				<td>{{  @$datas->province->name }}</td>
			</tr>
			<tr>
				<td>District</td>
				<td>{{ @$datas->district->name }}</td>
			</tr>
			<tr>
				<td>Gaupalika/Nagarpalika</td>
				<td>{{ @$datas->vdc }}</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>House Number</td>
				<td>Family Head Name</td>
				<td>Family Head Gender</td>
				<td>Water Collection Pot in Liter</td>
				<td>Time Consumed In Going *(Seconds)</td>
				<td>Time Consumed In Waiting *(Seconds)</td>
				<td>Time Consumed In Filling *(Seconds)</td>
				<td>Time Consumed In Return *(Seconds)</td>
				<td>Total Time Consumed *(Seconds)</td>
				<td>Total Time Consumed in 8 Round*(Seconds)</td>
				<td>Total Time Consumed in 8 Round*(Minutes)</td>
				<td>Total Time Consumed in 8 Round*(Hour)</td>
				
			</tr>

			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->house_no}}</td>
				<td>{{$value->head_name}}</td>
				<td>{{$value->head_gender}}</td>
				<td>{{$value->water_fetch[0]->water_collection_liter}}</td>
				<td>{{$value->water_fetch[0]->going_second}}</td>
				<td>{{$value->water_fetch[0]->waiting_second}}</td>
				<td>{{$value->water_fetch[0]->filling_second}}</td>
				<td>{{$value->water_fetch[0]->return_second}}</td>
				<td>{{$value->water_fetch[0]->total_second}}</td>
				<td>{{$value->water_fetch[0]->total_second_8round}}</td>
				<td>{{$value->water_fetch[0]->total_minute_8round}}</td>
				<td>{{$value->water_fetch[0]->total_hour_8round}}</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
</body>
</html>