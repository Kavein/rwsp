@php 
$datas=$data[0];
//  dd($data);
@endphp
<table border="1">
	<tbody>
		<tr>
			<td>Project Name</td>
			<td>{{  @$datas->projectname }}</td>
		</tr>
		<tr>
			<td>Project Number</td>
			<td>{{  @$datas->project_no }}</td>
		</tr>
		<tr>
			<td>Provience</td>
			<td>{{  @$datas->province->name }}</td>
		</tr>
		<tr>
			<td>District Name</td>
			<td>{{ @$datas->district->name }}</td>
		</tr>
		<tr>
			<td>Gaupalika/Nagarpalika</td>
			<td>{{ @$datas->vdc}}</td>
		</tr>
		<tr>
			<td>Ward No</td>
			<td>{{ @$datas->province->ward_no }}</td>
		</tr>
		
		<tr>
			<td colspan="3"></td>
			@if(@$datas->checkphase == "initial")
				<th colspan="8">Initial Phase Data</th>
			@elseif(@$datas->checkphase == "preparation")
				<th colspan="8">Preparation Phase Data</th>
			@elseif(@$datas->checkphase == "construction")
				<th colspan="8">Construction Phase Data</th>
			@endif
			
		</tr>
		<tr>
			<th>House Number</th>
			<th>Community Name</th>
			<th>Head Name</th>
			<th>House Surrounding Cleanliness</th>		
			<th>Domestic Animal Cleanliness Management</th>		
			<th>Drinking Water Jar Cleanliness</th>		
			<th>Food Covered</th>		
			<th>Kitchen Utensil</th>		
			<th>Waste Management</th>		
			<th>Handwash Available</th>		
			<th>Latrine Cleanliness Available</th>	
		
		</tr>
		
		@foreach($data as $value)
		<tr>
			<td>{{$value->house_no}}</td>
			{{-- @php dd($community); @endphp --}}
			<td>{{$value->community->community_name}}</td>
			
			<td>{{$value->head_name}}</td>
			
			@if(@$value->hygiene->home_cleaning == 0)
			<td>Yes</td>
			@elseif(@$value->hygiene->home_cleaning == 1)
			<td>No</td>
			@endif
			

			@if(@$value->hygiene->domestic_animal == 0)
			<td>Not Managed</td>
			@elseif(@$value->hygiene->domestic_animal == 1)
			<td>Managed</td>
			@endif

			@if(@$value->hygiene->drinking_water == 0)
			<td>Not Managed</td>
			@elseif(@$value->hygiene->drinking_water == 1)
			<td>Managed</td>
			@endif

			@if(@$value->hygiene->food_covered == 0)
			<td>Not Managed</td>
			@elseif(@$value->hygiene->food_covered == 1)
			<td>Managed</td>
			@endif

			@if(@$value->hygiene->utensils_clean == 0)
			<td>Not Managed</td>
			@elseif(@$value->hygiene->utensils_clean == 1)
			<td>Managed</td>
			@endif

			@if(@$value->hygiene->garbage_disposel_pit == 0)
			<td>Not Managed</td>
			@elseif(@$value->hygiene->garbage_disposel_pit == 1)
			<td>Managed</td>
			@endif

			@if(@$value->hygiene->latrine_available == 0)
				<td>Not Available</td>
				<td>Not Available</td>
			@elseif(@$value->hygiene->latrine_available == 1)
				@if(@$value->hygiene->latrine_cleansing_agent == 0)
				<td>Not Available</td>
				@elseif(@$value->hygiene->latrine_cleansing_agent == 1)
				<td>Available</td>
				@endif
				@if(@$value->hygiene->latrine_cleaning_brush == 0)
				<td>Available</td>
				@elseif(@$value->hygiene->latrine_cleaning_brush == 1)
				<td>Available</td>
				@endif
			@endif
		</tr>
		@endforeach
	</tbody>
</table>
