@php 
$datas=$data[0];

@endphp
<!DOCTYPE html>
<html>
<head>
	<title>House Detail</title>
</head>
<body>
	<table border="1">
		<tbody>
			<tr>
				<td colspan="21">HouseDetail  Report</td>
			</tr>
			<tr>
				<td>Project Name</td>
				<td>{{  @$datas->projectname }}</td>
			</tr>
			<tr>
				<td>Project Number</td>
				<td>{{  @$datas->project_no }}</td>
			</tr>
			<tr>
				<td>Provience</td>
				<td>{{  @$datas->province->name }}</td>
			</tr>
			<tr>
				<td>District</td>
				<td>{{ @$datas->district->name }}</td>
			</tr>
			<tr>
				<td>Gaupalika/Nagarpalika</td>
				<td>{{ @$datas->vdc }}</td>
			</tr>
			<tr>
				<td>S.No</td>
				<td>House Number</td>
				<td>Family Head Name</td>
				<td>Family Head Gender</td>
				<td>No. of people accessing a safely managed water supply service to the premises (yard tap) at least 12 hrs/day</td>
				<td>No. of people from poor and marginalised groups with access to safely managed water supply service at least 8 hrs/day</td>
				<td>Uptake of financing mechanism to leverage household contributions for yard taps connections in X% project-targeted schemes</td>
				<td>No. of schools, clinics, and public institutions with access to safely managed water supply service within their grounds at least 12 hrs/day, specifically DRR designed for those selected as safe havens</td>
				<td>No. of project-supported schemes that deliver water within national water quality standards at point of collection</td>
				<td>Number of functionality rates of water supply schemes across supported wards</td>
				<td>Number of schemes across the wards achieving or exceeding service standards</td>
				<td>No. of communities actively competing in ward-level cleanliness competitions</td>
				<td>Number of households in target wards with handwashing facility with soap and water within 1–2 m of toilet</td>
				<td>Number of households in target wards with clean toilet meeting local cleanliness competitions criteria</td>
				<td>Number of FCHVs trained and equipped to deliver effective BCC activities</td>
				<td>Number of schools trained and equipped and running participatory BCC and outreach sessions</td>
				<td>Number of schools, clinics, and public institutions with access to safe sanitation and handwashing facilities, specifically DRR designed for those selected as safe havens</td>
				<td>Environmental sanitation indicator in terms of clean, tidy ,and green surroundings (e.g. criteria defined for cleanliness competitions)</td>
				<td>Number of households performing grey water separation to use in kitchen garden (improving livelihoods and nutrition) SDI</td>
				<td>Number of households in lowest wealth quintile in Palika with (affordable) using safe water, practicing hand washing with soap and safe sanitation.</td>
				<td>No. of wards declared for Total Sanitation</td>
				
			</tr>

			@foreach($data as $key => $value)
			<tr>
				<td>{{$key+1}}</td>
				<td>{{$value->house_no}}</td>
				<td>{{$value->head_name}}</td>
				<td>{{$value->head_gender}}</td>
				<td>{{$value->wash[0]->safety_management_12}}</td>
				<td>{{$value->wash[0]->safety_management_8}}</td>
				<td>{{$value->wash[0]->uptake}}</td>
				<td>{{$value->wash[0]->school_safety_12}}</td>
				<td>{{$value->wash[0]->project_scheme}}</td>
				<td>{{$value->wash[0]->supply_scheme}}</td>
				<td>{{$value->wash[0]->exceeding_service}}</td>
				<td>{{$value->wash[0]->cleanliness_competitions}}</td>
				<td>{{$value->wash[0]->handwashing_facility}}</td>
				<td>{{$value->wash[0]->clean_toilet}}</td>
				<td>{{$value->wash[0]->fchv_trained}}</td>
				<td>{{$value->wash[0]->school_trained}}</td>
				<td>{{$value->wash[0]->school_safe_haven}}</td>
				<td>{{$value->wash[0]->indicator_clean}}</td>
				<td>{{$value->wash[0]->grey_water}}</td>
				<td>{{$value->wash[0]->affordable_wealth}}</td>
				<td>{{$value->wash[0]->total_sanitation}}</td>
			</tr>
			@endforeach
			
		</tbody>
	</table>
</body>
</html>