@extends('admin.layout.main')
@section('content')
<table border="1">
	<thead>
		<td>House Number</td>
		<td>ProjectId</td>
		<td>Head Name</td>
	</thead>
	<tr>
		<td>{{$household_detail->house_no}}</td>
		<td>{{$household_detail->project_id}}</td>
		<td>{{$household_detail->head_name}}</td>
	</tr>
</table>
@endsection