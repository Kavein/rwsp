<?php



Route::group(array('prefix'=>'admin/projects','module'=>'Household_detail','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_details/{id}','AdminHousehold_detailController@index')->name('admin.household_details');

    Route::post('household_details/gethousehold_detailsJson/{id}','AdminHousehold_detailController@gethousehold_detailsJson')->name('admin.household_details.getdatajson');

    Route::get('household_details/create/{id}','AdminHousehold_detailController@create')->name('admin.household_details.create');
    Route::post('household_details/store','AdminHousehold_detailController@store')->name('admin.household_details.store');
    Route::get('household_details/show/{id}','AdminHousehold_detailController@show')->name('admin.household_details.show');
    Route::get('household_details/edit/{id}','AdminHousehold_detailController@edit')->name('admin.household_details.edit');
    Route::match(['put', 'patch'], 'household_details/update/{id}','AdminHousehold_detailController@update')->name('admin.household_details.update');
    Route::get('household_details/delete/{id}', 'AdminHousehold_detailController@destroy')->name('admin.household_details.delete');
    Route::post('household_details/delete_selected', 'AdminHousehold_detailController@selectedDelete')->name('admin.household_details.delete_selected');

    Route::get('household_details/first_approve/{id}', 'AdminHousehold_detailController@firstApprove')->name('admin.household_details.firstapprove');
    Route::get('household_details/second_approve/{id}', 'AdminHousehold_detailController@secondApprove')->name('admin.household_details.secondapprove');

    Route::get('household_details/hygiene/export/{id}','AdminHousehold_detailController@exportHygieneData')->name('admin.hygiene.export');
    Route::get('household_details/healthdata/export/{id}','AdminHousehold_detailController@exportHealthData')->name('admin.healthData.export');
    Route::get('household_details/handwash/export/{id}','AdminHousehold_detailController@exportHandWash')->name('admin.handwash.export');
    Route::get('household_details/socialEconomic/export/{id}','AdminHousehold_detailController@exportSocialEconomic')->name('admin.socialEconomic.export');
    Route::get('household_details/house_detail/export/{id}','AdminHousehold_detailController@exporthousedetail')->name('admin.housedetail.export');
    Route::get('household_details/agegroup/export/{id}','AdminHousehold_detailController@exportageGroup')->name('admin.agegroup.export');
    Route::get('household_details/wash/export/{id}','AdminHousehold_detailController@exportwash')->name('admin.wash.export');
    Route::get('household_details/waterfetch/export/{id}','AdminHousehold_detailController@exportwaterfetch')->name('admin.waterfetch.export');
    Route::get('household_details/healthbehaviour/export/{id}','AdminHousehold_detailController@exporthealthBehaviour')->name('admin.healthbehaviour.export');
});




Route::group(array('module'=>'Household_detail','namespace' => 'App\Modules\Household_detail\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_details/','Household_detailController@index')->name('household_details');
    
});