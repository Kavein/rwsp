<?php

namespace App\Modules\Household_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Excel;
use DB;
use Illuminate\Http\Request;
use App\Exports\HouseHoldExport;
use App\Modules\Cast\Model\Cast;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Occupation\Model\Occupation;
use App\Modules\Cast_divison\Model\Cast_divison;
use App\Modules\Income_source\Model\Income_source;
use App\Modules\Household_detail\Model\Household_detail;
use App\Modules\Prep_household_detail\Model\Prep_household_detail;
use App\Model\AgeGroup;
use App\Model\WaterFetch;
use App\Model\Hygiene;
use App\Model\HealthData;
use App\Model\HealthBehaviour;
use App\Model\InclusionData;
//Model of Preparation Phase
use App\Model\PrepAgeGroup;
use App\Model\PrepWaterFetch;
use App\Model\PrepHygiene;
use App\Model\PrepHealthData;
use App\Model\PrepHealthBehaviour;
use App\Model\PrepInclusionData;

//Model of Construction Phase
use App\Modules\Household_const\Model\Household_const;
use App\Model\ConsAgeGroup;
use App\Model\ConsWaterFetch;
use App\Model\ConsHygiene;
use App\Model\ConsHealthData;
use App\Model\ConsHealthBehaviour;
use App\Model\ConsInclusionData;

use App\Modules\Project\Model\Project;

class AdminHousehold_detailController extends Controller
{   
    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $page['title'] = 'Household_detail';
        return view("household_detail::index",compact('page','id'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function gethousehold_detailsJson(Request $request,$id)
    {
        
        $household_detail = new Household_detail;
        $where = $this->_get_search_param($request);

        $roleID = DB::table('role_user')->where('user_id', Auth::user()->id)->pluck('role_id')->first();      
       
        // For pagination
        $filterTotal = $household_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where( function($query) use ($roleID) {
            if($roleID !== null) {
                if($roleID == 1){
                    $query->where('first_approve_status', 1);
                }
            }
        } )->where('del_flag',0)->where('project_id',$id)->get();

        // Display limited list        
        $rows = $household_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->where( function($query) use ($roleID) {
            if($roleID !== null) {
                if($roleID == 1){
                    $query->where('first_approve_status', 1);
                }
            }
        } )->limit($request->length)->offset($request->start)->with('projects')->where('del_flag',0)->where('project_id',$id)->get();

        //To count the total values present
        $total = $household_detail->where('del_flag',0)->where('project_id',$id)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $page['title'] = 'Household_detail | Create';
        $project = Project::where('id',$id)->first();
        // dd($project->fiscal_year);
        $cast_divisions = Cast_divison::all();
        $casts = Cast::all();
        $occupations = Occupation::all();
        $income_sources = Income_source::all();
        return view("household_detail::store.initial_phase",compact('page','id','casts','occupations','income_sources','cast_divisions','project'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token','age','water_fetch','hygiene','incidence','behavior','inclusion','wash');
        $occupations = $request->occupation_id;
        $incomes = $request->income;
        $healthData = $request->incidence;
        $behavior = $request->behavior;
        $inclusion = $request->inclusion;
        $wash = $request->wash;


        $healthData['fiscal_year_id'] = $data['fiscal_year_id'];
        $healthData['project_id'] = $data['project_id']; 

        $behavior['fiscal_year_id'] = $data['fiscal_year_id'];
        $behavior['project_id'] = $data['project_id'];

        $inclusion['fiscal_year_id'] = $data['fiscal_year_id'];
        $inclusion['project_id'] = $data['project_id'];

        $user = Auth::user()->id;
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        $data['total_member'] = $data['total_male']+$data['total_female'];
        $data['del_flag'] = 0;
        $data['income_pension'] = checkValue($request->income_pension);

        $data['status'] = checkValue($request->status);
        $data['religion_minority'] = checkValue($request->religion_minority);

        //water fetching variable part
        $water_fetch = $request->water_fetch;
        $water_fetch['fiscal_year_id'] = $data['fiscal_year_id'];
        $water_fetch['project_id'] = $data['project_id'];
        $water_fetch['total_second'] = $water_fetch['going_second'] + $water_fetch['waiting_second'] + $water_fetch['filling_second'] + $water_fetch['return_second'];
        $water_fetch['total_second_8round'] = $water_fetch['total_second'] * 8; 
        $water_fetch['total_minute_8round'] = $water_fetch['total_second_8round']/60; 
        $water_fetch['total_hour_8round'] = $water_fetch['total_minute_8round']/60; 

        //AgeGroup
        $age = $request->age;
        $age['fiscal_year_id'] = $data['fiscal_year_id'];
        $age['project_id'] = $data['project_id'];
        //Total male Population In Age Group
        $zeroTo5_male = $request->age['between_0to5_male']; 
        $sixTo15_male = $request->age['between_6to15_male']; 
        $sixteenTo65_male = $request->age['between_16to65_male']; 
        $above_65_male = $request->age['above65_male']; 
        
        //Total Female Population In Age Group
        $zeroTo5_female = $request->age['between_0to5_female']; 
        $sixTo15_female = $request->age['between_6to15_female']; 
        $sixteenTo65_female = $request->age['between_16to65_female']; 
        $above_65_female = $request->age['above65_female']; 

        $total_male_in_age = $zeroTo5_male + $sixTo15_male + $sixteenTo65_male + $above_65_male;

        $total_female_in_age = $zeroTo5_female + $sixTo15_female + $sixteenTo65_female + $above_65_female;

        // Total According to Age Group
        $age['between_0to5_total'] = $request->age['between_0to5_male'] + $request->age['between_0to5_female']; 
        $age['between_6to15_total'] = $request->age['between_6to15_male'] + $request->age['between_6to15_female']; 
        $age['between_16to65_total'] = $request->age['between_16to65_male'] + $request->age['between_16to65_female']; 
        $age['above65_total'] = $request->age['above65_male'] + $request->age['above65_female']; 
        $age['disabled_total'] = $request->age['disabled_male'] + $request->age['disabled_female'];

        //Hygiene check
        $hygiene = $request->hygiene;
        $hygiene['fiscal_year_id'] = $data['fiscal_year_id'];
        $hygiene['project_id'] = $data['project_id'];
        //latring available
        if(isset($hygiene['latrine_available'])){
            $hygiene['latrine_available'] = 1;
            if(isset($hygiene['latrine_cleansing_agent'])){
                 $hygiene['latrine_cleansing_agent'] = 1;
            }
            else{
                $hygiene['latrine_cleansing_agent'] = 0;
            }
            if(isset($hygiene['latrine_water_available'])){
                 $hygiene['latrine_water_available'] = 1;
            }
            else{
                $hygiene['latrine_water_available'] = 0;
            }
            if(isset($hygiene['latrine_cleaning_brush'])){
                 $hygiene['latrine_cleaning_brush'] = 1;
            }
            else{
                $hygiene['latrine_cleaning_brush'] = 0;
            }
        }
        else{
            $hygiene['latrine_cost'] = null;
            $hygiene['total_latrine'] = null;
            $hygiene['latrine_available'] = 0;
            $hygiene['latrine_cleansing_agent'] = 0;
            $hygiene['latrine_water_available'] = 0;
            $hygiene['latrine_cleaning_brush'] = 0;
        }
        
        if(isset($hygiene['home_cleaning'])){
             $hygiene['home_cleaning'] = 1;
        }
        else{
            $hygiene['home_cleaning'] = 0;
        }
        if(isset($hygiene['domestic_animal'])){
             $hygiene['domestic_animal'] = 1;
        }
        else{
            $hygiene['domestic_animal'] = 0;
        }
        if(isset($hygiene['drinking_water'])){
             $hygiene['drinking_water'] = 1;
        }
        else{
            $hygiene['drinking_water'] = 0;
        }
        if(isset($hygiene['food_covered'])){
             $hygiene['food_covered'] = 1;
        }
        else{
            $hygiene['food_covered'] = 0;
        }
        if(isset($hygiene['utensils_clean'])){
             $hygiene['utensils_clean'] = 1;
        }
        else{
            $hygiene['utensils_clean'] = 0;
        }
        if(isset($hygiene['garbage_disposel_pit'])){
             $hygiene['garbage_disposel_pit'] = 1;
        }
        else{
            $hygiene['garbage_disposel_pit'] = 0;
        }

        $healthData['diarrhoea_0to5_total'] = $healthData['diarrhoea_0to5_male'] + $healthData['diarrhoea_0to5_female'];
        $healthData['diarrhoea_above6_total'] = $healthData['diarrhoea_above6_male'] + $healthData['diarrhoea_above6_female'];
        $healthData['dysentry_0to5_total'] = $healthData['dysentry_0to5_male'] + $healthData['dysentry_0to5_female'];
        $healthData['dysentry_above6_total'] = $healthData['dysentry_above6_male'] + $healthData['dysentry_above6_female'];
        $healthData['jaundice_0to5_total'] = $healthData['jaundice_0to5_male'] + $healthData['jaundice_0to5_female'];
        $healthData['jaundice_above6_total'] = $healthData['jaundice_above6_male'] + $healthData['jaundice_above6_female'];
        $healthData['colera_0to5_total'] = $healthData['colera_0to5_male'] + $healthData['colera_0to5_female'];
        $healthData['colera_above6_total'] = $healthData['colera_above6_male'] + $healthData['colera_above6_female'];
        $healthData['worm_infection_0to5_total'] = $healthData['worm_infection_0to5_male'] + $healthData['worm_infection_0to5_female'];
        $healthData['worm_infection_above6_total'] = $healthData['worm_infection_above6_male'] + $healthData['worm_infection_above6_female'];
        $healthData['scabies_0to5_total'] = $healthData['scabies_0to5_male'] + $healthData['scabies_0to5_female'];
        $healthData['scabies_above6_total'] = $healthData['scabies_above6_male'] + $healthData['scabies_above6_female'];
        // dd($healthData);

        //Inclusion Data Other Effects
        if($inclusion['effect_on_women'] != "incident_women" ){
            $inclusion['other_incident_women'] = null;
        }

        if($inclusion['effect_on_children'] != "others" ){
            $inclusion['other_effect_on_children'] = null;
        }
        //ENd of Inclusion Data
        // dd($age);
        // $data['first_approve_status'] = null;
        // $data['first_approved_by'] = null;
        // $data['second_approved_by'] = null;
        // $data['second_approve_status'] = null;

        if(($total_male_in_age == null && $total_female_in_age == null) || ($request->total_male == $total_male_in_age && $request->total_female == $total_female_in_age))
        {
            $success = Household_detail::Create($data);

            $data['initial_id'] = $success->id;
            $data['del_flag'] = 1;

            $prep_success = Prep_household_detail::Create($data);

            $data['prep_id'] = $prep_success->id;
            $cons_success = Household_const::Create($data);

            //Age Store IN Initial Phase
            $age['house_id'] = $success->id;
            AgeGroup::Create($age);
            //Age Store in Prep Phase
            $age['house_id'] = $prep_success->id;
            PrepAgeGroup::Create($age);
            //Age Store in Cons Phase
            $age['house_id'] = $cons_success->id;
            ConsAgeGroup::Create($age);
            //storing wash initial
            DB::table('hh_wash')->insert([
                    'project_id' => $data['project_id'],
                    'house_id' => $success->id,
                    'safety_management_12' => $wash['safety_management_12'],
                    'safety_management_8' => $wash['safety_management_8'],
                    'uptake' => $wash['uptake'],
                    'school_safety_12' => $wash['school_safety_12'],
                    'project_scheme' => $wash['project_scheme'],
                    'supply_scheme' => $wash['supply_scheme'],
                    'exceeding_service' => $wash['exceeding_service'],
                    'cleanliness_competitions' => $wash['cleanliness_competitions'],
                    'handwashing_facility' => $wash['handwashing_facility'],
                    'clean_toilet' => $wash['clean_toilet'],
                    'fchv_trained' => $wash['fchv_trained'],
                    'school_trained' => $wash['school_trained'],
                    'school_safe_haven' => $wash['school_safe_haven'],
                    'indicator_clean' => $wash['indicator_clean'],
                    'grey_water' => $wash['grey_water'],
                    'affordable_wealth' => $wash['affordable_wealth'],
                    'total_sanitation' => $wash['total_sanitation'],
                ]);
                //storing wash prep
                DB::table('prep_hh_wash')->insert([
                        'project_id' => $data['project_id'],
                        'house_id' => $prep_success->id,
                        'safety_management_12' => $wash['safety_management_12'],
                        'safety_management_8' => $wash['safety_management_8'],
                        'uptake' => $wash['uptake'],
                        'school_safety_12' => $wash['school_safety_12'],
                        'project_scheme' => $wash['project_scheme'],
                        'supply_scheme' => $wash['supply_scheme'],
                        'exceeding_service' => $wash['exceeding_service'],
                        'cleanliness_competitions' => $wash['cleanliness_competitions'],
                        'handwashing_facility' => $wash['handwashing_facility'],
                        'clean_toilet' => $wash['clean_toilet'],
                        'fchv_trained' => $wash['fchv_trained'],
                        'school_trained' => $wash['school_trained'],
                        'school_safe_haven' => $wash['school_safe_haven'],
                        'indicator_clean' => $wash['indicator_clean'],
                        'grey_water' => $wash['grey_water'],
                        'affordable_wealth' => $wash['affordable_wealth'],
                        'total_sanitation' => $wash['total_sanitation'],
                    ]);
                //storing wash cons
                DB::table('cons_hh_wash')->insert([
                        'project_id' => $data['project_id'],
                        'house_id' => $cons_success->id,
                        'safety_management_12' => $wash['safety_management_12'],
                        'safety_management_8' => $wash['safety_management_8'],
                        'uptake' => $wash['uptake'],
                        'school_safety_12' => $wash['school_safety_12'],
                        'project_scheme' => $wash['project_scheme'],
                        'supply_scheme' => $wash['supply_scheme'],
                        'exceeding_service' => $wash['exceeding_service'],
                        'cleanliness_competitions' => $wash['cleanliness_competitions'],
                        'handwashing_facility' => $wash['handwashing_facility'],
                        'clean_toilet' => $wash['clean_toilet'],
                        'fchv_trained' => $wash['fchv_trained'],
                        'school_trained' => $wash['school_trained'],
                        'school_safe_haven' => $wash['school_safe_haven'],
                        'indicator_clean' => $wash['indicator_clean'],
                        'grey_water' => $wash['grey_water'],
                        'affordable_wealth' => $wash['affordable_wealth'],
                        'total_sanitation' => $wash['total_sanitation'],
                    ]);
            //sending to store occuption function
            $this->storeOccupation($occupations,$success->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function
            $this->storeIncome($incomes,$success->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);

            //sending to store occuption function Prep
            $this->storeOccupationPrep($occupations,$prep_success->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function Prep
            $this->storeIncomePrep($incomes,$prep_success->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);

            //sending to store occuption function COns
            $this->storeOccupationCons($occupations,$cons_success->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function Cons
            $this->storeIncomeCons($incomes,$cons_success->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);

            //Storing WaterFetch In Initial Phase
            $water_fetch['house_id'] = $success->id;
            WaterFetch::Create($water_fetch);

            //Storing WaterFetch In Prep Phase
            $water_fetch['house_id'] = $prep_success->id;
            PrepWaterFetch::Create($water_fetch);

            //Storing WaterFetch In Cons Phase
            $water_fetch['house_id'] = $cons_success->id;
            ConsWaterFetch::Create($water_fetch);

            //Storing Hygiene in Initial Phase
            $hygiene['house_id'] = $success->id;
            Hygiene::Create($hygiene);

            //Storing Hygiene in Prep Phase
            $hygiene['house_id'] = $prep_success->id;
            PrepHygiene::Create($hygiene);

            //Storing Hygiene in Cons Phase
            $hygiene['house_id'] = $cons_success->id;
            ConsHygiene::Create($hygiene);

            //Storing Health Data in Initial Phase
            $healthData['house_id'] = $success->id;
            HealthData::Create($healthData);

            //Storing Health Data in Prep Phase
            $healthData['house_id'] = $prep_success->id;
            PrepHealthData::Create($healthData);

            //Storing Health Data in Cons Phase
            $healthData['house_id'] = $cons_success->id;
            ConsHealthData::Create($healthData);

            //Storing Health Behaviour in Initial Phase
            $behavior['house_id'] = $success->id;
            HealthBehaviour::Create($behavior);

            //Storing Health Behaviour in Prep Phase
            $behavior['house_id'] = $prep_success->id;
            PrepHealthBehaviour::Create($behavior);

            //Storing Health Behaviour in Cons Phase
            $behavior['house_id'] = $cons_success->id;
            ConsHealthBehaviour::Create($behavior);

            //Storing Inclusion Data in Initial Phase
            $inclusion['house_id'] = $success->id;
            InclusionData::Create($inclusion);

            //Storing Inclusion Data in Prep Phase
            $inclusion['house_id'] = $prep_success->id;
            PrepInclusionData::Create($inclusion);

            //Storing Inclusion Data in Cons Phase
            $inclusion['house_id'] = $cons_success->id;
            ConsInclusionData::Create($inclusion);

            return redirect()->route('admin.household_details.create',$data['project_id'])->with('success','HouseHold Detail Have Been Inserted SuccessFully !!');
        }
        
        else
        {
            return redirect()->back()->with('error','Total number of family not matched please check age fields !');
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $page['title'] = 'HouseHold | Info';
        $household_detail = Household_detail::findOrFail($id);
        
        // dd($provinces->name);
        // dd($project);
         return view("project::household_edit.household_detail",compact('page','household_detail'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $household = Household_detail::findOrFail($id);
        $project = Project::where('id',$household->project_id)->first();
        $cast_divisions = Cast_divison::where('del_flag',0)->get();
        $casts = Cast::where('cast_divison_id',$household->cast_division)->get();
        $occupations = Occupation::all();
        $income_sources = Income_source::all();
        $hh_incomes = DB::table('hh_income_pensions')->where('house_id',$household->id)->get()->toArray();

        $wash = DB::table('hh_wash')->where('house_id',$id)->first();

        foreach ($hh_incomes as $key => $value) {
            $hh_income_obj[] = $value->income_pension;
        }
        $hh_occupation = DB::table('hh_occupations')->where('house_id',$household->id)->get()->toArray();
        foreach ($hh_occupation as $key => $value) {
            $hh_occupation_obj[] = $value->occupation_id;
        }
        $age_groups = AgeGroup::where('house_id',$id)->first();
        $hygienes = Hygiene::where('house_id',$id)->first();
        $water_fetcings = WaterFetch::where('house_id',$id)->first();
        $healthdatas = HealthData::where('house_id',$id)->first();
        $healthbehaviors = HealthBehaviour::where('house_id',$id)->first();
        $inclusiondatas = InclusionData::where('house_id',$id)->first();
        $page['title'] = 'Household_detail | Update';
        return view("household_detail::edit.edit",compact('page','casts','occupations','household','age_groups','hh_occupation_obj','hygienes','healthdatas','healthbehaviors','inclusiondatas','income_sources','hh_income_obj','water_fetcings','cast_divisions','project','wash'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if (isset($request->wash_id)) {
            $data = $request->except('_token','age','water_fetch','hygiene','incidence','behavior','inclusion','_method','occupation_id','income','wash','wash_id');
        }
        else{
            $data = $request->except('_token','age','water_fetch','hygiene','incidence','behavior','inclusion','_method','occupation_id','income','wash');
        }
        $wash = $request->wash;


        // dd($data['project_id']);
        $occupations = $request->occupation_id;
        $incomes = $request->income;
        $healthData = $request->incidence;
        $behavior = $request->behavior;
        $inclusion = $request->inclusion;
        
        $user = Auth::user()->id;
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        $data['total_member'] = $data['total_male']+$data['total_female'];
        $data['del_flag'] = 0;
        $data['income_pension'] = checkValue($request->income_pension);
        $data['status'] = checkValue($request->status);
        $data['religion_minority'] = checkValue($request->religion_minority);

        //water fetching variable part
        $water_fetch = $request->water_fetch;
        $water_fetch['total_second'] = $water_fetch['going_second'] + $water_fetch['waiting_second'] + $water_fetch['filling_second'] + $water_fetch['return_second'];
        $water_fetch['total_second_8round'] = $water_fetch['total_second'] * 8; 
        $water_fetch['total_minute_8round'] = $water_fetch['total_second_8round']/60; 
        $water_fetch['total_hour_8round'] = $water_fetch['total_minute_8round']/60; 
        //AgeGroup
        $age = $request->age;
        //Total male Population In Age Group
        $zeroTo5_male = $request->age['between_0to5_male']; 
        $sixTo15_male = $request->age['between_6to15_male']; 
        $sixteenTo65_male = $request->age['between_16to65_male']; 
        $above_65_male = $request->age['above65_male']; 
        
        //Total Female Population In Age Group
        $zeroTo5_female = $request->age['between_0to5_female']; 
        $sixTo15_female = $request->age['between_6to15_female']; 
        $sixteenTo65_female = $request->age['between_16to65_female']; 
        $above_65_female = $request->age['above65_female']; 

        $total_male_in_age = $zeroTo5_male + $sixTo15_male + $sixteenTo65_male + $above_65_male;

        $total_female_in_age = $zeroTo5_female + $sixTo15_female + $sixteenTo65_female + $above_65_female;

        // Total According to Age Group
        $age['between_0to5_total'] = $request->age['between_0to5_male'] + $request->age['between_0to5_female']; 
        $age['between_6to15_total'] = $request->age['between_6to15_male'] + $request->age['between_6to15_female']; 
        $age['between_16to65_total'] = $request->age['between_16to65_male'] + $request->age['between_16to65_female']; 
        $age['above65_total'] = $request->age['above65_male'] + $request->age['above65_female']; 
        $age['disabled_total'] = $request->age['disabled_male'] + $request->age['disabled_female'];

        //Hygiene check
        $hygiene = $request->hygiene;
        //latring available
        if(isset($hygiene['latrine_available'])){
            $hygiene['latrine_available'] = 1;
            if(isset($hygiene['latrine_cleansing_agent'])){
                 $hygiene['latrine_cleansing_agent'] = 1;
            }
            else{
                $hygiene['latrine_cleansing_agent'] = 0;
            }
            if(isset($hygiene['latrine_water_available'])){
                 $hygiene['latrine_water_available'] = 1;
            }
            else{
                $hygiene['latrine_water_available'] = 0;
            }
            if(isset($hygiene['latrine_cleaning_brush'])){
                 $hygiene['latrine_cleaning_brush'] = 1;
            }
            else{
                $hygiene['latrine_cleaning_brush'] = 0;
            }
        }
        else{
            $hygiene['latrine_cost'] = null;
            $hygiene['total_latrine'] = null;
            $hygiene['latrine_available'] = 0;
            $hygiene['latrine_cleansing_agent'] = 0;
            $hygiene['latrine_water_available'] = 0;
            $hygiene['latrine_cleaning_brush'] = 0;
        }
        
        if(isset($hygiene['home_cleaning'])){
             $hygiene['home_cleaning'] = 1;
        }
        else{
            $hygiene['home_cleaning'] = 0;
        }
        if(isset($hygiene['domestic_animal'])){
             $hygiene['domestic_animal'] = 1;
        }
        else{
            $hygiene['domestic_animal'] = 0;
        }
        if(isset($hygiene['drinking_water'])){
             $hygiene['drinking_water'] = 1;
        }
        else{
            $hygiene['drinking_water'] = 0;
        }
        if(isset($hygiene['food_covered'])){
             $hygiene['food_covered'] = 1;
        }
        else{
            $hygiene['food_covered'] = 0;
        }
        if(isset($hygiene['utensils_clean'])){
             $hygiene['utensils_clean'] = 1;
        }
        else{
            $hygiene['utensils_clean'] = 0;
        }
        if(isset($hygiene['garbage_disposel_pit'])){
             $hygiene['garbage_disposel_pit'] = 1;
        }
        else{
            $hygiene['garbage_disposel_pit'] = 0;
        }

        $healthData['diarrhoea_0to5_total'] = $healthData['diarrhoea_0to5_male'] + $healthData['diarrhoea_0to5_female'];
        $healthData['diarrhoea_above6_total'] = $healthData['diarrhoea_above6_male'] + $healthData['diarrhoea_above6_female'];
        $healthData['dysentry_0to5_total'] = $healthData['dysentry_0to5_male'] + $healthData['dysentry_0to5_female'];
        $healthData['dysentry_above6_total'] = $healthData['dysentry_above6_male'] + $healthData['dysentry_above6_female'];
        $healthData['jaundice_0to5_total'] = $healthData['jaundice_0to5_male'] + $healthData['jaundice_0to5_female'];
        $healthData['jaundice_above6_total'] = $healthData['jaundice_above6_male'] + $healthData['jaundice_above6_female'];
        $healthData['colera_0to5_total'] = $healthData['colera_0to5_male'] + $healthData['colera_0to5_female'];
        $healthData['colera_above6_total'] = $healthData['colera_above6_male'] + $healthData['colera_above6_female'];
        $healthData['worm_infection_0to5_total'] = $healthData['worm_infection_0to5_male'] + $healthData['worm_infection_0to5_female'];
        $healthData['worm_infection_above6_total'] = $healthData['worm_infection_above6_male'] + $healthData['worm_infection_above6_female'];
        $healthData['scabies_0to5_total'] = $healthData['scabies_0to5_male'] + $healthData['scabies_0to5_female'];
        $healthData['scabies_above6_total'] = $healthData['scabies_above6_male'] + $healthData['scabies_above6_female'];
        // dd($healthData);

        //Inclusion Data Other Effects
        if($inclusion['effect_on_women'] != "incident_women" ){
            $inclusion['other_incident_women'] = null;
        }

        if($inclusion['effect_on_children'] != "others" ){
            $inclusion['other_effect_on_children'] = null;
        }
        //ENd of Inclusion Data
        // dd($inclusion);

        if(($total_male_in_age == null && $total_female_in_age == null) || ($request->total_male == $total_male_in_age && $request->total_female == $total_female_in_age))
        {
            $success = Household_detail::where('id',$id)->where('del_flag',0)->update($data);

            unset($data['id']);

            $success_prep = Prep_household_detail::where('initial_id',$id)->where('del_flag',0)->update($data);
            $success_cons = Household_const::where('initial_id',$id)->update($data);
            $prep_data = Prep_household_detail::where('initial_id',$id)->first();
            $cons_data = Household_const::where('initial_id',$id)->first();

            //AgeGroup Update
            AgeGroup::where('house_id',$id)->update($age);
            //AgeGroup Update Prep
            PrepAgeGroup::where('house_id',$prep_data->id)->update($age);
            //AgeGroup Update Cons
            ConsAgeGroup::where('house_id',$cons_data->id)->update($age);
            //sending to store occuption function
            $this->storeOccupation($occupations,$id,$data['project_id'],$data['fiscal_year_id']);
            //sending to store occuption function Prep
            $this->storeOccupationPrep($occupations,$prep_data->id,$data['project_id'],$data['fiscal_year_id']);
            //sending to store occuption function Cons
            $this->storeOccupationCons($occupations,$cons_data->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function
            $this->storeIncome($incomes,$id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income functionPrep
            $this->storeIncomePrep($incomes,$prep_data->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income functionCons
            $this->storeIncomeCons($incomes,$cons_data->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);
            //Storing WaterFetch
            WaterFetch::where('house_id',$id)->update($water_fetch);
            //Storing WaterFetch Prep
            PrepWaterFetch::where('house_id',$prep_data->id)->update($water_fetch);
            //Storing WaterFetch Cons
            ConsWaterFetch::where('house_id',$cons_data->id)->update($water_fetch);

            //Storing Hygiene
            Hygiene::where('house_id',$id)->update($hygiene);
            //Storing Hygiene Prep
            PrepHygiene::where('house_id',$prep_data->id)->update($hygiene);
            //Storing Hygiene Cons
            ConsHygiene::where('house_id',$cons_data->id)->update($hygiene);

            //Storing Health Data
            HealthData::where('house_id',$id)->update($healthData);
            //Storing Health Data Prep
            PrepHealthData::where('house_id',$prep_data->id)->update($healthData);
            //Storing Health Data Cons
            ConsHealthData::where('house_id',$cons_data->id)->update($healthData);

            //Storing Health Behaviour
            HealthBehaviour::where('house_id',$id)->update($behavior);
            //Storing Health Behaviour Prep
            PrepHealthBehaviour::where('house_id',$prep_data->id)->update($behavior);
            //Storing Health Behaviour Cons
            ConsHealthBehaviour::where('house_id',$cons_data->id)->update($behavior);

            //Storing Inclusion Data
            InclusionData::where('house_id',$id)->update($inclusion);
            //Storing Inclusion Data Prep
            PrepInclusionData::where('house_id',$prep_data->id)->update($inclusion);
            //Storing Inclusion Data Cons
            ConsInclusionData::where('house_id',$cons_data->id)->update($inclusion);


            //Wash Initial
            // if(isset($request->wash_id)){
                DB::table('hh_wash')
                        ->where('id',$request->wash_id)
                        ->update([
                            "safety_management_12" => $wash['safety_management_12'],
                            "safety_management_8" => $wash['safety_management_8'],
                            "uptake" => $wash['uptake'],
                            "school_safety_12" => $wash['school_safety_12'],
                            "project_scheme" => $wash['project_scheme'],
                            "supply_scheme" => $wash['supply_scheme'],
                            "exceeding_service" => $wash['exceeding_service'],
                            "cleanliness_competitions" => $wash['cleanliness_competitions'],
                            "handwashing_facility" => $wash['handwashing_facility'],
                            "clean_toilet" => $wash['clean_toilet'],
                            "fchv_trained" => $wash['fchv_trained'],
                            "school_trained" => $wash['school_trained'],
                            "school_safe_haven" => $wash['school_safe_haven'],
                            "indicator_clean" => $wash['indicator_clean'],
                            "grey_water" => $wash['grey_water'],
                            "affordable_wealth" => $wash['affordable_wealth'],
                            "total_sanitation" => $wash['total_sanitation'],
                        ]);

                DB::table('prep_hh_wash')
                        ->leftjoin('tbl_prep_household_details','tbl_prep_household_details.id','=','prep_hh_wash.house_id')
                        ->where('tbl_prep_household_details.initial_id',$id)
                        ->update([
                            "safety_management_12" => $wash['safety_management_12'],
                            "safety_management_8" => $wash['safety_management_8'],
                            "uptake" => $wash['uptake'],
                            "school_safety_12" => $wash['school_safety_12'],
                            "project_scheme" => $wash['project_scheme'],
                            "supply_scheme" => $wash['supply_scheme'],
                            "exceeding_service" => $wash['exceeding_service'],
                            "cleanliness_competitions" => $wash['cleanliness_competitions'],
                            "handwashing_facility" => $wash['handwashing_facility'],
                            "clean_toilet" => $wash['clean_toilet'],
                            "fchv_trained" => $wash['fchv_trained'],
                            "school_trained" => $wash['school_trained'],
                            "school_safe_haven" => $wash['school_safe_haven'],
                            "indicator_clean" => $wash['indicator_clean'],
                            "grey_water" => $wash['grey_water'],
                            "affordable_wealth" => $wash['affordable_wealth'],
                            "total_sanitation" => $wash['total_sanitation'],
                        ]);

                DB::table('cons_hh_wash')
                        ->leftjoin('tbl_household_const','tbl_household_const.id','=','cons_hh_wash.house_id')
                        ->where('tbl_household_const.initial_id',$id)
                        ->update([
                            "safety_management_12" => $wash['safety_management_12'],
                            "safety_management_8" => $wash['safety_management_8'],
                            "uptake" => $wash['uptake'],
                            "school_safety_12" => $wash['school_safety_12'],
                            "project_scheme" => $wash['project_scheme'],
                            "supply_scheme" => $wash['supply_scheme'],
                            "exceeding_service" => $wash['exceeding_service'],
                            "cleanliness_competitions" => $wash['cleanliness_competitions'],
                            "handwashing_facility" => $wash['handwashing_facility'],
                            "clean_toilet" => $wash['clean_toilet'],
                            "fchv_trained" => $wash['fchv_trained'],
                            "school_trained" => $wash['school_trained'],
                            "school_safe_haven" => $wash['school_safe_haven'],
                            "indicator_clean" => $wash['indicator_clean'],
                            "grey_water" => $wash['grey_water'],
                            "affordable_wealth" => $wash['affordable_wealth'],
                            "total_sanitation" => $wash['total_sanitation'],
                        ]);
            return redirect()->route('admin.household_details',$data['project_id'])->with('success','HouseHold Detail Have Been Updated SuccessFully !!');
        }
        
        else
        {
            return redirect()->back()->with('error','Total number of family not matched please check age fields !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 1;
        $success = Household_detail::where('id', $id)->update($data);
        Prep_household_detail::where('initial_id',$id)->update($data);
        Household_const::where('initial_id',$id)->update($data);


        return redirect()->back();

        //
    }

    public function exportHygieneData($id)
    {   
        $data = Household_detail::where('project_id',$id)->where('del_flag',0)
                                ->get();
        // dd($data)
        foreach($data as &$value){
            // dd($value->community_name);
            $value->district=Project::where('del_flag',0)
            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
            ->where('tbl_projects.id',$id)
            ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();

            $value->hygiene = DB::table('hh_hygiene')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->checkphase = "initial";
        }
        // $community=DB::table('prj_community_infos')->where('project_id',$id)->first();
        if(isset($data[0]))
        {
            //  return "here";
            // return view ("household_detail::export.hygieneReport",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.hygieneReport",$data), 'hygiene.xlsx');
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        }
    }

    public function exportHealthData($id)
    {
        
        $data = Household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();
        // dd($data);
        foreach($data as &$value){
            // dd($value->community_name);
            $value->district=Project::where('del_flag',0)
            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
            ->where('tbl_projects.id',$id)
            ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
            ->where('tbl_projects.id',$value->id)
            ->first();
            $value->vdc =DB::table('mst_vdcs')
            ->where('district_id',$district_id)->pluck('name')->first();

            $value->healthData = DB::table('hh_health_datas')->where('house_id',$value->id)->first();
            $value->healthBehavior = DB::table('hh_health_behaviors')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();

        }
        if(isset($data[0]))
        {
            // return "here";
            // return view("household_detail::export.healthDataReport",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.healthDataReport",$data), 'healthData.xlsx');
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
        // dd($data);
        //  
        // dd($data[0]['id']);
    }

    public function exportHandWash($id)
    {
        $data = Household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();

        // dd($data);
        foreach($data as &$value){
            // dd($value->community_name);

            $value->district=Project::where('del_flag',0)
            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
            ->where('tbl_projects.id',$id)
            ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
            ->where('tbl_projects.id',$value->id)
            ->first();
            $value->vdc =DB::table('mst_vdcs')
            ->where('district_id',$district_id)->pluck('name')->first();

            $value->handWash = DB::table('hh_hygiene')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            

        }
        //dd($data);
        if(isset($data[0]))
        {
            // return "here";
         // return view("household_detail::export.handWash",compact('data'));
        return Excel::download(new HouseHoldExport("household_detail::export.handWash",$data), 'handWash.xlsx');
        // dd($data[0]['id']);
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
    }

    public function exportSocialEconomic($id){
        $data = Household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();

        foreach ($data as &$value) {


            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
            ->where('district_id',$district_id)->pluck('name')->first();


            $value->inclusionData = DB::table('hh_inclusion_datas')->where('house_id',$value->id)->first();
            $value->ageGroup = DB::table('hh_age_groups')->where('house_id',$value->id)->first();
            // $value->income_source = Income_source::get()->where('del_flag',0);
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->occupations = DB::table('hh_occupations')
                        ->join('mst_occupations','mst_occupations.id','=','hh_occupations.occupation_id')
                        ->where('house_id',$value->id)
                        ->get();
            $occarray = [];
            foreach ($value->occupations as $k => $v) {
                $occarray[] = $v->name;
            }
            // $array_occupation = [];
            // foreach ($occupations as $key => $occ) {
            //     $array_occupation[$occ->occupation_id] = $occ;
            //     $array_occupation[$occ->name] = $occ;
            // }
            // dd($occarray);
            $value->occupations = implode(',', $occarray);
            unset($occarray);

            $income_sources = DB::table('hh_income_pensions')->where('house_id',$value->id)->get();
            $array_new = [];
            foreach ($income_sources as $key => $v) {
                $array_new[$v->income_pension]  = $v;               
            }
            $value->income_sources = $array_new;
           
        }
        // dd($data);
            $data->income_sources = DB::table('mst_income_sources')->where('del_flag',0)->get();
        // dd($data);
         if(isset($data[0]))
        {
            // return "here";
            // return view("household_detail::export.socioEconomic",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.socioEconomic",$data), 'SocialEconomic Status.xlsx');
           
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
    }

    public function selectedDelete(Request $request)
    {
        $data = $request->except('_token');
        foreach ($data['id'] as $value) {
            $success = Household_detail::where('id', $value)->update(['del_flag' => 1 ]);
        }

        echo json_encode($success);
    }

    // Function For Store Occupation 
    public function storeOccupation($occupation,$id,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
            DB::table('hh_occupations')
                ->insert([
                    'occupation_id'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    //Function for store Income

    public function storeIncome($income,$id,$house_income,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        elseif(empty($incomes)){
            DB::table('hh_income_pensions')
                        ->insert([
                            'income_pension'=> null,
                            'house_id' => $id,
                            'project_id' => $projectid,
                            'fiscal_year_id' => $fiscalyear,
                        ]);
                }
        else{
            foreach ($incomes as $key => $value) {
            DB::table('hh_income_pensions')
                ->insert([
                    'income_pension'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    // Function For Store Occupation 
    public function storeOccupationPrep($occupation,$id,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('prep_hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('prep_hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
            DB::table('prep_hh_occupations')
                ->insert([
                    'occupation_id'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    //Function for store Income

    public function storeIncomePrep($income,$id,$house_income,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('prep_hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('prep_hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        elseif(empty($incomes)){
            DB::table('prep_hh_income_pensions')
                        ->insert([
                            'income_pension'=> null,
                            'house_id' => $id,
                            'project_id' => $projectid,
                            'fiscal_year_id' => $fiscalyear,
                        ]);
                }
        else{
            foreach ($incomes as $key => $value) {
            DB::table('prep_hh_income_pensions')
                ->insert([
                    'income_pension'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }

    //End Function

    // Function For Store Occupation 
    public function storeOccupationCons($occupation,$id,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('cons_hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('cons_hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
            DB::table('cons_hh_occupations')
                ->insert([
                    'occupation_id'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    //Function for store Income

    public function storeIncomeCons($income,$id,$house_income,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('cons_hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('cons_hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        elseif(empty($incomes)){
            DB::table('cons_hh_income_pensions')
                        ->insert([
                            'income_pension'=> null,
                            'house_id' => $id,
                            'project_id' => $projectid,
                            'fiscal_year_id' => $fiscalyear,
                        ]);
                }
        else{
            foreach ($incomes as $key => $value) {
            DB::table('cons_hh_income_pensions')
                ->insert([
                    'income_pension'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }

    //End Function

    public function firstApprove($id)
    {
        $user = Auth::user()->id;
        $get_status = Household_detail::find($id);
        
        if($get_status->first_approve_status == 1){
            $approve = 0;
        }
        else{
            $approve = 1;
        }
        $approved = DB::table('tbl_household_details')
                    ->where('id',$id)
                    ->update(['first_approved_by' => $user,'first_approve_status' => $approve]);
        return back();
    }

    public function secondApprove($id)
    {
        // dd($id);
        $user = Auth::user()->id;
        $get_status = Household_detail::find($id);
        
        if($get_status->second_approve_status == 1){
            $approve = 0;
        }
        else{
            $approve = 1;
        }
        $approved = DB::table('tbl_household_details')
                    ->where('id',$id)
                    ->update(['second_approved_by' => $user,'second_approve_status' => $approve]);
        if($approve == 1){
            DB::table('tbl_prep_household_details')->where('initial_id',$id)->update(['del_flag'=>0]);
        }else{
            DB::table('tbl_prep_household_details')->where('initial_id',$id)->update(['del_flag'=>1]);
        }
        return back();
    }

    public function exporthousedetail($id)
    {
        $data = Household_detail::where('project_id',$id)->where('del_flag',0)
                                ->get();
       foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->get_cast_division = DB::table('mst_cast_divisons')
                                    ->leftjoin('tbl_household_details','mst_cast_divisons.id','=','tbl_household_details.cast_division')
                                    ->where('mst_cast_divisons.id',$value->cast_division)
                                    ->pluck('name')->first();
            $value->get_cast = DB::table('mst_casts')
                                    ->leftjoin('tbl_household_details','mst_casts.id','=','tbl_household_details.cast_id')
                                    ->where('mst_casts.id',$value->cast_id)
                                    ->pluck('name')->first();

            //Occupation Data
            $hh_occupation = DB::table('hh_occupations')->where('house_id',$value->id)->get()->toArray();
            $hh_occupation_obj = array();
            foreach ($hh_occupation as $key => $val) {
                $hh_occupation_obj[] = $val->occupation_id;
            }
            $occip = DB::table('mst_occupations')->get()->toArray();
            unset($hh_occuos);
            foreach ($occip as $k => $v) { 
                if(in_array($v->id, $hh_occupation_obj)){
                    $hh_occuos[] =$v->name;
                }
            }
            $ociu = implode(',', $hh_occuos);
            //End Occupation

            //Income Source
            $hh_income = DB::table('hh_income_pensions')->where('house_id',$value->id)->get()->toArray();
            $hh_income_obj = array();
            foreach ($hh_income as $keys => $values) {
                // dd($values);
                $hh_income_obj[] = $values->income_pension;
            }
            $inco = DB::table('mst_income_sources')->get()->toArray();
            // dd($hh_income_obj);
            unset($hh_incos);
            foreach ($inco as $ke => $va) { 
                if(in_array($va->id, $hh_income_obj)){
                    $hh_incos[] =$va->name;
                }
            }
            $incoss = implode(',', $hh_incos);
            //End Income Source
            $value->occ = $ociu;
            $value->incos = $incoss;
        }
      
        // return view("household_detail::export.housedetail",compact('data'));
        return Excel::download(new HouseHoldExport("household_detail::export.housedetail",$data), 'Household Details.xlsx');
    }

    public function exportageGroup($id)
    {
        $data = Household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->age = DB::table('hh_age_groups')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.agegroup",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.agegroup",$data), 'Age Group Detail.xlsx');
    }

    public function exportwash($id)
    {
        $data = Household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->wash = DB::table('hh_wash')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.wash",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.wash",$data), 'Wash Detail.xlsx');
    }
    public function exportwaterfetch($id)
    {
        $data = Household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->water_fetch = DB::table('hh_water_fetching_time')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.waterfetching",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.waterfetching",$data), 'Water Fetching .xlsx');
    }

    public function exporthealthBehaviour($id)
    {
        $data = Household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->health = DB::table('hh_health_behaviors')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.HealthBehaviour",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.waterfetching",$data), 'Health Behaviour Detail.xlsx');
    }

}
