<?php

namespace App\Modules\Migration\Model;


use Illuminate\Database\Eloquent\Model;

class Migration extends Model
{
    public  $table = 'migrations';

    protected $fillable = ['id','migration','batch',];
}
