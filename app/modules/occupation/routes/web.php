<?php



Route::group(array('prefix'=>'admin/','module'=>'Occupation','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Occupation\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('occupations/','AdminOccupationController@index')->name('admin.occupations');
    Route::post('occupations/getoccupationsJson','AdminOccupationController@getoccupationsJson')->name('admin.occupations.getdatajson');
    Route::get('occupations/create','AdminOccupationController@create')->name('admin.occupations.create');
    Route::post('occupations/store','AdminOccupationController@store')->name('admin.occupations.store');
    Route::get('occupations/show/{id}','AdminOccupationController@show')->name('admin.occupations.show');
    Route::get('occupations/edit/{id}','AdminOccupationController@edit')->name('admin.occupations.edit');
    Route::match(['put', 'patch'], 'occupations/update/{id}','AdminOccupationController@update')->name('admin.occupations.update');
    Route::get('occupations/delete/{id}', 'AdminOccupationController@destroy')->name('admin.occupations.edit');
});




Route::group(array('module'=>'Occupation','namespace' => 'App\Modules\Occupation\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('occupations/','OccupationController@index')->name('occupations');
    
});