<?php

namespace App\Modules\Occupation\Model;


use Illuminate\Database\Eloquent\Model;

class Occupation extends Model
{
    public  $table = 'mst_occupations';

    protected $fillable = ['id','name','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
