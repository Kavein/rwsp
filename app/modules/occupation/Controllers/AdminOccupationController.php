<?php

namespace App\Modules\Occupation\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Occupation\Model\Occupation;

class AdminOccupationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Occupation';
        return view("occupation::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getoccupationsJson(Request $request)
    {
        $occupation = new Occupation;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $occupation->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $occupation->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $occupation->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Occupation | Create';
        return view("occupation::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required|unique:mst_occupations,name',
                        ],
                        [
                            'name.required' => 'Occupation Name Cannot Be Empty!!',
                        ]);
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        if($request->status == "on"){
            $data['status'] = 1;
        }
        elseif($request->status = "off"){
            $data['status'] = 0;
        }
        $data['del_flag'] = 0;
        $success = Occupation::Create($data);
        return redirect()->route('admin.occupations');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $occupation = Occupation::findOrFail($id);
        $page['title'] = 'Occupation | Update';
        return view("occupation::edit",compact('page','occupation'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $user = Auth::user()->id;
        $this->validate($request, [
                            'name' => 'required',
                        ],
                        [
                            'name.required' => 'Occupation Name Cannot Be Empty!!',
                        ]);
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        if($request->status){
           $data['status'] = 1; 
        }else{
            $data['status'] = 0;
        }
        
        $data['del_flag'] = 0;
        $success = Occupation::where('id', $id)->update($data);
        return redirect()->route('admin.occupations');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 0;
        $success = Occupation::where('id', $id)->update($data);
        return redirect()->route('admin.occupations');

        //
    }
}
