<?php

namespace App\Modules\Income_source\Model;


use Illuminate\Database\Eloquent\Model;

class Income_source extends Model
{
    public  $table = 'mst_income_sources';

    protected $fillable = ['id','name','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
