<?php



Route::group(array('prefix'=>'admin/','module'=>'Province','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Province\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('provinces/','AdminProvinceController@index')->name('admin.provinces');
    Route::post('provinces/getprovincesJson','AdminProvinceController@getprovincesJson')->name('admin.provinces.getdatajson');
    Route::get('provinces/create','AdminProvinceController@create')->name('admin.provinces.create');
    Route::post('provinces/store','AdminProvinceController@store')->name('admin.provinces.store');
    Route::get('provinces/show/{id}','AdminProvinceController@show')->name('admin.provinces.show');
    Route::get('provinces/edit/{id}','AdminProvinceController@edit')->name('admin.provinces.edit');
    Route::match(['put', 'patch'], 'provinces/update/{id}','AdminProvinceController@update')->name('admin.provinces.update');
    Route::get('provinces/delete/{id}', 'AdminProvinceController@destroy')->name('admin.provinces.edit');
});




Route::group(array('module'=>'Province','namespace' => 'App\Modules\Province\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('provinces/','ProvinceController@index')->name('provinces');
    
});