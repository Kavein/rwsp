<?php

namespace App\Modules\Prep_household_detail\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use Excel;
use App\Exports\HouseHoldExport;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Prep_household_detail\Model\Prep_household_detail;
use App\Modules\Cast\Model\Cast;
use App\Modules\Occupation\Model\Occupation;
use App\Modules\Income_source\Model\Income_source;
use App\Modules\Cast_divison\Model\Cast_divison;
use App\Modules\Household_detail\Model\Household_detail;
use App\Model\PrepAgeGroup;
use App\Model\PrepWaterFetch;
use App\Model\PrepHygiene;
use App\Model\PrepHealthData;
use App\Model\PrepHealthBehaviour;
use App\Model\PrepInclusionData;

//Model of Construction Phase
use App\Modules\Household_const\Model\Household_const;
use App\Model\ConsAgeGroup;
use App\Model\ConsWaterFetch;
use App\Model\ConsHygiene;
use App\Model\ConsHealthData;
use App\Model\ConsHealthBehaviour;
use App\Model\ConsInclusionData;

use App\Modules\Project\Model\Project;


class AdminPrep_household_detailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $page['title'] = 'Prep_household_detail';
        return view("prep_household_detail::index",compact('page','id'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getprep_household_detailsJson(Request $request,$id)
    {
        $prep_household_detail = new Prep_household_detail;
        $where = $this->_get_search_param($request);

        $roleID = DB::table('role_user')->where('user_id', Auth::user()->id)->pluck('role_id')->first();

        // For pagination
        $filterTotal = $prep_household_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where( function($query) use ($roleID) {
            if($roleID !== null) {
                if($roleID == 1){
                    $query->where('first_approve_status', 1);
                }
            }
        } )->where('del_flag',0)->where('project_id',$id)->get();

        // Display limited list        
        $rows = $prep_household_detail->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->where( function($query) use ($roleID) {
            if($roleID !== null) {
                if($roleID == 1){
                    $query->where('first_approve_status', 1);
                }
            }
        } )->limit($request->length)->offset($request->start)->with('projects')->where('del_flag',0)->where('project_id',$id)->get();

        //To count the total values present
        $total = $prep_household_detail->where('del_flag',0)->where('project_id',$id)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $project = Project::where('id',$id)->where('del_flag',0)->first();

        $page['title'] = 'HouseHold | Create';
        $cast_divisions = Cast_divison::all();
        $casts = Cast::all();
        $occupations = Occupation::all();
        $income_sources = Income_source::all();
        return view("prep_household_detail::preparation.prep_phase")->with(compact('page','id','casts','occupations','income_sources','cast_divisions','project'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token','age','water_fetch','hygiene','incidence','behavior','inclusion','wash');
        // dd($request->wash);
        $occupations = $request->occupation_id;
        $incomes = $request->income;
        $healthData = $request->incidence;
        $behavior = $request->behavior;
        $inclusion = $request->inclusion;
        $age = $request->age;
        $water_fetch = $request->water_fetch;
        $wash = $request->wash;

        $healthData['fiscal_year_id'] = $data['fiscal_year_id'];
        $healthData['project_id'] = $data['project_id'];

        $behavior['fiscal_year_id'] = $data['fiscal_year_id'];
        $behavior['project_id'] = $data['project_id'];

        $inclusion['fiscal_year_id'] = $data['fiscal_year_id'];
        $inclusion['project_id'] = $data['project_id'];

        $hygiene['fiscal_year_id'] = $data['fiscal_year_id'];
        $hygiene['project_id'] = $data['project_id'];

        $age['fiscal_year_id'] = $data['fiscal_year_id'];
        $age['project_id'] = $data['project_id'];

        $user = Auth::user()->id;
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        $data['total_member'] = $data['total_male']+$data['total_female'];
        $data['del_flag'] = 0;
        $data['income_pension'] = checkValue($request->income_pension);

        $data['status'] = checkValue($request->status);
        $data['religion_minority'] = checkValue($request->religion_minority);

        //water fetching variable part
        
        $water_fetch['total_second'] = $water_fetch['going_second'] + $water_fetch['waiting_second'] + $water_fetch['filling_second'] + $water_fetch['return_second'];
        $water_fetch['total_second_8round'] = $water_fetch['total_second'] * 8; 
        $water_fetch['total_minute_8round'] = $water_fetch['total_second_8round']/60; 
        $water_fetch['total_hour_8round'] = $water_fetch['total_minute_8round']/60; 

        //AgeGroup
        
        //Total male Population In Age Group
        $zeroTo5_male = $request->age['between_0to5_male']; 
        $sixTo15_male = $request->age['between_6to15_male']; 
        $sixteenTo65_male = $request->age['between_16to65_male']; 
        $above_65_male = $request->age['above65_male']; 
        
        //Total Female Population In Age Group
        $zeroTo5_female = $request->age['between_0to5_female']; 
        $sixTo15_female = $request->age['between_6to15_female']; 
        $sixteenTo65_female = $request->age['between_16to65_female']; 
        $above_65_female = $request->age['above65_female']; 

        $total_male_in_age = $zeroTo5_male + $sixTo15_male + $sixteenTo65_male + $above_65_male;

        $total_female_in_age = $zeroTo5_female + $sixTo15_female + $sixteenTo65_female + $above_65_female;

        // Total According to Age Group
        $age['between_0to5_total'] = $request->age['between_0to5_male'] + $request->age['between_0to5_female']; 
        $age['between_6to15_total'] = $request->age['between_6to15_male'] + $request->age['between_6to15_female']; 
        $age['between_16to65_total'] = $request->age['between_16to65_male'] + $request->age['between_16to65_female']; 
        $age['above65_total'] = $request->age['above65_male'] + $request->age['above65_female']; 
        $age['disabled_total'] = $request->age['disabled_male'] + $request->age['disabled_female'];

        //Hygiene check
        $hygiene = $request->hygiene;
        //latring available
        if(isset($hygiene['latrine_available'])){
            $hygiene['latrine_available'] = 1;
            if(isset($hygiene['latrine_cleansing_agent'])){
                 $hygiene['latrine_cleansing_agent'] = 1;
            }
            else{
                $hygiene['latrine_cleansing_agent'] = 0;
            }
            if(isset($hygiene['latrine_water_available'])){
                 $hygiene['latrine_water_available'] = 1;
            }
            else{
                $hygiene['latrine_water_available'] = 0;
            }
            if(isset($hygiene['latrine_cleaning_brush'])){
                 $hygiene['latrine_cleaning_brush'] = 1;
            }
            else{
                $hygiene['latrine_cleaning_brush'] = 0;
            }
        }
        else{
            $hygiene['latrine_cost'] = null;
            $hygiene['total_latrine'] = null;
            $hygiene['latrine_available'] = 0;
            $hygiene['latrine_cleansing_agent'] = 0;
            $hygiene['latrine_water_available'] = 0;
            $hygiene['latrine_cleaning_brush'] = 0;
        }
        
        if(isset($hygiene['home_cleaning'])){
             $hygiene['home_cleaning'] = 1;
        }
        else{
            $hygiene['home_cleaning'] = 0;
        }
        if(isset($hygiene['domestic_animal'])){
             $hygiene['domestic_animal'] = 1;
        }
        else{
            $hygiene['domestic_animal'] = 0;
        }
        if(isset($hygiene['drinking_water'])){
             $hygiene['drinking_water'] = 1;
        }
        else{
            $hygiene['drinking_water'] = 0;
        }
        if(isset($hygiene['food_covered'])){
             $hygiene['food_covered'] = 1;
        }
        else{
            $hygiene['food_covered'] = 0;
        }
        if(isset($hygiene['utensils_clean'])){
             $hygiene['utensils_clean'] = 1;
        }
        else{
            $hygiene['utensils_clean'] = 0;
        }
        if(isset($hygiene['garbage_disposel_pit'])){
             $hygiene['garbage_disposel_pit'] = 1;
        }
        else{
            $hygiene['garbage_disposel_pit'] = 0;
        }

        $healthData['diarrhoea_0to5_total'] = $healthData['diarrhoea_0to5_male'] + $healthData['diarrhoea_0to5_female'];
        $healthData['diarrhoea_above6_total'] = $healthData['diarrhoea_above6_male'] + $healthData['diarrhoea_above6_female'];
        $healthData['dysentry_0to5_total'] = $healthData['dysentry_0to5_male'] + $healthData['dysentry_0to5_female'];
        $healthData['dysentry_above6_total'] = $healthData['dysentry_above6_male'] + $healthData['dysentry_above6_female'];
        $healthData['jaundice_0to5_total'] = $healthData['jaundice_0to5_male'] + $healthData['jaundice_0to5_female'];
        $healthData['jaundice_above6_total'] = $healthData['jaundice_above6_male'] + $healthData['jaundice_above6_female'];
        $healthData['colera_0to5_total'] = $healthData['colera_0to5_male'] + $healthData['colera_0to5_female'];
        $healthData['colera_above6_total'] = $healthData['colera_above6_male'] + $healthData['colera_above6_female'];
        $healthData['worm_infection_0to5_total'] = $healthData['worm_infection_0to5_male'] + $healthData['worm_infection_0to5_female'];
        $healthData['worm_infection_above6_total'] = $healthData['worm_infection_above6_male'] + $healthData['worm_infection_above6_female'];
        $healthData['scabies_0to5_total'] = $healthData['scabies_0to5_male'] + $healthData['scabies_0to5_female'];
        $healthData['scabies_above6_total'] = $healthData['scabies_above6_male'] + $healthData['scabies_above6_female'];
        // dd($healthData);

        //Inclusion Data Other Effects
        if($inclusion['effect_on_women'] != "incident_women" ){
            $inclusion['other_incident_women'] = null;
        }

        if($inclusion['effect_on_children'] != "others" ){
            $inclusion['other_effect_on_children'] = null;
        }
        //ENd of Inclusion Data
        // dd($age);

        if(($total_male_in_age == null && $total_female_in_age == null) || ($request->total_male == $total_male_in_age && $request->total_female == $total_female_in_age))
        {

            $prep_success = Prep_household_detail::Create($data);

            $data['prep_id'] = $prep_success->id;
            $data['del_flag'] = 1;
            $cons_success = Household_const::Create($data);

            $age['house_id'] = $prep_success->id;
            PrepAgeGroup::Create($age);

            //Age Store in Cons Phase
            $age['house_id'] = $cons_success->id;
            ConsAgeGroup::Create($age);

             //sending to store occuption function Prep
            $this->storeOccupationPrep($occupations,$prep_success->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function Prep
            $this->storeIncomePrep($incomes,$prep_success->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);

            //sending to store occuption function COns
            $this->storeOccupationCons($occupations,$cons_success->id,$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income function Cons
            $this->storeIncomeCons($incomes,$cons_success->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);

            //Storing WaterFetch In Prep Phase
            $water_fetch['house_id'] = $prep_success->id;
            PrepWaterFetch::Create($water_fetch);
            //Storing WaterFetch In Cons Phase
            $water_fetch['house_id'] = $cons_success->id;
            ConsWaterFetch::Create($water_fetch);

            //Storing Hygiene in Prep Phase
            $hygiene['house_id'] = $prep_success->id;
            PrepHygiene::Create($hygiene);
            //Storing Hygiene in Cons Phase
            $hygiene['house_id'] = $cons_success->id;
            ConsHygiene::Create($hygiene);

            //Storing Health Data in Prep Phase
            $healthData['house_id'] = $prep_success->id;
            PrepHealthData::Create($healthData);
            //Storing Health Data in Cons Phase
            $healthData['house_id'] = $cons_success->id;
            ConsHealthData::Create($healthData);
    
            //Storing Health Behaviour in Prep Phase
            $behavior['house_id'] = $prep_success->id;
            PrepHealthBehaviour::Create($behavior);
            //Storing Health Behaviour in Cons Phase
            $behavior['house_id'] = $cons_success->id;
            ConsHealthBehaviour::Create($behavior);

            //Storing Inclusion Data in Prep Phase
            $inclusion['house_id'] = $prep_success->id;
            PrepInclusionData::Create($inclusion);
            //Storing Inclusion Data in Cons Phase
            $inclusion['house_id'] = $cons_success->id;
            ConsInclusionData::Create($inclusion);

            //Wash Prep
            DB::table('prep_hh_wash')->insert([
                        "safety_management_12" => $wash['safety_management_12'],
                        "safety_management_8" => $wash['safety_management_8'],
                        "uptake" => $wash['uptake'],
                        "school_safety_12" => $wash['school_safety_12'],
                        "project_scheme" => $wash['project_scheme'],
                        "supply_scheme" => $wash['supply_scheme'],
                        "exceeding_service" => $wash['exceeding_service'],
                        "cleanliness_competitions" => $wash['cleanliness_competitions'],
                        "handwashing_facility" => $wash['handwashing_facility'],
                        "clean_toilet" => $wash['clean_toilet'],
                        "fchv_trained" => $wash['fchv_trained'],
                        "school_trained" => $wash['school_trained'],
                        "school_safe_haven" => $wash['school_safe_haven'],
                        "indicator_clean" => $wash['indicator_clean'],
                        "grey_water" => $wash['grey_water'],
                        "affordable_wealth" => $wash['affordable_wealth'],
                        "total_sanitation" => $wash['total_sanitation'],
                        'house_id' => $prep_success->id,
                        'project_id' => $data['project_id'],
                    ]);
            //Wash Construction
            DB::table('cons_hh_wash')->insert([
                        "safety_management_12" => $wash['safety_management_12'],
                        "safety_management_8" => $wash['safety_management_8'],
                        "uptake" => $wash['uptake'],
                        "school_safety_12" => $wash['school_safety_12'],
                        "project_scheme" => $wash['project_scheme'],
                        "supply_scheme" => $wash['supply_scheme'],
                        "exceeding_service" => $wash['exceeding_service'],
                        "cleanliness_competitions" => $wash['cleanliness_competitions'],
                        "handwashing_facility" => $wash['handwashing_facility'],
                        "clean_toilet" => $wash['clean_toilet'],
                        "fchv_trained" => $wash['fchv_trained'],
                        "school_trained" => $wash['school_trained'],
                        "school_safe_haven" => $wash['school_safe_haven'],
                        "indicator_clean" => $wash['indicator_clean'],
                        "grey_water" => $wash['grey_water'],
                        "affordable_wealth" => $wash['affordable_wealth'],
                        "total_sanitation" => $wash['total_sanitation'],
                        'house_id' => $cons_success->id,
                        'project_id' => $data['project_id'],
                    ]);

            return redirect()->route('admin.prep_household_details',$data['project_id'])->with('success','HouseHold Detail Have Been Inserted SuccessFully !!');
        }
        
        else
        {
            return redirect()->back()->with('error','Total number of family not matched please check age fields !');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page['title'] = 'Project | Household Update';
        $household = Prep_household_detail::findOrFail($id);
        $project = Project::where('id',$household->project_id)->first();
        $wash = DB::table('prep_hh_wash')->where('house_id',$id)->first();
        $cast_divisions = Cast_divison::where('del_flag',0)->get();
        $casts = Cast::where('cast_divison_id',$household->cast_division)->get();
        $occupations = Occupation::all();
        $income_sources = Income_source::all();
        $hh_incomes = DB::table('prep_hh_income_pensions')->where('house_id',$household->id)->get()->toArray();
        foreach ($hh_incomes as $key => $value) {
            $hh_income_obj[] = $value->income_pension;
        }
        $hh_occupation = DB::table('prep_hh_occupations')->where('house_id',$household->id)->get()->toArray();
        foreach ($hh_occupation as $key => $value) {
            $hh_occupation_obj[] = $value->occupation_id;
        }
        $age_groups = PrepAgeGroup::where('house_id',$id)->first();
        $hygienes = PrepHygiene::where('house_id',$id)->first();
        $water_fetcings = PrepWaterFetch::where('house_id',$id)->first();
        $healthdatas = PrepHealthData::where('house_id',$id)->first();
        $healthbehaviors = PrepHealthBehaviour::where('house_id',$id)->first();
        $inclusiondatas = PrepInclusionData::where('house_id',$id)->first();
        $page['title'] = 'Household_detail | Update';
        return view("prep_household_detail::preparation_edit.edit",compact('page','casts','occupations','household','age_groups','hh_occupation_obj','hygienes','healthdatas','healthbehaviors','inclusiondatas','income_sources','hh_income_obj','water_fetcings','cast_divisions','project','wash'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
       
        $data = $request->except('_token','age','water_fetch','hygiene','incidence','behavior','inclusion','_method','occupation_id','income','wash','wash_id');
        // dd($data);
        $occupations = $request->occupation_id;
        $incomes = $request->income;
        $healthData = $request->incidence;
        $behavior = $request->behavior;
        $inclusion = $request->inclusion;
        $wash = $request->wash;
        
        $user = Auth::user()->id;
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        $data['total_member'] = $data['total_male']+$data['total_female'];
        $data['del_flag'] = 0;
        $data['income_pension'] = checkValue($request->income_pension);
        $data['status'] = checkValue($request->status);
        $data['religion_minority'] = checkValue($request->religion_minority);

        //water fetching variable part
        $water_fetch = $request->water_fetch;
        $water_fetch['total_second'] = $water_fetch['going_second'] + $water_fetch['waiting_second'] + $water_fetch['filling_second'] + $water_fetch['return_second'];
        $water_fetch['total_second_8round'] = $water_fetch['total_second'] * 8; 
        $water_fetch['total_minute_8round'] = $water_fetch['total_second_8round']/60; 
        $water_fetch['total_hour_8round'] = $water_fetch['total_minute_8round']/60; 

        //AgeGroup
        $age = $request->age;
        //Total male Population In Age Group
        $zeroTo5_male = $request->age['between_0to5_male']; 
        $sixTo15_male = $request->age['between_6to15_male']; 
        $sixteenTo65_male = $request->age['between_16to65_male']; 
        $above_65_male = $request->age['above65_male']; 
        
        //Total Female Population In Age Group
        $zeroTo5_female = $request->age['between_0to5_female']; 
        $sixTo15_female = $request->age['between_6to15_female']; 
        $sixteenTo65_female = $request->age['between_16to65_female']; 
        $above_65_female = $request->age['above65_female']; 

        $total_male_in_age = $zeroTo5_male + $sixTo15_male + $sixteenTo65_male + $above_65_male;

        $total_female_in_age = $zeroTo5_female + $sixTo15_female + $sixteenTo65_female + $above_65_female;

        // Total According to Age Group
        $age['between_0to5_total'] = $request->age['between_0to5_male'] + $request->age['between_0to5_female']; 
        $age['between_6to15_total'] = $request->age['between_6to15_male'] + $request->age['between_6to15_female']; 
        $age['between_16to65_total'] = $request->age['between_16to65_male'] + $request->age['between_16to65_female']; 
        $age['above65_total'] = $request->age['above65_male'] + $request->age['above65_female']; 
        $age['disabled_total'] = $request->age['disabled_male'] + $request->age['disabled_female'];

        //Hygiene check
        $hygiene = $request->hygiene;
        //latring available
        if(isset($hygiene['latrine_available'])){
            $hygiene['latrine_available'] = 1;
            if(isset($hygiene['latrine_cleansing_agent'])){
                 $hygiene['latrine_cleansing_agent'] = 1;
            }
            else{
                $hygiene['latrine_cleansing_agent'] = 0;
            }
            if(isset($hygiene['latrine_water_available'])){
                 $hygiene['latrine_water_available'] = 1;
            }
            else{
                $hygiene['latrine_water_available'] = 0;
            }
            if(isset($hygiene['latrine_cleaning_brush'])){
                 $hygiene['latrine_cleaning_brush'] = 1;
            }
            else{
                $hygiene['latrine_cleaning_brush'] = 0;
            }
        }
        else{
            $hygiene['latrine_cost'] = null;
            $hygiene['total_latrine'] = null;
            $hygiene['latrine_available'] = 0;
            $hygiene['latrine_cleansing_agent'] = 0;
            $hygiene['latrine_water_available'] = 0;
            $hygiene['latrine_cleaning_brush'] = 0;
        }
        
        if(isset($hygiene['home_cleaning'])){
             $hygiene['home_cleaning'] = 1;
        }
        else{
            $hygiene['home_cleaning'] = 0;
        }
        if(isset($hygiene['domestic_animal'])){
             $hygiene['domestic_animal'] = 1;
        }
        else{
            $hygiene['domestic_animal'] = 0;
        }
        if(isset($hygiene['drinking_water'])){
             $hygiene['drinking_water'] = 1;
        }
        else{
            $hygiene['drinking_water'] = 0;
        }
        if(isset($hygiene['food_covered'])){
             $hygiene['food_covered'] = 1;
        }
        else{
            $hygiene['food_covered'] = 0;
        }
        if(isset($hygiene['utensils_clean'])){
             $hygiene['utensils_clean'] = 1;
        }
        else{
            $hygiene['utensils_clean'] = 0;
        }
        if(isset($hygiene['garbage_disposel_pit'])){
             $hygiene['garbage_disposel_pit'] = 1;
        }
        else{
            $hygiene['garbage_disposel_pit'] = 0;
        }

        $healthData['diarrhoea_0to5_total'] = $healthData['diarrhoea_0to5_male'] + $healthData['diarrhoea_0to5_female'];
        $healthData['diarrhoea_above6_total'] = $healthData['diarrhoea_above6_male'] + $healthData['diarrhoea_above6_female'];
        $healthData['dysentry_0to5_total'] = $healthData['dysentry_0to5_male'] + $healthData['dysentry_0to5_female'];
        $healthData['dysentry_above6_total'] = $healthData['dysentry_above6_male'] + $healthData['dysentry_above6_female'];
        $healthData['jaundice_0to5_total'] = $healthData['jaundice_0to5_male'] + $healthData['jaundice_0to5_female'];
        $healthData['jaundice_above6_total'] = $healthData['jaundice_above6_male'] + $healthData['jaundice_above6_female'];
        $healthData['colera_0to5_total'] = $healthData['colera_0to5_male'] + $healthData['colera_0to5_female'];
        $healthData['colera_above6_total'] = $healthData['colera_above6_male'] + $healthData['colera_above6_female'];
        $healthData['worm_infection_0to5_total'] = $healthData['worm_infection_0to5_male'] + $healthData['worm_infection_0to5_female'];
        $healthData['worm_infection_above6_total'] = $healthData['worm_infection_above6_male'] + $healthData['worm_infection_above6_female'];
        $healthData['scabies_0to5_total'] = $healthData['scabies_0to5_male'] + $healthData['scabies_0to5_female'];
        $healthData['scabies_above6_total'] = $healthData['scabies_above6_male'] + $healthData['scabies_above6_female'];
        // dd($healthData);

        //Inclusion Data Other Effects
        if($inclusion['effect_on_women'] != "incident_women" ){
            $inclusion['other_incident_women'] = null;
        }

        if($inclusion['effect_on_children'] != "others" ){
            $inclusion['other_effect_on_children'] = null;
        }
        //ENd of Inclusion Data
        // dd($inclusion);

        if(($total_male_in_age == null && $total_female_in_age == null) || ($request->total_male == $total_male_in_age && $request->total_female == $total_female_in_age))
        {
            $success = Prep_household_detail::where('id',$id)->where('del_flag',0)->update($data);
            $success_cons = Household_const::where('prep_id',$id)->where('del_flag',0)->update($data);
            $cons_data = Household_const::where('prep_id',$id)->where('del_flag',0)->first();
            //AgeGroup Update
            PrepAgeGroup::where('house_id',$id)->update($age);
            //AgeGroup Update Cons
            ConsAgeGroup::where('house_id',$cons_data->id)->update($age);

            //sending to store occuption function Prep
            $this->storeOccupationPrep($occupations,$id,$data['project_id'],$data['fiscal_year_id']);
            //sending to store occuption function Cons
            $this->storeOccupationCons($occupations,$cons_data->id,$data['project_id'],$data['fiscal_year_id']);

            //Sendint to store income functionPrep
            $this->storeIncomePrep($incomes,$id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);
            //Sendint to store income functionCons
            $this->storeIncomeCons($incomes,$cons_data->id,$data['income_pension'],$data['project_id'],$data['fiscal_year_id']);
            //Storing WaterFetch
            PrepWaterFetch::where('house_id',$id)->update($water_fetch);
            //Storing WaterFetch Cons
            ConsWaterFetch::where('house_id',$cons_data->id)->update($water_fetch);

            //Storing Hygiene
            PrepHygiene::where('house_id',$id)->update($hygiene);
            //Storing Hygiene Cons
            ConsHygiene::where('house_id',$cons_data->id)->update($hygiene);

            //Storing Health Data
            PrepHealthData::where('house_id',$id)->update($healthData);
            //Storing Health Data Cons
            ConsHealthData::where('house_id',$cons_data->id)->update($healthData);

            //Storing Health Behaviour
            PrepHealthBehaviour::where('house_id',$id)->update($behavior);
            //Storing Health Behaviour Cons
            ConsHealthBehaviour::where('house_id',$cons_data->id)->update($behavior);

            //Storing Inclusion Data
            PrepInclusionData::where('house_id',$id)->update($inclusion);
            //Storing Inclusion Data Cons
            ConsInclusionData::where('house_id',$cons_data->id)->update($inclusion);

            DB::table('prep_hh_wash')->where('house_id',$id)->update($wash);
            DB::table('cons_hh_wash')->where('house_id',$cons_data->id)->update($wash);

            return redirect()->route('admin.prep_household_details',$data['project_id'])->with('success','HouseHold Detail Have Been Updated SuccessFully !!');
        }
        
        else
        {
            return redirect()->back()->with('error','Total number of family not matched please check age fields !');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 1;
        $success = Prep_household_detail::where('id', $id)->update($data);

        Household_const::where('prep_id',$id)->update($data);


        return redirect()->back();
    }    

    // Function For Store Occupation 
    public function storeOccupationPrep($occupation,$id,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('prep_hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('prep_hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
            DB::table('prep_hh_occupations')
                ->insert([
                    'occupation_id'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    //Function for store Income

    public function storeIncomePrep($income,$id,$house_income,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('prep_hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('prep_hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        elseif(empty($incomes)){
            DB::table('prep_hh_income_pensions')
                        ->insert([
                            'income_pension'=> null,
                            'house_id' => $id,
                            'project_id' => $projectid,
                            'fiscal_year_id' => $fiscalyear,
                        ]);
                }
        else{
            foreach ($incomes as $key => $value) {
            DB::table('prep_hh_income_pensions')
                ->insert([
                    'income_pension'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }

    //End Function

    // Function For Store Occupation 
    public function storeOccupationCons($occupation,$id,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('cons_hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('cons_hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
            DB::table('cons_hh_occupations')
                ->insert([
                    'occupation_id'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }
    //End Function

    //Function for store Income

    public function storeIncomeCons($income,$id,$house_income,$projectid,$fiscalyear)
    {
        if($id){
            DB::table('cons_hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('cons_hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
        }
        elseif(empty($incomes)){
            DB::table('cons_hh_income_pensions')
                        ->insert([
                            'income_pension'=> null,
                            'house_id' => $id,
                            'project_id' => $projectid,
                            'fiscal_year_id' => $fiscalyear,
                        ]);
                }
        else{
            foreach ($incomes as $key => $value) {
            DB::table('cons_hh_income_pensions')
                ->insert([
                    'income_pension'=>$value,
                    'house_id' => $id,
                    'project_id' => $projectid,
                    'fiscal_year_id' => $fiscalyear,
                ]);
            }
        } 
    }

    //End Function


    
    public function validationHouseHold($request){
        $this->validate($request, [
            'cast_division' => 'required',
            'cast_id' => 'required',

            // ---------------------------------------------------- HOUSEHOLD CATEGORY ---------------------------------------

            'house_no' => 'required',
            'head_name' => 'required',
            'tole_name' => 'required',
            'total_male' => 'nullable|integer|min:0',
            'total_female' => 'nullable|integer|min:0',
            'total_female' => 'nullable|integer|min:0',
            'total_female_widow' => 'nullable|integer|min:0',

            'age.between_0to5_male' => 'nullable|integer|min:0',
            'age.between_0to5_female' => 'nullable|integer|min:0',

            'age.between_6to15_male' => 'nullable|integer|min:0',
            'age.between_6to15_female' => 'nullable|integer|min:0',

            'age.between_16to65_male' => 'nullable|integer|min:0',
            'age.between_16to65_female' => 'nullable|integer|min:0',

            'age.above65_male' => 'nullable|integer|min:0',
            'age.above65_female' => 'nullable|integer|min:0',

            'age.disabled_male' => 'nullable|integer|min:0',
            'age.disabled_female' => 'nullable|integer|min:0',

            'litrate_no' => 'nullable|integer|min:0',
            'illitrate_no' => 'nullable|integer|min:0',

            // ----------------------------------------- WATER FETCHING TIME -------------------------------------------------

            'water_fetch.water_collection_liter' => 'nullable|integer|min:0',
            'water_fetch.going_second' => 'nullable|integer|min:0',
            'water_fetch.waiting_second' => 'nullable|integer|min:0',
            'water_fetch.filling_second' => 'nullable|integer|min:0',
            'water_fetch.return_second' => 'nullable|integer|min:0',

            // ----------------------------------------- DATA ON HYGIENE AND SANITATION --------------------------------------

            'total_latrine' => 'nullable|integer|min:0',
            'defecate_latrine' => 'nullable|integer|min:0',
            'defecate_open' => 'nullable|integer|min:0',
            'handwash_water_soap' => 'nullable|integer|min:0',
            'handwash_water' => 'nullable|integer|min:0',
            'handwash_ash' => 'nullable|integer|min:0',
            'handwash_others' => 'nullable|integer|min:0',
            'defecate_water_soap' => 'nullable|integer|min:0',
            'defecate_water' => 'nullable|integer|min:0',
            'defecate_ash' => 'nullable|integer|min:0',
            'handwash_infant' => 'nullable|integer|min:0',

            // ----------------------------------------- HEALTH DATA ---------------------------------------------------------

            'incidence.diarrhoea_male' => 'nullable|integer|min:0',
            'incidence.diarrhoea_female' => 'nullable|integer|min:0',
            'incidence.diarrhoea_frequency' => 'nullable|integer|min:0',
            'incidence.dysentry_male' => 'nullable|integer|min:0',
            'incidence.dysentry_female' => 'nullable|integer|min:0',
            'incidence.dysentry_frequency' => 'nullable|integer|min:0',
            'incidence.jaundice_male' => 'nullable|integer|min:0',
            'incidence.jaundice_female' => 'nullable|integer|min:0',
            'incidence.jaundice_frequency' => 'nullable|integer|min:0',
            'incidence.colera_male' => 'nullable|integer|min:0',
            'incidence.colera_female' => 'nullable|integer|min:0',
            'incidence.colera_frequency' => 'nullable|integer|min:0',
            'incidence.worm_infection_male' => 'nullable|integer|min:0',
            'incidence.worm_infection_female' => 'nullable|integer|min:0',
            'incidence.worm_infection_frequency' => 'nullable|integer|min:0',
            'incidence.scabies_male' => 'nullable|integer|min:0',
            'incidence.scabies_female' => 'nullable|integer|min:0',
            'incidence.scabies_frequency' => 'nullable|integer|min:0',
            'incidence.sickness_absence_school' => 'nullable|integer|min:0',
            'incidence.max_sick_days' => 'nullable|integer|min:0',
            'incidence.bedrest_adult' => 'nullable|integer|min:0',
            'incidence.bedrest_days' => 'nullable|integer|min:0',

                        
            // ----------------------------------------- HEALTH SEEKING BEHAVIOUR---------------------------------------------

            'behavior.annual_expenses' => 'nullable|integer|min:0',


            // ----------------------------------------- GENDER ON INCLUSION DATA --------------------------------------------

            'inclusion.hh_male' => 'nullable|integer|min:0',
            'inclusion.hh_female' => 'nullable|integer|min:0',
            'inclusion.hh_boys' => 'nullable|integer|min:0',
            'inclusion.hh_girls' => 'nullable|integer|min:0',
            'inclusion.wf_male' => 'nullable|integer|min:0',
            'inclusion.wf_female' => 'nullable|integer|min:0',
            'inclusion.wf_boys' => 'nullable|integer|min:0',
            'inclusion.wf_girls' => 'nullable|integer|min:0',
            'inclusion.pw_male' => 'nullable|integer|min:0',
            'inclusion.pw_female' => 'nullable|integer|min:0',
            'inclusion.pw_boys' => 'nullable|integer|min:0',
            'inclusion.pw_girls' => 'nullable|integer|min:0',
            'inclusion.cw_male' => 'nullable|integer|min:0',
            'inclusion.cw_female' => 'nullable|integer|min:0',
            'inclusion.cw_boys' => 'nullable|integer|min:0',
            'inclusion.cw_girls' => 'nullable|integer|min:0',
            'inclusion.out_migrating' => 'nullable|integer|min:0',
            'inclusion.school_going_children_male' => 'nullable|integer|min:0',
            'inclusion.school_going_children_female' => 'nullable|integer|min:0',
        ],[
            'cast_division.required' => 'Please Select A Cast Division!!',
            'cast_id.required' => 'Please Select A Cast!!',

            'house_no.required' => 'Please Input House Number!!',
            'head_name.required' => 'Please Input Family Head Name!!',
            'tole_name.required' => 'Please Input Tole Name!!',
            'total_male.min' => 'Number Of Male Cannot Be negative!!',
            'total_female.min' => 'Number Of Female Cannot Be Negative!!',
            'total_female_widow.min' => 'Number Of Female Cannot Be Negative!!',

            'age.between_0to5_male.min' => 'Number Of Male Cannot Be Negative!!',
            'age.between_0to5_female.min' => 'Number Of Female Cannot Be Negative!!',


            'age.between_6to15_male.min' => 'Number Of Male Cannot Be Negative!!',
            'age.between_6to15_female.min' => 'Number Of Female Cannot Be Negative!!',

            'age.between_16to65_male.min' => 'Number Of Male Cannot Be Negative!!',
            'age.between_16to65_female.min' => 'Number Of Female Cannot Be Negative!!',

            'age.above65_male.min' => 'Number Of Male Cannot Be Negative!!',
            'age.above65_female.min' => 'Number Of Female Cannot Be Negative!!',

            'age.disabled_male.min' => 'Number Of Desable Male Cannot Be Negative!!',
            'age.disabled_female.min' => 'Number Of Desable Female Cannot Be Negative!!',

            'litrate_no.min' => 'Number Of litrate Cannot Be Negative!!',
            'illitrate_no.min' => 'Number Of illitrate Cannot Be Negative!!',

            // ----------------------------------------- WATER FETCHING TIME -------------------------------------------------
            'water_fetch.water_collection_liter.min' => 'Field Cannot Be Negative',
            'water_fetch.going_second.min' => 'Field Cannot Be Negative',
            'water_fetch.waiting_second.min' => 'Field Cannot Be Negative',
            'water_fetch.filling_second.min' => 'Field Cannot Be Negative',
            'water_fetch.return_second.min' => 'Field Cannot Be Negative',

            // ----------------------------------------- DATA ON HYGIENE AND SANITATION --------------------------------------

            'total_latrine.min' => 'Field Cannot Be Negative',
            'defecate_latrine.min' => 'Field Cannot Be Negative',
            'defecate_open.min' => 'Field Cannot Be Negative',
            'handwash_water_soap.min' => 'Field Cannot Be Negative',
            'handwash_water.min' => 'Field Cannot Be Negative',
            'handwash_ash.min' => 'Field Cannot Be Negative',
            'handwash_others.min' => 'Field Cannot Be Negative',
            'defecate_water_soap.min' => 'Field Cannot Be Negative',
            'defecate_water.min' => 'Field Cannot Be Negative',
            'defecate_ash.min' => 'Field Cannot Be Negative',
            'handwash_infant.min' => 'Field Cannot Be Negative',

            // ----------------------------------------- HEALTH DATA ---------------------------------------------------------

            'incidence.diarrhoea_male.min' => 'Field Cannot Be Negative',
            'incidence.diarrhoea_female.min' => 'Field Cannot Be Negative',
            'incidence.diarrhoea_frequency.min' => 'Field Cannot Be Negative',
            'incidence.dysentry_male.min' => 'Field Cannot Be Negative',
            'incidence.dysentry_female.min' => 'Field Cannot Be Negative',
            'incidence.dysentry_frequency.min' => 'Field Cannot Be Negative',
            'incidence.jaundice_male.min' => 'Field Cannot Be Negative',
            'incidence.jaundice_female.min' => 'Field Cannot Be Negative',
            'incidence.jaundice_frequency.min' => 'Field Cannot Be Negative',
            'incidence.colera_male.min' => 'Field Cannot Be Negative',
            'incidence.colera_female.min' => 'Field Cannot Be Negative',
            'incidence.colera_frequency.min' => 'Field Cannot Be Negative',
            'incidence.worm_infection_male.min' => 'Field Cannot Be Negative',
            'incidence.worm_infection_female.min' => 'Field Cannot Be Negative',
            'incidence.worm_infection_frequency.min' => 'Field Cannot Be Negative',
            'incidence.scabies_male.min' => 'Field Cannot Be Negative',
            'incidence.scabies_female.min' => 'Field Cannot Be Negative',
            'incidence.scabies_frequency.min' => 'Field Cannot Be Negative',
            'incidence.sickness_absence_school.min' => 'Field Cannot Be Negative',
            'incidence.max_sick_days.min' => 'Field Cannot Be Negative',
            'incidence.bedrest_adult.min' => 'Field Cannot Be Negative',
            'incidence.bedrest_days.min' => 'Field Cannot Be Negative',

            // ----------------------------------------- HEALTH SEEKING BEHAVIOUR---------------------------------------------

            'behavior.annual_expenses.min' => 'Field Cannot Be Negative',

            // ----------------------------------------- GENDER ON INCLUSION DATA --------------------------------------------

            'inclusion.hh_male.min' => 'Field Cannot Be Negative',
            'inclusion.hh_female.min' => 'Field Cannot Be Negative',
            'inclusion.hh_boys.min' => 'Field Cannot Be Negative',
            'inclusion.hh_girls.min' => 'Field Cannot Be Negative',
            'inclusion.wf_male.min' => 'Field Cannot Be Negative',
            'inclusion.wf_female.min' => 'Field Cannot Be Negative',
            'inclusion.wf_boys.min' => 'Field Cannot Be Negative',
            'inclusion.wf_girls.min' => 'Field Cannot Be Negative',
            'inclusion.pw_male.min' => 'Field Cannot Be Negative',
            'inclusion.pw_female.min' => 'Field Cannot Be Negative',
            'inclusion.pw_boys.min' => 'Field Cannot Be Negative',
            'inclusion.pw_girls.min' => 'Field Cannot Be Negative',
            'inclusion.cw_male.min' => 'Field Cannot Be Negative',
            'inclusion.cw_female.min' => 'Field Cannot Be Negative',
            'inclusion.cw_boys.min' => 'Field Cannot Be Negative',
            'inclusion.cw_girls.min' => 'Field Cannot Be Negative',
            'inclusion.out_migrating.min' => 'Field Cannot Be Negative',
            'inclusion.school_going_children_male.min' => 'Field Cannot Be Negative',
            'inclusion.school_going_children_female.min' => 'Field Cannot Be Negative',
        ]);
    }

    public function wuscDetail($id)
    {
        $page['title'] = 'Prep_household_detail | Update';
        
        $wusc_details = DB::table('prep_wusc_details')->where('project_id',$id)->first();
        // dd($wusc_details);
        
       if($wusc_details == null){
            return view('prep_household_detail::wusc_detail',compact('page','id'));
        }
      
        else{
            $wusc_id=$wusc_details->id;
            $vhp_members = DB::table('tbl_vhp_members')->where('wusc_id',$wusc_id)->get(); 
            $css_members = DB::table('tbl_css_members')->where('wusc_id',$wusc_id)->get(); 
            $vmw_members = DB::table('tbl_vmw_members')->where('wusc_id',$wusc_id)->get(); 
            $wsp_members = DB::table('tbl_wsp_members')->where('wusc_id',$wusc_id)->get(); 
            // dd($vhp_members);
            return view('prep_household_detail::wusc_detail',compact('page','id','wusc_details','vhp_members','css_members','vmw_members','wsp_members'));
            
                

        }
    }
        // here
    public function wuscDetailStore(Request $request,$id)
    {
        dd($request->member_vhp_cast);
        // return $request;
            
        $project_id=$id;
        //    return $request;
        $wusc_details = DB::table('prep_wusc_details')->where('project_id',$id)->first();
         // dd($wusc_details->id);
        $agreement_water_structure = checkValue($request->agreement_water_structure);
        $agreement_pipeline = checkValue($request->agreement_pipeline);
        $agreement_mpc_tap = checkValue($request->agreement_mpc_tap);
        $agreement_other_tap = checkValue($request->agreement_other_tap);
        $agreement_contribution = checkValue($request->agreement_contribution);
        // dd($agreement_contribution);

        //Water Analysis Report FIle
        $survey['water_analysis_report']=$request->water_analysis_report;

        $this->validateWUSCDetail($request);

        if(Input::hasFile('water_analysis_report'))
        {
            $extension = $request->file('water_analysis_report')->getClientOriginalExtension();
            $filename  = str_random(15).'.'.$extension;

            $destination = public_path('images/water_analysis_report/'.$id);
            if(!file_exists($destination)) {   
               mkdir($destination, 0777, true);
            }
            $request->file('water_analysis_report')->move($destination, $filename);
            $survey['water_analysis_report']=$filename;
        }
        else{
            if($request->oldImage == null){
               $survey['water_analysis_report'] = null;   
            }
            else{
                $survey['water_analysis_report'] = $request->oldImage;
            }
        }

        //Agreement Pic
        $survey['agreement_pic']=$request->agreement_pic;
        if(Input::hasFile('agreement_pic'))
        {
            $extension = $request->file('agreement_pic')->getClientOriginalExtension();
            $filename  = str_random(15).'.'.$extension;

            $destination = public_path('images/agreement_pic/'.$id);
            if(!file_exists($destination)) {   
               mkdir($destination, 0777, true);
            }
            $request->file('agreement_pic')->move($destination, $filename);
            $survey['agreement_pic']=$filename;
        }
        else{
            if($request->oldImage == null){
               $survey['agreement_pic'] = null;   
            }
            else{
                $survey['agreement_pic'] = $request->agreement_pic_oldImage;
            }
        }
        
        if($wusc_details != null){
            DB::table('prep_wusc_details')->where('project_id',$id)->update([
                    'toilet_monitoring_date'=> $request->toilet_monitoring_date,
                    'dish_drying_demonstration_date'=> $request->dish_drying_demonstration_date,
                    'dish_drying_demonstration_type'=> $request->dish_drying_demonstration_type,
                    'dwrc_registration'=> $request->dwrc_registration,
                    'tripartie_agreement_date'=> $request->tripartie_agreement_date,
                    'letter_of_intent'=> $request->letter_of_intent,
                    'pre_construction'=> $request->pre_construction,
                    'pre_construction_participants'=> $request->pre_construction_participants,
                    'average_cost'=> $request->average_cost,
                    'number_dish_racks'=> $request->number_dish_racks,
                    'number_washing_platforms'=> $request->number_washing_platforms,
                    'number_waste_disposal'=> $request->number_waste_disposal,
                    'water_supply_plan_date'=> $request->water_supply_plan_date,
                    'om_fund'=> $request->om_fund,
                    'chairman_name'=> $request->chairman_name,
                    'chairman_gender'=> $request->chairman_gender,
                    'chairman_cast'=> $request->chairman_cast,
                    'vice_chairman_name'=> $request->vice_chairman_name,
                    'vice_chairman_gender'=> $request->vice_chairman_gender,
                    'vice_chairman_cast'=> $request->vice_chairman_cast,
                    'secretary_name'=> $request->secretary_name,
                    'secretary_gender'=> $request->secretary_gender,
                    'secretary_cast'=> $request->secretary_cast,
                    'vice_secretary_name'=> $request->vice_secretary_name,
                    'vice_secretary_gender'=> $request->vice_secretary_gender,
                    'vice_secretary_cast'=> $request->vice_secretary_cast,
                    'treasurer_name'=> $request->treasurer_name,
                    
                    'treasurer_gender'=> $request->treasurer_gender,
                    'treasurer_cast'=> $request->treasurer_cast,
            
                    'water_analysis_report' =>$survey['water_analysis_report'],
                    'project_id'=> $id,
                    'agreement_date'=> $request->agreement_date,
                    'agreement_water_structure' => $agreement_water_structure,
                    'agreement_pipeline' => $agreement_pipeline,
                    'agreement_mpc_tap' => $agreement_mpc_tap,
                    'agreement_other_tap' => $agreement_other_tap,
                    'agreement_contribution' => $agreement_contribution,
                    'agreement_pic' => $survey['agreement_pic'],
                ]);

            //VHP MEMBERS
            $i = 0;
            foreach ($request->member_vhp_name as $value) {
                if(isset($request->vhp_id[$i] )){
                    DB::table('tbl_vhp_members')->where('id',$request->vhp_id[$i])->update([

                        'name' => $value,
                        'gender' => $request->member_vhp_gender[$i],
                        'cast' => $request->member_vhp_cast[$i],
                        'project_id'=>$id,
                        'wusc_id'=>$wusc_details->id,
                    ]);
                }
                else{
                    DB::table('tbl_vhp_members')->insert([
                            'name' => $value,
                            'gender' => $request->member_vhp_gender[$i],
                            'cast' => $request->member_vhp_cast[$i],
                            'project_id'=> $id,
                            'wusc_id'=> $wusc_details->id,
                        ]);
                }
                $i++;
            }

            //VMW MEMBERS
            $i = 0;
            foreach ($request->vmw_name as $value) {
                if(isset($request->vmw_id[$i] )){
                    DB::table('tbl_vmw_members')->where('id',$request->vmw_id[$i])->update([

                        'name' => $value,
                        'gender' => $request->vmw_gender[$i],
                        'cast' => $request->vmw_cast[$i],
                        'project_id'=>$id,
                        'wusc_id'=>$wusc_details->id,
                    ]);
                }
                else{
                    DB::table('tbl_vmw_members')->insert([
                            'name' => $value,
                            'gender' => $request->vmw_gender[$i],
                            'cast' => $request->vmw_cast[$i],
                            'project_id'=> $id,
                            'wusc_id'=> $wusc_details->id,
                        ]);
                }
                $i++;
            }

            //CSS MEMBERS
            $i = 0;
            foreach ($request->css_name as $value) {
                if(isset($request->css_id[$i] )){
                    DB::table('tbl_css_members')->where('id',$request->css_id[$i])->update([

                        'name' => $value,
                        'gender' => $request->css_gender[$i],
                        'cast' => $request->css_cast[$i],
                        'project_id'=>$id,
                        'wusc_id'=>$wusc_details->id,
                    ]);
                }
                else{
                    DB::table('tbl_css_members')->insert([
                            'name' => $value,
                            'gender' => $request->css_gender[$i],
                            'cast' => $request->css_cast[$i],
                            'project_id'=> $id,
                            'wusc_id'=> $wusc_details->id,
                        ]);
                }
                $i++;
            }

            //WSP MEMBERS
            $i = 0;
            foreach ($request->wsp_name as $value) {
                if(isset($request->wsp_id[$i] )){
                    DB::table('tbl_wsp_members')->where('id',$request->wsp_id[$i])->update([

                        'name' => $value,
                        'gender' => $request->wsp_gender[$i],
                        'cast' => $request->wsp_cast[$i],
                        'project_id'=>$id,
                        'wusc_id'=>$wusc_details->id,
                    ]);
                }
                else{
                    DB::table('tbl_wsp_members')->insert([
                            'name' => $value,
                            'gender' => $request->wsp_gender[$i],
                            'cast' => $request->wsp_cast[$i],
                            'project_id'=> $id,
                            'wusc_id'=> $wusc_details->id,
                        ]);
                }
                $i++;
            }
              return redirect()->route('admin.projects.show',$id)->with('success','WUSC Detail Has been Updated SuccessFully !!!');
        }
        else
        {
            $wusc_detail_id = DB::table('prep_wusc_details')->insertGetId([
                    'toilet_monitoring_date'=> $request->toilet_monitoring_date,
                    'dish_drying_demonstration_date'=> $request->dish_drying_demonstration_date,
                    'dish_drying_demonstration_type'=> $request->dish_drying_demonstration_type,
                    'dwrc_registration'=> $request->dwrc_registration,
                    'tripartie_agreement_date'=> $request->tripartie_agreement_date,
                    'letter_of_intent'=> $request->letter_of_intent,
                    'pre_construction'=> $request->pre_construction,
                    'pre_construction_participants'=> $request->pre_construction_participants,
                    'average_cost'=> $request->average_cost,
                    'number_dish_racks'=> $request->number_dish_racks,
                    'number_washing_platforms'=> $request->number_washing_platforms,
                    'number_waste_disposal'=> $request->number_waste_disposal,
                    'water_supply_plan_date'=> $request->water_supply_plan_date,
                    'om_fund'=> $request->om_fund,
                    'chairman_name'=> $request->chairman_name,
                    'chairman_gender'=> $request->chairman_gender,
                    'chairman_cast'=> $request->chairman_cast,
                    'vice_chairman_name'=> $request->vice_chairman_name,
                    'vice_chairman_gender'=> $request->vice_chairman_gender,
                    'vice_chairman_cast'=> $request->vice_chairman_cast,
                    'secretary_name'=> $request->secretary_name,
                    'secretary_gender'=> $request->secretary_gender,
                    'secretary_cast'=> $request->secretary_cast,
                    'vice_secretary_name'=> $request->vice_secretary_name,
                    'vice_secretary_gender'=> $request->vice_secretary_gender,
                    'vice_secretary_cast'=> $request->vice_secretary_cast,
                    'treasurer_name'=> $request->treasurer_name,
                    'treasurer_gender'=> $request->treasurer_gender,
                    'treasurer_cast'=> $request->treasurer_cast,
                    
                   
                    'water_analysis_report' =>$survey['water_analysis_report'],
                    'project_id'=> $id,
                    'agreement_date'=> $request->agreement_date,
                    'agreement_water_structure' => $agreement_water_structure,
                    'agreement_pipeline' => $agreement_pipeline,
                    'agreement_mpc_tap' => $agreement_mpc_tap,
                    'agreement_other_tap' => $agreement_other_tap,
                    'agreement_contribution' => $agreement_contribution,
                    'agreement_pic' => $survey['agreement_pic'],
                ]);
                if($wusc_detail_id){
                    $i = 0;
                    foreach ($request->member_vhp_name as $value) {
                        DB::table('tbl_vhp_members')->insert([
                            'name' => $value,
                            'gender' => $request->member_vhp_gender[$i],
                            'cast' => $request->member_vhp_cast[$i],
                            'project_id'=>$id,
                            'wusc_id'=>$wusc_detail_id,
                        ]);
                        $i++;
                    }
                }
                if($wusc_detail_id){
                    $i = 0;
                    foreach ($request->vmw_name as $value) {
                        DB::table('tbl_vmw_members')->insert([
                            'name' => $value,
                            'gender' => $request->vmw_gender[$i],
                            'cast' => $request->vmw_cast[$i],
                            'project_id'=>$id,
                            'wusc_id'=>$wusc_detail_id,
                        ]);
                        $i++;
                    }
                }
                if($wusc_detail_id){
                    $i = 0;
                    foreach ($request->css_name as $value) {
                        DB::table('tbl_css_members')->insert([
                            'name' => $value,
                            'gender' => $request->css_gender[$i],
                            'cast' => $request->css_cast[$i],
                            'project_id'=>$id,
                            'wusc_id'=>$wusc_detail_id,
                        ]);
                        $i++;
                    }
                }
                if($wusc_detail_id){
                    $i = 0;
                    foreach ($request->wsp_name as $value) {
                        DB::table('tbl_wsp_members')->insert([
                            'name' => $value,
                            'gender' => $request->wsp_gender[$i],
                            'cast' => $request->wsp_cast[$i],
                            'project_id'=>$id,
                            'wusc_id'=>$wusc_detail_id,
                        ]);
                        $i++;
                    }
                }
                
              
                return redirect()->route('admin.projects.show',$id)->with('success','WUSC Detail Has been inserted SuccessFully !!!');
        }
    }

    public function validateWUSCDetail($request)
    {
         $this->validate($request, [
            'average_cost' => 'nullable|integer|min:0',
            'number_dish_racks' => 'nullable|integer|min:0',
            'number_washing_platforms' => 'nullable|integer|min:0',
            'number_waste_disposal' => 'nullable|integer|min:0',
            'om_fund' => 'nullable|integer|min:0',
            ],[
            'average_cost.min' => 'Average Cost Cannot Be Negative!!',
            'number_dish_racks.min' => 'Number Of Dish Racks Cannot Be Negative!!',
            'number_washing_platforms.min' => 'Number Ofx Washing Platform Cannot Be Negative!!',
            'number_waste_disposal.min' => 'Number Of Waste Disposal Cannot Be Negative!!',
            'om_fund.min' => 'OM Fund Cannot Be Negative!!',

            ]);
    }

    public function survey_design($id)
    {
        $page['title'] = 'survey_design';
        $survey_design = DB::table('prep_survey')->where('project_id',$id)->first();


        if($survey_design == null){
            return view('prep_household_detail::surveyDesign',compact('page','id'));
        }
        else{
            return view('prep_household_detail::surveyDesign',compact('page','id','survey_design'));

        }
    }
    public function store_survey(Request $request,$id)
    {
        $survey_design = DB::table('prep_survey')->where('project_id',$id)->first();
        $teast = $this->validatesurvey($request);
        $survey['design_pic']=$request->design_pic;
        if(Input::hasFile('design_pic'))
        {
            $extension = $request->file('design_pic')->getClientOriginalExtension();
            $filename  = str_random(15).'.'.$extension;

            $destination = public_path('images/survey_design_pic/'.$id);
            if(!file_exists($destination)) {   
               mkdir($destination, 0777, true);
            }
            $request->file('design_pic')->move($destination, $filename);
            $survey['design_pic']=$filename;
        }
        else{
            if($request->oldImage == null){
               $survey['design_pic'] = null;   
            }
            else{
                $survey['design_pic'] = $request->oldImage;
            }
        }
        // dd($request->water_source_record);
        if($survey_design != null){
            $success= DB::table('prep_survey')->where('project_id',$id)->update([
                    'project_id' =>$request->id,
                    'survey_date' =>$request->survey_date,
                    'design_date' =>$request->design_date,
                    'source_assessment' =>$request->source_assessment,
                    'water_structure' =>$request->water_structure,
                    'pipeline_length' =>$request->pipeline_length,
                    'gps_data' =>$request->gps_data,
                    'design_pic' => $survey['design_pic'],
                    // 'design_agreement_date' =>$request->design_agreement_date,
                    'water_source_record' =>$request->water_source_record,
                    'variation_taps'=>$request->variation_taps,
                    'variation_households' =>$request->variation_households,
                    'variation_popn' =>$request->variation_popn,
                ]);
            return redirect()->route('admin.projects.show',$id)->with('success','Survey Design Has been Updated SuccessFully !!!');
        }
        else{
            $success= DB::table('prep_survey')->insert([
                        'project_id' =>$request->id,
                        'survey_date' =>$request->survey_date,
                        'design_date' =>$request->design_date,
                        'source_assessment' =>$request->source_assessment,
                        'water_structure' =>$request->water_structure,
                        'pipeline_length' =>$request->pipeline_length,
                        'gps_data' =>$request->gps_data,
                        'design_pic' => $survey['design_pic'],
                        'design_agreement_date' =>$request->design_agreement_date,
                        'water_source_record' =>$request->water_source_record,
                        'variation_taps'=>$request->variation_taps,
                        'variation_households' =>$request->variation_households,
                        'variation_popn' =>$request->variation_popn,
                    ]);
            return redirect()->route('admin.projects.show',$id)->with('success','Survey Design Has been Updated SuccessFully !!!');
       }
        
    }
    
    public function validatesurvey($request)
    {
         $this->validate($request, [
            'pipeline_length' => 'nullable|integer|min:0',
            'variation_households' => 'nullable|integer|min:0',
            'variation_popn' => 'nullable|integer|min:0',
            'variation_taps'=>'nullable|integer|min:0',
           
        ],[
            'pipeline_length.min' => 'length of pipeline Cannot Be Negative!!',
            'variation_households.min' => 'Number Of households Cannot Be Negative!!',
            'variation_popn.min' => 'Number Of population Cannot Be Negative!!',
            'variation_taps.min' => 'Number Of population Cannot Be Negative!!',
        ]);
    }

    public function firstApprove($id)
    {
        $user = Auth::user()->id;
        // dd($user);
        $get_status = Prep_household_detail::find($id);
        
        if($get_status->first_approve_status == 1){
            $approve = 0;
        }
        else{
            $approve = 1;
        }
        $approved = DB::table('tbl_prep_household_details')
                    ->where('id',$id)
                    ->update(['first_approved_by' => $user,'first_approve_status' => $approve]);
        return back();
    }
    public function secondApprove($id)
    {
        // dd($id);
        $user = Auth::user()->id;
        $get_status = Prep_household_detail::find($id);
        
        if($get_status->second_approve_status == 1){
            $approve = 0;
        }
        else{
            $approve = 1;
        }
        $approved = DB::table('tbl_prep_household_details')
                    ->where('id',$id)
                    ->update(['second_approved_by' => $user,'second_approve_status' => $approve]);
        if($approve == 1){
            DB::table('tbl_household_const')->where('prep_id',$id)->update(['del_flag'=>0]);
        }else{
            DB::table('tbl_household_const')->where('prep_id',$id)->update(['del_flag'=>1]);
        }
        return back();
    }

    //Export Controllers

    //Export HouseHolde Detail
    public function exporthousedetail($id)
    {
        $data = Prep_household_detail::where('project_id',$id)->where('del_flag',0)
                                ->get();
        // dd($data);
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->get_cast_division = DB::table('mst_cast_divisons')
                                    ->leftjoin('tbl_prep_household_details','mst_cast_divisons.id','=','tbl_prep_household_details.cast_division')
                                    ->where('mst_cast_divisons.id',$value->cast_division)
                                    ->pluck('name')->first();
            $value->get_cast = DB::table('mst_casts')
                                    ->leftjoin('tbl_prep_household_details','mst_casts.id','=','tbl_prep_household_details.cast_id')
                                    ->where('mst_casts.id',$value->cast_id)
                                    ->pluck('name')->first();

            //Occupation Data
            $hh_occupation = DB::table('prep_hh_occupations')->where('house_id',$value->id)->get()->toArray();
            $hh_occupation_obj = array();
            foreach ($hh_occupation as $key => $val) {
                $hh_occupation_obj[] = $val->occupation_id;
            }
            $occip = DB::table('mst_occupations')->get()->toArray();
            unset($hh_occuos);
            foreach ($occip as $k => $v) { 
                if(in_array($v->id, $hh_occupation_obj)){
                    $hh_occuos[] =$v->name;
                }
            }
            $ociu = implode(',', $hh_occuos);
            //End Occupation

            //Income Source
            $hh_income = DB::table('prep_hh_income_pensions')->where('house_id',$value->id)->get()->toArray();
            $hh_income_obj = array();
            foreach ($hh_income as $keys => $values) {
                // dd($values);
                $hh_income_obj[] = $values->income_pension;
            }
            $inco = DB::table('mst_income_sources')->get()->toArray();
            // dd($hh_income_obj);
            unset($hh_incos);
            foreach ($inco as $ke => $va) { 
                if(in_array($va->id, $hh_income_obj)){
                    $hh_incos[] =$va->name;
                }
            }
            $incoss = implode(',', $hh_incos);
            //End Income Source
            $value->occ = $ociu;
            $value->incos = $incoss;
        }
      
        // return view("household_detail::export.housedetail",compact('data'));
        return Excel::download(new HouseHoldExport("household_detail::export.housedetail",$data), 'Preparation Household Details.xlsx');
    }

    //Export AgeGroup Detail
    public function exportageGroup($id)
    {
        $data = Prep_household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->age = DB::table('prep_hh_age_groups')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.agegroup",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.agegroup",$data), 'Preparation Age Group Detail.xlsx');
    }

    //Export WaterFetching Detial
    public function exportwaterfetch($id)
    {
        $data = Prep_household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->water_fetch = DB::table('prep_hh_water_fetching_time')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.waterfetching",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.waterfetching",$data), 'Preparation Water Fetching .xlsx');
    }

    //Export Hygiene Detail
    public function exportHygieneData($id)
    {   
        $data = Prep_household_detail::where('project_id',$id)->where('del_flag',0)
                                ->get();
        // dd($data)
        foreach($data as &$value){
            // dd($value->community_name);
            $value->district=Project::where('del_flag',0)
                                ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                                ->where('tbl_projects.id',$id)
                                ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                                ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();

            $value->hygiene = DB::table('prep_hh_hygienes')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->checkphase = "preparation";
        }
        // $community=DB::table('prj_community_infos')->where('project_id',$id)->first();
        if(isset($data[0]))
        {
            //  return "here";
            return view ("household_detail::export.hygieneReport",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.hygieneReport",$data), 'Preparation Hygiene.xlsx');
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        }
    }

    //Export HealthData Detail
    public function exportHealthData($id)
    {
        $data = Prep_household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();
        // dd($data);
        foreach($data as &$value){
            // dd($value->community_name);
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();

            $value->healthData = DB::table('prep_hh_health_datas')->where('house_id',$value->id)->first();
            $value->healthBehavior = DB::table('prep_hh_health_behaviors')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->checkphase = "preparation";

        }
        if(isset($data[0]))
        {
            // return "here";
            return view("household_detail::export.healthDataReport",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.healthDataReport",$data), 'Preparation HealthData.xlsx');
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
        // dd($data);
        //  
        // dd($data[0]['id']);
    }

    //Export HealthBehaviour Detail
    public function exporthealthBehaviour($id)
    {
        $data = Prep_household_detail::where('project_id',$id)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->health = DB::table('prep_hh_health_behaviors')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.HealthBehaviour",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.waterfetching",$data), 'Preparation Health Behaviour Detail.xlsx');
    }

    //Export Handwash Detail
    public function exportHandWash($id)
    {
        $data = Prep_household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();

        // dd($data);
        foreach($data as &$value){
            // dd($value->community_name);

            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id=$value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();

            $value->handWash = DB::table('prep_hh_hygienes')->where('house_id',$value->id)->first();
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->checkphase = "preparation";
            

        }
        //dd($data);
        if(isset($data[0]))
        {
            // return "here";
        return view("household_detail::export.handWash",compact('data'));
        return Excel::download(new HouseHoldExport("household_detail::export.handWash",$data), 'handWash.xlsx');
        // dd($data[0]['id']);
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
    }

    //Export Social Economic
    public function exportSocialEconomic($id){
        $data = Prep_household_detail::where('project_id',$id)
                                ->where('del_flag',0)->get();

        foreach ($data as &$value) {


            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
            ->where('district_id',$district_id)->pluck('name')->first();


            $value->inclusionData = DB::table('prep_hh_inclusion_datas')->where('house_id',$value->id)->first();
            $value->ageGroup = DB::table('prep_hh_age_groups')->where('house_id',$value->id)->first();
            // $value->income_source = Income_source::get()->where('del_flag',0);
            $value->community=DB::table('prj_community_infos')->where('project_id',$id)->first();
            $value->occupations = DB::table('prep_hh_occupations')
                        ->join('mst_occupations','mst_occupations.id','=','prep_hh_occupations.occupation_id')
                        ->where('house_id',$value->id)
                        ->get();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->checkphase = "preparation";
            $occarray = [];
            foreach ($value->occupations as $k => $v) {
                $occarray[] = $v->name;
            }
            // $array_occupation = [];
            // foreach ($occupations as $key => $occ) {
            //     $array_occupation[$occ->occupation_id] = $occ;
            //     $array_occupation[$occ->name] = $occ;
            // }
            // dd($occarray);
            $value->occupations = implode(',', $occarray);
            unset($occarray);

            $income_sources = DB::table('prep_hh_income_pensions')->where('house_id',$value->id)->get();
            $array_new = [];
            foreach ($income_sources as $key => $v) {
                $array_new[$v->income_pension]  = $v;               
            }
            $value->income_sources = $array_new;
           
        }
        // dd($data);
            $data->income_sources = DB::table('mst_income_sources')->where('del_flag',0)->get();
        // dd($data);
         if(isset($data[0]))
        {
            // return "here";
            return view("household_detail::export.socioEconomic",compact('data'));
            return Excel::download(new HouseHoldExport("household_detail::export.socioEconomic",$data), 'Preparation SocialEconomic Status.xlsx');
           
        }
        else{
            // return "yeha";
           return back()->withErrors(['msg', 'no data for downloading']);
        
        }
    }

    //Export Wash
    public function exportwash($id)
    {
        $data = Prep_household_detail::where('project_id',$id)->where('del_flag',0)
                                ->get();
        foreach ($data as &$value) {
            $value->district=Project::where('del_flag',0)
                            ->leftjoin('mst_districts','tbl_projects.district_id','=','mst_districts.id')
                            ->where('tbl_projects.id',$id)
                            ->first();
            $district_id = $value->district->id;
            $value->province=Project::where('del_flag',0)
                            ->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
                            ->where('tbl_projects.id',$value->id)
                            ->first();
            $value->vdc =DB::table('mst_vdcs')
                            ->where('district_id',$district_id)->pluck('name')->first();
            $value->projectname = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_name')->first();
            $value->project_no = Project::where('del_flag',0)->where('tbl_projects.id',$value->id)->pluck('project_no')->first();
            $value->wash = DB::table('prep_hh_wash')->where('house_id',$value->id)->get()->toArray();
            // dd($age);
        }
        // dd($data);
        return view("household_detail::export.wash",compact('data'));
        // return Excel::download(new HouseHoldExport("household_detail::export.wash",$data), 'Wash Detail.xlsx');
    }

}