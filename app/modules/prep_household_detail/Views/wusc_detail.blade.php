@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            WUSC Details 
            <small></small>                    
        </h1>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{route('wusc.detail.store',$id)}}"  method="post" enctype="multipart/form-data">
                <div class="box-body">     
                    <div class="form-group col-md-6">
                        <label for="">Toilet Monitoring Date</label>
                        <input type="text" name="toilet_monitoring_date" id="toilet_monitoring_date" class="form-control" autocomplete="off" value="{{@$wusc_details->toilet_monitoring_date}}" >
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Dish Drying Demonstration Date</label>
                        <input type="text" name="dish_drying_demonstration_date" id="dish_drying_demonstration" class="form-control" autocomplete="off" value="{{@$wusc_details->dish_drying_demonstration_date}}" >
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">Dish Drying Demonstration Type</label>
                        <select class="form-control" name="dish_drying_demonstration_type">
                            <option value="rack_build" {{@$wusc_details->dish_drying_demonstration_type == "rack_build" ? 'selected':''}}> Rack Build</option>
                            <option value="platform_build" {{@$wusc_details->dish_drying_demonstration_type == "platform_build" ? 'selected':''}}>Platform Build</option>
                        </select>
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">DWRC Registration Date</label>
                        <input type="text" name="dwrc_registration" id="dwrc_registration" class="form-control" autocomplete="off"  value="{{@$wusc_details->dwrc_registration}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Tripartie Agreement Date</label>
                        <input type="text" name="tripartie_agreement_date" id="tripartie_agreement" class="form-control" autocomplete="off"  value="{{@$wusc_details->tripartie_agreement_date}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Letter Of Intent Date</label>
                        <input type="text" name="letter_of_intent" id="letter_of_intent" class="form-control" autocomplete="off"  value="{{@$wusc_details->letter_of_intent}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">WUSC Pre-construction Training</label>
                        <input type="text" name="pre_construction" id="pre-construction" class="form-control" autocomplete="off"  value="{{@$wusc_details->pre_construction}}">
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">WUSC Pre-construction Participants</label>
                        <input type="text" name="pre_construction_participants" class="form-control" placeholder="Male & Female" autocomplete="off" value="{{@$wusc_details->pre_construction_participants}}" >
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">Average Cost(Latrines)</label>
                        <input type="number" name="average_cost" class="form-control" placeholder="Àverage Cost" autocomplete="off"  value="{{@$wusc_details->average_cost}}">
                        @if ($errors->has('average_cost'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('average_cost') }}</strong>
                            </span>
                        @endif
                    </div>  
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Dish Drying Racks</label>
                        <input type="number" name="number_dish_racks" class="form-control" placeholder="Total Number of Dish Drying Racks" autocomplete="off"  value="{{@$wusc_details->number_dish_racks}}">
                        @if ($errors->has('number_dish_racks'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_dish_racks') }}</strong>
                            </span>
                        @endif
                    </div>   
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Washing Platforms</label>
                        <input type="number" name="number_washing_platforms" class="form-control" placeholder="Total Number of Washing Platforms" autocomplete="off" value="{{@$wusc_details->number_washing_platforms}}" >
                        @if ($errors->has('number_washing_platforms'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_washing_platforms') }}</strong>
                            </span>
                        @endif
                    </div>   
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Waste disposal</label>
                        <input type="number" name="number_waste_disposal" class="form-control" placeholder="Total Number of Waste disposal" autocomplete="off"  value="{{@$wusc_details->number_waste_disposal}}">
                        @if ($errors->has('number_waste_disposal'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_waste_disposal') }}</strong>
                            </span>
                        @endif
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">Reflection of Water Supply Project in Gaunpalika Plan Date</label>
                        <input type="text" name="water_supply_plan_date" id="water_supply_plan_date" class="form-control" autocomplete="off"  value="{{@$wusc_details->water_supply_plan_date}}">
                    </div>          
                    <div class="form-group col-md-6">
                        <label for="">Total O&M Fund</label>
                        <input type="number" name="om_fund" class="form-control" placeholder="Total O&M Fund" autocomplete="off" value="{{@$wusc_details->om_fund}}" >
                        @if ($errors->has('om_fund'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('om_fund') }}</strong>
                            </span>
                        @endif
                    </div>    
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>   
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong></strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <label><h4><strong>Name</strong></h4></label>
                        </div>
                        
                        <div class="col-md-1">
                            <label><h4><strong>Gender</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <label><h4><strong>Cast</strong></h4></label>
                        </div>
                        
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>WUSC CHAIRMAN</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="chairman_name" placeholder=" Name" value="{{@$wusc_details->chairman_name}}">
                        </div>
                        <div class="col-md-1">
                            <select class="form-control" name="chairman_gender">
                             <option value="Male" {{@$wusc_details->chairman_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->chairman_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="chairman_cast">
                             <option value="upper" {{@$wusc_details->chairman_cast == "upper" ? 'selected':''}}>UpperCast</option>   
                             <option value="raj" {{@$wusc_details->chairman_cast == "raj" ? 'selected':''}}>Raj</option>   
                             <option value="rdj" {{@$wusc_details->chairman_cast == "rdj" ? 'selected':''}}>RDJ</option>   
                             <option value="dalit" {{@$wusc_details->chairman_cast == "dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Vice CHAIRMAN</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="vice_chairman_name" placeholder=" Name" value="{{@$wusc_details->vice_chairman_name}}">
                        </div>
                        <div class="col-md-1">
                            <select class="form-control" name="vice_chairman_gender">
                             <option value="vice_chairman_male" {{@$wusc_details->vice_chairman_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="vice_chairman_female" {{@$wusc_details->vice_chairman_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_chairman_cast">
                             <option value="upper" {{@$wusc_details->vice_chairman_cast == "upper" ? 'selected':''}}>UpperCast</option>   
                             <option value="raj" {{@$wusc_details->vice_chairman_cast == "raj" ? 'selected':''}}>Raj</option>   
                             <option value="rdj" {{@$wusc_details->vice_chairman_cast == "rdj" ? 'selected':''}}>RDJ</option>   
                             <option value="dalit" {{@$wusc_details->vice_chairman_cast == "dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Secretary</strong></h4></label>
                        </div>
                       <div class="col-md-2">
                            <input type="text" class="form-control" name="secretary_name" placeholder=" Name" value="{{@$wusc_details->secretary_name}}">
                        </div>
                        <div class="col-md-1">
                            <select class="form-control" name="secretary_gender">
                             <option value="Male" {{@$wusc_details->secretary_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->secretary_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="secretary_cast">
                             <option value="upper" {{@$wusc_details->secretary_cast == "upper" ? 'selected':''}}>UpperCast</option>   
                             <option value="raj" {{@$wusc_details->secretary_cast == "raj" ? 'selected':''}}>Raj</option>   
                             <option value="rdj" {{@$wusc_details->secretary_cast == "rdj" ? 'selected':''}}>RDJ</option>   
                             <option value="dalit" {{@$wusc_details->secretary_cast == "dalit" ? 'selected':''}}> Dalit </option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Vice Secretary</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="vice_secretary_name" placeholder=" Name" value="{{@$wusc_details->vice_secretary_name}}">
                        </div>
                        <div class="col-md-1">
                            <select class="form-control" name="vice_secretary_gender">
                             <option value="Male" {{@$wusc_details->vice_secretary_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->vice_secretary_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_secretary_cast">
                             <option value="upper" {{@$wusc_details->vice_secretary_cast == "upper" ? 'selected':''}}>UpperCast</option>   
                             <option value="raj" {{@$wusc_details->vice_secretary_cast == "raj" ? 'selected':''}}>Raj</option>   
                             <option value="rdj" {{@$wusc_details->vice_secretary_cast == "rdj" ? 'selected':''}}>RDJ</option>   
                             <option value="dalit" {{@$wusc_details->vice_secretary_cast == "dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Treasurer</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="treasurer_name" placeholder=" Name" value="{{@$wusc_details->treasurer_name}}">
                        </div>
                        <div class="col-md-1">
                            <select class="form-control" name="treasurer_gender">
                             <option value="Male" {{@$wusc_details->treasurer_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->treasurer_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="treasurer_cast">
                             <option value="upper" {{@$wusc_details->treasurer_cast == "upper" ? 'selected':''}}>UpperCast</option>   
                             <option value="raj" {{@$wusc_details->treasurer_cast == "raj" ? 'selected':''}}>Raj</option>   
                             <option value="rdj" {{@$wusc_details->treasurer_cast == "rdj" ? 'selected':''}}>RDJ</option>   
                             <option value="dalit" {{@$wusc_details->treasurer_cast == "dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Member (VHP)</strong></h4></label>
                        </div>
                        <div class="col-md-6" >
                            @if(isset($vhp_members))
                                @foreach($vhp_members as $members)
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="member_vhp_name[]" value="{{@$members->name}}" id="member_vhp_name_1" style="margin-bottom: 10px;">
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="member_vhp_gender[]" id="member_vhp_gender_1" style="margin-bottom: 10px;">
                                         <option value="Male" {{@$members->gender == "Male" ? 'selected':''}}>Male</option>   
                                         <option value="Female" {{@$members->gender == "Female" ? 'selected':''}}> Female</option>   
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="member_vhp_cast[]" id="member_vhp_cast_1" style="margin-bottom: 10px;">
                                         <option value="upper" {{@$members->cast == "upper" ? 'selected':''}}>UpperCast</option>   
                                         <option value="raj" {{@$members->cast == "raj" ? 'selected':''}}>Raj</option>   
                                         <option value="rdj" {{@$members->cast == "rdj" ? 'selected':''}}>RDJ</option>   
                                         <option value="dalit" {{@$members->cast == "dalit" ? 'selected':''}}>Dalit</option>   
                                        </select>
                                    </div>
                                    <input type="hidden" name="vhp_id[]" value="{{ $members->id }}">
                                @endforeach
                            @else
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="member_vhp_name[]" placeholder="Name" id="member_vhp_name_1" style="margin-bottom: 10px;">
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="member_vhp_gender[]" id="member_vhp_gender_1" style="margin-bottom: 10px;">
                                     <option value="Male">Male</option>   
                                     <option value="Female"> Female</option>   
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="member_vhp_cast[]" id="member_vhp_cast_1" style="margin-bottom: 10px;">
                                     <option value="upper">UpperCast</option>   
                                     <option value="raj">Raj</option>   
                                     <option value="rdj">RDJ</option>   
                                     <option value="dalit">Dalit</option>   
                                    </select>
                                </div>
                            @endif
                            <div id="first"></div>
                        </div>
                        
                        <div class="row">
                            <button type="button" id="btn_add_vhp" class="btn btn-primary ">Add More</button>
                            <button type="button" id="btn_del_vhp" class="btn btn-danger ">Remove</button>
                        </div>
                    </div>
                    
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>VMW</strong></h4></label>
                        </div>
                        <div class="col-md-6" >
                            @if(isset($vmw_members))
                                @foreach($vmw_members as $members)
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="vmw_name[]" value="{{@$members->name}}" id="vmw_name_1" style="margin-bottom: 10px;">
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="vmw_gender[]" id="vmw_gender_1" style="margin-bottom: 10px;">
                                         <option value="Male" {{@$members->gender == "Male" ? 'selected':''}}>Male</option>   
                                         <option value="Female" {{@$members->gender == "Female" ? 'selected':''}}> Female</option>   
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="vmw_cast[]" id="vmw_cast_1" style="margin-bottom: 10px;">
                                         <option value="upper" {{@$members->cast == "upper" ? 'selected':''}}>UpperCast</option>   
                                         <option value="raj" {{@$members->cast == "raj" ? 'selected':''}}>Raj</option>   
                                         <option value="rdj" {{@$members->cast == "rdj" ? 'selected':''}}>RDJ</option>   
                                         <option value="dalit" {{@$members->cast == "dalit" ? 'selected':''}}>Dalit</option>   
                                        </select>
                                    </div>
                                    <input type="hidden" name="vmw_id[]" value="{{ $members->id }}">
                                @endforeach
                            @else
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="vmw_name[]" placeholder="Name" id="vmw_name_1" style="margin-bottom: 10px;">
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="vmw_gender[]" id="vmw_gender_1" style="margin-bottom: 10px;">
                                     <option value="Male">Male</option>   
                                     <option value="Female"> Female</option>   
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="vmw_cast[]" id="vmw_cast_1" style="margin-bottom: 10px;">
                                     <option value="upper">UpperCast</option>   
                                     <option value="raj">Raj</option>   
                                     <option value="rdj">RDJ</option>   
                                     <option value="dalit">Dalit</option>   
                                    </select>
                                </div>
                            @endif
                            <div id="second"></div>
                        </div>
                        
                        <div class="row">
                            <button type="button" id="btn_add_vmw" class="btn btn-primary ">Add More</button>
                            <button type="button" id="btn_del_vmw" class="btn btn-danger ">Remove</button>
                        </div>
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>CSS</strong></h4></label>
                        </div>
                        <div class="col-md-6" >
                            @if(isset($css_members))
                                @foreach($css_members as $members)
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="css_name[]" value="{{@$members->name}}" id="css_name_1" style="margin-bottom: 10px;">
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="css_gender[]" id="css_gender_1" style="margin-bottom: 10px;">
                                         <option value="Male" {{@$members->gender == "Male" ? 'selected':''}}>Male</option>   
                                         <option value="Female" {{@$members->gender == "Female" ? 'selected':''}}> Female</option>   
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="css_cast[]" id="css_cast_1" style="margin-bottom: 10px;">
                                         <option value="upper" {{@$members->cast == "upper" ? 'selected':''}}>UpperCast</option>   
                                         <option value="raj" {{@$members->cast == "raj" ? 'selected':''}}>Raj</option>   
                                         <option value="rdj" {{@$members->cast == "rdj" ? 'selected':''}}>RDJ</option>   
                                         <option value="dalit" {{@$members->cast == "dalit" ? 'selected':''}}>Dalit</option>   
                                        </select>
                                    </div>
                                    <input type="hidden" name="css_id[]" value="{{ $members->id }}">
                                @endforeach
                            @else
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="css_name[]" placeholder="Name" id="css_name_1" style="margin-bottom: 10px;">
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="css_gender[]" id="css_gender_1" style="margin-bottom: 10px;">
                                     <option value="Male">Male</option>   
                                     <option value="Female"> Female</option>   
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="css_cast[]" id="css_cast_1" style="margin-bottom: 10px;">
                                     <option value="upper">UpperCast</option>   
                                     <option value="raj">Raj</option>   
                                     <option value="rdj">RDJ</option>   
                                     <option value="dalit">Dalit</option>   
                                    </select>
                                </div>
                            @endif
                            <div id="third"></div>
                        </div>
                        
                        <div class="row">
                            <button type="button" id="btn_add_css" class="btn btn-primary ">Add More</button>
                            <button type="button" id="btn_del_css" class="btn btn-danger ">Remove</button>
                        </div>
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>WSP Team</strong></h4></label>
                        </div>
                        <div class="col-md-6" >
                            @if(isset($wsp_members))
                            <?php $i = 1;?>
                                @foreach($wsp_members as $members)
                                    <div class="col-md-5">
                                        <input type="text" class="form-control" name="wsp_name[]" value="{{@$members->name}}" id="wsp_name_1" style="margin-bottom: 10px;">
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control" name="wsp_gender[]" id="wsp_gender_1" style="margin-bottom: 10px;">
                                         <option value="Male" {{@$members->gender == "Male" ? 'selected':''}}>Male</option>   
                                         <option value="Female" {{@$members->gender == "Female" ? 'selected':''}}> Female</option>   
                                        </select>
                                    </div>
                                    <div class="col-md-5">
                                        <select class="form-control" name="wsp_cast[]" id="wsp_cast_1" style="margin-bottom: 10px;">
                                         <option value="upper" {{@$members->cast == "upper" ? 'selected':''}}>UpperCast</option>   
                                         <option value="raj" {{@$members->cast == "raj" ? 'selected':''}}>Raj</option>   
                                         <option value="rdj" {{@$members->cast == "rdj" ? 'selected':''}}>RDJ</option>   
                                         <option value="dalit" {{@$members->cast == "dalit" ? 'selected':''}}>Dalit</option>   
                                        </select>
                                    </div>
                                    <input type="hidden" name="wsp_id[]" value="{{ $members->id }}">
                                @endforeach
                            @else
                                <div class="col-md-5">
                                    <input type="text" class="form-control" name="wsp_name[]" placeholder="Name" id="wsp_name_1" style="margin-bottom: 10px;">
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="wsp_gender[]" id="wsp_gender_1" style="margin-bottom: 10px;">
                                     <option value="Male">Male</option>   
                                     <option value="Female"> Female</option>   
                                    </select>
                                </div>
                                <div class="col-md-5">
                                    <select class="form-control" name="wsp_cast[]" id="wsp_cast_1" style="margin-bottom: 10px;">
                                     <option value="upper">UpperCast</option>   
                                     <option value="raj">Raj</option>   
                                     <option value="rdj">RDJ</option>   
                                     <option value="dalit">Dalit</option>   
                                    </select>
                                </div>
                            @endif
                            <div id="forth"></div>
                        </div>
                        
                        <div class="row">
                            <button type="button" id="btn_add_wsp" class="btn btn-primary ">Add More</button>
                            <button type="button" id="btn_del_wsp" class="btn btn-danger ">Remove</button>
                        </div>
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Water Analysis Report</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="file" name="water_analysis_report">
                        </div>
                        <input type="hidden" name="oldImage" value="{{(@$wusc_details->water_analysis_report)?$wusc_details->water_analysis_report:''}}">
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <label for="school_data" class="col-sm-12 col-form-label"><h3><strong><u>Agreement</u></strong></h3></label>   
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_water_structure" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_water_structure == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>Agreement of Water Structures</h4></label>
                        </div>
                        <div class="form-group col-md-7">
                            <div class="col-md-4">
                                <label for="home_cleaning"><h4>Agreement of Water Structures (PIC)</h4></label>   
                            </div>
                            <div class="col-md-6">
                                <input type="file" name="agreement_pic" style="padding-top: 10px;">
                                <input type="hidden" name="agreement_pic_oldImage" value="{{(@$wusc_details->agreement_pic)?$wusc_details->agreement_pic:''}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_pipeline" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_pipeline == 1 ? 'checked':''}}>
                            <label for="agreement_pipeline"><h4>Pipeline Agreement</h4></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_mpc_tap" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_mpc_tap == 1 ? 'checked':''}}>
                            <label for="agreement_mpc_tap"><h4>MPC Tap Agreement</h4></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_other_tap" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_other_tap == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>(PTS/MPC/NPC) Agreement for Taps</h4></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_contribution" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_contribution == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>Contribution Form </h4></label>
                        </div>
                    </div>
                <input type="hidden" name="id" id="id" />
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.projects.show',$id)}}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>

    <script type="text/javascript">
        var globel = 1;
        var vmw_globel = 1;
        var css_globel = 1;
        var wsp_globel = 1;
        $(document).ready(function() {
            $("#toilet_monitoring_date,#dish_drying_demonstration,#dwrc_registration,#tripartie_agreement,#letter_of_intent,#pre-construction,#water_supply_plan_date").daterangepicker({
                singleDatePicker:!0,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });     
        });
        $('#btn_add_vhp').click(function(){
                var id = globel;
                id++;
                globel++;
                $('#first').before(
                    '<div class="form-group added-row-vhp" >'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<input type="text" class="form-control" name="member_vhp_name[]" placeholder="Name" id="member_vhp_name_'+id+'">'
                        +'</div>'
                        +'<div class="col-md-2" style="margin-top: 10px;">'
                            +'<select class="form-control" name="member_vhp_gender[]" id="member_vhp_gender_'+id+'">'
                             +'<option value="Male">Male</option>'   
                             +'<option value="Female"> Female</option> '  
                            +'</select>'
                        +'</div>'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<select class="form-control" name="member_vhp_cast[]" id="member_vhp_cast_'+id+'">'
                             +'<option value="upper">UpperCast</option>'   
                             +'<option value="raj">Raj</option>'   
                             +'<option value="rdj">RDJ</option>'   
                             +'<option value="dalit">Dalit</option>'   
                            +'</select>'
                        +'</div>'
                    +'</div>'
                );
            });

        $('#btn_add_vmw').click(function(){
                var id = vmw_globel;
                id++;
                vmw_globel++;
                $('#second').before(
                    '<div class="form-group added-row-vmw" >'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<input type="text" class="form-control" name="vmw_name[]" placeholder="Name" id="vmw_name_'+id+'">'
                        +'</div>'
                        +'<div class="col-md-2" style="margin-top: 10px;">'
                            +'<select class="form-control" name="vmw_gender[]" id="vmw_gender_'+id+'">'
                             +'<option value="Male">Male</option>'   
                             +'<option value="Female"> Female</option> '  
                            +'</select>'
                        +'</div>'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<select class="form-control" name="vmw_cast[]" id="vmw_cast_'+id+'">'
                             +'<option value="upper">UpperCast</option>'   
                             +'<option value="raj">Raj</option>'   
                             +'<option value="rdj">RDJ</option>'   
                             +'<option value="dalit">Dalit</option>'   
                            +'</select>'
                        +'</div>'
                    +'</div>'
                );
            });

        $('#btn_add_css').click(function(){
                var id = css_globel;
                id++;
                css_globel++;
                $('#third').before(
                    '<div class="form-group added-row-css" >'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<input type="text" class="form-control" name="css_name[]" placeholder="Name" id="css_name_'+id+'">'
                        +'</div>'
                        +'<div class="col-md-2" style="margin-top: 10px;">'
                            +'<select class="form-control" name="css_gender[]" id="css_gender_'+id+'">'
                             +'<option value="Male">Male</option>'   
                             +'<option value="Female"> Female</option> '  
                            +'</select>'
                        +'</div>'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<select class="form-control" name="css_cast[]" id="css_cast_'+id+'">'
                             +'<option value="upper">UpperCast</option>'   
                             +'<option value="raj">Raj</option>'   
                             +'<option value="rdj">RDJ</option>'   
                             +'<option value="dalit">Dalit</option>'   
                            +'</select>'
                        +'</div>'
                    +'</div>'
                );
            });
        $('#btn_add_wsp').click(function(){
                var id = wsp_globel;
                id++;
                wsp_globel++;
                $('#forth').before(
                    '<div class="form-group added-row-wsp" >'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<input type="text" class="form-control" name="wsp_name[]" placeholder="Name" id="wsp_name_'+id+'">'
                        +'</div>'
                        +'<div class="col-md-2" style="margin-top: 10px;">'
                            +'<select class="form-control" name="wsp_gender[]" id="wsp_gender_'+id+'">'
                             +'<option value="Male">Male</option>'   
                             +'<option value="Female"> Female</option> '  
                            +'</select>'
                        +'</div>'
                        +'<div class="col-md-5" style="margin-top: 10px;">'
                            +'<select class="form-control" name="wsp_cast[]" id="wsp_cast_'+id+'">'
                             +'<option value="upper">UpperCast</option>'   
                             +'<option value="raj">Raj</option>'   
                             +'<option value="rdj">RDJ</option>'   
                             +'<option value="dalit">Dalit</option>'   
                            +'</select>'
                        +'</div>'
                    +'</div>'
                );
            });
        $('#btn_del_vhp').click(function(){
                $('.added-row-vhp').last().remove();
                if(globel >1){
                    globel--;
                }
            });
        $('#btn_del_vmw').click(function(){
                $('.added-row-vmw').last().remove();
                if(vmw_globel >1){
                    vmw_globel--;
                }
            });
        $('#btn_del_css').click(function(){
                $('.added-row-css').last().remove();
                if(css_globel >1){
                    css_globel--;
                }
            });
        $('#btn_del_wsp').click(function(){
                $('.added-row-wsp').last().remove();
                if(wsp_globel >1){
                    wsp_globel--;
                }
            });

            
    </script>
@endsection
