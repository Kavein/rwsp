@extends('admin.layout.main')
@section('content')
<section class="content-header">
    <h1>
       Survey and Design
                         
    </h1>
</section>
<section class="content">
<form role="form" action="{{route('store_survey',$id) }}"  method="post" enctype="multipart/form-data">
    <input type="hidden" name="id" id="id" class="form-control"value="{{ $id }}" autocomplete="off" >   
    <div class="box-body"> 
            <div class="form-group col-md-12" >
                <label for=""><h3><strong>Survey  and Design</strong></h3></label>
            </div> 
            <div class="form-group col-md-6" >
                <label for="survey_date">Survey Date</label>
                <input type="text" name="survey_date" id="survey_date" class="form-control" value="{{@$survey_design->survey_date}}" autocomplete="off" >
            </div>  
            <div class="form-group col-md-6" >
                <label for="design_date">Design Date</label>
                <input type="text" name="design_date"  id="design_date" class="form-control" autocomplete="off" value="{{@$survey_design->design_date}}">
            </div>  
            <div class="form-group col-md-12" >
                <label for="source_assessment">Source Assessment </label>
                <textarea name="source_assessment" id="source_assessment">{!! @$survey_design->source_assessment !!}</textarea>
            </div>
            <div class="form-group col-md-12" >
                <label for="water_structure">Water Structure Details (RT, CT, tap stands)</label>
                <textarea name="water_structure" id="water_structure">{!! @$survey_design->water_structure !!}</textarea>
            </div>
            <div class="form-group col-md-12" >
                <label for="pipeline_length">Length of Pipeline</label>
                <input type="number" name="pipeline_length" id="pipeline_length" class="form-control" autocomplete="off" value="{{@$survey_design->pipeline_length}}" >
                @if ($errors->has('pipeline_length'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('pipeline_length') }}</strong>
                </span>
                @endif  
            </div> 
            <div class="form-group col-md-12" >
                <label for="gps_data"> GPS data</label>
                <textarea name="gps_data" id="gps_data">{!! @$survey_design->water_structure !!}</textarea>
            </div>
            
            <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>
            
            <div class="form-group col-md-12" >
                <label for=""><h3><strong>Technical Design</strong></h3></label>
            </div>                              
            <div class="col-sm-12 row">
                <label class="col-sm-4">Village Sketch Map (Upload photo)</label>
                <input type="file" name="design_pic" id="design_pic" class="col-sm-8">
            </div>
            {{--
                <div class="form-group col-md-12" >
                    <label for="design_agreement_date">Design agreement with WUSC(Date) </label>
                    <input type="text" name="design_agreement_date" id="design_agreement_date" class="form-control" autocomplete="off" value="{{@$survey_design->design_agreement_date}}">
                </div> 
            --}}
            <div class="form-group col-md-12">
                <label for="water_source_record">Record of water source yield</label>
            <div class="radio">
                <label><input type="radio" value="1" name="water_source_record" {{@$survey_design->water_source_record == "1" ? 'checked':''}}/>Yes</label> 
                <label><input type="radio" value="0" name="water_source_record" {{@$survey_design->water_source_record == "0" ? 'checked':''}}/>No</label>
            </div>
            </div>
           <div class="form-group col-md-12" >
            
                <label for="variation"><h4><strong>Variation of tap stands records:</strong></h4></label>
           </div>
            <div class="form-group col-md-12">
                <label for="variation"> Variation of tap stands</label>
                <input type="text" name="variation_taps" id="variation_taps" class="form-control" placeholder=" Insert the name of the households" autocomplete="off" value="{{@$survey_design->variation_households}}">
                @if ($errors->has('variation_taps'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('variation_taps') }}</strong>
                    </span>
                @endif   
            </div>
                <div class="form-group col-md-6">
                       <label for="variation"> Number of HHs</label>
                <input type="text" name="variation_households" id="variation_households" class="form-control" placeholder=" Insert the name of the households" autocomplete="off" value="{{@$survey_design->variation_households}}">
                @if ($errors->has('variation_households'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('variation_households') }}</strong>
                </span>
                @endif   
            </div>
                <div class="form-group col-md-6">
                        <label for="variation"> Number of Pop</label>
                        <input type="text" name="variation_popn" id="variation_popn" class="form-control" placeholder=" Insert the name of the population" autocomplete="off" value="{{@$survey_design->variation_popn}}">
                        @if ($errors->has('variation_popn'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('variation_popn') }}</strong>
                        </span>
                        @endif
                    </div>
          
            <input type="hidden" name="oldImage" value="{{(@$survey_design->design_pic)?$survey_design->design_pic:''}}">


            {{ csrf_field() }}
        <div class="box-footer">
            <button type="submit" class="btn btn-primary">Save</button>
            <a href="" class="btn btn-danger">Cancel</a>
        </div>
    </div>
        </form>    

</section>
<script type="text/javascript">
    $(document).ready(function() {
        $("#survey_date,#design_date,#design_agreement_date").daterangepicker({
            singleDatePicker:!0,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });     
    });
    $(function () {
            CKEDITOR.replace('source_assessment');
            CKEDITOR.replace('water_structure');
            CKEDITOR.replace('gps_data');
            uploadReady();
        });
    </script>
@endsection