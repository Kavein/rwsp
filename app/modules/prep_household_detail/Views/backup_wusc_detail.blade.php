@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            WUSC Details 
            <small></small> 
                           
        </h1>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{route('wusc.detail.store',$id)}}" id="form" method="post" enctype="multipart/form-data">
                <input type="hidden" name="id" id="wuscid" class="form-control" value="{{ $id }}" autocomplete="off"  >  
                <input type="hidden" name="membercounter" id="membercounter" class="form-control" value="" autocomplete="off"  > 
                <div class="box-body">     
                    <div class="form-group col-md-6">
                           
                        <label for="">Toilet Monitoring Date</label>
                        <input type="text" name="toilet_monitoring_date" id="toilet_monitoring_date" class="form-control" autocomplete="off" value="{{@$wusc_details->toilet_monitoring_date}}" >
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Dish Drying Demonstration Date</label>
                        <input type="text" name="dish_drying_demonstration_date" id="dish_drying_demonstration" class="form-control" autocomplete="off" value="{{@$wusc_details->dish_drying_demonstration_date}}" >
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">Dish Drying Demonstration Type</label>
                        <select class="form-control" name="dish_drying_demonstration_type">
                            <option value="rack_build" {{@$wusc_details->dish_drying_demonstration_type == "rack_build" ? 'selected':''}}> Rack Build</option>
                            <option value="platform_build" {{@$wusc_details->dish_drying_demonstration_type == "platform_build" ? 'selected':''}}>Platform Build</option>
                        </select>
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">DWRC Registration Date</label>
                        <input type="text" name="dwrc_registration" id="dwrc_registration" class="form-control" autocomplete="off"  value="{{@$wusc_details->dwrc_registration}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Tripartie Agreement Date</label>
                        <input type="text" name="tripartie_agreement_date" id="tripartie_agreement" class="form-control" autocomplete="off"  value="{{@$wusc_details->tripartie_agreement_date}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">Letter Of Intent Date</label>
                        <input type="text" name="letter_of_intent" id="letter_of_intent" class="form-control" autocomplete="off"  value="{{@$wusc_details->letter_of_intent}}">
                    </div>       
                    <div class="form-group col-md-6">
                        <label for="">WUSC Pre-construction Training</label>
                        <input type="text" name="pre_construction" id="pre-construction" class="form-control" autocomplete="off"  value="{{@$wusc_details->pre_construction}}">
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">WUSC Pre-construction Participants</label>
                        <input type="text" name="pre_construction_participants" class="form-control" placeholder="Male & Female" autocomplete="off" value="{{@$wusc_details->pre_construction_participants}}" >
                    </div>
                    <div class="form-group col-md-6">
                        <label for="">Average Cost(Latrines)</label>
                        <input type="number" name="average_cost" class="form-control" placeholder="Àverage Cost" autocomplete="off"  value="{{@$wusc_details->average_cost}}">
                        @if ($errors->has('average_cost'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('average_cost') }}</strong>
                            </span>
                        @endif
                    </div>  
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Dish Drying Racks</label>
                        <input type="number" name="number_dish_racks" class="form-control" placeholder="Total Number of Dish Drying Racks" autocomplete="off"  value="{{@$wusc_details->number_dish_racks}}">
                        @if ($errors->has('number_dish_racks'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_dish_racks') }}</strong>
                            </span>
                        @endif
                    </div>   
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Washing Platforms</label>
                        <input type="number" name="number_washing_platforms" class="form-control" placeholder="Total Number of Washing Platforms" autocomplete="off" value="{{@$wusc_details->number_washing_platforms}}" >
                        @if ($errors->has('number_washing_platforms'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_washing_platforms') }}</strong>
                            </span>
                        @endif
                    </div>   
                    <div class="form-group col-md-6">
                        <label for="">Total Number of Waste disposal</label>
                        <input type="number" name="number_waste_disposal" class="form-control" placeholder="Total Number of Waste disposal" autocomplete="off"  value="{{@$wusc_details->number_waste_disposal}}">
                        @if ($errors->has('number_waste_disposal'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('number_waste_disposal') }}</strong>
                            </span>
                        @endif
                    </div> 
                    <div class="form-group col-md-6">
                        <label for="">Reflection of Water Supply Project in Gaunpalika Plan Date</label>
                        <input type="text" name="water_supply_plan_date" id="water_supply_plan_date" class="form-control" autocomplete="off"  value="{{@$wusc_details->water_supply_plan_date}}">
                    </div>          
                    <div class="form-group col-md-6">
                        <label for="">Total O&M Fund</label>
                        <input type="number" name="om_fund" class="form-control" placeholder="Total O&M Fund" autocomplete="off" value="{{@$wusc_details->om_fund}}" >
                        @if ($errors->has('om_fund'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('om_fund') }}</strong>
                            </span>
                        @endif
                    </div>    
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>   
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong></strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <label><h4><strong>Name</strong></h4></label>
                        </div>
                        
                        <div class="col-md-1">
                            <label><h4><strong>Gender</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <label><h4><strong>Cast</strong></h4></label>
                        </div>
                        
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>WUSC CHAIRMAN</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="chairman_name" placeholder=" Name" value="{{@$wusc_details->chairman_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="chairman_gender">
                             <option value="Male" {{@$wusc_details->chairman_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->chairman_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="chairman_cast">
                             <option value="UpperCast" {{@$wusc_details->chairman_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->chairman_cast == "RajRaj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->chairman_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->chairman_cast == "chairman_dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                        
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Vice CHAIRMAN</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="vice_chairman_name" placeholder=" Name" value="{{@$wusc_details->vice_chairman_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_chairman_gender">
                             <option value="Male" {{@$wusc_details->vice_chairman_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->vice_chairman_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_chairman_cast">
                             <option value="UpperCast" {{@$wusc_details->vice_chairman_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->vice_chairman_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->vice_chairman_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->vice_chairman_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Secretary</strong></h4></label>
                        </div>
                       <div class="col-md-2">
                            <input type="text" class="form-control" name="secretary_name" placeholder=" Name" value="{{@$wusc_details->secretary_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="secretary_gender">
                             <option value="Male" {{@$wusc_details->secretary_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->secretary_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="secretary_cast">
                             <option value="UpperCast" {{@$wusc_details->secretary_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->secretary_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->secretary_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->secretary_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Vice Secretary</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="vice_secretary_name" placeholder=" Name" value="{{@$wusc_details->vice_secretary_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_secretary_gender">
                             <option value="Male" {{@$wusc_details->vice_secretary_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->vice_secretary_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vice_secretary_cast">
                             <option value="UpperCast" {{@$wusc_details->vice_secretary_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->vice_secretary_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->vice_secretary_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->vice_secretary_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Treasurer</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="treasurer_name" placeholder=" Name" value="{{@$wusc_details->treasurer_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="treasurer_gender">
                             <option value="Male" {{@$wusc_details->treasurer_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->treasurer_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="treasurer_cast">
                             <option value="UpperCast" {{@$wusc_details->treasurer_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->treasurer_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->treasurer_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->treasurer_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Member (VHP)</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="member_vhp_name" placeholder=" Name" value="{{@$wusc_details->member_vhp_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="member_vhp_gender">
                             <option value="Male" {{@$wusc_details->member_vhp_gender == "Male" ? 'selected':''  }}>Male</option>   
                             <option value="Female" {{@$wusc_details->member_vhp_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="member_vhp_cast">
                             <option value="UpperCast" {{@$wusc_details->member_vhp_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->member_vhp_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->member_vhp_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->member_vhp_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    {{--here--}}
                    
                    @if(!isset($wusc_details_members))
                    {{-- {{ $wusc_details_members }} --}}
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Member</strong></h4></label>
                        </div>
                       
                        <input type="button" name="submit" id="submit" value="Add members"   class="btn btn-primary" onclick="add_wusc()">
                    </div>

                    <div class="form-group col-md-3" id="second"></div>
                    <input type="button" name="remove_add_member" id="remove_add_member" value="Remove"   class="btn btn-primary" onclick="remove()">
                    @elseif(@$wusc_details_members[0]->wusc_details_id !=$wusc_details->id)
                    {{-- {{ $wusc_details_members }} --}}
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Member</strong></h4></label>
                        </div>
                        
                        <input type="button" name="submit" id="submit" value="Add members"   class="btn btn-primary" onclick="add_wusc()">
                    </div>
                    <div class="form-group col-md-3" id="second"></div>
                    <input type="button" name="remove_add_member" id="remove_add_member" value="Remove"   class="btn btn-primary" onclick="remove()">
                        @else
                        <div id="original">
                                @php $wusc=true;  $existing_member_count=0;@endphp
                                {{-- {{$wusc_details_members}} --}}
                                <input type="hidden" class="form-control" name="existing_member_count" id="existing_member_count" value="{{count($wusc_details_members)}}" >
                            @foreach($wusc_details_members as $wusc_details_member )
                            
                            <div class="form-group col-md-12">
                                <div class="col-md-2">
                                        @if($wusc) <label><h4><strong>Member</strong></h4></label>@endif
                                </div>
                            <input type="hidden" class="form-control" name="member_id{{$existing_member_count}}" id="member_id{{$existing_member_count}}" value="{{@$wusc_details_member->id}}" >
                                <div class="col-md-2">
                                <input type="text" class="form-control" name="member_name{{$existing_member_count}}" id="member_name{{$existing_member_count}}" placeholder=" Name" value="{{@$wusc_details_member->member_name}}" >
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="member_gender{{$existing_member_count}}" id="member_gender{{$existing_member_count}}" >
                                    <option value="Male" @if(@$wusc_details_member->member_gender == "Male") {{"selected"}}@endif>Male</option>   
                                    <option value="Female" @if(@$wusc_details_member->member_gender == "Female") {{"selected"}} @endif>Female</option>   
                                    </select>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="member_cast{{$existing_member_count}}" id="member_cast"{{$existing_member_count}} >
                                    <option value="UpperCast" @if(@$wusc_details_member->member_cast == "UpperCast") {{"selected"}} @endif>UpperCast</option>   
                                    <option value="Raj" @if(@$wusc_details_member->member_cast == "Raj") {{"selected"}} @endif>Raj</option>   
                                    <option value="RDJ" @if(@$wusc_details_member->member_cast == "RDJ") {{"selected"}} @endif>RDJ</option>   
                                    <option value="Dalit" @if(@$wusc_details_member->member_cast == "Dalit") {{"selected"}} @endif>Dalit</option>   
                                    </select>
                                </div>
                                
                                <div class="form-group col-md-3">
                                        @if($wusc) <input type="button" name="submit" id="submit" value="Add more"   class="btn btn-primary" onclick="add_wusc()"> @endif
                                </div>
                            </div>
                            @php $wusc=false; $existing_member_count++; @endphp
                            @endforeach
                        </div>
                        <div class="form-group col-md-3" id="second"></div>
                        <input type="button" name="remove_add_member" id="remove_add_member" value="Remove"   class="btn btn-primary" onclick="remove()">
                    @endif
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    {{-- end --}}

                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>VMW</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="vmw_name" placeholder=" Name" value="{{@$wusc_details->vmw_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vmw_gender">
                             <option value="Male" {{@$wusc_details->vmw_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->vmw_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="vmw_cast">
                             <option value="UpperCast" {{@$wusc_details->vmw_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->vmw_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->vmw_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->vmw_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>CSS</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="css_name" placeholder=" Name" value="{{@$wusc_details->css_name}}">
                        </div>
                        <div class="col-md-2" name="css_gender">
                            <select class="form-control">
                             <option value="Male" {{@$wusc_details->css_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->css_gender == "Female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="css_cast">
                             <option value="UpperCast" {{@$wusc_details->css_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->css_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->css_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->css_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>

                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>WSP Team</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="wsp_name" placeholder=" Name" value="{{@$wusc_details->wsp_name}}">
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="wsp_gender">
                             <option value="Male" {{@$wusc_details->wsp_gender == "Male" ? 'selected':''}}>Male</option>   
                             <option value="Female" {{@$wusc_details->wsp_gender == "wsp_female" ? 'selected':''}}>Female</option>   
                            </select>
                        </div>
                        <div class="col-md-2">
                            <select class="form-control" name="wsp_cast">
                             <option value="UpperCast" {{@$wusc_details->css_cast == "UpperCast" ? 'selected':''}}>UpperCast</option>   
                             <option value="Raj" {{@$wusc_details->css_cast == "Raj" ? 'selected':''}}>Raj</option>   
                             <option value="RDJ" {{@$wusc_details->css_cast == "RDJ" ? 'selected':''}}>RDJ</option>   
                             <option value="Dalit" {{@$wusc_details->css_cast == "Dalit" ? 'selected':''}}>Dalit</option>   
                            </select>
                        </div>
                    </div>
                    <div class="form-group col-md-12">
                        <div class="col-md-2">
                            <label><h4><strong>Water Analysis Report</strong></h4></label>
                        </div>
                        <div class="col-md-2">
                            <input type="file" name="water_analysis_report">
                        </div>
                        <input type="hidden" name="oldImage" value="{{(@$wusc_details->water_analysis_report)?$wusc_details->water_analysis_report:''}}">
                    </div>
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <label for="school_data" class="col-sm-12 col-form-label"><h3><strong><u>Agreement</u></strong></h3></label>   
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_water_structure" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_water_structure == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>Agreement of Water Structures</h4></label>
                        </div>
                        <div class="form-group col-md-7">
                            <div class="col-md-4">
                                <label for="home_cleaning"><h4>Agreement of Water Structures (PIC)</h4></label>   
                            </div>
                            <div class="col-md-6">
                                <input type="file" name="agreement_pic" style="padding-top: 10px;">
                                <input type="hidden" name="agreement_pic_oldImage" value="{{(@$wusc_details->agreement_pic)?$wusc_details->agreement_pic:''}}">
                            </div>
                        </div>
                    </div>

                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_pipeline" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_pipeline == 1 ? 'checked':''}}>
                            <label for="agreement_pipeline"><h4>Pipeline Agreement</h4></label>
                        </div>
                        <div class="form-group col-md-7">
                                <div class="col-md-4">
                                    <label for="home_cleaning"><h4>Agreement Date</h4></label>   
                                </div>
                                <div class="col-md-4">
                                        <input type="text" name="agreement_date" id="agreement_date" class="form-control" autocomplete="off"  value="">
                   
                                  
                                </div>
                            </div>
    
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_mpc_tap" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_mpc_tap == 1 ? 'checked':''}}>
                            <label for="agreement_mpc_tap"><h4>MPC Tap Agreement</h4></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_other_tap" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_other_tap == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>(PTS/MPC/NPC) Agreement for Taps</h4></label>
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group col-md-5">
                            <input type="checkbox" name="agreement_contribution" value="1" style="height: 20px; width: 20px;" {{@$wusc_details->agreement_contribution == 1 ? 'checked':''}}>
                            <label for="home_cleaning"><h4>Contribution Form </h4></label>
                        </div>
                    </div>
                <input type="hidden" name="id" id="id" />
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary" onclick="before_send()">Save</button>
                    <a href="{{route('admin.projects.show',$id)}}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#toilet_monitoring_date,#dish_drying_demonstration,#dwrc_registration,#tripartie_agreement,#letter_of_intent,#pre-construction,#water_supply_plan_date,#agreement_date").daterangepicker({
                singleDatePicker:!0,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });
            if(counter==0)
            {
                $('#remove_add_member').hide();
            }     
        });

        
        var counter=0;
        var just_added=false;
        function add_wusc()
        {
            
            $( "#second" ).before( 
                '<div id="sec_'+counter+'">'+
                    '<div class="form-group col-md-12">'+
                        '<div class="col-md-2">'+
                           
                        '</div>'+
                        '<div class="col-md-2">'+
                            ' <input type="text" class="form-control" name="member_name_new'+counter+'" id="member_name_new'+counter+'" placeholder=" Name" value="">'+
                        '</div>'+
                        '<div class="col-md-2">'+
                            '<select class="form-control" name="member_gender_new'+counter+'" id="member_gender_new'+counter+'">'+
                                '<option value="Male">Male</option> '+
                                '<option value="Female" >Female</option>'+
                            '</select>'+
                        '</div>'+  
                        '<div class="col-md-2">'+           
                            '<select class="form-control" name="member_cast_new'+counter+'" id="member_cast_new'+counter+'">'+     
                                '<option value="UpperCast" >UpperCast</option>'+ 
                                '<option value="Raj" >Raj</option>  '+ 
                                '<option value="RDJ" >RDJ</option> '+   
                                '<option value="Dalit">Dalit</option>'+    
                            '</select>'+  
                        '</div>'+    
                    '</div>'+
                '</div>'
            );
            counter=counter+1; 
            just_added=true;
            $('#remove_add_member').show();
            console.log('the counter is '+counter);
                    }
        function before_send()
            {
                $('#membercounter').val(counter);
                $( "#form" ).submit();
            }
        function remove()
        {
            console.log('just added is '+just_added);
            console.log('the counter is '+counter);
            var cc;
            if(just_added==true)
            {
                cc=counter-1;    
            }
            else
            {
                cc=counter;
            }
            $('#sec_'+cc).remove();
            counter=counter-1;
            if(counter==0)
            {
                $('#remove_add_member').hide();
            }
        }

    </script>
@endsection
