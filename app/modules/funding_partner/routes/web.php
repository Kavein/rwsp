<?php



Route::group(array('prefix'=>'admin/','module'=>'Funding_partner','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Funding_partner\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('funding_partners/','AdminFunding_partnerController@index')->name('admin.funding_partners');
    Route::post('funding_partners/getfunding_partnersJson','AdminFunding_partnerController@getfunding_partnersJson')->name('admin.funding_partners.getdatajson');
    Route::get('funding_partners/create','AdminFunding_partnerController@create')->name('admin.funding_partners.create');
    Route::post('funding_partners/store','AdminFunding_partnerController@store')->name('admin.funding_partners.store');
    Route::get('funding_partners/show/{id}','AdminFunding_partnerController@show')->name('admin.funding_partners.show');
    Route::get('funding_partners/edit/{id}','AdminFunding_partnerController@edit')->name('admin.funding_partners.edit');
    Route::match(['put', 'patch'], 'funding_partners/update/{id}','AdminFunding_partnerController@update')->name('admin.funding_partners.update');
    Route::get('funding_partners/delete/{id}', 'AdminFunding_partnerController@destroy')->name('admin.funding_partners.edit');
});




Route::group(array('module'=>'Funding_partner','namespace' => 'App\Modules\Funding_partner\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('funding_partners/','Funding_partnerController@index')->name('funding_partners');
    
});