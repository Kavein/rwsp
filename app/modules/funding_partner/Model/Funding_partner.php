<?php

namespace App\Modules\Funding_partner\Model;


use Illuminate\Database\Eloquent\Model;

class Funding_partner extends Model
{
    public  $table = 'mst_funding_partners';

    protected $fillable = ['id','name','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
