@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Project Phase   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.project_phases') }}">Project Phases</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.project_phases.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Phase Name</label>
                        <input type="text" name="name" id="name" class="form-control" >
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label><br>
                        <input type="checkbox" name="status" style="height: 20px; width: 20px;" checked><br>
                    </div>
                        
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.project_phases') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
