@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Fiscal_years   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.fiscal_years') }}">fiscal_year</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.fiscal_years.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="start_date">Start Date</label>
                        <input type="text" name="start_date" id="start_date" class="form-control" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <label for="end_date">End Date</label>
                        <input type="text" name="end_date" id="end_date" class="form-control" autocomplete="off" >
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label><br>
                        <input type="checkbox" name="status" style="height: 20px; width: 20px;" checked><br>
                    </div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.fiscal_years') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>

<script type="text/javascript">
    $(document).ready(function() {
        $("#start_date,#end_date").daterangepicker({
            singleDatePicker:!0,
            locale: {
                format: 'YYYY-MM-DD'
            }
        });     
    });
</script>
@endsection
