@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Fiscal Year   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.fiscal_years') }}">fiscal_year</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.fiscal_years.update',$fiscal_year->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="start_date">Start_date</label>
                    <input type="text" value = "{{$fiscal_year->start_date}}"  name="start_date" id="start_date" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="end_date">End_date</label>
                    <input type="text" value = "{{$fiscal_year->end_date}}"  name="end_date" id="end_date" class="form-control" >
                </div>
                <div class="form-group">
                    <label for="status">Status</label><br>
                    <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" {{$fiscal_year->status == 1 ? 'checked':''}} >
                </div>
<input type="hidden" name="id" id="id" value = "{{$fiscal_year->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.fiscal_years') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
