@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Projects		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Projects</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.projects.create') }}" class="btn bg-green waves-effect"  title="create">Create</a>
						<!-- <a href="{{ url('admin/downloadProject') }}" class="btn bg-green waves-effect"  title="create">Excel</a> -->
					</div>
					
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<!-- <script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#project-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.projects.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
		            { data: "project_no",name: "project_no"},
		            { data: "project_name",name: "project_name"},
		            { data: "type",name: "type"},
		            { data: "fiscal_year",name: "fiscal_year"},
		            { data: "awcs.name",name: "awc_id"},
		            { data: "status",name: "status"},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn btn-default  waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' ><i class='fa fa-pencil'></i></a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"' onclick=\"return (confirm('Are you Sure'))?true:false\" class='btn btn-default  waves-effect' ><i class='fa fa-trash'></i></a>&nbsp";

					buttons += "<a href='"+site_url+"/show/"+data.id+"' class='btn btn-default  waves-effect' ><i class='fa fa-eye'></i></a>";

					

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script> -->
@endsection
