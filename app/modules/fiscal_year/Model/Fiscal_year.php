<?php

namespace App\Modules\Fiscal_year\Model;


use Illuminate\Database\Eloquent\Model;

class Fiscal_year extends Model
{
    public  $table = 'mst_fiscal_year';

    protected $fillable = ['id','start_date','end_date','created_by','updated_by','created_at','updated_at','del_flag','status',];
}
