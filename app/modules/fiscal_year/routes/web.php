<?php



Route::group(array('prefix'=>'admin/','module'=>'Fiscal_year','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Fiscal_year\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fiscal_years/','AdminFiscal_yearController@index')->name('admin.fiscal_years');
    Route::post('fiscal_years/getfiscal_yearsJson','AdminFiscal_yearController@getfiscal_yearsJson')->name('admin.fiscal_years.getdatajson');
    Route::get('fiscal_years/create','AdminFiscal_yearController@create')->name('admin.fiscal_years.create');
    Route::post('fiscal_years/store','AdminFiscal_yearController@store')->name('admin.fiscal_years.store');
    Route::get('fiscal_years/show/{id}','AdminFiscal_yearController@show')->name('admin.fiscal_years.show');
    Route::get('fiscal_years/edit/{id}','AdminFiscal_yearController@edit')->name('admin.fiscal_years.edit');
    Route::match(['put', 'patch'], 'fiscal_years/update/{id}','AdminFiscal_yearController@update')->name('admin.fiscal_years.update');
    Route::get('fiscal_years/delete/{id}', 'AdminFiscal_yearController@destroy')->name('admin.fiscal_years.edit');

    // Route::get('fiscal_years/list')
});




Route::group(array('module'=>'Fiscal_year','namespace' => 'App\Modules\Fiscal_year\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('fiscal_years/','Fiscal_yearController@index')->name('fiscal_years');
    
});