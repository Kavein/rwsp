<?php

namespace App\Modules\Fiscal_year\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Fiscal_year\Model\Fiscal_year;

class AdminFiscal_yearController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $page['title'] = 'Fiscal_year';
        return view("fiscal_year::index",compact('page'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getfiscal_yearsJson(Request $request)
    {
        $fiscal_year = new Fiscal_year;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $fiscal_year->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->get();

        // Display limited list        
        $rows = $fiscal_year->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->where('del_flag',0)->get();

        //To count the total values present
        $total = $fiscal_year->where('del_flag',0)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);


    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;

    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page['title'] = 'Fiscal_year | Create';
        return view("fiscal_year::add",compact('page'));
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->except('_token');
        $user = Auth::user()->id;
        $data['created_at'] = date('Y-m-d');
        $data['updated_at'] = date('Y-m-d');
        $data['created_by'] = $user;
        $data['updated_by'] = $user;
        if($request->status == "on"){
            $data['status'] = 1;
            Fiscal_year::where('status', 1)->update(['status' => 0]);
        }
        elseif($request->status = "off"){
            $data['status'] = 0;
        }
        $data['del_flag'] = 0;
        
        $success = Fiscal_year::Create($data);
        return redirect()->route('admin.fiscal_years');
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $fiscal_year = Fiscal_year::findOrFail($id);
        $page['title'] = 'Fiscal_year | Update';
        return view("fiscal_year::edit",compact('page','fiscal_year'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->except('_token','_method');
        $user = Auth::user()->id;
        $data['updated_at'] = date('Y-m-d');
        $data['updated_by'] = $user;
        if($request->status){
           $data['status'] = 1; 
           Fiscal_year::where('status', 1)->update(['status' => 0]);
        }else{
            $data['status'] = 0;
        }
        
        
        $data['del_flag'] = 0;
        $success = Fiscal_year::where('id', $id)->update($data);
        return redirect()->route('admin.fiscal_years');
        
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 0;
        $success = Fiscal_year::where('id', $id)->update($data);
        return redirect()->route('admin.fiscal_years');

        //
    }
}
