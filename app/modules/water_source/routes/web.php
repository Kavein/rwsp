<?php



Route::group(array('prefix'=>'admin/','module'=>'Water_source','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Water_source\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('water_sources/','AdminWater_sourceController@index')->name('admin.water_sources');
    Route::post('water_sources/getwater_sourcesJson','AdminWater_sourceController@getwater_sourcesJson')->name('admin.water_sources.getdatajson');
    Route::get('water_sources/create','AdminWater_sourceController@create')->name('admin.water_sources.create');
    Route::post('water_sources/store','AdminWater_sourceController@store')->name('admin.water_sources.store');
    Route::get('water_sources/show/{id}','AdminWater_sourceController@show')->name('admin.water_sources.show');
    Route::get('water_sources/edit/{id}','AdminWater_sourceController@edit')->name('admin.water_sources.edit');
    Route::match(['put', 'patch'], 'water_sources/update/{id}','AdminWater_sourceController@update')->name('admin.water_sources.update');
    Route::get('water_sources/delete/{id}', 'AdminWater_sourceController@destroy')->name('admin.water_sources.edit');
});




Route::group(array('module'=>'Water_source','namespace' => 'App\Modules\Water_source\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('water_sources/','Water_sourceController@index')->name('water_sources');
    
});