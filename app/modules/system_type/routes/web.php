<?php



Route::group(array('prefix'=>'admin/','module'=>'System_type','middleware' => ['web','auth'], 'namespace' => 'App\Modules\System_type\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('system_types/','AdminSystem_typeController@index')->name('admin.system_types');
    Route::post('system_types/getsystem_typesJson','AdminSystem_typeController@getsystem_typesJson')->name('admin.system_types.getdatajson');
    Route::get('system_types/create','AdminSystem_typeController@create')->name('admin.system_types.create');
    Route::post('system_types/store','AdminSystem_typeController@store')->name('admin.system_types.store');
    Route::get('system_types/show/{id}','AdminSystem_typeController@show')->name('admin.system_types.show');
    Route::get('system_types/edit/{id}','AdminSystem_typeController@edit')->name('admin.system_types.edit');
    Route::match(['put', 'patch'], 'system_types/update/{id}','AdminSystem_typeController@update')->name('admin.system_types.update');
    Route::get('system_types/delete/{id}', 'AdminSystem_typeController@destroy')->name('admin.system_types.edit');
});




Route::group(array('module'=>'System_type','namespace' => 'App\Modules\System_type\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('system_types/','System_typeController@index')->name('system_types');
    
});