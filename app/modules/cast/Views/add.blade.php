@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Casts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.casts') }}">mst_casts</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.casts.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" id="name" class="form-control" >
                        @if ($errors->has('name'))
                            <span class="help-block">
                                <strong>{{ $errors->first('name') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-group">
                        <label for="cast_divison_id">Cast Divison</label>
                        <select class="form-control" name="cast_divison_id" id="cast_divison_id">
                            @foreach($cast_divisons as $cast_divison)
                            <option value="{{$cast_divison->id}}">{{$cast_divison->name}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label for="status">Status</label><br>
                        <input type="checkbox" name="status" style="height: 20px; width: 20px;" checked><br>
                    </div>
                <input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.casts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
