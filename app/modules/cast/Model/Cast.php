<?php

namespace App\Modules\Cast\Model;


use Illuminate\Database\Eloquent\Model;

class Cast extends Model
{
    public  $table = 'mst_casts';

    protected $fillable = ['id','name','cast_divison_id','status','del_flag','created_by','updated_by','created_at','updated_at',];

    public function cast_divisons()
    {
    	return $this->belongsTo('App\Modules\Cast_divison\Model\cast_divison', 'cast_divison_id');
    }
}
