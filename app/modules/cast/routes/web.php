<?php



Route::group(array('prefix'=>'admin/','module'=>'Cast','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Cast\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('casts/','AdminCastController@index')->name('admin.casts');
    Route::post('casts/getcastsJson','AdminCastController@getcastsJson')->name('admin.casts.getdatajson');
    Route::get('casts/create','AdminCastController@create')->name('admin.casts.create');
    Route::post('casts/store','AdminCastController@store')->name('admin.casts.store');
    Route::get('casts/show/{id}','AdminCastController@show')->name('admin.casts.show');
    Route::get('casts/edit/{id}','AdminCastController@edit')->name('admin.casts.edit');
    Route::match(['put', 'patch'], 'casts/update/{id}','AdminCastController@update')->name('admin.casts.update');
    Route::get('casts/delete/{id}', 'AdminCastController@destroy')->name('admin.casts.edit');
});




Route::group(array('module'=>'Cast','namespace' => 'App\Modules\Cast\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('casts/','CastController@index')->name('casts');
    
});