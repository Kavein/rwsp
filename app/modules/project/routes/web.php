<?php



Route::group(array('prefix'=>'admin/','module'=>'Project','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Project\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('projects/','AdminProjectController@index')->name('admin.projects');
    Route::post('projects/getprojectsJson','AdminProjectController@getprojectsJson')->name('admin.projects.getdatajson');
    Route::get('projects/create','AdminProjectController@create')->name('admin.projects.create');
    Route::post('projects/store','AdminProjectController@store')->name('admin.projects.store');
    Route::get('projects/show/{id}','AdminProjectController@show')->name('admin.projects.show');
    Route::get('projects/edit/{id}','AdminProjectController@edit')->name('admin.projects.edit');
    Route::match(['put', 'patch'], 'projects/update/{id}','AdminProjectController@update')->name('admin.projects.update');
    Route::get('projects/delete/{id}', 'AdminProjectController@destroy')->name('admin.projects.edit');

    Route::post('projects/district','AdminProjectController@selectDistrict')->name('admin.projects.district');
    Route::post('projects/vdc','AdminProjectController@selectVdc')->name('admin.projects.vdc');
    Route::post('projects/cast_division','AdminProjectController@selectCastDivision')->name('admin.projects.cast_division');

    //Initial Phase Store
    Route::get('projects/initial_phase/create/{id}','AdminProjectController@createInitialPhase')->name('admin.projects.initial.create');
    Route::post('projects/initial_phase/store','AdminProjectController@storeInitialPhase')->name('admin.projects.initial.store');
    Route::get('projects/initial_phase/edit/{id}','AdminProjectController@householdEdit')->name('admin.projects.initial.edit');
    Route::match(['put', 'patch'], 'projects/initial_phase/update/{id}','AdminProjectController@householdUpdate')->name('admin.projects.household.update');
    Route::get('projects/3years/{id}','AdminProjectController@Evaluation')->name('project.evaluation');
    Route::post('projects/3years/store/{id}','AdminProjectController@EvaluationStore')->name('project.evaluation.store');

    Route::get('projects/assign-list/{id}','AdminProjectController@ProjectAssign')->name('project.assign');
    Route::post('projects/assign-list-store','AdminProjectController@storeAssign')->name('assign.store');

    

});




Route::group(array('module'=>'Project','namespace' => 'App\Modules\Project\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('projects/','ProjectController@index')->name('projects');
    
});