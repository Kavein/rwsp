@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            3 Years Evaluation 
            <small></small>                    
        </h1>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{route('project.evaluation.store',$id)}}"  method="post" enctype="multipart/form-data">
                <div class="box-body">     
                	
                    <label for="school_data" class="col-sm-12 col-form-label"><h3><strong><u>Outcome Evaluation </u></strong></h3></label>  
                    <div class="col-md-12">
                        <div class="form-group col-md-6">
                            <label for="">Time Fetching</label>
                            <input type="text" name="time_fetching" class="form-control" autocomplete="off" value="{{@$evaluation->time_fetching}}">
                        </div> 

                        <div class="form-group col-md-6">
                            <label for="">ODF Declaration</label>
                            <input type="text" name="odf_declaration" id="odf_declaration" class="form-control" autocomplete="off" value="{{@$evaluation->odf_declaration}}">
                        </div>  

                        <div class="form-group col-md-6">
                            <label for="">Used of second time</label>
                            <input type="text" name="used_second_time" class="form-control" autocomplete="off" value="{{@$evaluation->used_second_time}}">
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="">Handwashing before eating/after defecation</label>
                            <input type="text" name="handwashing" class="form-control" autocomplete="off" value="{{@$evaluation->handwashing}}" >
                        </div>
                        <div class="form-group col-md-6">
                            <label for="">Health data survey</label>
                            <input type="text" name="health_data_survey" class="form-control" autocomplete="off" value="{{@$evaluation->health_data_survey}}" >
                        </div> 
                        <div class="form-group col-md-6">
                            <label for="">Health Data (Health post) </label>
                            <input type="text" name="health_data_health_post" class="form-control" autocomplete="off" value="{{@$evaluation->health_data_health_post}}">
                        </div>  
                    </div>
                    
                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 20px;"> </div>
                    <label class="col-sm-12 col-form-label"><h3><strong><u>Substantiality Index</u></strong></h3></label>  
                    <div class="col-md-12">
                        <label for="school_data" class="col-sm-12 col-form-label"><h3>Environment Sustainability</h3></label> 
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Water Quality Test</label>
                                <input type="text" name="water_quality_test_env" id="water_quality_test_env" class="form-control" autocomplete="off" value="{{@$evaluation->water_quality_test_env}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">WSP Implementation</label>
                                <input type="text" name="wsp_implementaion" id="wsp_implementaion" class="form-control" autocomplete="off" value="{{@$evaluation->wsp_implementaion}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Disaster (Destruction causes due to disaster)</label>
                                <input type="text" name="disaster"  class="form-control" autocomplete="off" value="{{@$evaluation->disaster}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Toilet (uses before and after)</label>
                                <input type="text" name="toilet"  class="form-control" autocomplete="off" value="{{@$evaluation->toilet}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Hygiene Practices Implementation</label>
                                <input type="text" name="hygiene_practice"  class="form-control" autocomplete="off" value="{{@$evaluation->hygiene_practice}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Sanitation</label>
                                <input type="text" name="sanitation"  class="form-control" autocomplete="off" value="{{@$evaluation->sanitation}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Dish Drying</label>
                                <input type="text" name="dish_drying"  class="form-control" autocomplete="off" value="{{@$evaluation->dish_drying}}">
                            </div>
                        </div> 
                        <label for="school_data" class="col-sm-12 col-form-label"><h3>Financial Sustainably</h3></label>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">O & M Fund</label>
                                <input type="text" name="om_fund" class="form-control" autocomplete="off" value="{{@$evaluation->om_fund}}">
                            </div>
                        </div>
                        <label for="school_data" class="col-sm-12 col-form-label"><h3>Functionally Survey</h3></label>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Sanitary Inspection </label>
                                <input type="text" name="sanitary_inspection" class="form-control" autocomplete="off" value="{{@$evaluation->sanitary_inspection}}" >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">water Quality Test</label>
                                <input type="text" name="water_quality_test_fs" class="form-control" autocomplete="off" value="{{@$evaluation->water_quality_test_fs}}">
                            </div>
                        </div>
                        
                        <label for="school_data" class="col-sm-12 col-form-label"><h3>Institutional Sustainability</h3></label>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Effectiveness of VNW, VHT, WUSC</label>
                                <input type="text" name="effectiveness" class="form-control" autocomplete="off" value="{{@$evaluation->effectiveness}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Status of Taps, Latrines</label>
                                <input type="text" name="status_tap" class="form-control" autocomplete="off" value="{{@$evaluation->status_tap}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Triprate Agreement</label>
                                <input type="text" name="triparte_agreement" class="form-control" autocomplete="off" value="{{@$evaluation->triparte_agreement}}" >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Implement of awareness program</label>
                                <input type="text" name="awareness_program" class="form-control" autocomplete="off" value="{{@$evaluation->awareness_program}}">
                            </div>
                        </div>
                        <label for="school_data" class="col-sm-12 col-form-label"><h3>Equity</h3></label>
                        <div class="col-md-12">
                            <div class="form-group col-md-6">
                                <label for="">Gender WUSC</label>
                                <input type="text" name="gender_wusc" class="form-control" autocomplete="off" value="{{@$evaluation->gender_wusc}}" >
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Key Decisions Making</label>
                                <input type="text" name="key_decision" class="form-control" autocomplete="off" value="{{@$evaluation->key_decision}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">VMW(Female)</label>
                                <input type="text" name="wmv_female" class="form-control" autocomplete="off" value="{{@$evaluation->wmv_female}}">
                            </div>
                            <div class="form-group col-md-6">
                                <label for="">Gender Analysis </label>
                                <input type="text" name="gender_analysis" class="form-control" autocomplete="off" value="{{@$evaluation->gender_analysis}}">
                            </div>
                        </div>
                    </div>

				<input type="hidden" name="id" id="id" />
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{route('admin.projects.show',$id)}}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>

    <script type="text/javascript">
    	$(document).ready(function() {
            $("#odf_declaration,#water_quality_test,#wsp_implementaion").daterangepicker({
                singleDatePicker:!0,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });     
        });

            
    </script>
@endsection
