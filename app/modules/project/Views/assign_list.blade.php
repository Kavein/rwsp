@extends('admin.layout.main')
@section('content')
	<div class="col-md-12">
		<form action="{{route('assign.store')}}" method="post">

			<select multiple="multiple" id="my-select" name="myselect[]">
				@foreach($users as $user)
					@if($user_assigned == null)
						<option value='{{$user->id}}'>{{$user->username}}</option>
					@else
						<option value='{{$user->id}}' <?php echo (in_array($user->id, $user_assigned)?'selected':'') ?>>{{$user->username}}</option>
					@endif
				@endforeach
			</select><br>
			<input type="hidden" name="project_id" value="{{$id}}">
			<div>
				<button type="submit" class="btn btn-primary">Save</button>
			</div>
			{{csrf_field()}}
		</form>
	</div>
			
		
	<script type="text/javascript">
		$(document).ready(function() {
			$('#my-select').multiSelect({

			});
		});
		
	</script>
@endsection