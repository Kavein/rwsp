@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Projects   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.projects') }}">Projects</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
            <form role="form" action="{{ route('admin.projects.update',$project->id) }}"  method="post" enctype="multipart/form-data">
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                        <li class=""><a href="#project_details" data-toggle="tab" aria-expanded="false">Project Details</a></li>
                        <li class=""><a href="#community_information" data-toggle="tab" aria-expanded="false">Community Information</a></li>
                        <li class=""><a href="#disaster" data-toggle="tab" aria-expanded="false">Disaster</a></li>
                        <li class=""><a href="#capacity" data-toggle="tab" aria-expanded="false">Capacity Strengthening</a></li>
                        <li class=""><a href="#knowledge" data-toggle="tab" aria-expanded="false">Knowledge and Learnings</a></li>
                    </ul>
                    <div class="tab-content">
                        <div class="box-body tab-pane active" id="project_details">
                            @include('project::project_detail.edit_detail')
                        </div>
                        <div id="community_information" class="box-body tab-pane">
                            @include('project::project_detail.edit_community')
                        </div>
                        <div id="disaster" class="box-body tab-pane">
                            @include('project::project_detail.edit_disaster')
                        </div>
                        <div id="capacity" class="box-body tab-pane">
                            @include('project::project_detail.edit_capacity')
                        </div>
                        <div id="knowledge" class="box-body tab-pane">
                            @include('project::project_detail.edit_knowledge')
                        </div>
                    </div>
                    <input type="hidden" name="community_id_get" value="{{$community_data->id}}">
                    {{method_field('PATCH')}}
                    {{ csrf_field() }}
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Save</button>
                        <a href="{{ route('admin.projects') }}" class="btn btn-danger">Cancel</a>
                    </div>
                </div>

            </form>
    </section>
    <script type="text/javascript">
        function getDistrict(){
            var province_id = $('#province_id').val();
            $.post("{{route('admin.projects.district')}}",{province_id : province_id,_token:'{{ csrf_token() }}'},function(result){
                    $('#district_id').empty();
                    var html="";
                    $.each(result,function(k,v){
                        html+= "<option value='"+v.id+"'>"+v.name+"</option>"
                    });
                    $('#district_id').html(html);
            },'json');
        }

        function getVdc(){
            var district_id = $('#district_id').val();
            $.post("{{route('admin.projects.vdc')}}",{district_id : district_id,_token:'{{ csrf_token() }}'},function(result){
                    $('#vdc_id').empty();
                    var html="";
                    $.each(result,function(k,v){
                        html+= "<option value='"+v.id+"'>"+v.name+"</option>"
                    });
                    $('#vdc_id').html(html);
            },'json');
        }

        $(document).ready(function() {
            $("#start_date,#end_date,#app_received_date,#feasibility_assessment_date,#profiling_date,#system_built_date").daterangepicker({
                singleDatePicker:!0,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });     
        });

        $(function () {
            CKEDITOR.replace('ph_result');
            CKEDITOR.replace('h2s_result');
            uploadReady();
        });
    </script>
@endsection
