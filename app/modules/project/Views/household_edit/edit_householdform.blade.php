<div class="box-body">                
    <!-- <div class="form-group col-md-6">
        <label for="community_name">Community Name</label>
        <input type="text" name="community_name" id="community_name" class="form-control" value="{{$household->community_name}}" autocomplete="off">
    </div> -->
    <div class="form-group col-md-6">
        <label for="house_no">House no</label>
        <input type="text" name="house_no" id="house_no" class="form-control" value="{{$household->house_no}}">
        @if ($errors->has('house_no'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('house_no') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="family_head_name">Family Head Name</label>
        <input type="text" name="head_name" id="family_head_name" class="form-control" value="{{$household->head_name}}">
        @if ($errors->has('head_name'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('head_name') }}</strong>
            </span>
        @endif
    </div>
    <input type="hidden" name="project_id" value="{{$household->project_id}}" >
    <div class="form-group col-md-6">
        <label for="family_head_gender">Family Head Gender</label>
        <select class="form-control" name="head_gender">
            <option value="male" {{$household->head_gender == "male" ? 'selected':''}}>Male</option>
            <option value="female" {{$household->head_gender == "female" ? 'selected':''}}>Female</option>
        </select>
    </div>
    <div class="form-group col-md-6">
        <label for="family_head_name">Tole Name</label>
        <input type="text" name="tole_name" id="tole_name" class="form-control" value="{{$household->tole_name}}">
        @if ($errors->has('tole_name'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('tole_name') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="province_id">Cast Division</label>
        <select class="form-control" name="cast_division" id="cast_division" onChange="getCastDivision()">
            <option value=""> Select Cast Division</option>
            @foreach($cast_divisions as $cast_division)
            <option value="{{$cast_division->id}}" {{$household->cast_division == $cast_division->id ? 'selected':''}}> {{$cast_division->name}}</option>
            @endforeach
        </select>
        @if ($errors->has('cast_division'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('cast_division') }}</strong>
            </span>
        @endif
    </div>
    <div class="form-group col-md-6">
        <label for="cast_id">Cast</label>
        <select class="form-control" name="cast_id" id="cast_id">
            @foreach($casts as $cast)
                <option value="{{$cast->id}}" {{$household->cast_id == $cast->id ? 'selected':''}} >{{$cast->name}}</option>
            @endforeach
        </select>
        @if ($errors->has('cast_id'))
            <span class="help-block">
                <strong style="color: red;">{{ $errors->first('cast_id') }}</strong>
            </span>
        @endif
    </div>

    {{--

    <div class="form-group col-md-6">
        <label for="religion_id">Religion</label>
        <select class="form-control" name="religion_id">
            @foreach($religions as $religion)
                <option value="{{$religion->id}}" {{$household->religion_id == $religion->id ? 'selected':''}} >
                    {{$religion->name}}</option>
            @endforeach
        </select>
    </div>
        --}}
   
    <div class="col-md-12 ">
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
        <div class="form-group row">
            <label for="name" class="col-sm-4 col-form-label">Total Member in a Family</label>
            <div class="col-sm-4">
                <input type="number" class="form-control" name="total_male"  placeholder="MALE" value="{{$household->total_male}}">
                @if ($errors->has('total_male'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('total_male') }}</strong>
                    </span>
                @endif
            </div>
            <div class="col-sm-4">
                <input type="number" class="form-control" name="total_female" placeholder="FEMALE" value="{{$household->total_female}}">
                @if ($errors->has('total_female'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('total_female') }}</strong>
                    </span>
                @endif
            </div>
        </div>
        <div class="form-group row">
            <label class="col-sm-4 col-form-label">Total number of widow Women</label>
            <div class="col-sm-4">
                <input type="number" name="total_female_widow" placeholder="NUMBER OF WIDOW WOMEN" value="{{$household->total_female_widow}}" class="form-control">
                @if ($errors->has('total_female_widow'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('total_female_widow') }}</strong>
                    </span>
                @endif
            </div>
        </div>
    </div>
    
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
        <div class="" style="padding-top: 10px;">
            <label for="total_male_member">AGE GROUP</label>
            <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">Age Group (0 - 5 YEAR)</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_0to5_male]" id="0to5_male" value="{{$age_groups->between_0to5_male}}" placeholder="MALE">
                    @if ($errors->has('age.between_0to5_male'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_0to5_male') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_0to5_female]" id="0to5_female" placeholder="FEMALE" value="{{$age_groups->between_0to5_female}}">
                    @if ($errors->has('age.between_0to5_female'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_0to5_female') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">Age Group (6 - 15 YEAR)</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_6to15_male]" id="6to15_male" placeholder="MALE" value="{{$age_groups->between_6to15_male}}">
                    @if ($errors->has('age.between_6to15_male'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_6to15_male') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_6to15_female]" id="6to15_female" placeholder="FEMALE" value="{{$age_groups->between_6to15_female}}" >
                    @if ($errors->has('age.between_6to15_female'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_6to15_female') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">Age Group (16 - 65 YEAR)</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_16to65_male]" id="16to65_male" placeholder="MALE" value="{{$age_groups->between_16to65_male}}">
                    @if ($errors->has('age.between_16to65_male'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_16to65_male') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[between_16to65_female]" id="16to65_female" placeholder="FEMALE" value="{{$age_groups->between_16to65_female}}">
                    @if ($errors->has('age.between_16to65_female'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.between_16to65_female') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">Age Group (Above 65 YEAR)</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[above65_male]" id="65above_male" placeholder="MALE" value="{{$age_groups->above65_male}}">
                    @if ($errors->has('age.above65_male'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.above65_male') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control" name="age[above65_female]" id="65above_female" placeholder="FEMALE" value="{{$age_groups->above65_female}}">
                    @if ($errors->has('age.above65_female'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.above65_female') }}</strong>
                        </span>
                    @endif
                </div>

            </div>

            <div class="form-group row">
                <label for="name" class="col-sm-4 col-form-label">DISABILITY</label>
                <div class="col-sm-4">
                    <input type="number" class="form-control" id="disabled_male" name="age[disabled_male]" placeholder="MALE" value="{{$age_groups->disabled_male}}">
                    @if ($errors->has('age.disabled_male'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.disabled_male') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="col-sm-4">
                    <input type="number" class="form-control" id="disabled_female" name="age[disabled_female]" placeholder="FEMALE" value="{{$age_groups->disabled_female}}">
                    @if ($errors->has('age.disabled_female'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('age.disabled_female') }}</strong>
                        </span>
                    @endif
                </div>

            </div>
        </div>

    </div>
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom:10px; ">
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-3 col-form-label">Number Of Litrate People</label>
        <div class="col-sm-3">
            <input type="number" class="form-control" name="litrate_no" value="{{$household->litrate_no}}" placeholder="Litrate People">
            @if ($errors->has('litrate_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('litrate_no') }}</strong>
                </span>
            @endif
        </div>
        <label for="name" class="col-sm-3 col-form-label">Number Of Illitrate People</label>
        <div class="col-sm-3">
            <input type="number" class="form-control" name="illitrate_no" value="{{$household->illitrate_no}}" placeholder="Illitrate People">
            @if ($errors->has('illitrate_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('illitrate_no') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom:10px; ">
    </div>

    <div class="form-group col-md-6">
        <label for="occupation_id">Occupation</label>
        <select class="form-control" multiple name="occupation_id[]" id="multiple">
            @foreach($occupations as $occupation)
                <option value="{{$occupation->id}}" {{ in_array($occupation->id, $hh_occupation_obj)?'selected':'' }} >{{$occupation->name}}</option>  
            @endforeach
        </select>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-4">
            <label for="latrine_available">Income Source and Pension</label><br>
            <input type="checkbox" value="1" name="income_pension" id="income_pension" {{$household->income_pension == 1 ? 'checked':''}} style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4" id="" hidden>
            <!-- <input type="text" name="total_latrine" class="form-control" placeholder="Total Latrine Available" > -->
        </div>
        <div class="col-md-12" hidden id="income_pension_option">
            @foreach($income_sources as $income)
                <div class="form-group col-md-2" id="income-{{$income->id}}">
                    <input type="checkbox" value="{{$income->id}}" name="income[income-{{$income->id}}]" style="height: 20px; width: 20px;" {{ in_array($income->id, $hh_income_obj)?'checked':'' }}>
                    <label for="latrine_cleansing_agent" style="margin-top: -20px;">{{$income->name}} </label>
                </div>
            @endforeach
        </div> 
    </div>
    <div class="form-group col-md-6">
        <label for="religion_minority">Religion Minority</label><br>
        <input type="checkbox" name="religion_minority" value="1" style="height: 20px; width: 20px;" {{$household->religion_minority == 1 ? 'checked':''}}><br>
    </div>
    <div class="form-group col-md-3">
        <label for="status">Status</label><br>
        <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" {{$household->status == 1 ? 'checked':''}}><br>
    </div>
    
    <input type="hidden" name="id" id="id" value="{{$household->id}}" />
</div>
<script>
        jQuery(document).ready(function($) {
            $('#multiple').select2();
        });
</script>