<div class="box-body">
    <div class="col-md-12">
        <div class="form-group col-md-4">
            <label for="latrine_available">Latrine Availability</label><br>
            <input type="checkbox" name="latrine_available" id="latrine_available" style="height: 20px; width: 20px;" value="1" {{$hygienes->latrine_available == 1 ? 'checked':''}} >
        </div>
        <div class="form-group col-md-4" id="total_latrine" hidden>
            <input type="number" name="total_latrine" class="form-control" placeholder="Total Latrine Available" value="{{$hygienes->total_latrine}}" >
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-4" hidden id="latrine_cleansing_agent">
                <label for="latrine_cleansing_agent">LATRINE WITH CLEANSING AGENT KEPT INSIDE</label>
                <br>
                <input type="checkbox" name="latrine_cleansing_agent" value="1" style="height: 20px; width: 20px;" {{$hygienes->latrine_cleansing_agent == 1 ? 'checked':''}}>
            </div>
            <div class="form-group col-md-4" hidden id="latrine_water_available">
                <label for="latrine_water_available">LATRINE WITH WATER JAR KEPT INSIDE</label>
                <br>
                <input type="checkbox" name="latrine_water_available" style="height: 20px; width: 20px;" value="1" {{$hygienes->latrine_water_available == 1 ? 'checked':''}}>
            </div>
            <div class="form-group col-md-4" hidden id="latrine_cleaning_brush">
                <label for="latrine_cleaning_brush">LATRINE WITH CLEANING BRUSH KEPT INSIDE</label>
                <br><input type="checkbox" name="latrine_cleaning_brush" style="height: 20px; width: 20px;" 
                value="1" {{$hygienes->latrine_cleaning_brush == 1 ? 'checked':''}}>
            </div>
        </div>
        
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6" >
            <label> Defecating in Latrine</label>
            <input type="number" name="defecate_latrine" class="form-control" placeholder="Total People Defecate in Latrine" value="{{$hygienes->defecate_latrine}}" >
        </div>
        <div class="form-group col-md-6" >
            <label> Defecating on Open</label>
            <input type="number" name="defecate_open" class="form-control" placeholder="Total People Defecate in Open Place" value="{{$hygienes->defecate_open}}" >
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-4">
            <label for="home_cleaning">CLEANING HOME REGULARLY</label><br>
            <input type="checkbox" name="home_cleaning" style="height: 20px; width: 20px;" value="1" {{$hygienes->home_cleaning == 1 ? 'checked':''}}>
        </div>
        <div class="form-group col-md-4">
            <label for="domestic_animal">KEEPING ANIMALS TO A SAFE DISTANCE</label><br>
            <input type="checkbox" name="domestic_animal" style="height: 20px; width: 20px;" value="1" {{$hygienes->domestic_animal == 1 ? 'checked':''}}>
        </div>
        <div class="form-group col-md-4">
            <label for="drinking_water">KEEPING WATER VESSEL COVERED</label><br>
            <input type="checkbox" name="drinking_water" style="height: 20px; width: 20px;" value="1" {{$hygienes->drinking_water == 1 ? 'checked':''}}>
        </div>
        <div class="form-group col-md-4">
            <label for="food_covered">KEEPING FOOD COVERED</label><br>
            <input type="checkbox" name="food_covered" style="height: 20px; width: 20px;" value="1" {{$hygienes->food_covered == 1 ? 'checked':''}}>
        </div>
        <div class="form-group col-md-4">
            <label for="utensils_clean">KEEPING UTENSILS CLEAN</label><br>
            <input type="checkbox" name="utensils_clean" style="height: 20px; width: 20px;" value="1" {{$hygienes->utensils_clean == 1 ? 'checked':''}}>
        </div>
        <div class="form-group col-md-4">
            <label for="garbage_disposel_pit">MANAGING GARBAGE IN DISPOSAL PIT</label>
            <br>
            <input type="checkbox" name="garbage_disposel_pit" style="height: 20px; width: 20px;" value="1" {{$hygienes->garbage_disposel_pit == 1 ? 'checked':''}}>
        </div>
        
    </div>

    
    
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
       
        <h3 for="total_male_member" class="col-md-12"><strong><label>Personal Hygiene</label></strong></h3 >
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
        {{--
            <div class="form-group col-md-6 "> 
            <label for="handwash_before_eating">HAND WASH BEFORE EATING</label>   
                <select class="form-control" name="handwash_before_eating">
                    <option value="soap&water" {{$hygienes->handwash_before_eating == "soap&water" ? 'checked':''}}>
                        Wash Hand With Soap And Water
                    </option>
                    <option value="wateronly" {{$hygienes->handwash_before_eating == "wateronly" ? 'checked':''}}>
                        Wash Hand With Water Only
                    </option>
                    <option value="none" {{$hygienes->handwash_before_eating == "none" ? 'checked':''}}>
                        Donot Wash
                    </option>
                </select>
            </div>


        <div class="form-group col-md-6 "> 
            <label for="handwash_after_defecating">WASH HAND AFTER DEFECATION </label>   
            <select class="form-control" name="handwash_after_defecating">
                <option value="soap&water" {{$hygienes->handwash_after_defecating == "soap&water" ? 'checked':''}}>
                    Wash Hand With Soap And Water</option>
                <option value="wateronly" {{$hygienes->handwash_after_defecating == "wateronly" ? 'checked':''}}>
                    Wash Hand With Water Only
                </option>
                <option value="none" {{$hygienes->handwash_after_defecating == "none" ? 'checked':''}}>
                    Donot Wash
                </option>
            </select>
        </div>
        --}}

        <div class="form-group col-md-6 "> 
            <label for="bath_frequency">BATHING FREQUENCY</label>   
            <select class="form-control" name="bath_frequency">
                <option value="daily" {{$hygienes->bath_frequency == "daily" ? 'checked':''}}>
                    DAILY
                </option>
                <option value="once" {{$hygienes->bath_frequency == "once" ? 'checked':''}}>
                    ONCE A WEEK
                </option>
                <option value="twice" {{$hygienes->bath_frequency == "twice" ? 'checked':''}}>
                    MORE THAN ONCE A WEEK
                </option>
            </select>
        </div>

        <div class="form-group col-md-6 "> 
            <label for="cloth_wash_frequency">CLOTHES WASHING FREQUENCY</label>   
            <select class="form-control" name="cloth_wash_frequency">
                <option value="once" {{$hygienes->cloth_wash_frequency == "once" ? 'checked':''}}>
                    ONCE A WEEK
                </option>
                <option value="more" {{$hygienes->cloth_wash_frequency == "more" ? 'checked':''}}>
                    MORE THAN ONCE A WEEK
                </option>
                <option value="monthly" {{$hygienes->cloth_wash_frequency == "monthly" ? 'checked':''}}>
                    ONCE A MONTH
                </option>
            </select>
        </div>

        <div class="form-group col-md-6 "> 
            <label for="knowledge_menstrual">WOMEN KNOW ABOUT MANAGEMENT OF MENSTRUAL HYGIENE</label>   <br>
            <input class="form-check-input" type="radio" id="traditional_preference" value="Yes" name="menstrual_knowledge" {{$hygienes->menstrual_knowledge == "Yes" ? 'checked':''}}>
            <label class="form-check-label" for="traditional_preference">&nbsp&nbsp&nbsp&nbspYes</label>

            <input class="form-check-input" type="radio" id="traditional_preference" value="No" name="menstrual_knowledge" style="margin-left: 100px;" {{$hygienes->menstrual_knowledge == "No" ? 'checked':''}}>
            <label class="form-check-label" for="traditional_preference">&nbsp&nbsp&nbsp&nbspNo</label>
        </div>

        <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
            <h3 for="total_male_member" class="col-md-12"><strong><label>Hand Washing</label></strong></h3 >
        
            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label"></label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">With Soap and Water</label>
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">Water Only</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">With Ash</label>
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">Others</label>
                    </div>
                </div>  
            </div> 

            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">HandWash Before Eating</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="handwash_water_soap" placeholder="Number" value="{{$hygienes->handwash_water_soap}}">
                        @if ($errors->has('handwash_water_soap'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_water_soap') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="handwash_water" placeholder="Number" value="{{$hygienes->handwash_water}}">
                        @if ($errors->has('handwash_water'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_water') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="handwash_ash" placeholder="Number" value="{{$hygienes->handwash_ash}}">
                        @if ($errors->has('handwash_ash'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_ash') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="handwash_others" placeholder="Number" value="{{$hygienes->handwash_others}}">
                        @if ($errors->has('handwash_others'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_others') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>

            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">HandWash After Defecation</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="defecate_water_soap" placeholder="Number" value="{{$hygienes->defecate_water_soap}}">
                        @if ($errors->has('defecate_water_soap'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_water_soap') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="defecate_water" placeholder="Number" value="{{$hygienes->defecate_water}}">
                        @if ($errors->has('defecate_water'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_water') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="defecate_ash" placeholder="Number" value="{{$hygienes->defecate_ash}}">
                        @if ($errors->has('defecate_ash'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_ash') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>
            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">Infanct Children Number Who doesnt wash Hands</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-6">
                        <input class="form-check-input form-control" type="number" name="handwash_infant" placeholder="Number" value="{{$hygienes->handwash_infant}}">
                        @if ($errors->has('handwash_infant'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_infant') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>