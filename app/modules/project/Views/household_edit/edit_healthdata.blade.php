<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><u>INCIDENCE OF WATER-BORNE DISEASES</u></strong></h3 >
    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DIARRHOEA </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male </label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_male]" value="{{$healthdatas->diarrhoea_male}}" placeholder="MALE">
            @if ($errors->has('incidence.diarrhoea_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female </label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_female]" value="{{$healthdatas->diarrhoea_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.diarrhoea_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_frequency]" id="" placeholder="Total" value="{{$healthdatas->diarrhoea_frequency}}">
            @if ($errors->has('incidence.diarrhoea_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DYSENTERY </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_male]" value="{{$healthdatas->dysentry_male}}" placeholder="MALE">
            @if ($errors->has('incidence.dysentry_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_female]" value="{{$healthdatas->dysentry_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.dysentry_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_frequency]" id="" placeholder="Total" value="{{$healthdatas->dysentry_frequency}}">
            @if ($errors->has('incidence.dysentry_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY JAUNDICE </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_male]" value="{{$healthdatas->jaundice_male}}" placeholder="MALE">
            @if ($errors->has('incidence.jaundice_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_female]" value="{{$healthdatas->jaundice_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.jaundice_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness </label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_frequency]" id="" placeholder="Total" value="{{$healthdatas->jaundice_frequency}}">
            @if ($errors->has('incidence.jaundice_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY COLERA </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_male]" value="{{$healthdatas->colera_male}}" placeholder="MALE">
            @if ($errors->has('incidence.colera_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_female]" value="{{$healthdatas->colera_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.colera_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_frequency]" id="" placeholder="Total" value="{{$healthdatas->colera_frequency}}">
            @if ($errors->has('incidence.colera_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>   

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY WORMS INFESTATION </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_male]" value="{{$healthdatas->worm_infection_male}}" placeholder="MALE">
            @if ($errors->has('incidence.worm_infection_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_female]" value="{{$healthdatas->worm_infection_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.worm_infection_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_frequency]" id="" placeholder="Total" value="{{$healthdatas->worm_infection_frequency}}">
            @if ($errors->has('incidence.worm_infection_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY SCABIES</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_male]" value="{{$healthdatas->scabies_male}}" placeholder="MALE">
            @if ($errors->has('incidence.scabies_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_male') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_female]" value="{{$healthdatas->scabies_female}}" placeholder="FEMALE">
            @if ($errors->has('incidence.scabies_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_frequency]" id="" placeholder="Total" value="{{$healthdatas->scabies_frequency}}">
            @if ($errors->has('incidence.scabies_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
       
        <!-- <h3 for="total_male_member" class="col-md-12"><strong><label>Personal Hygiene</label></strong></h3 > -->
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
        <div style="margin-top: 20px; ">
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Days Absent From School due to Sickness</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[sickness_absence_school]" id="" placeholder="Total" value="{{$healthdatas->sickness_absence_school}}">   
                @if ($errors->has('incidence.sickness_absence_school'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.sickness_absence_school') }}</strong>
                    </span>
                @endif 
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Max Sick Days </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[max_sick_days]" id="" placeholder="Total" value="{{$healthdatas->max_sick_days}}">
                @if ($errors->has('incidence.max_sick_days'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.max_sick_days') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of bed rest adults due to water borne diseases </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_adult]" value="{{$healthdatas->bedrest_adult}}" placeholder="Total">   
                @if ($errors->has('incidence.bedrest_adult'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.bedrest_adult') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of days where adults have bed rest </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_days]" value="{{$healthdatas->bedrest_days}}" placeholder="Total">
                @if ($errors->has('incidence.bedrest_days'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.bedrest_days') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Death Cause</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <select class="form-control" name="incidence[death_cause]">
                    <option value="diarrohea" {{$healthdatas->death_cause == "diarrohea" ? 'selected':''}}>Diarrohea</option>
                    <option value="dysentry" {{$healthdatas->death_cause == "dysentry" ? 'selected':''}}>Dysentry</option>
                    <option value="jaundice" {{$healthdatas->death_cause == "jaundice" ? 'selected':''}}>Jaundice</option>
                    <option value="colera" {{$healthdatas->death_cause == "colera" ? 'selected':''}}>Colera</option>
                    <option value="worms" {{$healthdatas->death_cause == "worms" ? 'selected':''}}>Worms Infection</option>
                    <option value="scabies" {{$healthdatas->death_cause == "scabies" ? 'selected':''}}>Scabies</option>
                </select>   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Reason</label>   
            </div>
        </div>

    </div>
</div>