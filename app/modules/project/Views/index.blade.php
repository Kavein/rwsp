@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Projects		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Projects</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.projects.create') }}" class="btn bg-green waves-effect"  title="create">Create</a><br><br>
						
					</div>
					<div class="box-header">
						<div class="col-md-1">
							<h5><strong>
							Fiscal Year Filter
							</strong></h5>
						</div>
						<div class="col-md-2">
							<select class="form-control" id="fiscal_year" >
								<option value="0">Select Fiscal For Filter</option>
								@foreach($fiscal_years as $fiscal_yearss)
								<option value="{{$fiscal_yearss->id}}" >{{$fiscal_yearss->start_date}} to {{$fiscal_yearss->end_date}}</option>
								@endforeach
							</select>
						    
						</div>
						<div class="col-md-1">
						    <button type="button" name="filter" id="filter" class="btn btn-info">Filter</button>
						</div>
						<div class="col-md-offset-2 col-md-1">
							<label><h5><strong>Generate Report</strong></h5></label>
						</div>
						<div class="col-md-2">
							<select class="form-control" id="generate">
								<option value="">Select Export to Generate</option>
								<option value="{{route('admin.ageGroup.export',@$id)}}">Age Group</option>
								<option value="{{route('admin.project.hygiene.export',@$id)}}">Hygiene</option>
								<option value="{{route('admin.project.improvedhygiene.export',@$id)}}">Improved Hygiene</option>
								<option value="{{route('admin.project.handwash.export',@$id)}}">Sanitary Handwash Habit</option>
								<option value="{{route('admin.project.socialeconomic.export',@$id)}}">Social Economic Status</option>
								<option value="{{route('admin.project.waterBorne.export',@$id)}}">WaterBorne Disease Status</option>
								<option value="{{route('admin.project.womenStatus.export',@$id)}}">Women Status</option>
								<option value="{{route('admin.project.waterFetching.export',@$id)}}">Water Fetching</option>
								<option value="{{route('admin.project.castWise.export',@$id)}}">Cast Wise</option>
								<option value="{{route('admin.project.average_cost.export',@$id)}}">Average Latrine Cost</option>
								<option value="{{route('admin.project.logframe.export',@$id)}}">Logframe</option>
							</select>
						</div>
						<div class="col-md-1">
							<button type="button" name="report_generate" id="report_generate" class="btn btn-info">Generate</button>
						</div>

					</div>
					
					<!-- /.box-header -->
					<div class="box-body">
						<table id="project-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Project Number</th>
								<th >Project Name</th>
								<th >Type</th>
								<th >Fiscal Year</th>
								<th >Funding By</th>
								<th >Status</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		$(document).ready(function(){
			var dataTable; 
			var site_url = window.location.href;
			fill_datatable();

			function fill_datatable(fiscal_year = '')
			{
				
				var dataTable = $('#project-datatable').DataTable({
					dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
					        "<'row'<'col-sm-12'tr>>" +
					        "<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
					"processing" : true,
					"serverSide" : true,
					"order" : [],
					// "searching" : true,
					'ajax' : { url: "{{ route('admin.projects.getdatajson') }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' ,fiscal_year:fiscal_year} },
					columns: [
					      	{ data: function (data, type, row, meta) {
					            return meta.row + meta.settings._iDisplayStart + 1;
					        },name: "sn", searchable: false },
				            { data: "project_no",name: "project_no", searchable: true},
				            { data: "project_name",name: "project_name"},
				            { data: "type",name: "type"},
				            { data: function(data,b,c,table) {
				              return data.fiscal_year.start_date +" to " + data.fiscal_year.end_date ;
				            },name:'fiscal_year', searchable: false},
					        { data: "fundings.name",name: "funding_partner_id"},
				            { data:  function (data, type, row, meta) {
					       		if(data.status == 1){
					       			return 'Enabled'
					       		}else{
					       			return 'Disabled'
					       		}
					      	},name: "status"},
					        
							{ data: function(data,b,c,table) { 
							var buttons = '';

							buttons += "<a class='btn btn-default  waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' title='Edit Project' ><i class='fa fa-pencil'></i></a>&nbsp"; 

							buttons += "<a href='"+site_url+"/delete/"+data.id+"' onclick=\"return (confirm('Are you Sure'))?true:false\" class='btn btn-default  waves-effect' title='Delete'><i class='fa fa-trash'></i></a>&nbsp";

							buttons += "<a href='"+site_url+"/show/"+data.id+"' class='btn btn-default  waves-effect' title='Enter Project'><i class='fa fa-eye' ></i></a>&nbsp";

							buttons += "<a href='"+site_url+"/assign-list/"+data.id+"' class='btn btn-default  waves-effect' title='Assign List' ><i class='fa fa-list-ul'></i></a>";



							return buttons;
							}, name:'action',searchable: false},
					    ]
				});
			}
		
			$('#filter').click(function(){

				var fiscal_year = $('#fiscal_year').val();
				// alert(fiscal_year);
				if(fiscal_year != '')
				{
				$('#project-datatable').DataTable().destroy();
					fill_datatable(fiscal_year);
				}
				else
				{
					$('#project-datatable').DataTable().destroy();
					fill_datatable();
				}
			});

			$('#report_generate').click(function(){
				var fiscal_year = $('#fiscal_year').val();
				var generate = $('#generate').val();
				if(fiscal_year == '' || fiscal_year == null){
					if (generate != '') { // require a URL
			            window.location = generate + '/'+ 0; // redirect
			        }
			        return false;
				}
				else{
					if (generate != '') { // require a URL
			            window.location = generate+'/' + fiscal_year; // redirect
			        }
			        return false;
				}

			});

			// $('#report_generate').click(function(){
			// 	var generate = $('#generate').val();

			// 	if (generate != '') { // require a URL
		 //            window.location = generate; // redirect
		 //        }
		 //        return false;

			// });

		});
		
	</script>

@endsection
