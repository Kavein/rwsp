<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><u>INCIDENCE OF WATER-BORNE DISEASES</u></strong></h3 >
    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DIARRHOEA</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.diarrhoea_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.diarrhoea_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.diarrhoea_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.diarrhoea_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.diarrhoea_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.diarrhoea_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.diarrhoea_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DYSENTERY</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.dysentry_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.dysentry_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.dysentry_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.dysentry_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.dysentry_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_above6_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.dysentry_above6_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.dysentry_above6_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY JAUNDICE </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.jaundice_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.jaundice_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.jaundice_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.jaundice_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.jaundice_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_above6_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.jaundice_above6_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.jaundice_above6_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY COLERA</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.colera_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.colera_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.colera_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.colera_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.colera_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_above6_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.colera_above6_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.colera_above6_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY WORMS INFESTATION</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.worm_infection_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.worm_infection_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.worm_infection_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.worm_infection_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.worm_infection_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_above6_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.worm_infection_above6_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.worm_infection_above6_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY SCABIES </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_0to5_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.scabies_0to5_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_0to5_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_0to5_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.scabies_0to5_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_0to5_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( 0 - 5 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_0to5_frequency]" id="" placeholder="FEMALE">
            @if ($errors->has('incidence.scabies_0to5_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_0to5_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Age Group ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_above6_male]" id="" placeholder="MALE">
            @if ($errors->has('incidence.scabies_above6_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_above6_male') }}</strong>
                </span>
            @endif
        </div>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_above6_female]" id="" placeholder="MALE">
            @if ($errors->has('incidence.scabies_above6_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_above6_female') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness ( Above Age of 6 )</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_above6_frequency]" id="" placeholder="Total">
            @if ($errors->has('incidence.scabies_above6_frequency'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('incidence.scabies_above6_frequency') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
       
        <!-- <h3 for="total_male_member" class="col-md-12"><strong><label>Personal Hygiene</label></strong></h3 > -->
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
        <div style="margin-top: 20px; ">
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Days Absent From School due to Sickness</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[sickness_absence_school]" id="" placeholder="Total">
                @if ($errors->has('incidence.sickness_absence_school'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.sickness_absence_school') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Max Sick Days (Children) </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[max_sick_days]" id="" placeholder="Total">
                @if ($errors->has('incidence.max_sick_days'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.max_sick_days') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of bed rest adults due to water borne diseases </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_adult]" id="" placeholder="Total">
                @if ($errors->has('incidence.bedrest_adult'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.bedrest_adult') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of days where adults have bed rest </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_days]" id="" placeholder="Total">
                @if ($errors->has('incidence.bedrest_days'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('incidence.bedrest_days') }}</strong>
                    </span>
                @endif   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Death Cause</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <select class="form-control" name="incidence[death_cause]">
                    <option value="none">Select Death Cause</option>
                    <option value="diarrohea">Diarrohea</option>
                    <option value="dysentry">Dysentry</option>
                    <option value="jaundice">Jaundice</option>
                    <option value="colera">Colera</option>
                    <option value="worms">Worms Infection</option>
                    <option value="scabies">Scabies</option>
                </select>   
            </div>
            {{--
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Reason</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <textarea name="incidence[reason]" id="reason">
                    
                </textarea>  
            </div>
                --}}
        </div>

    </div>
</div>