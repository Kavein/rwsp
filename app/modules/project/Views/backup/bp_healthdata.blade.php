<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><u>INCIDENCE OF WATER-BORNE DISEASES</u></strong></h3 >
    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DIARRHOEA</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[diarrhoea_frequency]" id="" placeholder="Total">
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY DYSENTERY</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[dysentry_frequency]" id="" placeholder="Total">
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY JAUNDICE </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[jaundice_frequency]" id="" placeholder="Total">
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY COLERA</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[colera_frequency]" id="" placeholder="Total">
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY WORMS INFESTATION</strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[worm_infection_frequency]" id="" placeholder="Total">
        </div>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong>INCIDENCE BY SCABIES </strong></h4 >

    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Male</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_male]" id="" placeholder="MALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Female</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_female]" id="" placeholder="FEMALE">
        </div>
    </div>
    <div class="form-group col-md-12">
        <label for="name" class="col-sm-4 col-form-label">Frequency Of Sickness</label>
        <div class="col-sm-4">
            <input type="number" class="form-control" name="incidence[scabies_frequency]" id="" placeholder="Total">
        </div>
    </div>
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
       
        <!-- <h3 for="total_male_member" class="col-md-12"><strong><label>Personal Hygiene</label></strong></h3 > -->
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->
        <div style="margin-top: 20px; ">
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Days Absent From School due to Sickness</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[sickness_absence_school]" id="" placeholder="Total">   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Number of Max Sick Days (Children) </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[max_sick_days]" id="" placeholder="Total">   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of bed rest adults due to water borne diseases </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_adult]" id="" placeholder="Total">   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">No of days where adults have bed rest </label>   
            </div>
            <div class="form-group col-md-9 "> 
                <input type="number" class="form-control" name="incidence[bedrest_days]" id="" placeholder="Total">   
            </div>
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Death Cause</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <select class="form-control" name="incidence[death_cause]">
                    <option value="none">Select Death Cause</option>
                    <option value="diarrohea">Diarrohea</option>
                    <option value="dysentry">Dysentry</option>
                    <option value="jaundice">Jaundice</option>
                    <option value="colera">Colera</option>
                    <option value="worms">Worms Infection</option>
                    <option value="scabies">Scabies</option>
                </select>   
            </div>
            {{--
            <div class="form-group col-md-3 "> 
            <label for="handwash_before_eating">Reason</label>   
            </div>
            <div class="form-group col-md-9 "> 
                <textarea name="incidence[reason]" id="reason">
                    
                </textarea>  
            </div>
                --}}
        </div>

    </div>
</div>