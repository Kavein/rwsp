@extends('admin.layout.main')
@section('content')

<section class="content">
      
    <div class="row">
        <!-- /.col -->
        <div class="col-md-12">
            <form action="{{route('admin.projects.household.update',$household->id)}}" method="post">  
            {{method_field('PATCH')}}          
                <h3>Initial Phase</h3>
                <div class="nav-tabs-custom">
                    <ul class="nav nav-tabs">
                            <li class=""><a href="#household" data-toggle="tab" aria-expanded="false">HouseHold Category</a></li>
                            <li class=""><a href="#water_fetching" data-toggle="tab" aria-expanded="false">Water Fetching</a></li>
                            <li class=""><a href="#hygiene" data-toggle="tab" aria-expanded="false">Data On Hygiene And Sanitation</a></li>
                            <li class=""><a href="#healthData" data-toggle="tab" aria-expanded="false">Health Data</a></li>
                            <li class=""><a href="#healthBehavior" data-toggle="tab" aria-expanded="false">Health Seeking Behaviors</a></li>

                            <li class=""><a href="#inclusionData" data-toggle="tab" aria-expanded="false">Gender On Social Inclusion Data</a></li>
                            

                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="household">
                            @include('project::household_edit.edit_householdform')
                        </div>
                        <div class="tab-pane" id="water_fetching">
                            @include('project::household_edit.edit_water_fetching')
                        </div>
                        <div class="tab-pane" id="hygiene">
                            @include('project::household_edit.edit_hygiene')
                        </div>
                        <div class="tab-pane" id="healthData">
                            @include('project::household_edit.edit_healthdata')
                        </div>
                        <div class="tab-pane" id="healthBehavior">
                            @include('project::household_edit.edit_healthBehavior')
                        </div>
                        <div class="tab-pane" id="inclusionData">
                            @include('project::household_edit.edit_inclusionData')
                        </div> 
                            
                    </div>
                <!-- /.tab-content -->
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ URL::previous() }}" class="btn btn-danger">Cancel</a>
                </div>
                </div>
            <!-- /.nav-tabs-custom -->

            </form>
        </div>
        <!-- /.col -->
        
      </div>
    </section>
<script type="text/javascript">
        // Get the checkbox
    $('#latrine_available').click(function(){
        if($('#latrine_available').is(":checked")){
            $('#total_latrine').show();
            $('#latrine_cleaning_brush').show();
            $('#latrine_cleansing_agent').show();
            $('#latrine_water_available').show();
        }else{
            $('#total_latrine').hide();
            $('#latrine_cleansing_agent').hide();
            $('#latrine_cleaning_brush').hide();
            $('#latrine_water_available').hide();
        }
    })

    $('#income_pension').click(function(){
        if($('#income_pension').is(":checked")){
            $('#income_pension_option').show();
        }else{
            $('#income_pension_option').hide();
        }
    })

    /*$('#incident_women').click(function(){
        if($('#incident_women').is(":checked")){
            $('#other_incident_women').show();
        }else{
            $('#other_incident_women').hide();
        }
    })*/
    $('.water_collection_task').click(function(){
        var value = $(this).val();
        if(value == 'incident_women'){
            $('#other_incident_women').show();
        }else{
            $('#other_incident_women').hide();
        }
    });

    $('.effect_on_children').click(function(){
        var value = $(this).val();
        if(value == 'others'){
            $('#other_effect_on_children').show();
        }else{
            $('#other_effect_on_children').hide();
        }
    });

    $(function () {
        CKEDITOR.replace('reason');
        uploadReady();
    });

    function getCastDivision(){
        var cast_division = $('#cast_division').val();
        $.post("{{route('admin.projects.cast_division')}}",{cast_division : cast_division,_token:'{{ csrf_token() }}'},function(result){
                $('#cast_id').empty();
                var html="";
                $.each(result,function(k,v){
                    html+= "<option value='"+v.id+"'>"+v.name+"</option>"
                });
                $('#cast_id').html(html);
        },'json');
    }

    // var checkBox = document.getElementById("latrine_available");
    // // Get the output text
    // var text = document.getElementById("total_latrine");
    // if (checkBox.checked == true){
    //     text.hidden = false;
    // } else {
    //     text.hidden = true;
    // }
</script>
@endsection
