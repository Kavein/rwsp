<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><label>Gender On Social Inclusion Data</label></strong></h3 >
    <h4 for="total_male_member" class="col-md-12"><strong><label>WORK DIVISION BY GENDER AT HOUSEHOLD LEVEL</label></strong></h4 >
    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Household Work</label>    
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[hh_male]" placeholder="Male" value="{{$inclusiondatas->hh_male}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[hh_female]" placeholder="Female" value="{{$inclusiondatas->hh_female}}">
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[hh_boys]" placeholder="Boys" value="{{$inclusiondatas->hh_boys}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[hh_girls]" placeholder="Girls" value="{{$inclusiondatas->hh_girls}}">
            </div>
        </div>  
    </div>
    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Water Fetching</label>    
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[wf_male]" placeholder="Male" value="{{$inclusiondatas->wf_male}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[wf_female]" placeholder="Female" value="{{$inclusiondatas->wf_female}}">
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[wf_boys]" placeholder="Boys" value="{{$inclusiondatas->wf_boys}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[wf_girls]" placeholder="Girls" value="{{$inclusiondatas->wf_girls}}">
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Productive Work</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[pw_male]" placeholder="Male" value="{{$inclusiondatas->pw_male}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[pw_female]" placeholder="Female" value="{{$inclusiondatas->pw_female}}">
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[pw_boys]" placeholder="Boys" value="{{$inclusiondatas->pw_boys}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[pw_girls]" placeholder="Girls" value="{{$inclusiondatas->pw_girls}}">
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Community Work</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[cw_male]" placeholder="Male" value="{{$inclusiondatas->cw_male}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[cw_female]" placeholder="Female" value="{{$inclusiondatas->cw_female}}">
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[cw_boys]" placeholder="Boys" value="{{$inclusiondatas->cw_boys}}">
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[cw_girls]" placeholder="Girls" value="{{$inclusiondatas->cw_girls}}">
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">PRACTICE OF KITCHEN GARDENING</label>
        
        <div class="col-md-4">
            <input type="radio" name="inclusion[kitchen_gardening_practice]" value="Yes" {{$inclusiondatas->kitchen_gardening_practice == "Yes" ? 'checked':''}}>
            <label>Yes</label>
            <input type="radio" name="inclusion[kitchen_gardening_practice]" value="No" style="margin-left: 100px;"{{$inclusiondatas->kitchen_gardening_practice == "No" ? 'checked':''}}>
            <label>No</label>
        </div>
        
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong><label>OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER</label></strong></h4 >
    <h5 for="total_male_member" class="col-md-12"><strong><label>EFFECT ON WOMEN DUE TO WATER COLLECTION TASK</label></strong></h5 >
    <div class="col-md-12">
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="increased_burdon" class="water_collection_task" {{$inclusiondatas->effect_on_women == "increased_burdon" ? 'checked':''}}>
            <label>INCREASED BURDON OF HOUSEHOLD WORK</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="adverse_affect" class="water_collection_task" {{$inclusiondatas->effect_on_women == "adverse_affect" ? 'checked':''}}>
            <label>ADVERSE AFFECT ON WOMEN'S HEALTH</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="discomfort" class="water_collection_task" {{$inclusiondatas->effect_on_women == "discomfort" ? 'checked':''}}>
            <label>DISCOMFORT AND DRUDGERY</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="incident_women" id="incident_women" class="water_collection_task" {{$inclusiondatas->effect_on_women == "incident_women" ? 'checked':''}} >
            <label>OTHER INCIDENCES ON WOMEN</label>
        </div>
        <textarea rows="5" cols="150" id="other_incident_women" hidden name="inclusion[other_incident_women]">
            {{$inclusiondatas->other_incident_women}}
        </textarea>
    </div>

    <h5 for="total_male_member" class="col-md-12"><strong><label>Effect On Children Due To Water Collection Task</label></strong></h5 >
    <div class="col-md-12">
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value="homework" class="effect_on_children" {{$inclusiondatas->effect_on_children == "homework" ? 'checked':''}}>
            <label>NO TIME FOR HOMEWORK</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value="absent_from_school" class="effect_on_children" {{$inclusiondatas->effect_on_children == "absent_from_school" ? 'checked':''}}>
            <label>ABSENT FROM SCHOOL CLASS</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value="others" class="effect_on_children" {{$inclusiondatas->effect_on_children == "others" ? 'checked':''}}>
            <label>OTHER</label>
        </div>
        <textarea rows="5" cols="150" id="other_effect_on_children" hidden name="inclusion[other_effect_on_children]">
            {{$inclusiondatas->other_effect_on_children}}
        </textarea>
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong><label>OTHER INCIDENCES ON CHILDREN</label></strong></h4 >
    
    
    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER</label>
        
        <div class="col-sm-6">
            <input type="text" name="inclusion[out_migrating]" class="form-control" placeholder="OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER" value="{{$inclusiondatas->out_migrating}}">
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">NUMBER OF CHILDREN GOING TO SCHOOL</label>
        
        <div class="col-sm-3">
            <input type="text" name="inclusion[school_going_children_male]" class="form-control" value="{{$inclusiondatas->school_going_children_male}}" placeholder="Boys">
        </div>  
        <div class="col-sm-3">
            <input type="text" name="inclusion[school_going_children_female]" class="form-control" value="{{$inclusiondatas->school_going_children_female}}" placeholder="Girls">
        </div>  
    </div> 
</div>