
    <div class="form-group col-md-12">
        <div class="form-group col-md-6">
            <label for="app_received_date">Application Received Date</label>
            <input type="text" name="app_received_date" id="app_received_date" class="form-control" autocomplete="off" value="{{$project->app_received_date}}" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="project_no">Project Number</label>
            <input type="text" name="project_no" id="project_no" class="form-control" value = "{{$project->project_no}}" >
            @if ($errors->has('project_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('project_no') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="project_name">Project Name</label>
            <input type="text" name="project_name" id="project_name" class="form-control" value = "{{$project->project_name}}">
            @if ($errors->has('project_name'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('project_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">Province Name</label>
            <select class="form-control" name="province_id" id="province_id" onChange="getDistrict()">
                <option value=""> Select Province</option>
                @foreach($provinces as $province)
                    <option value="{{$province->id}}" {{$project->province_id == $province->id ? 'selected':''}}> {{$province->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('province_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('province_id') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="confirm_password">District Name</label>
            <select class="form-control" name="district_id" id="district_id" onChange="getVdc()"> 
                @foreach($districts as $district)
                    <option value="{{$district->id}}" {{$project->district_id == $district->id ? 'selected':''}}> {{$district->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('district_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('district_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">VDCs / Muncipality</label>
            <select class="form-control" name="vdc_id" id="vdc_id">
                @foreach($vdcs as $vdc)
                    <option value="{{$vdc->id}}" {{$project->vdc_id == $vdc->id ? 'selected':''}}> {{$vdc->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('vdc_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('vdc_id') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="form-group col-md-6">
            <label for="ward_no">Ward Number</label>
            <input type="number" name="ward_no" id="ward_no" class="form-control" value = "{{$project->ward_no}}" >
            @if ($errors->has('ward_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('ward_no') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">Project Type</label>
            <select class="form-control" name="type" id="type">
               <option value="new" {{$project->type == "new" ? 'selected':''}}>New</option>
            <option value="re-build" {{$project->type == "re-build" ? 'selected':''}}> Re-Build</option>Terai</option>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="geographical_region">Geographical Region</label>
            <select class="form-control" name="geographical_region" id="geographical_region">
                <option value="mountain" {{$project->geographical_region == "mountain" ? 'selected':''}}>Mountain</option>
                <option value="hill" {{$project->geographical_region == "hill" ? 'selected':''}}> Hilly</option>
                <option value="terai" {{$project->geographical_region == "terai" ? 'selected':''}}> Terai</option>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        
        <div class="form-group col-md-6">
            <label for="feasibility_assessment_date">Date Of Initial Feasibility And Need Assessment</label>
            <input type="text" name="feasibility_assessment_date" id="feasibility_assessment_date" class="form-control" autocomplete="off" value="{{$project->feasibility_assessment_date}}">
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="system_type_id">System Type</label>
            <select class="form-control" name="system_type_id" id="system_type_id" >
                @foreach($systems as $system)
                    <option value="{{$system->id}}" {{$project->system_type_id == $system->id ? 'selected':''}} >{{$system->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="service_type_id">Service Type</label>
            <select class="form-control" name="service_type_id" id="service_type_id" >
                @foreach($services as $service)
                    <option value="{{$service->id}}" {{$project->service_type_id == $service->id ? 'selected':''}}>{{$service->name}}</option>
                @endforeach
            </select>
        </div> 
    </div>
    <div class="col-md-12"> 
        <div class="form-group col-md-6">
            <label for="water_source_id">Sources Of Drinking Water</label>
            <!-- <input type="text" name="water_source_id" id="water_source_id" class="form-control" autocomplete="off" > -->
            <select class="form-control" name="water_source_id[]" id="water_source_id" multiple >
                @foreach($water_sources as $source)
                    <option value="{{$source->id}}" {{ in_array($source->id, $pjr_water_obj)?'selected':'' }}>{{$source->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="no_existing_water_system">Number Of Existing Water Supply System</label>
            <input type="number" name="no_existing_water_system" id="no_existing_water_system" class="form-control" autocomplete="off" value="{{$project->no_existing_water_system}}" >
            @if ($errors->has('no_existing_water_system'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('no_existing_water_system') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="no_reservoir">Number of Reservoir Tanks</label>
            <input type="number" name="no_reservoir" id="no_reservoir" class="form-control" autocomplete="off" value="{{$project->no_reservoir}}" >
        </div>

        <div class="form-group col-md-6">
            <label for="no_taps">Number of Taps</label>
            <input type="number" name="no_taps" id="no_taps" class="form-control" autocomplete="off" value="{{$project->no_taps}}" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="h2s_result">H2S Strip Result</label>
            <textarea name="h2s_result" id="h2s_result">
                {{$project->h2s_result}}
            </textarea>
        </div>

        <div class="form-group col-md-6">
            <label for="ph_result">Result Of pH Test Of Water</label>
            <textarea name="ph_result" id="ph_result">
                {{$project->ph_result}}
            </textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="system_built_date">System Bulit Date</label>
            <input type="text" name="system_built_date" id="system_built_date" class="form-control" autocomplete="off" value="{{$project->system_built_date}}" >
        </div>
        <div class="form-group col-md-6">
            <label for="population">Population</label>
            <input type="text" name="population" id="population" class="form-control" autocomplete="off" value="{{$project->population}}" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="start_date">Start Date</label>
            <input type="text" name="start_date" id="start_date" class="form-control" autocomplete="off" value="{{$project->start_date}}">
        </div>
        <div class="form-group col-md-6">
            <label for="end_date">End Date</label>
            <input type="text" name="end_date" value="{{$project->end_date}}" id="end_date" class="form-control" autocomplete="off" >
        </div>
    </div>

    <input type="hidden" name="fiscal_year" id="fiscal_year" class="form-control" value="{{$fiscal_year->id}}" readonly>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="funding_partner_id">Funding Partner Name</label>
            <select class="form-control" name="funding_partner_id" id="funding_partner_id">
                @foreach($fundings as $funding)
                    <option value="{{$funding->id}}" {{$project->funding_partner_id == $funding->id ? 'selected':''}}> {{$funding->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="awc_id">Awc Name</label>
             <input type="text" name="awc_id" class="form-control" autocomplete="off" value="{{$project->awc_id}}" >
            {{--
                <select class="form-control" name="awc_id" id="awc_id">
                    @foreach($awcs as $awc)
                        <option value="{{$awc->id}}" {{$project->awc_id == $awc->id ? 'selected':''}}> {{$awc->name}}</option>
                    @endforeach
                </select>
            --}}
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-12">
            <label for="status">Status</label><br>
            <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" {{$project->status == 1 ? 'checked':''}} >
        </div>
    </div>
    
    <input type="hidden" name="id" id="id" value = "{{$project->id}}" />
    <script>
            jQuery(document).ready(function($) {
                $('#water_source_id').select2();
            });
    </script>