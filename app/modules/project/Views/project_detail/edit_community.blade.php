    
    <label for="school_data" class="col-form-label"><h3><strong>Community Name</strong></h3></label>
    <div class="row">
        <div class="col-md-6 form-group">
                <div class="form-check form-check-inline">
                    <input class="form-check-input form-control" type="text" name="community[community_name]" placeholder="Community Name " value="{{$community_data->community_name}}">
                </div>
        </div>
    </div>
    <div class="" style="border-top: 1px solid #b7c2cc; ">
    </div>
        <?php $i = 1;?>
        <h3 for="" class=""><strong><label><u>School Data</u></label></strong></h3 >
        @foreach($school_detail as $detail)

        <div id="school_info" style="border: 1px solid #b7c2cc;padding-top: 15px;margin-bottom:4px; ">
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">School Name</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="text" name="school_name[]" id="school_name_1" value="{{$detail->school_name}}" >
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Male Students</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_male[]" id="school_male_1" value="{{$detail->male_number}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Female Students</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_female[]" id="school_female_1" value="{{$detail->female_number}}">
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Total Students</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_student[]" id="school_student_1" value="{{$detail->total_student}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Total Staff</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_staff[]" id="school_staff_1" value="{{$detail->total_staff}}">
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Total Existing Latrine</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_latrine[]" id="school_latrine_1" value="{{$detail->total_latrine}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Tapstand Required</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="school_tapstand[]" id="school_tapstand_1" value="{{$detail->tapstand_required}}">
                        </div>
                    </div>  
                </div>
            </div>
            <input type="hidden" name="school_find_id[]" value="{{ $detail->id }}">
        </div>
        <?php $i++; ?>
        @endforeach
    <div id="second"></div>
    <div class="row" style="padding: 10px;">
        <button type="button" id="btnAdd" class="btn btn-primary ">Add More</button>
        <button type="button" id="btnDel" class="btn btn-danger ">Remove</button>
    </div>

    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
    </div>

    <?php $i = 1;?>
        <h3 for="" class=""><strong><label><u>Other Institution</u></label></strong></h3 >

        @foreach($institute_detail as $detail)

        <div id="school_info" style="border: 1px solid #b7c2cc;padding-top: 15px;margin-bottom:4px; ">
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Institute Name</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="text" name="institute_name[]" id="institute_name_1" value="{{$detail->institute_name}}" >
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Male Staff</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_male[]" id="institute_male_1" value="{{$detail->male_number}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Female Staff</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_female[]" id="institute_female_1" value="{{$detail->female_number}}">
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Total Staff</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_staff[]" id="institute_staff_1" value="{{$detail->total_staff}}">
                        </div>
                    </div>  
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Total Existing Latrine</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_latrine[]" id="institute_latrine_1" value="{{$detail->total_latrine}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Latrine Required</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_latrine_required[]" id="institute_latrine_required_1" value="{{$detail->latrine_required}}">
                        </div>
                    </div>  
                </div>
                <div class="col-md-6 form-group "> 
                    <label for="school_data" class="col-sm-3 col-form-label">Tapstand Required</label>
                    
                    <div class="col-sm-8">
                        <div class="form-check form-check-inline">
                            <input class="form-check-input form-control" type="number" name="institute_tapstand[]" id="institute_tapstand_1" value="{{$detail->tapstand_required}}">
                        </div>
                    </div>  
                </div>
            </div>
            <input type="hidden" name="institute_find_id[]" value="{{ $detail->id }}">
        </div>
        <?php $i++; ?>
        @endforeach
    <div id="third"></div>
    <div class="row" style="padding: 10px;">
        <button type="button" id="btn_ins_Add" class="btn btn-primary ">Add More</button>
        <button type="button" id="btn_ins_Del" class="btn btn-danger ">Remove</button>
    </div>
   

    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
    </div>

    <h3 for="" class="col-md-12"><strong><label><u>Health data provided by Health Post or Primary Health Care Center</u></label></strong></h3 >

    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-3 col-form-label"></label>
        <label for="school_data" class="col-sm-3 col-form-label"><h4><strong>Male</strong></h4></label>
        <label for="school_data" class="col-sm-3 col-form-label"><h4><strong>Female</strong></h4></label>
        <label for="school_data" class="col-sm-3 col-form-label"><h4><strong>Total </strong></h4></label>                             
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Diarrhoea </label>
        <div class="col-md-3">
            <input type="number" name="community[diarrhoea_male]" class="form-control" value="{{$community_data->diarrhoea_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[diarrhoea_female]" class="form-control" value="{{$community_data->diarrhoea_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[diarrhoea_total]" class="form-control" value="{{$community_data->diarrhoea_total}}">
        </div>       
    </div>
    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Dysentery </label>
        <div class="col-md-3">
            <input type="number" name="community[dysentery_male]" class="form-control" value="{{$community_data->dysentery_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[dysentery_female]" class="form-control" value="{{$community_data->dysentery_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[dysentery_total]" class="form-control" value="{{$community_data->dysentery_total}}">
        </div>       
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Jaundice </label>
        <div class="col-md-3">
            <input type="number" name="community[jaundice_male]" class="form-control" value="{{$community_data->jaundice_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[jaundice_female]" class="form-control" value="{{$community_data->jaundice_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[jaundice_total]" class="form-control" value="{{$community_data->jaundice_total}}">
        </div>       
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Colera </label>
        <div class="col-md-3">
            <input type="number" name="community[colera_male]" class="form-control" value="{{$community_data->colera_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[colera_female]" class="form-control" value="{{$community_data->colera_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[colera_total]" class="form-control" value="{{$community_data->colera_total}}">
        </div>       
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Worm Infection </label>
        <div class="col-md-3">
            <input type="number" name="community[worms_male]" class="form-control" value="{{$community_data->worms_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[worms_female]" class="form-control" value="{{$community_data->worms_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[worms_total]" class="form-control" value="{{$community_data->worms_total}}">
        </div>       
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Scabies </label>
        <div class="col-md-3">
            <input type="number" name="community[scabies_male]" class="form-control" value="{{$community_data->scabies_male}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[scabies_female]" class="form-control" value="{{$community_data->scabies_female}}">
        </div>
        <div class="col-md-3">
            <input type="number" name="community[scabies_total]" class="form-control" value="{{$community_data->scabies_total}}">
        </div>       
    </div>

    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; padding-bottom: 25px ">
    </div>

    <div class="col-md-12 form-group row">
        <label for="school_data" class="col-sm-3 col-form-label">Date of Social Profiling </label>
        <div class="col-md-4">
            <input type="text" name="community[profiling_date]" id="profiling_date" class="form-control" value="{{$community_data->profiling_date}}">
        </div>      
    </div>

    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
    </div>

    <h3 for="" class="col-md-12"><strong><label><u>Total Number of Disability</u></label></strong></h3 >
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label"><h4><strong>Characteristics</strong></h4></label>
        <label for="school_data" class="col-sm-4 col-form-label"><h4><strong>Male</strong></h4></label>
        <label for="school_data" class="col-sm-4 col-form-label"><h4><strong>Female</strong></h4></label>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Physical disability</label>
        <div class="col-md-4">
            <input type="number" name="community[disabled_male]" class="form-control" value="{{$community_data->disabled_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[disabled_female]" class="form-control" value="{{$community_data->disabled_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Blindness/Low vision</label>
        <div class="col-md-4">
            <input type="number" name="community[blind_male]" class="form-control" value="{{$community_data->blind_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[blind_female]" class="form-control" value="{{$community_data->blind_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Deaf/hard to hearing</label>
        <div class="col-md-4">
            <input type="number" name="community[deaf_male]" class="form-control" value="{{$community_data->deaf_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[deaf_female]" class="form-control" value="{{$community_data->deaf_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Deaf-blind</label>
        <div class="col-md-4">
            <input type="number" name="community[blind_deaf_male]" class="form-control" value="{{$community_data->blind_deaf_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[blind_deaf_female]" class="form-control" value="{{$community_data->blind_deaf_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Speech problem</label>
        <div class="col-md-4">
            <input type="number" name="community[speech_male]" class="form-control" value="{{$community_data->speech_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[speech_female]" class="form-control" value="{{$community_data->speech_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Mental disability</label>
        <div class="col-md-4">
            <input type="number" name="community[mental_male]" class="form-control" value="{{$community_data->mental_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[mental_female]" class="form-control" value="{{$community_data->mental_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Intellectual disability</label>
        <div class="col-md-4">
            <input type="number" name="community[intel_male]" class="form-control" value="{{$community_data->intel_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[intel_female]" class="form-control" value="{{$community_data->intel_female}}">
        </div>
    </div>
    <div class="col-md-12 form-group row"> 
        <label for="school_data" class="col-sm-4 col-form-label">Multiple disability</label>
        <div class="col-md-4">
            <input type="number" name="community[multiple_male]" class="form-control" value="{{$community_data->multiple_male}}">
        </div>
        <div class="col-md-4">
            <input type="number" name="community[multiple_female]" class="form-control" value="{{$community_data->multiple_female}}">
        </div>
    </div>

   
    <div class="col-sm-12 row">
        <label class="col-sm-4">Community mapping (Just need to Upload photo)</label>
        <input type="file" name="community_mapping" class="col-sm-8">
        <input type="hidden" name="oldImage" value="{{($community_data->community_mapping)?$community_data->community_mapping:''}}">

    </div>

    <script type="text/javascript">
        var globel = 1;
        var ins_glob = 1;
        $('#btnAdd').click(function(){ 
        var id = globel;
        id++;
        globel++;
        $('#second').before('<div class="added-row" style="border: 1px solid #b7c2cc;padding-top: 15px;margin-bottom:4px; ">'
                +'<div class="row">'
                    +'<div class="col-md-6 form-group "> '
                        +'<label for="school_data" class="col-sm-3 col-form-label">School Name</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="text" name="school_name[]" id="school_name_'+id+'" placeholder="School Name">'
                            +'</div>'
                        +'</div>  '
                    +'</div>'
                +'</div>'
                +'<div class="row">'
                    +'<div class="col-md-6 form-group "> '
                        +'<label for="school_data" class="col-sm-3 col-form-label">Male Students</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="number" name="school_male[]" id="school_male_'+id+'" placeholder="Male Students">'
                            +'</div>'
                        +'</div>  '
                    +'</div>'
                    +'<div class="col-md-6 form-group "> '
                        +'<label for="school_data" class="col-sm-3 col-form-label">Female Students</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="number" name="school_female[]" id="school_female_'+id+'" placeholder="Female Students">'
                            +'</div>'
                        +'</div>  '
                    +'</div>'
                +'</div>'

                +'<div class="row">'
                    +'<div class="col-md-6 form-group ">'
                        +'<label for="school_data" class="col-sm-3 col-form-label">Total Students</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="number" name="school_student[]" id="school_student_'+id+'" placeholder="Total Students">'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                    +'<div class="col-md-6 form-group ">'
                        +'<label for="school_data" class="col-sm-3 col-form-label">Total Staff</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="number" name="school_staff[]" id="school_staff_'+id+'" placeholder="Total Staff">'
                            +'</div>'
                        +'</div>'
                    +'</div>'
                +'</div>'
                +'<div class="row">'
                    +'<div class="col-md-6 form-group ">'
                        +'<label for="school_data" class="col-sm-3 col-form-label">Total Existing Latrine</label>'
                        
                        +'<div class="col-sm-8">'
                            +'<div class="form-check form-check-inline">'
                                +'<input class="form-check-input form-control" type="number" name=