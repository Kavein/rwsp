<div class="form-group row">
	<div class="col-md-12">
		<label>Number of reduced death toll / economic output remains stable in face of disasters</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[death_toll]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. Wards  with DRR capacity in place</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[ddr]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of GWT project staff trained on emergency preparedness and response</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[gwt_staff]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of district and palika representatives and FCHV staff trained in leadership for emergency preparedness response </label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[fchv_staff]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of AWC pre-positioned with stockpile of emergency response items</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[awc_stock]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of wards  that have established an EOC and prepared a DPRP</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[eoc_dprp]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of communities that have established/strengthened community disaster management committees (CDMCs) and task forces under the WSUC and are capacitated for emergency preparedness and response</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[cdmc_wsuc]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of CDMCs that have prepared emergency preparedness plans</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[cdmc_plan]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of CDMCs that have conducted awareness-raising activities for emergency preparedness, particularly for the most vulnerable households in WASH programme communities</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[cdmc_wash]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of CDMC task force members trained on community-based search and rescue (CBSAR), first aid, EWS, assessment, logistics management, and E-WASH </label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[cmdc_task]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Number of most vulnerable CDMCs pre-positioned with hazard-specific search and rescue material, relief materials for most vulnerable</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="disaster[cdmc_search]" class="form-control">
	</div>
</div>