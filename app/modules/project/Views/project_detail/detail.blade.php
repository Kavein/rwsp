
    <div class="form-group col-md-12">
        <div class="form-group col-md-6">
            <label for="app_received_date">Application Received Date</label>
            <input type="text" name="app_received_date" id="app_received_date" class="form-control" autocomplete="off" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="project_no">Project Number</label>
            <input type="text" name="project_no" id="project_no" class="form-control" value="{{ old('project_no') }}" >
            @if ($errors->has('project_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('project_no') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="project_name">Project Name</label>
            <input type="text" name="project_name" id="project_name" class="form-control" value="{{ old('project_name') }}">
            @if ($errors->has('project_name'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('project_name') }}</strong>
                </span>
            @endif
        </div>
    </div>
    
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">Province Name</label>
            <select class="form-control" name="province_id" id="province_id" onChange="getDistrict()">
                <option value=""> Select Province</option>
                @foreach($provinces as $province)
                <option value="{{$province->id}}">{{$province->name}}</option>
                @endforeach
            </select>
            @if ($errors->has('province_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('province_id') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-6">
            <label for="confirm_password">District Name</label>
            <select class="form-control" name="district_id" id="district_id" onChange="getVdc()"> 
                <option value="">Select Province For District</option>
            </select>
            @if ($errors->has('district_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('district_id') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">Gaupalika / Nagarpalika</label>
            <select class="form-control" name="vdc_id" id="vdc_id">
                <option value="">Select District For Gaupalika / Nagarpalika</option>
            </select>
            @if ($errors->has('vdc_id'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('vdc_id') }}</strong>
                </span>
            @endif
        </div>
        
        <div class="form-group col-md-6">
            <label for="ward_no">Ward Number</label>
            <input type="number" name="ward_no" id="ward_no" class="form-control" value="{{ old('ward_no') }}" >
            @if ($errors->has('ward_no'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('ward_no') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="confirm_password">Project Type</label>
            <select class="form-control" name="type" id="type">
               <option value="new">New</option>
               <option value="re-build">Re-Build</option>
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="geographical_region">Geographical Region</label>
            <select class="form-control" name="geographical_region" id="geographical_region">
               <option value="mountain">Mountain</option>
               <option value="hill">Hilly</option>
               <option value="terai">Terai</option>
            </select>
        </div>
    </div>
    <div class="col-md-12">
        
        <div class="form-group col-md-6">
            <label for="feasibility_assessment_date">Date Of Initial Feasibility And Need Assessment</label>
            <input type="text" name="feasibility_assessment_date" id="feasibility_assessment_date" class="form-control" autocomplete="off" >
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="system_type_id">System Type</label>
            <select class="form-control" name="system_type_id" id="system_type_id" >
                @foreach($systems as $system)
                <option value="{{$system->id}}">{{$system->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="service_type_id">Service Type</label>
            <select class="form-control" name="service_type_id" id="service_type_id" >
                @foreach($services as $service)
                <option value="{{$service->id}}">{{$service->name}}</option>
                @endforeach
            </select>
        </div> 
    </div>
    <div class="col-md-12"> 
        <div class="form-group col-md-6">
            <label for="water_source_id">Sources Of Drinking Water</label>
            <!-- <input type="text" name="water_source_id" id="water_source_id" class="form-control" autocomplete="off" > -->
            <select class="form-control" name="water_source_id[]" id="water_source_id" multiple >
                @foreach($water_sources as $source)
                <option value="{{$source->id}}">{{$source->name}}</option>
                @endforeach
            </select>
        </div>

        <div class="form-group col-md-6">
            <label for="no_existing_water_system">Number Of Existing Water Supply System</label>
            <input type="number" name="no_existing_water_system" id="no_existing_water_system" class="form-control" autocomplete="off" >
            @if ($errors->has('no_existing_water_system'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('no_existing_water_system') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="no_reservoir">Number of Reservoir Tanks</label>
            <input type="number" name="no_reservoir" id="no_reservoir" class="form-control" autocomplete="off" >
        </div>

        <div class="form-group col-md-6">
            <label for="no_taps">Number of Taps</label>
            <input type="number" name="no_taps" id="no_taps" class="form-control" autocomplete="off" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="h2s_result">H2S Strip Result</label>
            <textarea name="h2s_result" id="h2s_result">
                
            </textarea>
        </div>

        <div class="form-group col-md-6">
            <label for="ph_result">Result Of pH Test Of Water</label>
            <textarea name="ph_result" id="ph_result">
                
            </textarea>
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="system_built_date">System Bulit Date</label>
            <input type="text" name="system_built_date" id="system_built_date" class="form-control" autocomplete="off" >
        </div>
        <div class="form-group col-md-6">
            <label for="population">Population</label>
            <input type="number" name="population" id="population" class="form-control" autocomplete="off" >
        </div>
    </div>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="start_date">Start Date</label>
            <input type="text" name="start_date" id="start_date" class="form-control" autocomplete="off">
        </div>
        <div class="form-group col-md-6">
            <label for="end_date">End Date</label>
            <input type="text" name="end_date" id="end_date" class="form-control" autocomplete="off" >
        </div>
    </div>

    <input type="hidden" name="fiscal_year" id="fiscal_year" class="form-control" value="{{$fiscal_year->id}}" readonly>

    <div class="col-md-12">
        <div class="form-group col-md-6">
            <label for="funding_partner_id">Funding Partner Name</label>
            <select class="form-control" name="funding_partner_id" id="funding_partner_id">
                @foreach($fundings as $funding)
                <option value="{{$funding->id}}">{{$funding->name}}</option>
                @endforeach
            </select>
        </div>
        <div class="form-group col-md-6">
            <label for="awc_id">Awc Name</label>
            <input type="text" name="awc_id" class="form-control" autocomplete="off" >
            {{--
                <select class="form-control" name="awc_id" id="awc_id">
                    @foreach($awcs as $awc)
                    <option value="{{$awc->id}}">{{$awc->name}}</option>
                    @endforeach
                </select>
            --}}
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-12">
            <label for="status">Status</label><br>
            <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" checked><br>
        </div>
    </div>
    
    <input type="hidden" name="id" id="id"/>
    <script>
            jQuery(document).ready(function($) {
                $('#water_source_id').select2();
            });
    </script>