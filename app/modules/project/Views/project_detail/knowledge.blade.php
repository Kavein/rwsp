<div class="form-group row">
	<div class="col-md-12">
		<label>No. of presentations made by women and minority representatives at key national/provincial sector events on learning and good practice</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[wo_pres]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>GWT participated at X% of relevant sector forums (DRR and WASH sustainability technical working groups)</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[drr_wash]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of exchange visits to the project for key sector stakeholders</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[visit_project]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of trainings delivered by GWT to other agencies</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[train_gwt]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of knowledge products (lesson learned, case studies, voices from the field) developed and shared with wider sector</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[product]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of learning and sharing events organised at palika and province level</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[pre_learn]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of radio programmes broadcast by local FM stations on water and sanitation programming, key messages on DRR and emergency preparedness in X wards </label>
	</div>
	<div class="col-md-12">
		<input type="number" name="know[radio_message]" class="form-control">
	</div>
</div>