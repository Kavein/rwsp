<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have a designated WASH focal person and active WASH coordinating committee </label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[designated_wash]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  local governments have updated, publicly available lifecycle-costed GESI-informed plans on how to achieve and sustain universal access to WASH services</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[gesi_wash]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have GESI-informed local policies or guidelines related to tariff setting and community contributions for water supply schemes</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[gesi_supply_schemes]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have GESI-informed guidelines on community selection and resilient water and sanitation infrastructure for different conteNo of ts</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[esi_informed]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have water supply options for different content of its (step-wise investment for appropriate level of service)</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[investment]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  annually award cleanest communities and best-performing water supply service providers ,reaching all members of the community based on robust monitoring data</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[award]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Number of schemes joint funded between local government and GWT</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[gwt_fund]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  with basket funding facility for water supply schemes and sanitation facilities (public toilets, FSTPs, etc.)</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[fund_water]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  undertaking social and public audits annually at community / ward levels</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[annual_social]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have municipality-wide datasets on water resources, water supply assets, and service provider roster, and capacity and process in place to update and analyse these in annual WASH planning process</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[wash_water]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  have public signposting of plans and services, and complaints response mechanism established for WASH services</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[public_plan]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>NO OF  wards  receives additional resources from successfully completing the Minimum Condition and Performance Measure Assessment</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[condition_success]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of private or informal sector workers gaining or increasing income from WASH-related activities</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[private_wash]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No of WSUCs with performance-based contract with trained and equipped maintenance service provider</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[wusc_contract]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of manual drillers/handpump installers trained on improving safe water services and mitigation / emergency response measures</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[manual_drillers]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Number of toilets upgraded or newly installed by trained providers to the agreed sanitation standard and with GESI-focused designs</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[gesi_toilets]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of approved small businesses providing safe FSM services for household toilets</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[fsm_business]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of well-managed faecal sludge (FS) disposal sites developed and overseen by local governments</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[managed_fs]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Active WSUCs in place for > 90% of water supply schemes in the target wards  with trained women and representatives from disadvantaged groups in key positions as per policy</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[trained_women]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs across the wards  which score >70% in pre-defined WSUC competency checklist </label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[wusc_checklist]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs with cost-reflective and GESI-informed tariffs (covering full O&M costs and provision for major repairs)</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[gesi_informed]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs with updated monthly records of water source yields and water quality monitoring based on NDWQS 2004</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[ndwqs_update]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs that have WSPs in place and are taking active measures to protect the water source</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[wsp_water]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs, female community health volunteers (FCHVs) and health posts with household information on simple sanitation and hygiene indicators based on cleanliness competition criteria</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[fchv_clean]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of WSUCs with service restoration in one day and ability to withstand additional 10% demand without compromising service standards</label>
	</div>
	<div class="col-md-12">
		<input type="number" name="capacity[wsuc_restore]" class="form-control">
	</div>
</div>