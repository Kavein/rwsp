@if($know == null)
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of presentations made by women and minority representatives at key national/provincial sector events on learning and good practice</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[wo_pres]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>GWT participated at X% of relevant sector forums (DRR and WASH sustainability technical working groups)</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[drr_wash]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of exchange visits to the project for key sector stakeholders</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[visit_project]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of trainings delivered by GWT to other agencies</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[train_gwt]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of knowledge products (lesson learned, case studies, voices from the field) developed and shared with wider sector</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[product]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>X no. of learning and sharing events organised at palika and province level</label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[pre_learn]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>X no. of radio programmes broadcast by local FM stations on water and sanitation programming, key messages on DRR and emergency preparedness in X wards </label>
		</div>
		<div class="col-md-12">
			<input type="number" name="know[radio_message]" class="form-control">
		</div>
	</div>
@else
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of presentations made by women and minority representatives at key national/provincial sector events on learning and good practice</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->wo_pres}}" name="know[wo_pres]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>GWT participated at X% of relevant sector forums (DRR and WASH sustainability technical working groups)</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->drr_wash}}" name="know[drr_wash]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of exchange visits to the project for key sector stakeholders</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->visit_project}}" name="know[visit_project]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of trainings delivered by GWT to other agencies</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->train_gwt}}" name="know[train_gwt]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>No. of knowledge products (lesson learned, case studies, voices from the field) developed and shared with wider sector</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->product}}" name="know[product]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>X no. of learning and sharing events organised at palika and province level</label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->pre_learn}}" name="know[pre_learn]" class="form-control">
		</div>
	</div>
	<div class="form-group row">
		<div class="col-md-12">
			<label>X no. of radio programmes broadcast by local FM stations on water and sanitation programming, key messages on DRR and emergency preparedness in X wards </label>
		</div>
		<div class="col-md-12">
			<input type="number" value="{{$know->radio_message}}" name="know[radio_message]" class="form-control">
		</div>
	</div>
	<input type="hidden" name="know_id" value="{{$know->id}}">
@endif