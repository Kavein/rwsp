<?php

namespace App\Modules\Project\Model;


use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    public  $table = 'tbl_projects';

    protected $fillable = ['id','project_no','project_name','province_id','district_id','vdc_id','ward_no','type','geographical_region','fiscal_year','start_date','end_date','funding_partner_id','awc_id','created_by','updated_by','created_at','updated_at','status','del_flag','app_received_date','system_type_id','service_type_id','no_existing_water_system','no_reservoir','no_taps','system_built_date','h2s_result','ph_result','population','feasibility_assessment_date','cast_division'];

    public function provinces()
    {
    	return $this->belongsTo('App\Modules\Province\Model\Province', 'province_id');
    }
    public function districts()
    {
    	return $this->belongsTo('App\Modules\District\Model\District', 'province_id');
    }
    public function vdcs()
    {
    	return $this->belongsTo('App\Modules\VDC\Model\VDC', 'province_id');
    }
    
    public function fundings()
    {
        return $this->belongsTo('App\Modules\Funding_partner\Model\Funding_partner', 'funding_partner_id');
    }

    public function fiscal_year()
    {
        return $this->belongsTo('App\Modules\Fiscal_year\Model\Fiscal_year', 'fiscal_year');
    }
}
