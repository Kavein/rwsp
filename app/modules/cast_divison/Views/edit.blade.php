@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Cast Divisons   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.cast_divisons') }}">Cast Divisons</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.cast_divisons.update',$cast_divison->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="name">Cast Name</label>
                    <input type="text" value = "{{$cast_divison->name}}"  name="name" id="name" class="form-control" >
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif  
                </div>
                <div class="form-group">
                    <label for="status">Status</label><br>
                    <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" {{$cast_divison->status == 1 ? 'checked':''}} >
                </div>
                <input type="hidden" name="id" id="id" value = "{{$cast_divison->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.cast_divisons') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
