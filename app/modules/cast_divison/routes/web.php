<?php



Route::group(array('prefix'=>'admin/','module'=>'Cast_divison','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Cast_divison\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('cast_divisons/','AdminCast_divisonController@index')->name('admin.cast_divisons');
    Route::post('cast_divisons/getcast_divisonsJson','AdminCast_divisonController@getcast_divisonsJson')->name('admin.cast_divisons.getdatajson');
    Route::get('cast_divisons/create','AdminCast_divisonController@create')->name('admin.cast_divisons.create');
    Route::post('cast_divisons/store','AdminCast_divisonController@store')->name('admin.cast_divisons.store');
    Route::get('cast_divisons/show/{id}','AdminCast_divisonController@show')->name('admin.cast_divisons.show');
    Route::get('cast_divisons/edit/{id}','AdminCast_divisonController@edit')->name('admin.cast_divisons.edit');
    Route::match(['put', 'patch'], 'cast_divisons/update/{id}','AdminCast_divisonController@update')->name('admin.cast_divisons.update');
    Route::get('cast_divisons/delete/{id}', 'AdminCast_divisonController@destroy')->name('admin.cast_divisons.edit');
});




Route::group(array('module'=>'Cast_divison','namespace' => 'App\Modules\Cast_divison\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('cast_divisons/','Cast_divisonController@index')->name('cast_divisons');
    
});