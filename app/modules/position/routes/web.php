<?php



Route::group(array('prefix'=>'admin/','module'=>'Position','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Position\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('positions/','AdminPositionController@index')->name('admin.positions');
    Route::post('positions/getpositionsJson','AdminPositionController@getpositionsJson')->name('admin.positions.getdatajson');
    Route::get('positions/create','AdminPositionController@create')->name('admin.positions.create');
    Route::post('positions/store','AdminPositionController@store')->name('admin.positions.store');
    Route::get('positions/show/{id}','AdminPositionController@show')->name('admin.positions.show');
    Route::get('positions/edit/{id}','AdminPositionController@edit')->name('admin.positions.edit');
    Route::match(['put', 'patch'], 'positions/update/{id}','AdminPositionController@update')->name('admin.positions.update');
    Route::get('positions/delete/{id}', 'AdminPositionController@destroy')->name('admin.positions.edit');
});




Route::group(array('module'=>'Position','namespace' => 'App\Modules\Position\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('positions/','PositionController@index')->name('positions');
    
});