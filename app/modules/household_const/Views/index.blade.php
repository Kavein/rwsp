@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Household List		
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			<li><a href="#">Household List</a></li>

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.household_consts.create',$id) }}" class="btn bg-green waves-effect"  title="create">Create</a>
						<a href="{{ route('admin.consthousedetail.export',$id) }}" class="btn bg-green waves-effect"  title="create">House Detail</a>
						<a href="{{ route('admin.constagegroup.export',$id) }}" class="btn bg-green waves-effect"  title="create">Age Group</a>
						<a href="{{ route('admin.constwaterfetch.export',$id) }}" class="btn bg-green waves-effect"  title="create">Water Fetching</a>
						<a href="{{ route('admin.consthygiene.export',$id) }}" class="btn bg-green waves-effect"  title="create">Hygiene Excel</a>
						<a href="{{ route('admin.consthealthData.export',$id) }}" class="btn bg-green waves-effect"  title="create">HealthData Excel</a>
						<a href="{{ route('admin.consthealthbehaviour.export',$id) }}" class="btn bg-green waves-effect"  title="create">Health Behaviour Excel</a>
						<a href="{{ route('admin.consthandwash.export',$id) }}" class ="btn bg-green waves-effect"  title="create">HandWash Excel</a>
						<a href="{{ route('admin.constsocialEconomic.export',$id) }}" class="btn bg-green waves-effect"  title="create">SocialEconomic Excel</a>
						<a href="{{ route('admin.constwash.export',$id) }}" class="btn bg-green waves-effect"  title="create">Wash Excel</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="household_const-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								<th >Project Number</th>
								<th >House Number</th>
								<th >Family Head Name</th>
								<th >Gender</th>
								<th >First Approve</th>
								<th >Second Approve</th>
								<th>Action</th>
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = window.location.href;
		$(function(){
			dataTable = $('#household_const-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.household_consts.getdatajson',$id) }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: "projects.project_no",name: "project_id"},
		            // { data: "projects.project_name",name: "project_name"},
		            { data: "house_no",name: "house_no"},
		            { data: "head_name",name: "head_name"},
		            { data: "head_gender",name: "head_gender"},
		            { data: "first_approve_status",render:function (data){
                        if(data == 1){
                            return '<span class="label label-success lb-lg"><i class="fa fa-check-circle"></i> Approved</span>';
                        }
                        else{
                            return'<span class="label label-danger lb-lg"><i class="fa fa-clock-o"></i> Pending</span>';
                        }
                    } ,searchable:false,orderable:false},
                    { data: "second_approve_status",render:function (data){
                        if(data == 1){
                            return '<span class="label label-success lb-lg"><i class="fa fa-check-circle"></i> Approved</span>';
                        }
                        else{
                            return'<span class="label label-danger lb-lg"><i class="fa fa-clock-o"></i> Pending</span>';
                        }
                    } ,searchable:false,orderable:false},
            
					{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn btn-default  waves-effect' href='{{url('admin/household_consts/edit')}}/"+data.id+"' type='button' ><i class='fa fa-pencil'></i></a>&nbsp"; 

					buttons += "<a href='{{url('admin/household_consts/delete/')}}/"+data.id+"' onclick=\"return (confirm('Are you Sure'))?true:false\" class='btn btn-default  waves-effect' ><i class='fa fa-trash'></i></a>&nbsp";
					@if(control('first_approve'))
						if(data.first_approve_status == 1){
							buttons += "<a href='{{url('admin/projects/household_consts/first_approve/')}}/"+data.id+"' class='btn btn-default  waves-effect'>Disapprove</a>&nbsp";
						}
						else{
							buttons += "<a href='{{url('admin/projects/household_consts/first_approve/')}}/"+data.id+"' class='btn btn-default  waves-effect'> Approve</a>&nbsp";
						}
                    	
                    @endif
                    @if(control('second_approve'))
	                    if(data.first_approve_status != '' && data.first_approve_status != null && data.first_approve_status == 1 ){
	                    	if(data.second_approve_status == 1){
	                    		buttons += "<a href='{{url('admin/projects/household_consts/second_approve/')}}/"+data.id+"'  class='btn btn-default  waves-effect' >Disapprove</a>";
	                    	}else{
		                    	buttons += "<a href='{{url('admin/projects/household_consts/second_approve/')}}/"+data.id+"'  class='btn btn-default  waves-effect'>Approve</a>";
	                    	}
	                    }
                    @endif

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
