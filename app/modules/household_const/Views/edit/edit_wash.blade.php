<div class="form-group row">
	<div class="col-md-12">
		<label>No. of people accessing a safely managed water supply service to the premises (yard tap) at least 12 hrs/day</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->safety_management_12}}" name="wash[safety_management_12]" class="form-control">
	</div>
</div>
	 


<div class="form-group row">
	<div class="col-md-12">
		<label>No. of people from poor and marginalised groups with access to safely managed water supply service at least 8 hrs/day</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->safety_management_8}}" name="wash[safety_management_8]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Uptake of financing mechanism to leverage household contributions for yard taps connections in X% project-targeted schemes</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->uptake}}" name="wash[uptake]" class="form-control">
	</div>
</div>
	

<div class="form-group row">
	<div class="col-md-12">
		<label>No. of schools, clinics, and public institutions with access to safely managed water supply service within their grounds at least 12 hrs/day, specifically DRR designed for those selected as safe havens</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->school_safety_12}}" name="wash[school_safety_12]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>% of project-supported schemes that deliver water within national water quality standards at point of collection</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->project_scheme}}" name="wash[project_scheme]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>Number of functionality rates of water supply schemes across supported wards</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->supply_scheme}}" name="wash[supply_scheme]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>Number of schemes across the wards achieving or exceeding service standards</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->exceeding_service}}" name="wash[exceeding_service]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>No. of communities actively competing in ward-level cleanliness competitions</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->cleanliness_competitions}}" name="wash[cleanliness_competitions]" class="form-control">
	</div>
</div>
<div class="form-group row">
	<div class="col-md-12">
		<label>X% of households in target wards  with handwashing facility with soap and water within 1–2 m of toilet</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->handwashing_facility}}" name="wash[handwashing_facility]" class="form-control">
	</div>
</div>


<div class="form-group row">
	<div class="col-md-12">
		<label>X% of households in target wards  with clean toilet meeting local cleanliness competitions criteria</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->clean_toilet}}" name="wash[clean_toilet]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>No. of FCHVs trained and equipped to deliver effective BCC activities</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->fchv_trained}}" name="wash[fchv_trained]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>X% of schools trained and equipped and running participatory BCC and outreach sessions</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->school_trained}}" name="wash[school_trained]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>Number of schools, clinics, and public institutions with access to safe sanitation and handwashing facilities, specifically DRR designed for those selected as safe havens</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->school_safe_haven}}" name="wash[school_safe_haven]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>Environmental sanitation indicator in terms of clean, tidy ,and green surroundings (e.g. criteria defined for cleanliness competitions)</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->indicator_clean}}" name="wash[indicator_clean]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>X% of households performing grey water separation to use in kitchen garden (improving livelihoods and nutrition) SDI</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->grey_water}}" name="wash[grey_water]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>Number of households in lowest wealth quintile in Palika with (affordable) using safe water, practicing hand washing with soap and safe sanitation.</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->affordable_wealth}}" name="wash[affordable_wealth]" class="form-control">
	</div>
</div>

<div class="form-group row">
	<div class="col-md-12">
		<label>No. of wards declared for Total Sanitation</label>
	</div>
	<div class="col-md-12">
		<input type="number" value="{{$wash->total_sanitation}}" name="wash[total_sanitation]" class="form-control">
	</div>
</div>
