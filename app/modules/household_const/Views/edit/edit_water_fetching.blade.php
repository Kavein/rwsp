<div class="box-body">
    
    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Water Collection Pot in Liter</label>
        
        <div class="col-sm-6">
            <input type="number" name="water_fetch[water_collection_liter]" class="form-control" value="{{$water_fetcings->water_collection_liter}}">
            @if ($errors->has('water_fetch.water_collection_liter'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('water_fetch.water_collection_liter') }}</strong>
                </span>
            @endif
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Time Consumed In Going *(Seconds)</label>
        
        <div class="col-sm-6">
            <input type="number" name="water_fetch[going_second]" class="form-control"  value="{{$water_fetcings->going_second}}">
            @if ($errors->has('water_fetch.going_second'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('water_fetch.going_second') }}</strong>
                </span>
            @endif
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Time Consumed In Waiting *(Seconds)</label>
        
        <div class="col-sm-6">
            <input type="number" name="water_fetch[waiting_second]" class="form-control" value="{{$water_fetcings->waiting_second}}">
            @if ($errors->has('water_fetch.waiting_second'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('water_fetch.waiting_second') }}</strong>
                </span>
            @endif
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Time Consumed In Filling *(Seconds)</label>
        
        <div class="col-sm-6">
            <input type="number" name="water_fetch[filling_second]" class="form-control" value="{{$water_fetcings->filling_second}}">
            @if ($errors->has('water_fetch.filling_second'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('water_fetch.filling_second') }}</strong>
                </span>
            @endif
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">Time Consumed In Return *(Seconds)</label>
        
        <div class="col-sm-6">
            <input type="number" name="water_fetch[return_second]" class="form-control" value="{{$water_fetcings->return_second}}">
            @if ($errors->has('water_fetch.return_second'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('water_fetch.return_second') }}</strong>
                </span>
            @endif
        </div>  
    </div>
   
</div>