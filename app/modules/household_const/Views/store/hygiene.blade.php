<div class="box-body">
    <div class="col-md-12">
        <div class="form-group col-md-12">
            <label for="latrine_available">Latrine Availability</label><br>
            <input type="checkbox" value="1" name="hygiene[latrine_available]" id="latrine_available" style="height: 20px; width: 20px;">
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-6" id="total_latrine" hidden>
                <label> Total Number of Latrine</label>
                <input type="number" name="hygiene[total_latrine]" class="form-control" placeholder="Total Latrine Available" >
                @if ($errors->has('total_latrine'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('total_latrine') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-group col-md-6" id="latrine_cost" hidden>
                <label> Total Latrine Cost</label>
                <input type="number" name="hygiene[latrine_cost]" class="form-control" placeholder="Total Latrine Cost" >
                @if ($errors->has('latrine_cost'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('latrine_cost') }}</strong>
                    </span>
                @endif
            </div>
            
        </div>
        <div class="col-md-12">
            <div class="form-group col-md-4" hidden id="latrine_cleansing_agent">
                <label for="latrine_cleansing_agent">LATRINE WITH CLEANSING AGENT KEPT INSIDE</label>
                <br>
                <input type="checkbox" value="1" name="hygiene[latrine_cleansing_agent]" style="height: 20px; width: 20px;">
            </div>
            <div class="form-group col-md-4" hidden id="latrine_water_available">
                <label for="latrine_water_available">LATRINE WITH WATER JAR KEPT INSIDE</label>
                <br>
                <input type="checkbox" value="1" name="hygiene[latrine_water_available]" style="height: 20px; width: 20px;">
            </div>
            <div class="form-group col-md-4" hidden id="latrine_cleaning_brush">
                <label for="latrine_cleaning_brush">LATRINE WITH CLEANING BRUSH KEPT INSIDE</label>
                <br><input type="checkbox" value="1" name="hygiene[latrine_cleaning_brush]" style="height: 20px; width: 20px;">
            </div>
        </div>
        
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-6" >
            <input type="number" name="hygiene[defecate_latrine]" class="form-control" placeholder="Total People Defecate in Latrine" >
            @if ($errors->has('defecate_latrine'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('defecate_latrine') }}</strong>
                </span>
            @endif
        </div>
        <div class="form-group col-md-6" >
            <input type="number" name="hygiene[defecate_open]" class="form-control" placeholder="Total People Defecate in Open Place" >
            @if ($errors->has('defecate_open'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('defecate_open') }}</strong>
                </span>
            @endif
        </div>
    </div>
    <div class="col-md-12">
        <div class="form-group col-md-4">
            <label for="home_cleaning">CLEANING HOME REGULARLY</label><br>
            <input type="checkbox" name="hygiene[home_cleaning]" value="1" style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4">
            <label for="domestic_animal">KEEPING ANIMALS TO A SAFE DISTANCE</label><br>
            <input type="checkbox" name="hygiene[domestic_animal]" value="1" style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4">
            <label for="drinking_water">KEEPING WATER VESSEL COVERED</label><br>
            <input type="checkbox" name="hygiene[drinking_water]" value="1" style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4">
            <label for="food_covered">KEEPING FOOD COVERED</label><br>
            <input type="checkbox" name="hygiene[food_covered]" value="1" style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4">
            <label for="utensils_clean">KEEPING UTENSILS CLEAN</label><br>
            <input type="checkbox" name="hygiene[utensils_clean]" value="1" style="height: 20px; width: 20px;">
        </div>
        <div class="form-group col-md-4">
            <label for="garbage_disposel_pit">MANAGING GARBAGE IN DISPOSAL PIT</label>
            <br>
            <input type="checkbox" name="hygiene[garbage_disposel_pit]" value="1" style="height: 20px; width: 20px;">
        </div>
        
    </div>

    
    
    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
    </div>
       
        <h3 for="total_male_member" class="col-md-12"><strong><label>Personal Hygiene</label></strong></h3 >
        <!-- <label for="name" class="col-sm-3 col-form-label">Age Group</label> -->

        <div class="form-group col-md-6 "> 
        <label for="bath_frequency">BATHING FREQUENCY</label>   
        <select class="form-control" name="hygiene[bath_frequency]">
            <option value="daily">DAILY</option>
            <option value="once">ONCE A WEEK</option>
            <option value="twice">MORE THAN ONCE A WEEK</option>
        </select>
        </div>

        <div class="form-group col-md-6 "> 
        <label for="cloth_wash_frequency">CLOTHES WASHING FREQUENCY</label>   
        <select class="form-control" name="hygiene[cloth_wash_frequency]">
            <option value="once">ONCE A WEEK</option>
            <option value="more">MORE THAN ONCE A WEEK</option>
            <option value="monthly">ONCE A MONTH</option>
        </select>
        </div>

        <div class="form-group col-md-6 "> 
            <label for="knowledge_menstrual">WOMEN KNOW ABOUT MANAGEMENT OF MENSTRUAL HYGIENE</label>   <br>
            <input class="form-check-input" type="radio" id="traditional_preference" value="Yes" name="hygiene[menstrual_knowledge]" checked >
            <label class="form-check-label" for="traditional_preference">&nbsp&nbsp&nbsp&nbspYes</label>

            <input class="form-check-input" type="radio" id="traditional_preference" value="No" name="hygiene[menstrual_knowledge]" style="margin-left: 100px;">
            <label class="form-check-label" for="traditional_preference">&nbsp&nbsp&nbsp&nbspNo</label>
        </div>


        <div class="col-md-12" style="border-top: 1px solid #b7c2cc; ">
        </div>
            <h3 for="total_male_member" class="col-md-12"><strong><label>Hand Washing</label></strong></h3 >
        
            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label"></label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">With Soap and Water</label>
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">Water Only</label>
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">With Ash</label>
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <label for="healthpost_preference" class="col-form-label">Others</label>
                    </div>
                </div>  
            </div> 

            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">HandWash Before Eating</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[handwash_water_soap]" placeholder="Number">
                        @if ($errors->has('handwash_water_soap'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_water_soap') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[handwash_water]" placeholder="Number">
                        @if ($errors->has('handwash_water'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_water') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[handwash_ash]" placeholder="Number">
                        @if ($errors->has('handwash_ash'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_ash') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[handwash_others]" placeholder="Number">
                        @if ($errors->has('handwash_others'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_others') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>

            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">HandWash After Defecation</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[defecate_water_soap]" placeholder="Number">
                        @if ($errors->has('defecate_water_soap'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_water_soap') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class=" form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[defecate_water]" placeholder="Number">
                        @if ($errors->has('defecate_water'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_water') }}</strong>
                            </span>
                        @endif
                    </div>
                    <div class="form-check form-check-inline col-sm-3">
                        <input class="form-check-input form-control" type="number" name="hygiene[defecate_ash]" placeholder="Number">
                        @if ($errors->has('defecate_ash'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('defecate_ash') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>
            <div class="col-md-12 form-group row">
                <label for="healthpost_preference" class="col-sm-4 col-form-label">Infanct Children Number Who doesnt wash Hands</label>
                
                <div class="col-sm-8">
                    <div class="form-check form-check-inline col-sm-6">
                        <input class="form-check-input form-control" type="number" name="hygiene[handwash_infant]" placeholder="Number">
                        @if ($errors->has('handwash_infant'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('handwash_infant') }}</strong>
                            </span>
                        @endif
                    </div>
                </div>  
            </div>
        
    
</div>