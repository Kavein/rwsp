<div class="box-body">
    <h3 for="total_male_member" class="col-md-12"><strong><label>Gender On Social Inclusion Data</label></strong></h3 >
    <h4 for="total_male_member" class="col-md-12"><strong><label>WORK DIVISION BY GENDER AT HOUSEHOLD LEVEL</label></strong></h4 >
    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Household Work</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[hh_male]" placeholder="Male">
                @if ($errors->has('inclusion.hh_male'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.hh_male') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[hh_female]" placeholder="Female">
                @if ($errors->has('inclusion.hh_female'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.hh_female') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[hh_boys]" placeholder="Boys">
                @if ($errors->has('inclusion.hh_boys'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.hh_boys') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[hh_girls]" placeholder="Girls">
                @if ($errors->has('inclusion.hh_girls'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.hh_girls') }}</strong>
                    </span>
                @endif
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Water Fetching</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" name="inclusion[wf_male]" placeholder="Male">
                @if ($errors->has('inclusion.wf_male'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.wf_male') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" name="inclusion[wf_female]" placeholder="Female">
                @if ($errors->has('inclusion.wf_female'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.wf_female') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" name="inclusion[wf_boys]" placeholder="Boys">
                @if ($errors->has('inclusion.wf_boys'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.wf_boys') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" name="inclusion[wf_girls]" placeholder="Girls">
                @if ($errors->has('inclusion.wf_girls'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.wf_girls') }}</strong>
                    </span>
                @endif
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Productive Work</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[pw_male]" placeholder="Male">
                @if ($errors->has('inclusion.pw_male'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.pw_male') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[pw_female]" placeholder="Female">
                @if ($errors->has('inclusion.pw_female'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.pw_female') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[pw_boys]" placeholder="Boys">
                @if ($errors->has('inclusion.pw_boys'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.pw_boys') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[pw_girls]" placeholder="Girls">
                @if ($errors->has('inclusion.pw_girls'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.pw_girls') }}</strong>
                    </span>
                @endif
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="healthpost_preference" class="col-sm-4 col-form-label">Community Work</label>
        
        <div class="col-sm-8">
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[cw_male]" placeholder="Male">
                @if ($errors->has('inclusion.cw_male'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.cw_male') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[cw_female]" placeholder="Female">
                @if ($errors->has('inclusion.cw_female'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.cw_female') }}</strong>
                    </span>
                @endif
            </div>
            <div class="form-check form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_male" name="inclusion[cw_boys]" placeholder="Boys">
                @if ($errors->has('inclusion.cw_boys'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.cw_boys') }}</strong>
                    </span>
                @endif
            </div>
            <div class=" form-check-inline col-sm-3">
                <input class="form-check-input form-control" type="number" id="hh_female" name="inclusion[cw_girls]" placeholder="Girls">
                @if ($errors->has('inclusion.cw_girls'))
                    <span class="help-block">
                        <strong style="color: red;">{{ $errors->first('inclusion.cw_girls') }}</strong>
                    </span>
                @endif
            </div>
        </div>  
    </div>

    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-5 col-form-label">PRACTICE OF KITCHEN GARDENING</label>
        
        <div class="col-md-4">
            <input type="radio" name="inclusion[kitchen_gardening_practice]" value="Yes" checked >
            <label>Yes</label>
            <input type="radio" name="inclusion[kitchen_gardening_practice]" value="No" style="margin-left: 100px;">
            <label>No</label>
        </div>
        
    </div>

    <h4 for="total_male_member" class="col-md-12"><strong><label>OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER</label></strong></h4 >
    <h5 for="total_male_member" class="col-md-12"><strong><label>EFFECT ON WOMEN DUE TO WATER COLLECTION TASK</label></strong></h5 >
    <div class="col-md-12">
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="increased_burdon" class="water_collection_task" checked>
            <label>INCREASED BURDON OF HOUSEHOLD WORK</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="adverse_affect" class="water_collection_task">
            <label>ADVERSE AFFECT ON WOMEN'S HEALTH</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="discomfort" class="water_collection_task">
            <label>DISCOMFORT AND DRUDGERY</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_women]" value="incident_women" id="incident_women" class="water_collection_task">
            <label>OTHER INCIDENCES ON WOMEN</label>
        </div>
        <textarea rows="5" cols="150" id="other_incident_women" hidden name="inclusion[other_incident_women]">
            
        </textarea>
    </div>

    <h5 for="total_male_member" class="col-md-12"><strong><label>Effect On Children Due To Water Collection Task</label></strong></h5 >
    <div class="col-md-12">
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value="homework" class="effect_on_children" checked>
            <label>NO TIME FOR HOMEWORK</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value=" absent from school" class="effect_on_children">
            <label>ABSENT FROM SCHOOL CLASS</label>
        </div>
        <div class="col-md-6">
            <input type="radio" name="inclusion[effect_on_children]" value="others" class="effect_on_children">
            <label>OTHER</label>
        </div>
        <textarea rows="5" cols="150" id="other_effect_on_children" hidden name="inclusion[other_effect_on_children]">
            
        </textarea>
    </div>

    

    <h4 for="total_male_member" class="col-md-12"><strong><label>DATA ON CHILDREN</label></strong></h4 >
    
    
    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER</label>
        
        <div class="col-sm-6">
            <input type="number" name="inclusion[out_migrating]" class="form-control" placeholder="OUT-MIGRATION OF HH DUE TO ACUTE HARDSHIP OF WATER">
            @if ($errors->has('inclusion.out_migrating'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('inclusion.out_migrating') }}</strong>
                </span>
            @endif
        </div>  
    </div>
    <div class="col-md-12 form-group row">
        <label for="home_medecine_preference" class="col-sm-4 col-form-label">NUMBER OF CHILDREN GOING TO SCHOOL</label>
        
        <div class="col-sm-3">
            <input type="number" name="inclusion[school_going_children_male]" class="form-control" placeholder="Boys">
            @if ($errors->has('inclusion.school_going_children_male'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('inclusion.school_going_children_male') }}</strong>
                </span>
            @endif
        </div>  
        <div class="col-sm-3">
            <input type="number" name="inclusion[school_going_children_female]" class="form-control" placeholder="Girls">
            @if ($errors->has('inclusion.school_going_children_female'))
                <span class="help-block">
                    <strong style="color: red;">{{ $errors->first('inclusion.school_going_children_female') }}</strong>
                </span>
            @endif
        </div>  
    </div>    
</div>