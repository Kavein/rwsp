<?php

namespace App\Modules\Household_const\Model;


use Illuminate\Database\Eloquent\Model;

class Household_const extends Model
{
    public  $table = 'tbl_household_const';

    protected $fillable = ['id','house_no','project_id','initial_id','head_name','head_gender','tole_name','total_male','total_female','total_member','cast_id','cast_division','total_female_widow','litrate_no','illitrate_no','income_pension','other_income_source','religion_minority','status','del_flag','created_by','updated_by','created_at','updated_at','prep_id','fiscal_year_id','first_approved_by','first_approve_status','second_approve_status','second_approved_by'];

    public function projects()
    {
    	return $this->belongsTo('App\Modules\Project\Model\Project', 'project_id');
    }
}
