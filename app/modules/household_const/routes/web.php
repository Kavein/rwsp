<?php



Route::group(array('prefix'=>'admin/','module'=>'Household_const','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Household_const\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_consts/{id}','AdminHousehold_constController@index')->name('admin.household_consts');
    Route::post('household_consts/gethousehold_constsJson/{id}','AdminHousehold_constController@gethousehold_constsJson')->name('admin.household_consts.getdatajson');
    Route::get('household_consts/create/{id}','AdminHousehold_constController@create')->name('admin.household_consts.create');
    Route::post('household_consts/store','AdminHousehold_constController@store')->name('admin.household_consts.store');
    Route::get('household_consts/show/{id}','AdminHousehold_constController@show')->name('admin.household_consts.show');
    Route::get('household_consts/edit/{id}','AdminHousehold_constController@edit')->name('admin.household_consts.edit');
    Route::match(['put', 'patch'], 'household_consts/update/{id}','AdminHousehold_constController@update')->name('admin.household_consts.update');
    Route::get('household_consts/delete/{id}', 'AdminHousehold_constController@destroy')->name('admin.household_consts.edit');

    Route::get('projects/household_consts/first_approve/{id}', 'AdminHousehold_constController@firstApprove')->name('admin.household_consts.firstapprove');
    Route::get('projects/household_consts/second_approve/{id}', 'AdminHousehold_constController@secondApprove')->name('admin.household_consts.secondapprove');

    //Export Route
    //Household Export
    Route::get('projects/household_consts/house_detail/export/{id}','AdminHousehold_constController@exporthousedetail')->name('admin.consthousedetail.export');
    //AgeGroup Route
    Route::get('projects/household_consts/agegroup/export/{id}','AdminHousehold_constController@exportageGroup')->name('admin.constagegroup.export');
    //WaterFetching Route
    Route::get('projects/household_consts/export/{id}','AdminHousehold_constController@exportwaterfetch')->name('admin.constwaterfetch.export');
    //Hygiene Route
    Route::get('projects/household_consts/hygiene/export/{id}','AdminHousehold_constController@exportHygieneData')->name('admin.consthygiene.export');
    //HealthData Route
    Route::get('projects/household_consts/healthdata/export/{id}','AdminHousehold_constController@exportHealthData')->name('admin.consthealthData.export');
    //HealthBehaviour Route
    Route::get('projects/household_consts/healthbehaviour/export/{id}','AdminHousehold_constController@exporthealthBehaviour')->name('admin.consthealthbehaviour.export');
    //Handwash Route
    Route::get('projects/household_consts/handwash/export/{id}','AdminHousehold_constController@exportHandWash')->name('admin.consthandwash.export');
    //SocialEconomic Route
    Route::get('projects/household_consts/socialEconomic/export/{id}','AdminHousehold_constController@exportSocialEconomic')->name('admin.constsocialEconomic.export');
    //Wash Route
    Route::get('projects/household_consts/wash/export/{id}','AdminHousehold_constController@exportwash')->name('admin.constwash.export');
    //End Export Route
});




Route::group(array('module'=>'Household_const','namespace' => 'App\Modules\Household_const\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('household_consts/','Household_constController@index')->name('household_consts');
    
});