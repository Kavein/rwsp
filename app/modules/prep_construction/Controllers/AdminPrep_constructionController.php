<?php

namespace App\modules\prep_construction\Controllers;

use App\Http\Controllers\Controller;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\modules\prep_construction\Model\Prep_construction;
use Carbon\Carbon;

class AdminPrep_constructionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $page['title'] = 'Prep_construction';
        return view("prep_construction::index",compact('page','id'));

        //
    }
    /**
     * Get datatable format json file.
     *
     * 
     */

    public function getprep_constructionsJson(Request $request,$id)
    {
        $prep_construction = new Prep_construction;
        $where = $this->_get_search_param($request);

        // For pagination
        $filterTotal = $prep_construction->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        } )->where('del_flag',0)->where('project_id',$id)->get();
        // dd($filterTotal);
        // Display limited list        
        $rows = $prep_construction->where( function($query) use ($where) {
            if($where !== null) {
                foreach($where as $val) {
                    $query->orWhere($val[0],$val[1],$val[2]);
                }
            }
        })->limit($request->length)->offset($request->start)->with('projects')->where('del_flag',0)->where('project_id',$id)->get();

        //To count the total values present
        $total = $prep_construction->where('del_flag',0)->where('project_id',$id)->get();
        // $total = $prep_household_detail->where('del_flag',0)->where('project_id',$id)->get();


        echo json_encode(['draw'=>$request['draw'],'recordsTotal'=>count($total),'recordsFiltered'=>count($filterTotal),'data'=>$rows]);
    }

    /**
     *Search Params
     *
     * @return \Illuminate\Http\Response
     */


    public function _get_search_param($params)
    {
        $where = null;
        foreach ($params['columns'] as $value) {
            if($value['searchable'] == 'true'){
                
                if($params['search']['value'] != '')
                {
                    $where[] = [ $value['name'], 'like' , "%".$params['search']['value']."%" ];
                }

                if($value['search']['value'] != '')
                {
                }
            }
        }
        
        return $where;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $page['title'] = 'construction Phase';
        // return  $id;
        $prep_construction=Prep_construction::where('project_id',$id)->first();
        // dd($prep_construction);
        if($prep_construction)
        {
            $cons_id=$prep_construction->id;
            $store_check=DB::table('cons_store_check')->where('construction_id',$cons_id)->get();
            return view('prep_construction::construction',compact('page','id','prep_construction','store_check'));
        }
        else
        {
            return view('prep_construction::construction',compact('page','id','prep_construction'));

        }
       
       
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $user_id=Auth::user()->id;
        $created_at= Carbon::today(); 
        $project_id=$request->id;
        $delflag=0;
        $data = $request->except('_token');    
        $this->validateconstruction($request);
        $store_check_id=$request->store_check_id;

        //Village Sketch Map
        $village_sketch['village_sketch']=$request->village_sketch;
        if(Input::hasFile('village_sketch'))
        {
            $extension = $request->file('village_sketch')->getClientOriginalExtension();
            $filename  = str_random(15).'.'.$extension;

            $destination = public_path('images/construction_phase/'.$request->id);
            if(!file_exists($destination)) {   
               mkdir($destination, 0777, true);
            }
            $request->file('village_sketch')->move($destination, $filename);
            $village_sketch['village_sketch']=$filename;
        }
        else{
            if($request->oldImage == null){
               $village_sketch['village_sketch'] = null;   
            }
            else{
                $village_sketch['village_sketch'] = $request->oldImage;
            }
        }
        // dd($village_sketch['village_sketch']);
        //End Village sketch map
        $data=array(
            'project_id' =>$request->id,
            'quantity_bill_record_date' =>$request->quantity_bill_record_date,
            'rough_estimation_cost' =>round($request->rough_estimation_cost),
            'amended_estimation_cost' =>round($request->amended_estimation_cost),
            'MOU_agreement_date' =>$request->MOU_agreement_date,
            'program_implementation' => $request->program_implementation,
            'vmw_training_date' =>$request->vmw_training_date,
            'vmw_participants_male' =>$request->vmw_participants_male,
            'vmw_participants_female'=>$request->vmw_participants_female,
            'supervisor_name'=>$request->supervisor_name,
            'supervisor_age' =>$request->supervisor_age,
            'sex' =>$request->sex,
            'mid_visit_date' =>$request->mid_visit_date,
            'structure_monitor' =>$request->structure_monitor,
            'wsp_completion' =>$request->wsp_completion,
            'wst_date' => $request->wst_date,
            'wst_participants' =>$request->wst_participants,
            'om_fund' =>round($request->om_fund),
            'variation' =>$request->variation,
            'variation_households'=>$request->variation_households,
            'variation_population'=>$request->variation_population,
            'comissioning_date'=>$request->comissioning_date,
            'post_construction_date'=>$request->post_construction_date,
            'post_construction_participants_male'=>$request->post_construction_participants_male,
            'post_construction_participants_female'=>$request->post_construction_participants_female,
            'wsp_training_date'=>$request->wsp_training_date,
            'wsp_training_participation_male'=>$request->wsp_training_participation_male,
            'wsp_training_participation_female'=>$request->wsp_training_participation_female,
            'sanitary_funtion_survey'=>$request->sanitary_funtion_survey,
            'created_by'=>$user_id,
            'created_at'=>$created_at,
            'updated_by'=>$user_id,
            'updated_at'=>Carbon::today(),
            'del_flag'=>$delflag,
            'village_sketch' => $village_sketch['village_sketch'],
        );
       
        $datas =  Prep_construction::where('project_id', '=', $project_id)->first();
        if ($datas == null) 
        {
            $success= Prep_construction::insert($data);
            $info= Prep_construction::where('project_id', '=', $project_id)->first();
            $construction_id=$info->id;
           $successes=  DB::table('cons_store_check')->insert([
                    'construction_id' => $construction_id,
                    'store_check_Date'=>$request->store_check_date,
                    'store_check'=>$request->store_check,
                    'created_at'=>Carbon::today()

                ]);
            return redirect()->route('admin.projects.show',$project_id)->with('success','data added successfully');
        }
        else
        {
            $success= Prep_construction::where('project_id', '=', $project_id)->update($data);
            $info= Prep_construction::where('project_id', '=', $project_id)->first();
            $construction_id=$info->id;
            $cons_store_check = DB::table('cons_store_check')->where([['construction_id','=',$construction_id],['store_check_Date','=',$request->store_check_date_new]])->first();
            
            if(empty($cons_store_check))
            {
                $successes=  DB::table('cons_store_check')->insert([
                    'construction_id' => $construction_id,
                    'store_check_Date'=>$request->store_check_date_new,
                    'store_check'=>$request->store_check_new,
                    'created_at'=>Carbon::today()
                ]);
            }
            else
            {
               
                $cons_store_check = DB::table('cons_store_check')
                ->where([['construction_id','=',$construction_id],['store_check_Date','=',$request->store_check_date_new]])
                            ->first();
                $created_date=$cons_store_check->created_at;
                // return $created_date;
                $create_at=Carbon::today();
                $store_check_Date_new=$request->store_check_date_new;
                $data=array(
                    'construction_id' => $construction_id,
                    'store_check_Date'=>$store_check_Date_new,
                    'store_check'=>$request->store_check_new,
                    'created_at'=>$created_date
                    );
                if($created_date == $create_at) 
                {
                    DB::table('cons_store_check')->where('id','=',$cons_store_check->id)->update($data);
                }
                else
                {
                   // DB::table('cons_store_check')->insert($data);

                }
            }
                      
            return redirect()->route('admin.projects.show',$project_id)->with('success','data updated successfully');
         }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        // return "here";
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $prep_construction = Prep_construction::findOrFail($id);
        $page['title'] = 'Prep_construction | Update';
       
        return view("prep_construction::edit",compact('page','prep_construction','id'));

        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
    
        $data = $request->except('_token');
        // $user_id=Auth::user()->id;
        // return $request;
        $this->validateconstruction($request);

        $project_id=$request->project_id;
    //   return $project_id;
        $created_at=$request->created_at;


        $rough_estimation_cost=round($request->rough_estimation_cost);
        $amended_estimation_cost=round($request->amended_estimation_cost);
        
        $om_fund=round($request->om_fund);
        $data=array(
            'project_id' =>$request->project_id,
            'quantity_bill_record_date' =>$request->quantity_bill_record_date,
            'rough_estimation_cost' =>$rough_estimation_cost,
            'amended_estimation_cost' =>$amended_estimation_cost,
            'MOU_agreement_date' =>$request->MOU_agreement_date,
            'store_check_date' =>$request->store_check_date,
            'store_check' =>$request->store_check,
            'program_implementation' => $request->program_implementation,
            'vmw_training_date' =>$request->vmw_training_date,
            'vmw_participants_male' =>$request->vmw_participants_male,
            'vmw_participants_female' =>$request->vmw_participants_female,
            'supervisor_name'=>$request->supervisor_name,
            'supervisor_age' =>$request->supervisor_age,
            'sex' =>$request->sex,
            'mid_visit_date' =>$request->mid_visit_date,
            'structure_monitor' =>$request->structure_monitor,
            'wsp_completion' =>$request->wsp_completion,
            'wst_date' => $request->wst_date,
            'wst_participants' =>$request->wst_participants,
            'om_fund' =>$om_fund,
            'variation' =>$request->variation,
            'variation_households'=>$request->variation_households,
            'variation_population'=>$request->variation_population,
            'comissioning_date'=>$request->comissioning_date,
            'post_construction_date'=>$request->post_construction_date,
            'post_construction_participants_male'=>$request->post_construction_participants_male,
            'post_construction_participants_female'=>$request->post_construction_participants_female,
            'wsp_training_date'=>$request->wsp_training_date,
            'wsp_training_participation_male'=>$request->wsp_training_participation_male,
            'wsp_training_participation_female'=>$request->wsp_training_participation_female,
            'sanitary_funtion_survey'=>$request->sanitary_funtion_survey,
            'created_by'=>$request->created_by,
            'created_at'=>$request->created_at,
            'updated_by'=>Auth::user()->id,
            'updated_at'=>new carbon(),
        );
       
       
        $success = Prep_construction::where('project_id', '=', $project_id)
        ->where('created_at','=',$created_at)
        ->update($data);
        return redirect()->route('admin.prep_constructions',$project_id)->with('success','data updated successfully');
        
        //
    }
  
    public function validateconstruction($request){
        $this->validate($request, [

                            'rough_estimation_cost' => 'nullable|integer|min:0',
                            'amended_estimation_cost' => 'required|integer|min:0',
                            'vmw_participants_male' => 'required|integer|min:0',
                            'vmw_participants_female' => 'required|integer|min:0',
                            'supervisor_age' => 'required|integer|min:0',
                            'wst_participants' => 'required|integer|min:0',
                            'om_fund' => 'required|integer|min:0',
                            'variation' => 'required|integer|min:0',
                            'variation_households' => 'required|integer|min:0',
                            'variation_population' => 'required|integer|min:0',
                            'post_construction_participants_male' => 'required|integer|min:0',
                            'post_construction_participants_female' => 'required|integer|min:0',
                            'wsp_training_participation_male' => 'required|integer|min:0',
                            'wsp_training_participation_female' => 'required|integer|min:0',
                        ],
                        [
                            'rough_estimation_cost.required' => 'Cost Cannot Be Empty!!',
                            'amended_estimation_cost.required' => 'Cost Cannot Be Empty!!',
                            'vmw_participants_male.required' => 'Participants Cannot Be Empty!!',
                            'vmw_participants_female.required' => 'Participants Cannot Be Empty!!',
                            'supervisor_age.required' => 'Age cannot be Empty!!',
                            'wst_participants.required' => 'Participants Cannot Be Empty!!',
                            'om_fund.required' => 'Fund Cannot Be Empty!!',
                            'variation.required' => 'Fund Cannot Be Empty!!',
                            'variation_households.required' => 'variation of households Cannot Be Empty!!',
                            'variation_population.required' => 'variation of population  Cannot Be Empty!!',
                            'post_construction_participants_male.required' => 'post construction participants Cannot Be Empty!!',
                            'post_construction_participants_female.required' => 'post construction participants Cannot Be Empty!!',
                            'wsp_training_participation_male.required' => 'wsp training participationn Cannot Be Empty!!',
                            'wsp_training_participation_female.required' => 'wsp training participationn Cannot Be Empty!!',

                            'rough_estimation_cost.min' => 'Cost Cannot Be negetive!!',
                            'amended_estimation_cost.min' => 'Cost Cannot Be negetive!!',
                            'vmw_participants_male.min' => 'Participants Cannot Be negetive!!',
                            'vmw_participants_female.min' => 'Participants Cannot Be negetive!!',
                            'supervisor_age.min' => 'Age cannot be  negetive!!',
                            'wst_participants.min' => 'Participants Cannot Be negetive!!',
                            'om_fund.min' => 'Fund Cannot Be negetive!!',
                            'variation.min' => 'Fund Cannot Be  negetive!!',
                            'variation_households.min' => 'variation of households Cannot Be negetive!!',
                            'variation_population.min' => 'variation of population Cannot Be negetive!!',
                            'post_construction_participants_male.min' => 'post construction participants Cannot Be negetive!!',
                            'post_construction_participants_female.min' => 'post construction participants Cannot Be negetive!!',
                            'wsp_training_participation_male.min' => 'wsp training participation Cannot Be negetive!!',
                            'wsp_training_participation_female.min' => 'wsp training participation Cannot Be negetive!!',
                        ]);
        }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data['del_flag'] = 1;
        $success = Prep_construction::where('id', $id)->update($data);
        return redirect()->back();

        //
    }
 
  


}
