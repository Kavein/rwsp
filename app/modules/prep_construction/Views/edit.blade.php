@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Construction 
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            {{-- <li><a href="{{ route('admin.prep_constructions',$id) }}">prep_construction</a></li>
            <li class="active">Edit</li> --}}
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.prep_constructions.update',$id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                
                    <input type="hidden" value = "{{$prep_construction->project_id}}"  name="project_id" id="project_id" class="form-control"  >
               
                <div class="form-group">
                    <label for="quantity_bill_record_date">Bill record of quantity</label>
                    <input type="text" value = "{{$prep_construction->quantity_bill_record_date}}"  name="quantity_bill_record_date" id="quantity_bill_record_date" class="form-control" >
                   
                </div>
                <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>
            
                <div class="form-group col-md-12" >
                        <label for=""><h3><strong>Estimation cost</strong></h3></label>
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="rough_estimation_cost">Rough Estimation Cost-Estimated Project Cost </label>
                        <input type="text" name="rough_estimation_cost" id="rough_estimation_cost" class="form-control"  value = "{{$prep_construction->rough_estimation_cost}}"   autocomplete="off" >
                        @if ($errors->has('rough_estimation_cost'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('rough_estimation_cost') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="amended_estimation_cost">Amended Estimation cost- Estimated Project Cost</label>
                        <input type="text" name="amended_estimation_cost" id="amended_estimation_cost" class="form-control"   value = "{{$prep_construction->amended_estimation_cost}}" autocomplete="off" >
                        @if ($errors->has('amended_estimation_cost'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('amended_estimation_cost') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>

                    <div class="form-group col-md-12" >
                        <label for="MOU_agreement_date">Date of MOU Agreements and Procurement</label>
                        <input type="text" name="MOU_agreement_date" id="MOU_agreement_date" class="form-control" value = "{{$prep_construction->MOU_agreement_date}}"  autocomplete="off" >
                    </div>

                    
                    <div class="form-group col-md-6" >
                        <label for="store_check_date">Store check(Date)</label>
                        <input type="text" name="store_check_date" id="store_check_date"  value = "{{$prep_construction->store_check_date}}" class="form-control"  autocomplete="off" >
                    </div>
                    <div class="form-group col-md-6" >
                            <label for="store_check">Store check (Check Construction Materials status)</label>
                           <div class="radio">
                                @if( $prep_construction->store_check == 1)

                                <label><input type="radio" value="1" name="store_check" id="store_check" checked/>Yes</label> 
                                <label><input type="radio" value="0" name="store_check" id="store_check"  />No</label>
                                @else
                                <label><input type="radio" value="1" name="store_check" id="store_check" />Yes</label> 
                                <label><input type="radio" value="0" name="store_check" id="store_check" checked />No</label>
                                @endif
                            </div>
                        </div>

                    <div class="form-group col-md-12" >
                        <label for="program_implementation">Program Implementation (Date)</label>
                        <input type="text" name="program_implementation" id="program_implementation" class="form-control" value = "{{$prep_construction->program_implementation}}"  autocomplete="off" >
                    </div>

                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>
            
                    <div class="form-group col-md-12" >
                        <label for=""><h3><strong>Training for VMW</strong></h3></label>
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="vmw_training_date">Training date </label>
                        <input type="text" name="vmw_training_date" id="vmw_training_date" class="form-control" value = "{{$prep_construction->vmw_training_date}}" autocomplete="off" >
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="vmw_participants">total no. of Participants(M&F)  </label>
                        <input type="text" name="vmw_participants" id="vmw_participants" class="form-control"  value = "{{$prep_construction->vmw_participants}}" autocomplete="off" >
                        @if ($errors->has('vmw_participants'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('vmw_participants') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>

                    <div class="form-group col-md-12" >
                        <label for=""><h3><strong> Community Safeguarding Supervisor Appointed </strong></h3></label>
                    </div>

                    <div class="form-group col-md-12" >
                        <label for="supervisor_name">Name  </label>
                        <input type="text" name="supervisor_name" id="supervisor_name" class="form-control" value = "{{$prep_construction->supervisor_name}}"  autocomplete="off" >
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="supervisor_age">Age</label>
                        <input type="text" name="supervisor_age" id="supervisor_age" class="form-control" value = "{{$prep_construction->supervisor_age}}" autocomplete="off" >
                    </div>
                    <div class="form-group col-md-6" >
                        <label for="sex">Sex</label>
                       <div class="radio">
                           @if($prep_construction->sex == "male")

                            <label><input type="radio" value="male" name="sex" id="sex" checked />Male</label> 
                            <label><input type="radio" value="female" name="sex" id="sex"  />Female</label>
                            @else
                            <label><input type="radio" value="male" name="sex" id="sex"/>Male</label> 
                            <label><input type="radio" value="female" name="sex" id="sex"  checked/>Female</label>
                            @endif
                        </div>
                    </div>
                    
                    <div class="col-md-12 " style="border-top: 1px solid #b7c2cc; "> </div>
                    <div class="form-group col-md-12" >
                            <label for=""><h3><strong> Mid Visit:  </strong></h3></label>
                        </div>
                        <div class="form-group col-md-6" >
                                <label for="mid_visit_date">date  </label>
                                <input type="text" name="mid_visit_date" id="mid_visit_date" class="form-control" value = "{{$prep_construction->mid_visit_date}}" autocomplete="off" >
                            </div>
                    <div class="form-group col-md-6">
                        <label for="structure_monitor">Structure Monitor</label>
                    <div class="radio">
                        @if($prep_construction->structure_monitor==1)
                        <label><input type="radio" value="1" name="structure_monitor" id="structure_monitor" checked/>Yes</label> 
                        <label><input type="radio" value="0" name="structure_monitor" id="structure_monitor"  />No</label>
                        @else
                        <label><input type="radio" value="1" name="structure_monitor" id="structure_monitor"/>Yes</label> 
                        <label><input type="radio" value="0" name="structure_monitor" id="structure_monitor"  checked/>No</label>
                        @endif
                    </div>
                    </div>

                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>

                    <div class="form-group col-md-12" >
                        <label for=""><h3><strong> Construction visit: </strong></h3></label>
                    </div>

                    <div class="form-group col-md-12" >
                        <label for="wsp_completion">Completion of water supply project date  </label>
                        <input type="text" name="wsp_completion" id="wsp_completion" class="form-control" value = "{{$prep_construction->wsp_completion}}" autocomplete="off" >
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="wst_date"> Wash strengthen Training Date</label>
                        <input type="text" name="wst_date" id="wst_date" class="form-control" value = "{{$prep_construction->wst_date}}" autocomplete="off" >
                    </div>
                    <div class="form-group col-md-6" >
                        <label for="wst_participants">Wash strengthen Training Participation </label>
                        <input type="text" name="wst_participants" id="wst_participants" class="form-control" value = "{{$prep_construction->wst_participants}}" autocomplete="off" >
                        @if ($errors->has('wst_participants'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('wst_participants') }}</strong>
                        </span>
                        @endif
                    </div>
                    
                    <div class="form-group col-md-12" >
                        <label for="om_fund">O&M Fund</label>
                        <input type="text" name="om_fund" id="om_fund" class="form-control" value = "{{$prep_construction->om_fund}}" autocomplete="off" >
                        @if ($errors->has('om_fund'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('om_fund') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-12" >
                            <label for="variation">Variation of tap stands</label>
                            <input type="text" name="variation" id="variation" class="form-control" value = "{{$prep_construction->variation}}"   autocomplete="off" >
                            @if ($errors->has('variation'))
                            <span class="help-block">
                                <strong style="color: red;">{{ $errors->first('variation') }}</strong>
                            </span>
                            @endif
                        </div>
                    <div class="form-group col-md-6" >
                        <label for="variation_households">Variation of tap stands(Number of Households) </label>
                        <input type="text" name="variation_households" id="variation_households" class="form-control" value = "{{$prep_construction->variation_households}}"   autocomplete="off" >
                        @if ($errors->has('variation_households'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('variation_households') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="variation_population">Variation of tap stands(Number of Population)  </label>
                        <input type="text" name="variation_population" id="variation_population" class="form-control" value = "{{$prep_construction->variation_population}}" autocomplete="off" >
                        @if ($errors->has('variation_population'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('variation_population') }}</strong>
                        </span>
                        @endif
                    </div>

                    <div class="col-md-12" style="border-top: 1px solid #b7c2cc; "> </div>


                    <div class="form-group col-md-12" >
                        <label for=""><h3><strong>Commission Visit:  </strong></h3></label>
                    </div>

                    <div class="form-group col-md-12" >
                        <label for="comissioning_date">Commissioning Date  </label>
                        <input type="text" name="comissioning_date" id="comissioning_date" class="form-control"  value = "{{$prep_construction->comissioning_date}}"   autocomplete="off" >
                    </div>
                    <div class="form-group col-md-12" >
                        <label for=""><h4><strong>Post Construction  </strong></h4></label>
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="post_construction_date">Training Date  </label>
                        <input type="text" name="post_construction_date" id="post_construction_date" class="form-control" value = "{{$prep_construction->post_construction_date}}" autocomplete="off" >
                    </div>
                    <div class="form-group col-md-6" >
                        <label for="post_construction_participants">Total no. of Participation(M&F) </label>
                        <input type="text" name="post_construction_participants" id="post_construction_participants" class="form-control" value = "{{$prep_construction->post_construction_participants}}" autocomplete="off" >
                        @if ($errors->has('post_construction_participants'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('post_construction_participants') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-12" >
                        <label for=""><h4><strong>Water Safety Plan Training   </strong></h4></label>
                    </div>

                    <div class="form-group col-md-6" >
                        <label for="wsp_training_date">Training Date  </label>
                        <input type="text" name="wsp_training_date" id="wsp_training_date" class="form-control" value = "{{$prep_construction->wsp_training_date}}" autocomplete="off" >
                    </div>
                    <div class="form-group col-md-6" >
                        <label for="wsp_training_participation">Total no. of Participation(M&F) </label>
                        <input type="text" name="wsp_training_participation" id="wsp_training_participation" class="form-control"   value = "{{$prep_construction->wsp_training_participation}}" autocomplete="off" >
                        @if ($errors->has('wsp_training_participation'))
                        <span class="help-block">
                            <strong style="color: red;">{{ $errors->first('wsp_training_participation') }}</strong>
                        </span>
                        @endif
                    </div>
                    <div class="form-group col-md-12" >
                        <label for="sanitary_funtion_survey">Sanitary and Function Survey </label>
                        <input type="text" name="sanitary_funtion_survey" id="sanitary_funtion_survey" class="form-control" value = "{{$prep_construction->sanitary_funtion_survey}}" autocomplete="off" >
                    </div>
                   
                    <input type="hidden" value = "{{$prep_construction->created_by}}"  name="created_by" id="created_by" class="form-control" >
                    <input type="hidden" value = "{{$prep_construction->created_at}}"  name="created_at" id="created_at" class="form-control" >
                    <input type="hidden" value = "{{$prep_construction->updated_by}}"  name="updated_by" id="updated_by" class="form-control" >
                    <input type="hidden" value = "{{$prep_construction->updated_at}}"  name="updated_at" id="updated_at" class="form-control" >

                
                
                
                <input type="hidden" name="id" id="id" value = "{{$prep_construction->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.prep_constructions',$prep_construction->project_id) }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>

    <script type="text/javascript">
        $(document).ready(function() {
            $("#quantity_bill_record_date,#MOU_agreement_date,#store_check_date,#vmw_training_date,#program_implementation,#mid_visit_date,#wsp_completion,#wst_date,#comissioning_date,#post_construction_date,#wsp_training_date,#sanitary_funtion_survey").daterangepicker({
                singleDatePicker:!0,
                locale: {
                    format: 'YYYY-MM-DD'
                }
            });     
        });
       
        </script>
@endsection
