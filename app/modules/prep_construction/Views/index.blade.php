@extends('admin.layout.main')
@section('content')
	<section class="content-header">
		<h1>
			Construction Details	
		</h1>
		<ol class="breadcrumb">
			<li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
			{{-- <li><a href="#">Prep_constructions</a></li> --}}

		</ol>
	</section>
	<section class="content">
		<div class="row">
			<div class="col-xs-12">
				<div class="box">
					<div class="box-header">
						<a href="{{ route('admin.prep_constructions.create',$id) }}" class="btn bg-green waves-effect"  title="create">Create</a>
					</div>
					<!-- /.box-header -->
					<div class="box-body">
						<table id="prep_construction-datatable" class="table table-striped table-bordered">
							<thead>
								<th>SN</th>
								
								<th >Created Date</th>		
								<th>Action</th>
							
							</thead>
						</table>
					</div>
					<!-- /.box-body -->
				</div>
				<!-- /.box -->
			</div>
			<!-- /.col -->
		</div>
		<!-- /.row -->
	</section>
	
	<script language="javascript" type="text/javascript">
		var dataTable; 
		var site_url = "{{ url('/') }}"+"/admin/prep_constructions";
		$(function(){
			dataTable = $('#prep_construction-datatable').DataTable({
				dom: "<'row'<'col-sm-6'l><'col-sm-6'f>>" +
		      	"<'row'<'col-sm-12'tr>>" +
		      	"<'row'<'col-sm-4'i><'col-sm-8 text-right'p>>",
		      	serverSide: true,
		      	processing: true,
	      		'ajax' : { url: "{{ route('admin.prep_constructions.getdatajson',$id) }}",type: 'POST', data: {'_token': '{{ csrf_token() }}' } },
				columns: [
					{ data: function (data, type, row, meta) {
				        return meta.row + meta.settings._iDisplayStart + 1;
			      	},name: "sn", searchable: false },
					{ data: function (data) {
						return 	moment( data.created_at	).format("YYYY-MM-DD");
					},name: "created_at", searchable: true },
          			{ data: function(data,b,c,table) { 
					var buttons = '';

					buttons += "<a class='btn bg-green waves-effect' href='"+site_url+"/edit/"+data.id+"' type='button' >Edit</a>&nbsp"; 

					buttons += "<a href='"+site_url+"/delete/"+data.id+"'  onclick=\"return (confirm('Are you Sure'))?true:false\" class='btn bg-red waves-effect' >Delete</a>";

					return buttons;
					}, name:'action',searchable: false},
				]
			});
		});

		
	</script>
@endsection
