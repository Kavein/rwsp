@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Add Prep_constructions   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.prep_constructions') }}">prep_construction</a></li>
            <li class="active">Add</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.prep_constructions.store') }}"  method="post">
                <div class="box-body">                
                    <div class="form-group">
                                    <label for="project_id">Project_id</label><input type="text" name="project_id" id="project_id" class="form-control" ></div><div class="form-group">
                                    <label for="quantity_bill_record_date">Quantity_bill_record_date</label><input type="text" name="quantity_bill_record_date" id="quantity_bill_record_date" class="form-control" ></div><div class="form-group">
                                    <label for="rough_estimation_cost">Rough_estimation_cost</label><input type="text" name="rough_estimation_cost" id="rough_estimation_cost" class="form-control" ></div><div class="form-group">
                                    <label for="amended_estimation_cost">Amended_estimation_cost</label><input type="text" name="amended_estimation_cost" id="amended_estimation_cost" class="form-control" ></div><div class="form-group">
                                    <label for="MOU_agreement_date">MOU_agreement_date</label><input type="text" name="MOU_agreement_date" id="MOU_agreement_date" class="form-control" ></div><div class="form-group">
                                    <label for="store_check_date">Store_check_date</label><input type="text" name="store_check_date" id="store_check_date" class="form-control" ></div><div class="form-group">
                                    <label for="store_check">Store_check</label><input type="text" name="store_check" id="store_check" class="form-control" ></div><div class="form-group">
                                    <label for="program_implementation">Program_implementation</label><input type="text" name="program_implementation" id="program_implementation" class="form-control" ></div><div class="form-group">
                                    <label for="vmw_training_date">Vmw_training_date</label><input type="text" name="vmw_training_date" id="vmw_training_date" class="form-control" ></div><div class="form-group">
                                    <label for="vmw_participants">Vmw_participants</label><input type="text" name="vmw_participants" id="vmw_participants" class="form-control" ></div><div class="form-group">
                                    <label for="supervisor_name">Supervisor_name</label><input type="text" name="supervisor_name" id="supervisor_name" class="form-control" ></div><div class="form-group">
                                    <label for="supervisor_age">Supervisor_age</label><input type="text" name="supervisor_age" id="supervisor_age" class="form-control" ></div><div class="form-group">
                                    <label for="sex">Sex</label><input type="text" name="sex" id="sex" class="form-control" ></div><div class="form-group">
                                    <label for="mid_visit_date">Mid_visit_date</label><input type="text" name="mid_visit_date" id="mid_visit_date" class="form-control" ></div><div class="form-group">
                                    <label for="structure_monitor">Structure_monitor</label><input type="text" name="structure_monitor" id="structure_monitor" class="form-control" ></div><div class="form-group">
                                    <label for="wsp_completion">Wsp_completion</label><input type="text" name="wsp_completion" id="wsp_completion" class="form-control" ></div><div class="form-group">
                                    <label for="wst_date">Wst_date</label><input type="text" name="wst_date" id="wst_date" class="form-control" ></div><div class="form-group">
                                    <label for="wst_participants">Wst_participants</label><input type="text" name="wst_participants" id="wst_participants" class="form-control" ></div><div class="form-group">
                                    <label for="om_fund">Om_fund</label><input type="text" name="om_fund" id="om_fund" class="form-control" ></div><div class="form-group">
                                    <label for="variation">Variation</label><input type="text" name="variation" id="variation" class="form-control" ></div><div class="form-group">
                                    <label for="variation_households">Variation_households</label><input type="text" name="variation_households" id="variation_households" class="form-control" ></div><div class="form-group">
                                    <label for="variation_population">Variation_population</label><input type="text" name="variation_population" id="variation_population" class="form-control" ></div><div class="form-group">
                                    <label for="comissioning_date">Comissioning_date</label><input type="text" name="comissioning_date" id="comissioning_date" class="form-control" ></div><div class="form-group">
                                    <label for="post_construction_date">Post_construction_date</label><input type="text" name="post_construction_date" id="post_construction_date" class="form-control" ></div><div class="form-group">
                                    <label for="post_construction_participants">Post_construction_participants</label><input type="text" name="post_construction_participants" id="post_construction_participants" class="form-control" ></div><div class="form-group">
                                    <label for="wsp_training_date">Wsp_training_date</label><input type="text" name="wsp_training_date" id="wsp_training_date" class="form-control" ></div><div class="form-group">
                                    <label for="wsp_training_participation">Wsp_training_participation</label><input type="text" name="wsp_training_participation" id="wsp_training_participation" class="form-control" ></div><div class="form-group">
                                    <label for="sanitary_funtion_survey">Sanitary_funtion_survey</label><input type="text" name="sanitary_funtion_survey" id="sanitary_funtion_survey" class="form-control" ></div><div class="form-group">
                                    <label for="created_by">Created_by</label><input type="text" name="created_by" id="created_by" class="form-control" ></div><div class="form-group">
                                    <label for="created_at">Created_at</label><input type="text" name="created_at" id="created_at" class="form-control" ></div><div class="form-group">
                                    <label for="updated_by">Updated_by</label><input type="text" name="updated_by" id="updated_by" class="form-control" ></div><div class="form-group">
                                    <label for="updated_at">Updated_at</label><input type="text" name="updated_at" id="updated_at" class="form-control" ></div>
<input type="hidden" name="id" id="id"/>
                </div>
                {{ csrf_field() }}
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.prep_constructions') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
