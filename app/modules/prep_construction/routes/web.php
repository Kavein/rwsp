<?php



Route::group(array('prefix'=>'admin/','module'=>'prep_construction','middleware' => ['web','auth'], 'namespace' => 'App\modules\prep_construction\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('prep_constructions/{id}','AdminPrep_constructionController@index')->name('admin.prep_constructions');
    Route::post('prep_constructions/getprep_constructionsJson/{id}','AdminPrep_constructionController@getprep_constructionsJson')->name('admin.prep_constructions.getdatajson');
    Route::get('prep_constructions/create/{id}','AdminPrep_constructionController@create')->name('admin.prep_constructions.create');
    Route::post('prep_constructions/store','AdminPrep_constructionController@store')->name('admin.prep_constructions.store');
    Route::get('prep_constructions/show/{id}','AdminPrep_constructionController@show')->name('admin.prep_constructions.show');
    Route::get('prep_constructions/edit/{id}','AdminPrep_constructionController@edit')->name('admin.prep_constructions.edit');
    Route::match(['put', 'patch'], 'prep_constructions/update/{id}','AdminPrep_constructionController@update')->name('admin.prep_constructions.update');
    Route::get('prep_constructions/delete/{id}', 'AdminPrep_constructionController@destroy')->name('admin.prep_constructions.delete');

    // Route::get('prep_construction/construction/{id}','AdminPrep_constructionController@construction')->name('admin.prep_constructions.construction');
    Route::post('prep_construction/store_construction','AdminPrep_constructionController@store')->name('store');
    // Route::post('prep_construction/construction_details/{id}','AdminPrep_constructionController@index')->name('construction_details');
    // Route::get('prep_construction/getdatajson/{id}', 'AdminPrep_constructionController@getdatajson')->name('admin.prep_constructions.getdatajson');


});




Route::group(array('module'=>'prep_construction','namespace' => 'App\modules\prep_construction\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('prep_constructions/','Prep_constructionController@index')->name('prep_constructions');
    
});