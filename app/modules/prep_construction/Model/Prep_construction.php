<?php

namespace App\modules\Prep_construction\Model;


use Illuminate\Database\Eloquent\Model;

class Prep_construction extends Model
{
    public  $table = 'prep_construction';

    protected $fillable = ['id','project_id','quantity_bill_record_date','rough_estimation_cost',
                        'amended_estimation_cost','MOU_agreement_date','store_check_date','store_check',
                        'program_implementation','vmw_training_date','vmw_participants','supervisor_name',
                        'supervisor_age','sex','mid_visit_date','structure_monitor','wsp_completion','wst_date',
                        'wst_participants','om_fund','variation','variation_households','variation_population',
                        'comissioning_date','post_construction_date','post_construction_participants',
                        'wsp_training_date','wsp_training_participation','sanitary_funtion_survey','created_by',
                        'created_at','updated_by','updated_at','village_sketch'];

    public function projects()
    {
    	return $this->belongsTo('App\Modules\Project\Model\Project', 'project_id');
    }
}
