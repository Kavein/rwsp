<?php

namespace App\Modules\Religion\Model;


use Illuminate\Database\Eloquent\Model;

class Religion extends Model
{
    public  $table = 'mst_religions';

    protected $fillable = ['id','name','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
