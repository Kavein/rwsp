<?php



Route::group(array('prefix'=>'admin/','module'=>'Religion','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Religion\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('religions/','AdminReligionController@index')->name('admin.religions');
    Route::post('religions/getreligionsJson','AdminReligionController@getreligionsJson')->name('admin.religions.getdatajson');
    Route::get('religions/create','AdminReligionController@create')->name('admin.religions.create');
    Route::post('religions/store','AdminReligionController@store')->name('admin.religions.store');
    Route::get('religions/show/{id}','AdminReligionController@show')->name('admin.religions.show');
    Route::get('religions/edit/{id}','AdminReligionController@edit')->name('admin.religions.edit');
    Route::match(['put', 'patch'], 'religions/update/{id}','AdminReligionController@update')->name('admin.religions.update');
    Route::get('religions/delete/{id}', 'AdminReligionController@destroy')->name('admin.religions.edit');
});




Route::group(array('module'=>'Religion','namespace' => 'App\Modules\Religion\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('religions/','ReligionController@index')->name('religions');
    
});