@extends('admin.layout.main')
@section('content')
    <section class="content-header">
        <h1>
            Edit Districts   
            <small></small>                    
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('home') }}"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('admin.districts') }}">districts</a></li>
            <li class="active">Edit</li>
        </ol>
    </section>
    <section class="content">
        <div class="box box-primary">
            <form role="form" action="{{ route('admin.districts.update',$district->id) }}"  method="post">
                <div class="box-body">    
                {{method_field('PATCH')}}            
                <div class="form-group">
                    <label for="name">District Name</label>
                    <input type="text" value = "{{$district->name}}"  name="name" id="name" class="form-control" >
                    @if ($errors->has('name'))
                        <span class="help-block">
                            <strong>{{ $errors->first('name') }}</strong>
                        </span>
                    @endif
                </div>
                <!-- <div class="form-group">
                    <label for="province_id">Province_id</label>
                    <input type="text" value = "{{$district->province_id}}"  name="province_id" id="province_id" class="form-control" >
                </div> -->

                <div class="form-group">
                    <label for="province_id">Province Name</label>
                    <select class="form-control" name="province_id">
                        @foreach($provinces as $province)
                        <option value="{{$province->id}}" {{$district->province_id == $province->id ? 'selected':''}}>{{$province->name}}</option>
                        @endforeach
                    </select>
                </div>

                <div class="form-group">
                    <label for="code">Code</label>
                    <input type="text" value = "{{$district->code}}"  name="code" id="code" class="form-control" >
                    @if ($errors->has('code'))
                        <span class="help-block">
                            <strong>{{ $errors->first('code') }}</strong>
                        </span>
                    @endif
                </div>
                <div class="form-group">
                    <label for="status">Status</label><br>
                    <input type="checkbox" name="status" value="1" style="height: 20px; width: 20px;" {{$district->status == 1 ? 'checked':''}} >
                </div>
<input type="hidden" name="id" id="id" value = "{{$district->id}}" />
                {{ csrf_field() }}
                </div>
                <div class="box-footer">
                    <button type="submit" class="btn btn-primary">Save</button>
                    <a href="{{ route('admin.districts') }}" class="btn btn-danger">Cancel</a>
                </div>
            </form>
        </div>
    </section>
@endsection
