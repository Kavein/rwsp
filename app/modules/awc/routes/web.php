<?php



Route::group(array('prefix'=>'admin/','module'=>'Awc','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Awc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('awcs/','AdminAwcController@index')->name('admin.awcs');
    Route::post('awcs/getawcsJson','AdminAwcController@getawcsJson')->name('admin.awcs.getdatajson');
    Route::get('awcs/create','AdminAwcController@create')->name('admin.awcs.create');
    Route::post('awcs/store','AdminAwcController@store')->name('admin.awcs.store');
    Route::get('awcs/show/{id}','AdminAwcController@show')->name('admin.awcs.show');
    Route::get('awcs/edit/{id}','AdminAwcController@edit')->name('admin.awcs.edit');
    Route::match(['put', 'patch'], 'awcs/update/{id}','AdminAwcController@update')->name('admin.awcs.update');
    Route::get('awcs/delete/{id}', 'AdminAwcController@destroy')->name('admin.awcs.edit');
});




Route::group(array('module'=>'Awc','namespace' => 'App\Modules\Awc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('awcs/','AwcController@index')->name('awcs');
    
});