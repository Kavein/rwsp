<?php

namespace App\Modules\Awc\Model;


use Illuminate\Database\Eloquent\Model;

class Awc extends Model
{
    public  $table = 'mst_awcs';

    protected $fillable = ['id','name','address','contact_no','status','del_flag','created_by','updated_by','created_at','updated_at',];
}
