<?php



Route::group(array('prefix'=>'admin/','module'=>'Vdc','middleware' => ['web','auth'], 'namespace' => 'App\Modules\Vdc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('vdcs/','AdminVdcController@index')->name('admin.vdcs');
    Route::post('vdcs/getvdcsJson','AdminVdcController@getvdcsJson')->name('admin.vdcs.getdatajson');
    Route::get('vdcs/create','AdminVdcController@create')->name('admin.vdcs.create');
    Route::post('vdcs/store','AdminVdcController@store')->name('admin.vdcs.store');
    Route::get('vdcs/show/{id}','AdminVdcController@show')->name('admin.vdcs.show');
    Route::get('vdcs/edit/{id}','AdminVdcController@edit')->name('admin.vdcs.edit');
    Route::match(['put', 'patch'], 'vdcs/update/{id}','AdminVdcController@update')->name('admin.vdcs.update');
    Route::get('vdcs/delete/{id}', 'AdminVdcController@destroy')->name('admin.vdcs.edit');
});




Route::group(array('module'=>'Vdc','namespace' => 'App\Modules\Vdc\Controllers'), function() {
    //Your routes belong to this module.
    Route::get('vdcs/','VdcController@index')->name('vdcs');
    
});