<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\FromCollection;

class HandWashExport implements FromView
{
    public function __construct($view, $data = "")
	{
	    $this->view = $view;
	    $this->data = $data;
	    // $this->remaining = $remaining;
	}
	public function view(): View
	{
		// $data= $this->data;
	    return view($this->view,[
	        'data'=>$this->data
	    ]);
	}
}
