<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Modules\Project\Model\Project;
use Maatwebsite\Excel\Concerns\WithHeadings;

class ProjectExport implements FromCollection,WithHeadings
{
    /**
    * @return \Illuminate\Support\Collection
    */
    public function collection()
    {
        return Project::all();
    }

    public function headings(): array
    {
        return [
            '#',
            'Project Number',
        ];
    }
}
