<?php

namespace App\Http\Controllers;

use http\Env\Response;
use Illuminate\Http\Request;
use App\Modules\Project\Model\Project;
use App\Modules\Province\Model\Province;
use App\Modules\District\Model\District;
use App\Modules\Vdc\Model\Vdc;
use App\Modules\Service_type\Model\Service_type;
use App\Modules\System_type\Model\System_type;
use App\Modules\Awc\Model\Awc;
use App\Modules\Funding_partner\Model\Funding_partner;
use App\Modules\Water_source\Model\Water_source;
use Auth;
use DB;
use Illuminate\Support\Facades\Input;
use Validator;

class ProjectApiController extends Controller
{
    public function list(Request $request)
    {
    	// dd($request->header('token'));
    	if(tokenCheck($request->header('token'))){
    		$data = Project::where('del_flag',0)->get();
    		
    		return response(json_encode(array('project'=>$data)));
    	}
    	else{
    		return response("Try Again!!");
    	}
    }

    public function ProjectDetail(Request $request)
    {
        if(tokenCheck($request->header('token'))){
            $find_project = Project::find($request->id);
            if($find_project){
                $data = Project::where('id',$request->id)->first();

                        // dd($data->district_id);
                        $response[]= $data;
                        $response['province'] = Province::where('id',$data->province_id)->pluck('name')->first();
                        $response['district'] = District::where('id',$data->district_id)->pluck('name')->first();
                        $response['vdc'] = Vdc::where('id',$data->vdc_id)->pluck('name')->first();
                        $response['service_type'] = Service_type::where('id',$data->service_type_id)->pluck('name')->first();
                        $response['system_type'] = Service_type::where('id',$data->system_type_id)->pluck('name')->first();
                        $response['funding_partner'] = Funding_partner::where('id',$data->funding_partner_id)->pluck('name')->first();
                        $response['awc'] = Awc::where('id',$data->awc_id)->pluck('name')->first();

                        return response(json_encode(array('project_data'=>$response)));
            }
            else{
                return response(json_encode(array('message'=>"Project Doesnt Exists !!")));
            }
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function getAllLocation()
    {
        $province = Province::where('status',1)->pluck('name','id');
        foreach ($province as $key => $value) {
            $districts = District::where('status',1)->where('province_id',$key)->pluck('name','id');
            $dArray = array();
            foreach ($districts as $k => $v) {               
               $vcds = Vdc::where('status',0)->where('district_id',$k)->pluck('name','id');  
               $vcdArray = array();
               foreach($vcds as $ii=>$vv)
               {
                 $vcdArray[] = array('id' => $ii, 'value' => $vv);
               }
               $dArray[] = array('id' => $k, 'value' => $v, 'vdcs' => $vcdArray);               
            } 
            $response['regions'][] = array('provience_id' => $key, 'provience_name' => $value, 'districts' => $dArray);    
        }
        $response['service_type'] = Service_type::where('del_flag',0)->pluck('name','id');
        $response['system_type'] = System_type::where('del_flag',0)->pluck('name','id');
        $response['funding_partner'] = Funding_partner::where('del_flag',0)->pluck('name','id');
        $response['awc'] = Awc::where('del_flag',0)->pluck('name','id');
        $response['water_source'] = Water_source::where('del_flag',0)->pluck('name','id');
        $response['mansoon_type'] = config('project.mansoon_type');
        $response['project_type'] = config('project.project_type');
        $response['geographical_region'] = config('project.geographical_region');
        // dd($response);

        return response(json_encode(array('data'=>$response)));

    }

    public function AddProject(Request $request)
    {
    	if (tokenCheck($request->header('token'))) {
            $data = $request->all();
            $user = Auth::user()->id;
            $year = DB::table('mst_fiscal_year')->where('status',1)->first();
            $data['fiscal_year'] = $year->id;
            $data['created_at'] = date('Y-m-d');
            $data['updated_at'] = date('Y-m-d');
            $data['created_by'] = $user;
            $data['updated_by'] = $user;
            $data['status'] = checkValue($request->status);
            $data['del_flag'] = 0;
            try {
                $success = Project::Create($data);
                return response(json_encode(array('success'=>true,'message' => 'Data Has been Added SuccessFully !!')));
            } catch (Exception $e) {
                return response(json_encode(array('message' => 'OOPS Something went Wrong !!')));
            }
            
        }
        else{
            return response(json_encode(array('message' => 'Login Please !!')));
        }
    }

    public function province(Request $request){
        if(tokenCheck($request->header('token'))){
            $data = DB::table('mst_provinces')->select(['id','name','code'])->where('status',1)->get();
            return response()->json($data);
        }else{
            return response("Please Login To Access Data");
        }
    }

    public function district($id = null,Request $request){
        if(tokenCheck($request->header('token'))) {
            if ($id) {
                if (DB::table('mst_districts')->select(['id', 'name', 'code'])->where('province_id', $id)->count() > 0) {
                    $data = DB::table('mst_districts')->select(['id', 'name', 'code'])->where('province_id', $id)->get();
                    return response()->json($data);
                } else {
                    return response()->json(null);
                }
            } else {
                return response()->json(null);
            }
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function vdc($id,Request $request ){
        if(tokenCheck($request->header('token'))) {
        if(DB::table('mst_vdcs')->select(['id','name','code'])->where('district_id',$id)->count() > 0 ){
            $data = DB::table('mst_vdcs')->select(['id','name','code'])->where('district_id',$id)->get();
            return response()->json($data);
        }
        }
        else{
            return response("Please Login To Access Data");
        }
    }


    public function formOthers(Request $request){
        if(tokenCheck($request->header('token'))) {
        $funding = DB::table('mst_funding_partners')->select(['id','name'])->where('del_flag',0)->get();
        $system = DB::table('mst_system_types')->select(['id','name'])->where('del_flag',0)->get();
        $service = DB::table('mst_service_types')->select(['id','name'])->where('del_flag',0)->get();
        $sources = DB::table('mst_water_sources')->select(['id','name'])->where('del_flag',0)->get();
        $awc = DB::table('mst_awcs')->select(['id','name'])->where('del_flag',0)->get();
        $result = array([
            'geographical_region'   =>array(array('name'=>'mountain'),array('name'=>'hill'),array('name'=>'terai')),
            'mansoon'               =>array(array('name'=>'pre-mansoon'),array('name'=>'post-mansoon')),
            'funding'               =>$funding,
            'system_type'           =>$system,
            'service_type'          =>$service,
            'sources_drinking_water'=>$sources,
            'awc'                   =>$awc,
            'project_type'          =>array(array('name'=>'new'),array('name'=>'re-build')),
        ]);
        return response()->json($result);
    }
    else{
    return response("Please Login To Access Data");
    }
    }

    public function store(Request $request)
    {
        if(tokenCheck($request->header('token'))) {
            $data = $request->except('_token','water_source_id','community','community_mapping');
            $send_sources = (isset($request->water_source_id) ? $request->water_source_id:array());

            $userID = DB::table('users')->where('remember_token',$request->header('token'))->first();
            $user = $userID->id;
            $validator = Validator::make($request->all(), [
                'project_no' => 'required',
                'project_name' => 'required',
                'ward_no' => 'required',
                'province_id' => 'required',
                'district_id' => 'required',
                'vdc_id' => 'required',
            ],
            [
                'project_no.required' => 'Project Number Cannot Be Empty!!',
                'project_name.required' => 'Project Name Cannot Be Empty!!',
                'ward_no.required' => 'Ward Number Cannot Be Empty!!',
                'province_id.required' => 'Please Select a Province!!',
                'district_id.required' => 'Please Select a District!!',
                'vdc_id.required' => 'Please Select a VDC / Muncipality!!',
            ]);
            if ($validator->fails()) {
                return response()->json($validator->messages(), 200);
            }
            $data['created_at'] = date('Y-m-d');
            $data['updated_at'] = date('Y-m-d');
            $data['created_by'] = $user;
            $data['updated_by'] = $user;
            $data['status'] = checkValue($request->status);
            $data['del_flag'] = 0;
            $community = $request->community;

            $success = Project::Create($data);

            if($request->community['community_mapping'])
            {
                $images = $request->community['community_mapping'];
                $image  = base64_decode($images);
                $imageName= str_random()."."."png";

                $destination = public_path('images/community/'.$success->id);
                if(!file_exists($destination)) {
                    mkdir($destination, 0777, true);
                }
                file_put_contents($destination.'/'.$imageName, $image);
                $community['community_mapping']=$imageName;
            }

            $this->CommunityInfo($community,$success->id);

            if($send_sources == null){
                DB::table('prj_project_water_sources')
                    ->insert([
                        'water_source_id'=> null,
                        'project_id' => $success->id
                    ]);
            }
            else{
                foreach ($send_sources as $key => $value) {
                    DB::table('prj_project_water_sources')
                        ->insert([
                            'water_source_id'=>$value,
                            'project_id' => $success->id
                        ]);
                }
            }

            // DB::
            return response()->json(array('success'=>true),200);
            //
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function CommunityInfo($communityData,$id)
    {

            $community = $communityData;
            if ($id) {
                DB::table('prj_community_infos')->where('project_id', $id)->delete();
            }
            DB::table('prj_community_infos')->insert([
                'community_name' => $community['community_name'],
                'male_students' => $community['male_students'],
                'female_students' => $community['female_students'],
                'total_students' => $community['total_students'],
                'total_staff' => $community['total_staff'],
                'existing_latrines' => $community['existing_latrines'],
                'tapstand_required' => $community['tapstand_required'],
                'other_existing_latrines' => $community['other_existing_latrines'],
                'other_tapstand_required' => $community['other_tapstand_required'],
                'diarrhoea_male' => $community['diarrhoea_male'],
                'diarrhoea_female' => $community['diarrhoea_female'],
                'diarrhoea_total' => $community['diarrhoea_male']+$community['diarrhoea_female'],
                'dysentery_male' => $community['dysentery_male'],
                'dysentery_female' => $community['dysentery_female'],
                'dysentery_total' => $community['dysentery_male']+$community['dysentery_female'],
                'jaundice_male' => $community['jaundice_male'],
                'jaundice_female' => $community['jaundice_female'],
                'jaundice_total' => $community['jaundice_male']+$community['jaundice_female'],
                'colera_male' => $community['colera_male'],
                'colera_female' => $community['colera_female'],
                'colera_total' => $community['colera_male']+$community['colera_female'],
                'worms_male' => $community['worms_male'],
                'worms_female' => $community['worms_female'],
                'worms_total' => $community['worms_male']+$community['worms_female'],
                'scabies_male' => $community['scabies_male'],
                'scabies_female' => $community['scabies_female'],
                'scabies_total' => $community['scabies_male']+$community['scabies_female'],
                'profiling_date' => $community['profiling_date'],
                'disabled_male' => $community['disabled_male'],
                'disabled_female' => $community['disabled_female'],
                'blind_male' => $community['blind_male'],
                'blind_female' => $community['blind_female'],
                'deaf_male' => $community['deaf_male'],
                'deaf_female' => $community['deaf_female'],
                'blind_deaf_male' => $community['blind_deaf_male'],
                'blind_deaf_female' => $community['blind_deaf_female'],
                'speech_male' => $community['speech_male'],
                'speech_female' => $community['speech_female'],
                'mental_male' => $community['mental_male'],
                'mental_female' => $community['mental_female'],
                'intel_male' => $community['intel_male'],
                'intel_female' => $community['intel_female'],
                'multiple_male' => $community['multiple_male'],
                'multiple_female' => $community['multiple_female'],
                'community_mapping' => $community['community_mapping'],
                'project_id' => $id,
            ]);
        }
}
