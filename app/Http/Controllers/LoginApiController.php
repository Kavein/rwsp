<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Hash;

class LoginApiController extends Controller
{
    public function Login(Request $request)
    {
    	try {
    		$data = $request->all();
    		// dd($data);
    		$user = DB::table('users')->where('email',$data['email'])->where('status',0)->first();
    		if(Hash::check($data['password'],$user->password))
    		{
    			$response['username'] = $user->username;
    			$response['email'] = $user->email;
    			$response['token'] = str_random(25);
    			DB::table('users')->where('id',$user->id)->update(['remember_token'=>$response['token']]);
    			// dd($response);
    			return response($response);
    		}
    	} catch (Exception $e) {
    		return response('User Doesnot Exists!!');
    	}
    }
}
