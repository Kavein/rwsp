<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Model\AgeGroup;
use App\Model\HealthBehaviour;
use App\Model\HealthData;
use App\Model\Hygiene;
use App\Model\InclusionData;
use App\Model\WaterFetch;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Schema;
use App\Modules\Project\Model\Project;
use App\Modules\Province\Model\Province;
use App\Modules\District\Model\District;
use App\Modules\VDC\Model\VDC;
use App\Modules\Fiscal_year\Model\Fiscal_year;
use App\Modules\Project_phase\Model\Project_phase;
use App\Modules\Awc\Model\Awc;
use App\Modules\Funding_partner\Model\Funding_partner;
use App\Modules\Service_type\Model\Service_type;
use App\Modules\System_type\Model\System_type;
use App\Modules\Water_source\Model\Water_source;
use App\Modules\Cast\Model\Cast;
use App\Modules\Religion\Model\Religion;
use App\Modules\Occupation\Model\Occupation;
use App\Modules\Household_detail\Model\Household_detail;
use App\Modules\Income_source\Model\Income_source;
use Excel;
use App\Exports\ProjectExport;
use App\Modules\Cast_divison\Model\Cast_divison;
class HouseholdApiController extends Controller
{

    public function casts_master(Request $request){
        if(tokenCheck($request->header('token'))) {
        $result = DB::table('mst_cast_divisons')->select(['id','name'])->where('del_flag',0)->get();
        return response()->json($result);
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function cast($id,Request $request){
        if(tokenCheck($request->header('token'))) {
        $result = DB::table('mst_casts')->select(['id','name'])->where('cast_divison_id',$id)->where('del_flag',0)->get();
        return response()->json($result);
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function occupation(Request $request){
        if(tokenCheck($request->header('token'))) {
            $result = DB::table('mst_occupations')->select(['id','name'])->where('del_flag',0)->get();
            return response()->json($result);
        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function store(Request $request)
    {
        if (tokenCheck($request->header('token'))) {

            $data = $request->except('_token');

            // ------------------------------ This is wehre the validation function is called ----------------------------------------
            $this->formValidation($request);

            //HouseHold Store
            $houseHold['project_id'] = $data['project_id'];
            //$houseHold['community_name'] = $data['community_name'];
            $houseHold['house_no'] = $data['house_no'];
            $houseHold['head_name'] = $data['head_name'];
            $houseHold['head_gender'] = $data['head_gender'];
            $houseHold['tole_name'] = $data['tole_name'];
            $houseHold['total_male'] = $data['total_male'];
            $houseHold['total_female'] = $data['total_female'];
            $houseHold['total_member'] = $data['total_female'] + $data['total_male'];
            $houseHold['cast_id'] = $data['cast_id'];
            $houseHold['cast_division'] = $data['cast_division'];
            // $houseHold['religion_id'] = $data['religion_id'];
            $userID = DB::table('users')->where('remember_token',$request->header('token'))->first();
            $user = $userID->id;
            $houseHold['created_at'] = date('Y-m-d');
            $houseHold['updated_at'] = date('Y-m-d');
            $houseHold['created_by'] = $user;
            $houseHold['updated_by'] = $user;
            $houseHold['phase'] = $data['phase'];
            $houseHold['del_phase1'] = 0;
            $houseHold['del_flag'] = 0;

            $houseHold['status'] = checkValue($request->status);
            $houseHold['religion_minority'] = checkValue($request->religion_minority);

            //Later Added
            $houseHold['total_female_widow'] = $data['total_female_widow'];
            $houseHold['litrate_no'] = $data['litrate_no'];
            $houseHold['illitrate_no'] = $data['illitrate_no'];
            $houseHold['income_pension'] = checkValue($request->income_pension);
            //End


            $data['latrine_available'] = checkValue($request->latrine_available);
            $data['latrine_cleansing_agent'] = checkValue($request->latrine_cleansing_agent);
            $data['latrine_water_available'] = checkValue($request->latrine_water_available);
            $data['latrine_cleaning_brush'] = checkValue($request->latrine_cleaning_brush);
            $data['home_cleaning'] = checkValue($request->home_cleaning);
            $data['domestic_animal'] = checkValue($request->domestic_animal);
            $data['drinking_water'] = checkValue($request->drinking_water);
            $data['food_covered'] = checkValue($request->food_covered);
            $data['utensils_clean'] = checkValue($request->utensils_clean);
            $data['garbage_disposel_pit'] = checkValue($request->garbage_disposel_pit);
            $data['occupation_id'] = (isset($request->occupation_id) ? $request->occupation_id : array());

            $data['income'] = (isset($request->income) ? $request->income: array());

            // dd($houseHold);

//            $hygiene = array(
//                'latrine_available' => $data['latrine_available'],
//                'total_latrine' => $data['total_latrine'],
//                'latrine_cleansing_agent' => $data['latrine_cleansing_agent'],
//                'latrine_water_available' => $data['latrine_water_available'],
//                'latrine_cleaning_brush' => $data['latrine_cleaning_brush'],
//                'defecate_latrine' => $data['defecate_latrine'],
//                'defecate_open' => $data['defecate_open'],
//                'home_cleaning' => $data['home_cleaning'],
//                'domestic_animal' => $data['domestic_animal'],
//                'drinking_water' => $data['drinking_water'],
//                'food_covered' => $data['food_covered'],
//                'utensils_clean' => $data['utensils_clean'],
//                'garbage_disposel_pit' => $data['garbage_disposel_pit'],
//                'handwash_water_soap' => $data['handwash_water_soap'],
//                'handwash_water' => $data['handwash_water'],
//                'handwash_ash' => $data['handwash_ash'],
//                'handwash_others' => $data['handwash_others'],
//                'defecate_water_soap' => $data['defecate_water_soap'],
//                'defecate_water' => $data['defecate_water'],
//                'defecate_ash' => $data['defecate_ash'],
//                'handwash_infant' => $data['handwash_infant'],
//                'bath_frequency' => $data['bath_frequency'],
//                'cloth_wash_frequency' => $data['cloth_wash_frequency'],
//                'menstrual_knowledge' => $data['menstrual_knowledge'],
//            );

            $zeroTo5_male = $request->age['between_0to5_male'];
            $sixTo15_male = $request->age['between_6to15_male'];
            $sixteenTo65_male = $request->age['between_16to65_male'];
            $above_65_male = $request->age['above65_male'];
            $dis_able_male = $request->age['disabled_male'];

            $zeroTo5_female = $request->age['between_0to5_female'];
            $sixTo15_female = $request->age['between_6to15_female'];
            $sixteenTo65_female = $request->age['between_16to65_female'];
            $above_65_female = $request->age['above65_female'];
            $dis_able_female = $request->age['disabled_female'];

            $total_male_in_age = $zeroTo5_male + $sixTo15_male + $sixteenTo65_male + $above_65_male + $dis_able_male;

            $total_female_in_age = $zeroTo5_female + $sixTo15_female + $sixteenTo65_female + $above_65_female + $dis_able_female;

            // dd($request->total_male);

            if ($request->total_male == $total_male_in_age && $request->total_female == $total_female_in_age) {

                $success = Household_detail::Create($houseHold);
//                //End HouseHold Store
//
//                //storing WaterFetching Time
//                $this->waterFetching($data['water_fetch'], $success->id);
//
//                //Storing ageGroup OF HouseHold
                $this->storeAgeGroup($data['age'], $success->id);
                
//
//                //Storing Occupation
                $this->storeOccupation($data['occupation_id'], $success->id);
//
//                //Storing Income Source and Pension
                $this->storeIncome($data['income'], $success->id, $houseHold['income_pension']);
//
//                //Storing Hygiene
//                $this->storeHygiene($hygiene, $success->id);
//
//                //storing Health Data
//                $this->storeIncidence($data['incidence'], $success->id);
//
//                //Storing Health Behavior
//                $this->storeHealthBehavior($data['behavior'], $success->id);
//
//                //Storing Inclusion Data
//                $this->storeInclusionData($data['inclusion'], $success->id);
//

                return response()->json(array('household_id'=>$success->id,'success'=>true), 200);

            } else {
                return response()->json(array('error '=>500,'success'=>false));
            }




        }
        else{
            return response("Please Login To Access Data");
        }
    }

    public function formValidation($request)
        {
            $this->validate($request, [
                'cast_division' => 'required',
                'cast_id' => 'required',

                // ---------------------------------------------------- HOUSEHOLD CATEGORY ---------------------------------------
                'house_no' => 'required',
                'head_name' => 'required',
                'tole_name' => 'required',
                'total_male' => 'nullable|integer|min:0',
                'total_female' => 'nullable|integer|min:0',
                'total_female' => 'nullable|integer|min:0',
                'total_female_widow' => 'nullable|integer|min:0',

                'age.between_0to5_male' => 'nullable|integer|min:0',
                'age.between_0to5_female' => 'nullable|integer|min:0',

                'age.between_6to15_male' => 'nullable|integer|min:0',
                'age.between_6to15_female' => 'nullable|integer|min:0',

                'age.between_16to65_male' => 'nullable|integer|min:0',
                'age.between_16to65_female' => 'nullable|integer|min:0',

                'age.above65_male' => 'nullable|integer|min:0',
                'age.above65_female' => 'nullable|integer|min:0',

                'age.disabled_male' => 'nullable|integer|min:0',
                'age.disabled_female' => 'nullable|integer|min:0',

                'litrate_no' => 'nullable|integer|min:0',
                'illitrate_no' => 'nullable|integer|min:0',

                // ----------------------------------------- WATER FETCHING TIME -------------------------------------------------

                'water_fetch.water_collection_liter' => 'nullable|integer|min:0',
                'water_fetch.going_second' => 'nullable|integer|min:0',
                'water_fetch.waiting_second' => 'nullable|integer|min:0',
                'water_fetch.filling_second' => 'nullable|integer|min:0',
                'water_fetch.return_second' => 'nullable|integer|min:0',

                // ----------------------------------------- DATA ON HYGIENE AND SANITATION --------------------------------------

                'handwash_water_soap' => 'nullable|integer|min:0',
                'handwash_water' => 'nullable|integer|min:0',
                'handwash_ash' => 'nullable|integer|min:0',
                'handwash_others' => 'nullable|integer|min:0',
                'defecate_water_soap' => 'nullable|integer|min:0',
                'defecate_water' => 'nullable|integer|min:0',
                'defecate_ash' => 'nullable|integer|min:0',
                'handwash_infant' => 'nullable|integer|min:0',

                // ----------------------------------------- HEALTH DATA ---------------------------------------------------------

                'incidence.diarrhoea_male' => 'nullable|integer|min:0',
                'incidence.diarrhoea_female' => 'nullable|integer|min:0',
                'incidence.diarrhoea_frequency' => 'nullable|integer|min:0',
                'incidence.dysentry_male' => 'nullable|integer|min:0',
                'incidence.dysentry_female' => 'nullable|integer|min:0',
                'incidence.dysentry_frequency' => 'nullable|integer|min:0',
                'incidence.jaundice_male' => 'nullable|integer|min:0',
                'incidence.jaundice_female' => 'nullable|integer|min:0',
                'incidence.jaundice_frequency' => 'nullable|integer|min:0',
                'incidence.colera_male' => 'nullable|integer|min:0',
                'incidence.colera_female' => 'nullable|integer|min:0',
                'incidence.colera_frequency' => 'nullable|integer|min:0',
                'incidence.worm_infection_male' => 'nullable|integer|min:0',
                'incidence.worm_infection_female' => 'nullable|integer|min:0',
                'incidence.worm_infection_frequency' => 'nullable|integer|min:0',
                'incidence.scabies_male' => 'nullable|integer|min:0',
                'incidence.scabies_female' => 'nullable|integer|min:0',
                'incidence.scabies_frequency' => 'nullable|integer|min:0',
                'incidence.sickness_absence_school' => 'nullable|integer|min:0',
                'incidence.max_sick_days' => 'nullable|integer|min:0',
                'incidence.bedrest_adult' => 'nullable|integer|min:0',
                'incidence.bedrest_days' => 'nullable|integer|min:0',

                // ----------------------------------------- GENDER ON INCLUSION DATA --------------------------------------------

                'inclusion.hh_male' => 'nullable|integer|min:0',
                'inclusion.hh_female' => 'nullable|integer|min:0',
                'inclusion.hh_boys' => 'nullable|integer|min:0',
                'inclusion.hh_girls' => 'nullable|integer|min:0',
                'inclusion.wf_male' => 'nullable|integer|min:0',
                'inclusion.wf_female' => 'nullable|integer|min:0',
                'inclusion.wf_boys' => 'nullable|integer|min:0',
                'inclusion.wf_girls' => 'nullable|integer|min:0',
                'inclusion.pw_male' => 'nullable|integer|min:0',
                'inclusion.pw_female' => 'nullable|integer|min:0',
                'inclusion.pw_boys' => 'nullable|integer|min:0',
                'inclusion.pw_girls' => 'nullable|integer|min:0',
                'inclusion.cw_male' => 'nullable|integer|min:0',
                'inclusion.cw_female' => 'nullable|integer|min:0',
                'inclusion.cw_boys' => 'nullable|integer|min:0',
                'inclusion.cw_girls' => 'nullable|integer|min:0',
                'inclusion.out_migrating' => 'nullable|integer|min:0',
                'inclusion.school_going_children_male' => 'nullable|integer|min:0',
                'inclusion.school_going_children_female' => 'nullable|integer|min:0',


            ],
                [
                    'cast_division.required' => 'Please Select A Cast Division!!',
                    'cast_id.required' => 'Please Select A Cast!!',

                    'house_no.required' => 'Please Input House Number!!',
                    'head_name.required' => 'Please Input Family Head Name!!',
                    'tole_name.required' => 'Please Input Tole Name!!',
                    'total_male.min' => 'Number Of Male Cannot Be negative!!',
                    'total_female.min' => 'Number Of Female Cannot Be Negative!!',
                    'total_female_widow.min' => 'Number Of Female Cannot Be Negative!!',

                    'age.between_0to5_male.min' => 'Number Of Male Cannot Be Negative!!',
                    'age.between_0to5_female.min' => 'Number Of Female Cannot Be Negative!!',


                    'age.between_6to15_male.min' => 'Number Of Male Cannot Be Negative!!',
                    'age.between_6to15_female.min' => 'Number Of Female Cannot Be Negative!!',

                    'age.between_16to65_male.min' => 'Number Of Male Cannot Be Negative!!',
                    'age.between_16to65_female.min' => 'Number Of Female Cannot Be Negative!!',

                    'age.above65_male.min' => 'Number Of Male Cannot Be Negative!!',
                    'age.above65_female.min' => 'Number Of Female Cannot Be Negative!!',

                    'age.disabled_male.min' => 'Number Of Desable Male Cannot Be Negative!!',
                    'age.disabled_female.min' => 'Number Of Desable Female Cannot Be Negative!!',

                    'litrate_no.min' => 'Number Of litrate Cannot Be Negative!!',
                    'illitrate_no.min' => 'Number Of illitrate Cannot Be Negative!!',

                    // ----------------------------------------- WATER FETCHING TIME -------------------------------------------------
                    'water_fetch.water_collection_liter.min' => 'Field Cannot Be Negative',
                    'water_fetch.going_second.min' => 'Field Cannot Be Negative',
                    'water_fetch.waiting_second.min' => 'Field Cannot Be Negative',
                    'water_fetch.filling_second.min' => 'Field Cannot Be Negative',
                    'water_fetch.return_second.min' => 'Field Cannot Be Negative',

                    // ----------------------------------------- DATA ON HYGIENE AND SANITATION --------------------------------------

                    'handwash_water_soap.min' => 'Field Cannot Be Negative',
                    'handwash_water.min' => 'Field Cannot Be Negative',
                    'handwash_ash.min' => 'Field Cannot Be Negative',
                    'handwash_others.min' => 'Field Cannot Be Negative',
                    'defecate_water_soap.min' => 'Field Cannot Be Negative',
                    'defecate_water.min' => 'Field Cannot Be Negative',
                    'defecate_ash.min' => 'Field Cannot Be Negative',
                    'handwash_infant.min' => 'Field Cannot Be Negative',

                    // ----------------------------------------- HEALTH DATA ---------------------------------------------------------

                    'incidence.diarrhoea_male.min' => 'Field Cannot Be Negative',
                    'incidence.diarrhoea_female.min' => 'Field Cannot Be Negative',
                    'incidence.diarrhoea_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.dysentry_male.min' => 'Field Cannot Be Negative',
                    'incidence.dysentry_female.min' => 'Field Cannot Be Negative',
                    'incidence.dysentry_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.jaundice_male.min' => 'Field Cannot Be Negative',
                    'incidence.jaundice_female.min' => 'Field Cannot Be Negative',
                    'incidence.jaundice_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.colera_male.min' => 'Field Cannot Be Negative',
                    'incidence.colera_female.min' => 'Field Cannot Be Negative',
                    'incidence.colera_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.worm_infection_male.min' => 'Field Cannot Be Negative',
                    'incidence.worm_infection_female.min' => 'Field Cannot Be Negative',
                    'incidence.worm_infection_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.scabies_male.min' => 'Field Cannot Be Negative',
                    'incidence.scabies_female.min' => 'Field Cannot Be Negative',
                    'incidence.scabies_frequency.min' => 'Field Cannot Be Negative',
                    'incidence.sickness_absence_school.min' => 'Field Cannot Be Negative',
                    'incidence.max_sick_days.min' => 'Field Cannot Be Negative',
                    'incidence.bedrest_adult.min' => 'Field Cannot Be Negative',
                    'incidence.bedrest_days.min' => 'Field Cannot Be Negative',

                    // ----------------------------------------- GENDER ON INCLUSION DATA --------------------------------------------

                    'inclusion.hh_male.min' => 'Field Cannot Be Negative',
                    'inclusion.hh_female.min' => 'Field Cannot Be Negative',
                    'inclusion.hh_boys.min' => 'Field Cannot Be Negative',
                    'inclusion.hh_girls.min' => 'Field Cannot Be Negative',
                    'inclusion.wf_male.min' => 'Field Cannot Be Negative',
                    'inclusion.wf_female.min' => 'Field Cannot Be Negative',
                    'inclusion.wf_boys.min' => 'Field Cannot Be Negative',
                    'inclusion.wf_girls.min' => 'Field Cannot Be Negative',
                    'inclusion.pw_male.min' => 'Field Cannot Be Negative',
                    'inclusion.pw_female.min' => 'Field Cannot Be Negative',
                    'inclusion.pw_boys.min' => 'Field Cannot Be Negative',
                    'inclusion.pw_girls.min' => 'Field Cannot Be Negative',
                    'inclusion.cw_male.min' => 'Field Cannot Be Negative',
                    'inclusion.cw_female.min' => 'Field Cannot Be Negative',
                    'inclusion.cw_boys.min' => 'Field Cannot Be Negative',
                    'inclusion.cw_girls.min' => 'Field Cannot Be Negative',
                    'inclusion.out_migrating.min' => 'Field Cannot Be Negative',
                    'inclusion.school_going_children_male.min' => 'Field Cannot Be Negative',
                    'inclusion.school_going_children_female.min' => 'Field Cannot Be Negative',


                ]);
        }

    public function waterFetching(Request $request)
    {
        if(tokenCheck($request->header('token'))) {

            $data = $request->except('_token');
            $waterFetching = $data['water_fetch'];
            $id = $waterFetching['house_id'];
            if ($id) {
                DB::table('hh_water_fetching_time')->where('house_id', $id)->delete();
            }
            $waterFetchings = $waterFetching;

            $total_second = $waterFetchings['going_second'] + $waterFetchings['waiting_second'] + $waterFetchings['filling_second'] + $waterFetchings['return_second'];
            $total_second_8round = $total_second * 8;
            $total_min_8round = round($total_second_8round / 60);
            $total_hour_8round = round($total_min_8round / 60);

            DB::table('hh_water_fetching_time')->insert([
                'water_collection_liter' => $waterFetchings['water_collection_liter'],
                'going_second' => $waterFetchings['going_second'],
                'waiting_second' => $waterFetchings['waiting_second'],
                'filling_second' => $waterFetchings['filling_second'],
                'return_second' => $waterFetchings['return_second'],
                'total_second' => $total_second,
                'total_second_8round' => $total_second_8round,
                'total_minute_8round' => $total_min_8round,
                'total_hour_8round' => $total_hour_8round,
                'house_id' => $id
            ]);

            return response()->json(array('household_id'=>$id,'success'=>true), 200);
        }
         else{
        return response("Please Login To Access Data");
    }
    }

//    public function storeAgeGroup(Request $request)
//    {
//        if(tokenCheck($request->header('token'))) {
//            $data = $request->except('_token');
//            $age = $data['age'];
//            $id = $request->household_id;
//            if ($id) {
//                DB::table('hh_age_groups')->where('house_id', $id)->delete();
//            }
//            $ageGroup = $age;
//            $zeroto5_total = $ageGroup['between_0to5_male'] + $ageGroup['between_0to5_female'];
//            $sixto15_total = $ageGroup['between_6to15_male'] + $ageGroup['between_6to15_female'];
//            $sixteento65_total = $ageGroup['between_16to65_male'] + $ageGroup['between_16to65_female'];
//            $sixtyfive_above_total = $ageGroup['above65_male'] + $ageGroup['above65_female'];
//            $disabled_total = $ageGroup['disabled_male'] + $ageGroup['disabled_female'];
//            DB::table('hh_age_groups')
//                ->insert([
//                    'between_0to5_male' => $ageGroup['between_0to5_male'],
//                    'between_0to5_female' => $ageGroup['between_0to5_female'],
//                    'between_0to5_total' => $zeroto5_total,
//                    'between_6to15_male' => $ageGroup['between_6to15_male'],
//                    'between_6to15_female' => $ageGroup['between_6to15_female'],
//                    'between_6to15_total' => $sixto15_total,
//                    'between_16to65_male' => $ageGroup['between_16to65_male'],
//                    'between_16to65_female' => $ageGroup['between_16to65_female'],
//                    'between_16to65_total' => $sixteento65_total,
//                    'above65_male' => $ageGroup['above65_male'],
//                    'above65_female' => $ageGroup['above65_female'],
//                    'above65_total' => $sixtyfive_above_total,
//                    'disabled_male' => $ageGroup['disabled_male'],
//                    'disabled_female' => $ageGroup['disabled_female'],
//                    'disabled_total' => $disabled_total,
//                    'house_id' => $id
//                ]);
//        }
//    else{
//        return response("Please Login To Access Data");
//    }
//    }
    public function storeAgeGroup($age,$id)
    {
        if($id){
            DB::table('hh_age_groups')->where('house_id',$id)->delete();
        }
        $ageGroup = $age;
        $zeroto5_total = $ageGroup['between_0to5_male'] + $ageGroup['between_0to5_female'];
        $sixto15_total = $ageGroup['between_6to15_male'] + $ageGroup['between_6to15_female'];
        $sixteento65_total = $ageGroup['between_16to65_male'] + $ageGroup['between_16to65_female'];
        $sixtyfive_above_total = $ageGroup['above65_male'] + $ageGroup['above65_female'];
        $disabled_total = $ageGroup['disabled_male'] + $ageGroup['disabled_female'];
        DB::table('hh_age_groups')
            ->insert([
                'between_0to5_male'=>$ageGroup['between_0to5_male'],
                'between_0to5_female'=>$ageGroup['between_0to5_female'],
                'between_0to5_total'=>$zeroto5_total,
                'between_6to15_male'=>$ageGroup['between_6to15_male'],
                'between_6to15_female'=>$ageGroup['between_6to15_female'],
                'between_6to15_total'=>$sixto15_total,
                'between_16to65_male'=>$ageGroup['between_16to65_male'],
                'between_16to65_female'=>$ageGroup['between_16to65_female'],
                'between_16to65_total'=>$sixteento65_total,
                'above65_male'=>$ageGroup['above65_male'],
                'above65_female'=>$ageGroup['above65_female'],
                'above65_total'=>$sixtyfive_above_total,
                'disabled_male'=>$ageGroup['disabled_male'],
                'disabled_female'=>$ageGroup['disabled_female'],
                'disabled_total'=>$disabled_total,
                'house_id' => $id
            ]);
    }
//    public function storeIncome(Request $request)
//    {
//        if(tokenCheck($request->header('token'))) {
//            $id = $request->household_id;
//            $data = $request->except('_token');
//            $income = $data['income'];
//            $house_income = checkValue($request->income_pension);
//
//            if ($id) {
//                DB::table('hh_income_pensions')->where('house_id', $id)->delete();
//            }
//            $incomes = $income;
//            // dd($house_income);
//            if (empty($house_income)) {
//                DB::table('hh_income_pensions')
//                    ->insert([
//                        'income_pension' => null,
//                        'house_id' => $id
//                    ]);
//            } elseif (empty($incomes)) {
//                DB::table('hh_income_pensions')
//                    ->insert([
//                        'income_pension' => null,
//                        'house_id' => $id
//                    ]);
//            } else {
//                foreach ($incomes as $key => $value) {
//                    DB::table('hh_income_pensions')
//                        ->insert([
//                            'income_pension' => $value,
//                            'house_id' => $id
//                        ]);
//                }
//            }
//        }
//        else{
//            return response("Please Login To Access Data");
//        }
//    }
    public function storeIncome($income,$id,$house_income)
    {
        if($id){
            DB::table('hh_income_pensions')->where('house_id',$id)->delete();
        }
        $incomes = $income;
        // dd($house_income);
        if(empty($house_income)){
            DB::table('hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id
                ]);
        }
        elseif(empty($incomes)){
            DB::table('hh_income_pensions')
                ->insert([
                    'income_pension'=> null,
                    'house_id' => $id
                ]);
        }
        else{
            foreach ($incomes as $key => $value) {
                DB::table('hh_income_pensions')
                    ->insert([
                        'income_pension'=>$value,
                        'house_id' => $id
                    ]);
            }
        }
    }
//    public function storeOccupation(Request $request)
//    {
//        if(tokenCheck($request->header('token'))) {
//            $id = $request->household_id;
//            $data = $request->except('_token');
//            $occupation = $data['occupation_id'];
//            if ($id) {
//                DB::table('hh_occupations')->where('house_id', $id)->delete();
//            }
//            $occupations = $occupation;
//            if ($occupations == null) {
//                DB::table('hh_occupations')
//                    ->insert([
//                        'occupation_id' => null,
//                        'house_id' => $id
//                    ]);
//            } else {
//                foreach ($occupations as $key => $value) {
//                    DB::table('hh_occupations')
//                        ->insert([
//                            'occupation_id' => $value,
//                            'house_id' => $id
//                        ]);
//                }
//            }
//        } else{
//            return response("Please Login To Access Data");
//        }
//    }

    public function storeOccupation($occupation,$id)
    {
        if($id){
            DB::table('hh_occupations')->where('house_id',$id)->delete();
        }
        $occupations = $occupation;
        if($occupations == null){
            DB::table('hh_occupations')
                ->insert([
                    'occupation_id'=> null,
                    'house_id' => $id
                ]);
        }
        else{
            foreach ($occupations as $key => $value) {
                DB::table('hh_occupations')
                    ->insert([
                        'occupation_id'=>$value,
                        'house_id' => $id
                    ]);
            }
        }
    }
    public function storeHygiene(Request $request)
    {
        if(tokenCheck($request->header('token'))) {
            $id = $request->house_id;
            $data = $request->except('_token');
            $hygienes = array(
                'latrine_available' => $data['latrine_available'],
                'total_latrine' => $data['total_latrine'],
                'latrine_cleansing_agent' => $data['latrine_cleansing_agent'],
                'latrine_water_available' => $data['latrine_water_available'],
                'latrine_cleaning_brush' => $data['latrine_cleaning_brush'],
                'defecate_latrine' => $data['defecate_latrine'],
                'defecate_open' => $data['defecate_open'],
                'home_cleaning' => $data['home_cleaning'],
                'domestic_animal' => $data['domestic_animal'],
                'drinking_water' => $data['drinking_water'],
                'food_covered' => $data['food_covered'],
                'utensils_clean' => $data['utensils_clean'],
                'garbage_disposel_pit' => $data['garbage_disposel_pit'],
                'handwash_water_soap' => $data['handwash_water_soap'],
                'handwash_water' => $data['handwash_water'],
                'handwash_ash' => $data['handwash_ash'],
                'handwash_others' => $data['handwash_others'],
                'defecate_water_soap' => $data['defecate_water_soap'],
                'defecate_water' => $data['defecate_water'],
                'defecate_ash' => $data['defecate_ash'],
                'handwash_infant' => $data['handwash_infant'],
                'bath_frequency' => $data['bath_frequency'],
                'cloth_wash_frequency' => $data['cloth_wash_frequency'],
                'menstrual_knowledge' => $data['menstrual_knowledge'],
            );
            if ($id) {
                DB::table('hh_hygiene')->where('house_id', $id)->delete();
            }
            $hygiene = $hygienes;
            if ($hygiene['latrine_available'] == 1) {
                $hygiene['latrine_available'] = 1;
                $hygiene['total_latrine'];

                $hygiene['latrine_cleansing_agent'];
                $hygiene['latrine_water_available'];
                $hygiene['latrine_cleaning_brush'];
            } elseif ($hygiene['latrine_available'] = 0) {
                $hygiene['latrine_available'] = 0;
                $hygiene['total_latrine'] = null;
                $hygiene['latrine_cleansing_agent'] = null;
                $hygiene['latrine_water_available'] = null;
                $hygiene['latrine_cleaning_brush'] = null;
            }

            DB::table('hh_hygiene')->insert([
                'latrine_available' => $hygiene['latrine_available'],
                'total_latrine' => $hygiene['total_latrine'],
                'latrine_cleansing_agent' => $hygiene['latrine_cleansing_agent'],
                'latrine_water_available' => $hygiene['latrine_water_available'],
                'latrine_cleaning_brush' => $hygiene['latrine_cleaning_brush'],
                'defecate_latrine' => $hygiene['defecate_latrine'],
                'defecate_open' => $hygiene['defecate_open'],
                'home_cleaning' => $hygiene['home_cleaning'],
                'domestic_animal' => $hygiene['domestic_animal'],
                'drinking_water' => $hygiene['drinking_water'],
                'food_covered' => $hygiene['food_covered'],
                'utensils_clean' => $hygiene['utensils_clean'],
                'garbage_disposel_pit' => $hygiene['garbage_disposel_pit'],
                'handwash_water_soap' => $hygiene['handwash_water_soap'],
                'handwash_water' => $hygiene['handwash_water'],
                'handwash_ash' => $hygiene['handwash_ash'],
                'handwash_others' => $hygiene['handwash_others'],
                'defecate_water_soap' => $hygiene['defecate_water_soap'],
                'defecate_water' => $hygiene['defecate_water'],
                'defecate_ash' => $hygiene['defecate_ash'],
                'handwash_infant' => $hygiene['handwash_infant'],
                'bath_frequency' => $hygiene['bath_frequency'],
                'cloth_wash_frequency' => $hygiene['cloth_wash_frequency'],
                'menstrual_knowledge' => $hygiene['menstrual_knowledge'],
                'house_id' => $id
            ]);

            return response()->json(array('household_id'=>$id,'success'=>true), 200);
        }
    else{
        return response("Please Login To Access Data");
    }
    }

    public function storeIncidence(Request $request)
    {
        if(tokenCheck($request->header('token'))) {
            $healthData = $request->incidence;
            $healthData['house_id'] = $request->house_id;
            HealthData::Create($healthData);
                return response()->json(array('household_id' => $request->house_id, 'success' => true), 200);
            } else {
                return response("Please Login To Access Data");
            }
    }


    public function storeHealthBehavior(Request $request)
    {

        if(tokenCheck($request->header('token'))) {
            $id = $request->house_id;
            $data = $request->except('_token');
            $healthbehaviors = $data['behavior'];
            if($id){
                DB::table('hh_health_behaviors')->where('house_id',$id)->delete();
            }
            $behavior = $healthbehaviors;

            DB::table('hh_health_behaviors')->insert([
                'healthpost_preference' => $behavior['healthpost_preference'],
                'traditional_preference' => $behavior['traditional_preference'],
                'home_medecine_preference' => $behavior['home_medecine_preference'],
                'annual_expenses' => $behavior['annual_expenses'],
                'latrine_knowledge' => $behavior['latrine_knowledge'],
                'disease_knowledge' => $behavior['disease_knowledge'],
                'safewater_knowledge' => $behavior['safewater_knowledge'],
                'handwashing_knowledge' => $behavior['handwashing_knowledge'],
                'house_id' => $id
            ]);
            return response()->json(array('household_id'=>$id,'success'=>true), 200);
        }

        else{
            return response("Please Login To Access Data");
        }
    }

    public function storeInclusionData(Request $request)
    {
        if(tokenCheck($request->header('token'))) {
            $id = $request->household_id;
            $data = $request->except('token');
            $inclusionData = $data['inclusion'];
            if($id){
                DB::table('hh_inclusion_datas')->where('house_id',$id)->delete();
            }
            $inclusion = $inclusionData;
            if($inclusion['effect_on_women'] != "incident_women" ){
                $inclusion['other_incident_women'] = null;
            }

            if($inclusion['effect_on_children'] != "others" ){
                $inclusion['other_effect_on_children'] = null;
            }

            DB::table('hh_inclusion_datas')->insert([
                'hh_male' =>$inclusion['hh_male'],
                'hh_female' =>$inclusion['hh_female'],
                'hh_boys' =>$inclusion['hh_boys'],
                'hh_girls' =>$inclusion['hh_girls'],
                'wf_male' =>$inclusion['wf_male'],
                'wf_female' =>$inclusion['wf_female'],
                'wf_boys' =>$inclusion['wf_boys'],
                'wf_girls' =>$inclusion['wf_girls'],
                'pw_male' =>$inclusion['pw_male'],
                'pw_female' =>$inclusion['pw_female'],
                'pw_boys' =>$inclusion['pw_boys'],
                'pw_girls' =>$inclusion['pw_girls'],
                'cw_male' =>$inclusion['cw_male'],
                'cw_female' =>$inclusion['cw_female'],
                'cw_boys' =>$inclusion['cw_boys'],
                'cw_girls' =>$inclusion['cw_girls'],
                'effect_on_women' =>$inclusion['effect_on_women'],
                'other_incident_women' =>$inclusion['other_incident_women'],
                'effect_on_children' =>$inclusion['effect_on_children'],
                'other_effect_on_children' =>$inclusion['other_effect_on_children'],
                'kitchen_gardening_practice' =>$inclusion['kitchen_gardening_practice'],
                'school_going_children_male' =>$inclusion['school_going_children_male'],
                'school_going_children_female' =>$inclusion['school_going_children_female'],
                'out_migrating' =>$inclusion['out_migrating'],
                'house_id' =>$id,
            ]);
            return response()->json(array('household_id'=>$id,'success'=>true), 200);
        }

        else{
            return response("Please Login To Access Data");
        }
    }

    public function incomeSource(Request $request){
        if(tokenCheck($request->header('token'))){
            $data = DB::table('mst_income_sources')->where('del_flag',0)->get();
        return response()->json($data,200);

        }else{
            return response("Please Login To Access Data");
        }
    }

}