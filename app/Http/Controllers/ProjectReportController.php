<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Project\Model\Project;
use DB;
use Excel;
use App\Exports\HouseHoldExport;
use App\Modules\Cast_divison\Model\Cast_divison;
use App\Modules\Cast\Model\Cast;
use App\Modules\Household_detail\Model\Household_detail;

class ProjectReportController extends Controller
{
    public function exportAgeGroup($id)
    {
    	// exit;
    	// dd($id);
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach($data as &$value)
    	{
			$agegrp0to5female = 0;
			$agegrp0to5male = 0;
			$agegrp6to15female = 0;
			$agegrp6to15male = 0;
			$agegrp16to65female = 0;
			$agegrp16to65male = 0;
			$agegrpabove65male = 0;
			$agegrpabove65female = 0;

            $value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
            $value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		$houseHold = DB::table('tbl_household_details')
    						->leftjoin('hh_age_groups','hh_age_groups.house_id','=','tbl_household_details.id')
    						->where('tbl_household_details.project_id',$value->id)->get();
			$value->hh = count($houseHold);
            $newarray = [];
			foreach ($houseHold as $k => $v) {
	    		$agegrp0to5female += $v->between_0to5_female;
	    		$agegrp0to5male += $v->between_0to5_male;
	    		$agegrp6to15female += $v->between_6to15_female;
	    		$agegrp6to15male += $v->between_6to15_male;
	    		$agegrp16to65female += $v->between_16to65_female;
	    		$agegrp16to65male += $v->between_16to65_male;
	    		$agegrpabove65male += $v->above65_male;
	    		$agegrpabove65female += $v->above65_female;
	    	}  

	    	$value->sub_male =  $agegrp0to5male   + $agegrp6to15male   + $agegrp16to65male + $agegrpabove65male ;
	    	$value->sub_female = $agegrp0to5female  + $agegrp6to15female  + $agegrp16to65female   + $agegrpabove65female;
	    	$value->pop = $agegrp0to5female + $agegrp0to5male + $agegrp6to15female + $agegrp6to15male + $agegrp16to65female + $agegrp16to65male + $agegrpabove65male + $agegrpabove65female;

	    	$value->agegrp0to5female = $agegrp0to5female;
			$value->agegrp0to5male = $agegrp0to5male;
			$value->agegrp6to15female = $agegrp6to15female;
			$value->agegrp6to15male = $agegrp6to15male;
			$value->agegrp16to65female = $agegrp16to65female;
			$value->agegrp16to65male = $agegrp16to65male;
			$value->agegrpabove65male = $agegrpabove65male;
			$value->agegrpabove65female = $agegrpabove65female;

		}
    	// dd($data);

		// return view("admin.export.agegroup",compact('data'));
		libxml_use_internal_errors(true);
    	return Excel::download(new HouseHoldExport("admin.export.agegroup",$data), 'AgeGroup Report.xlsx');
    }

    public function exportHygiene($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}

    	foreach ($data as &$value) {
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
            $value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		// $houseHold = DB::table('tbl_household_details')
						// ->where('project_id',$value->id)
						// ->where('del_flag',0)
						// ->get();
			$houseHold = DB::table('tbl_household_details')
						->leftjoin('hh_hygiene','hh_hygiene.house_id','=','tbl_household_details.id')
						->where('tbl_household_details.project_id',$value->id)
						->where('del_flag',0)
						->get();

			$home_cleaning = $houseHold->where('home_cleaning',1);
			$bad_home_cleaning = $houseHold->where('home_cleaning',0);
			$animal_managed = $houseHold->where('domestic_animal',1);
			$animal_not_managed = $houseHold->where('domestic_animal',0);
			$drinking_water_managed = $houseHold->where('drinking_water',1);
			$drinking_water_not_managed = $houseHold->where('drinking_water',0);
			$food_covered = $houseHold->where('food_covered',1);
			$food_not_covered = $houseHold->where('food_covered',0);
			$utensils_cleaned = $houseHold->where('utensils_clean',1);
			$utensils_not_cleaned = $houseHold->where('utensils_clean',0);
			$garbage_managed = $houseHold->where('garbage_disposel_pit',1);
			$garbage_not_managed = $houseHold->where('garbage_disposel_pit',0);
			$cleansing_agent = $houseHold->where('latrine_cleansing_agent',1);
			$latrine_water_available = $houseHold->where('latrine_water_available',1);
			$latrine_cleaning_brush = $houseHold->where('latrine_cleaning_brush',1);


			$value->hh = count($houseHold);
			$value->hh_clean_house = count($home_cleaning);
			$value->hh_not_clean_house = count($bad_home_cleaning);
			$value->hh_animal_managed = count($animal_managed);
			$value->hh_animal_not_managed = count($animal_not_managed);
			$value->drinking_water_managed = count($drinking_water_managed);
			$value->drinking_water_not_managed = count($drinking_water_not_managed);
			$value->food_covered = count($food_covered);
			$value->food_not_covered = count($food_not_covered);
			$value->utensils_cleaned = count($utensils_cleaned);
			$value->utensils_not_cleaned = count($utensils_not_cleaned);
			$value->garbage_managed = count($garbage_managed);
			$value->garbage_not_managed = count($garbage_not_managed);
			$value->cleansing_agent_available = count($cleansing_agent);
			$value->latrine_water_available = count($latrine_water_available);
			$value->latrine_cleaning_brush = count($latrine_cleaning_brush);
    	}
		// dd($data);
		// return view("admin.export.hygiene",compact('data'));
		libxml_use_internal_errors(true);

    	return Excel::download(new HouseHoldExport("admin.export.hygiene",$data), 'WRO_Baseline_household_hygiene Report.xlsx');
    }
    public function exportImprovedHygiene($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach ($data as &$value) {
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
            $value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		
			$houseHold = DB::table('tbl_household_details')
						->leftjoin('hh_hygiene','hh_hygiene.house_id','=','tbl_household_details.id')
						->where('tbl_household_details.project_id',$value->id)
						->where('del_flag',0)
						->get();

			$cleansing_agent = $houseHold->where('latrine_cleansing_agent',1);
			$latrine_water_available = $houseHold->where('latrine_water_available',1);
			$latrine_cleaning_brush = $houseHold->where('latrine_cleaning_brush',1);

			$value->hh = count($houseHold);
			$value->cleansing_agent_available = count($cleansing_agent);
			$value->latrine_water_available = count($latrine_water_available);
			$value->latrine_cleaning_brush = count($latrine_cleaning_brush);
		}

		// dd($data);
		return view("admin.export.improved_hygiene",compact('data'));
		libxml_use_internal_errors(true);

    	return Excel::download(new HouseHoldExport("admin.export.improved_hygiene",$data), 'WRO_Baseline_improved_household_hygiene Report.xlsx');
    }

    public function exportHandwash($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}

		foreach ($data as &$value)
		{
	    	$hh_handwash_water_soap = 0;
	    	$hh_handwash_water = 0;
	    	$hh_handwash_ash = 0;
	    	$hh_handwash_others = 0;
	    	$hh_defecate_water_soap = 0;
	    	$hh_defecate_water = 0;
	    	$hh_defecate_ash = 0;
	    	$hh_handwash_infant = 0;
			$hh_population = 0;
			$hh_handwash = 0;
			$hh_defecate=0;
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$houseHold = DB::table('tbl_household_details')
						->leftjoin('hh_hygiene','hh_hygiene.house_id','=','tbl_household_details.id')
						->leftjoin('hh_age_groups','hh_age_groups.house_id','=','tbl_household_details.id')
						->where('tbl_household_details.project_id',$value->id)
						->where('del_flag',0)
						->get()->toArray();
		if(!empty($houseHold)){
			foreach ($houseHold as $k => $v) {
				$hh_handwash_water_soap += $v->handwash_water_soap;
				$hh_handwash_water += $v->handwash_water;
				$hh_handwash_ash += $v->handwash_ash;
				$hh_handwash_others += $v->handwash_others;
				$hh_defecate_water_soap += $v->defecate_water_soap;
				$hh_defecate_water += $v->defecate_water;
				$hh_defecate_ash += $v->defecate_ash;
				$hh_handwash_infant += $v->handwash_infant;
				$hh_population += $v->total_member;
				$hh_handwash =$hh_handwash_water_soap + $hh_handwash_water + $hh_handwash_ash + $hh_handwash_others ;
				$hh_defecate= $hh_defecate_water_soap +$hh_defecate_water+$hh_defecate_ash+$hh_handwash_infant ;
			}	
		
		}
						// dd('asdasd');
			
				
			
			

			  
			$value->hh = count($houseHold);
	    	$value->hh_handwash_water_soap = $hh_handwash_water_soap;
	    	$value->hh_handwash_water = $hh_handwash_water;
	    	$value->hh_handwash_ash = $hh_handwash_ash;
	    	$value->hh_handwash_others = $hh_handwash_others;
	    	$value->hh_defecate_water_soap = $hh_defecate_water_soap;
	    	$value->hh_defecate_water = $hh_defecate_water;
	    	$value->hh_defecate_ash = $hh_defecate_ash;
	    	$value->hh_handwash_infant = $hh_handwash_infant;
			$value->hh_population = $hh_population;
			$value->hh_handwash=$hh_handwash;
			$value->hh_defecate=$hh_defecate;	
			if(!empty($houseHold)){
				$value->hygiene=DB::Select("select COUNT(if(bath_frequency='daily',1,NULL)) as daily_bath,
							    COUNT(if(bath_frequency='once',1,NULL)) as once_bath,
							    COUNT(if(bath_frequency='twice',1,NULL)) as twice_bath,
							    COUNT(if(cloth_wash_frequency='once',1,NULL)) as once_cloth,
								COUNT(if(cloth_wash_frequency='more',1,NULL)) as more_cloth,
								COUNT(if(cloth_wash_frequency='monthly',1,NULL)) as monthly_cloth 
								from hh_hygiene inner join tbl_household_details
						      	on hh_hygiene.house_id=tbl_household_details.id
							    where tbl_household_details.project_id=$value->id 
								group by tbl_household_details.project_id");
			}	
			else{
				$test = [['daily_bath' => 0,'once_bath' => 0,'twice_bath' => 0,'once_cloth' => 0,'more_cloth' => 0,'monthly_cloth' => 0]];
				$value->hygiene = json_decode(json_encode($test));
			}


			
			
// $value->test1 = json_decode($test);
			// else{
				// $value->hygiene = [['0'=>'asd','1'=>'asd']];
			// }
			
// dd($value);

    	}
		// dd($data);
		
		return view("admin.export.sanitary_handwash",compact('data'));
		libxml_use_internal_errors(true);

    	// return Excel::download(new HouseHoldExport("admin.export.sanitary_handwash",$data), 'WRO_Baseline_Sanitar_Handwash Report.xlsx');
    }

    public function SocialEconomicExport($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach ($data as $value) {
	        $hh_population = 0;
	        $hh_school_male = 0;
	        $hh_school_female = 0;
	        $hh_fetching_male = 0;
	        $hh_fetching_female = 0;
	        $hh_fetching_boys = 0;
	        $hh_fetching_girls = 0;
	        $hh_kitchen_gardening_yes = 0;
	        $hh_kitchen_gardening_no = 0;
	        $hh_homework = 0;
	        $hh_absent_school = 0;
	        $hh_female_head = 0;
	        $hh_female_widow = 0;
	        $hh_disabled = 0;
	        $hh_litrate = 0;
	        $hh_illitrate = 0;


    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		$houseHold = DB::table('tbl_household_details')  
    					->leftjoin('hh_inclusion_datas','hh_inclusion_datas.house_id','=','tbl_household_details.id') 					
						->leftjoin('hh_age_groups','hh_age_groups.house_id','=','tbl_household_details.id')    					
						->where('tbl_household_details.project_id',$value->id)
						->where('del_flag',0)
						->get();
			$income_arr = [];
			$income_ssource = DB::table('tbl_household_details')
						->select(DB::raw('count( hh_income_pensions.income_pension) as count'),'tbl_household_details.project_id')
						->leftjoin('hh_income_pensions','hh_income_pensions.house_id','=','tbl_household_details.id')    					
						->where('tbl_household_details.project_id',$value->id)
						->where('del_flag',0)
						->groupBy('hh_income_pensions.income_pension','project_id')
						->get();

            foreach($income_ssource as $ks => $vs){
            	$income_arr[] = $vs;
            }
            // dd($income_arr);
			// dd($houseHold);     
			foreach ($houseHold as $k => $v) {
				$hh_population += $v->total_member;
				$hh_litrate += $v->litrate_no;
				$hh_illitrate += $v->illitrate_no;
				$hh_school_male += $v->school_going_children_male;
				$hh_school_female += $v->school_going_children_female;
				$hh_fetching_male += $v->hh_male;
				$hh_fetching_female += $v->hh_female;
				$hh_fetching_boys += $v->hh_boys;
				$hh_fetching_girls += $v->hh_girls;
				if($v->kitchen_gardening_practice == "Yes"){
					$hh_kitchen_gardening_yes += 1;
				}elseif($v->kitchen_gardening_practice == "No"){
					$hh_kitchen_gardening_no += 1;
				}
				if($v->effect_on_children == "homework"){
					$hh_homework += 1;
				}
				elseif($v->effect_on_children == "absent_from_school"){
					$hh_absent_school += 1;
				}
				if($v->head_gender == "female"){
					$hh_female_head += 1;
				}
				$hh_female_widow += $v->total_female_widow;
				$hh_disabled += $v->disabled_total;
			}

    		$income_sources = DB::table('hh_income_pensions')->where('house_id',$value->id)->get();
            
            $value->hh = count($houseHold);
            $value->hh_population = $hh_population;
            $value->hh_litrate = $hh_litrate;
            $value->hh_illitrate = $hh_illitrate;
            $value->income_ssource = $income_arr;
            $value->hh_school_male = $hh_school_male;
            $value->hh_school_female = $hh_school_female;
            $value->hh_fetching_male = $hh_fetching_male;
            $value->hh_fetching_female = $hh_fetching_female;
            $value->hh_fetching_boys = $hh_fetching_boys;
            $value->hh_fetching_girls = $hh_fetching_girls;
            $value->hh_kitchen_gardening_yes = $hh_kitchen_gardening_yes;
            $value->hh_kitchen_gardening_no = $hh_kitchen_gardening_no;
            $value->hh_homework = $hh_homework;
            $value->hh_absent_school = $hh_absent_school;
            $value->hh_female_head = $hh_female_head;
            $value->hh_female_widow = $hh_female_widow;
            $value->hh_disabled = $hh_disabled;
			// unset($houseHold);
    	}
			// dd($data);
			// return $data;

    	$data->income_sources = DB::table('mst_income_sources')->where('del_flag',0)->get();
		return view("admin.export.socialeconomic_status",compact('data'));
		libxml_use_internal_errors(true);

    	// return Excel::download(new HouseHoldExport("admin.export.socialeconomic_status",$data), 'WRO_Baseline_SocialEconomic_status Report.xlsx');
    }

    public function exportWaterBorne($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}

    	foreach($data as &$value)
    	{
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();

    		//diarrhoea
    		$diarrhoea_between0and5_total = 0 ;
    		$diarrhoea_between0and5_frequency = 0 ;
			$diarrhoea_above6_total = 0 ;
    		$diarrhoea_above6_frequency = 0 ;
    		//Dysentry
    		$dysentry_between0and5_total = 0 ;
    		$dysentry_between0and5_frequency = 0 ;
			$dysentry_above6_total = 0 ;
    		$dysentry_above6_frequency = 0 ;
    		//jaundice
    		$jaundice_between0and5_total = 0 ;
    		$jaundice_between0and5_frequency = 0 ;
			$jaundice_above6_total = 0 ;
    		$jaundice_above6_frequency = 0 ;
    		//colera
    		$colera_between0and5_total = 0 ;
    		$colera_between0and5_frequency = 0 ;
			$colera_above6_total = 0 ;
    		$colera_above6_frequency = 0 ;
    		//worms
    		$worms_between0and5_total = 0 ;
    		$worms_between0and5_frequency = 0 ;
			$worms_above6_total = 0 ;
    		$worms_above6_frequency = 0 ;
    		//scabies
    		$scabies_between0and5_total = 0 ;
    		$scabies_between0and5_frequency = 0 ;
			$scabies_above6_total = 0 ;
    		$scabies_above6_frequency = 0 ;

    		//health seeking behaviour
    		$healthpost_preference = 0;
    		$traditional_preference = 0;
    		$home_medecine_preference = 0; 

    		$houseHold = DB::table('tbl_household_details')
    					->leftjoin('hh_health_datas','hh_health_datas.house_id','=','tbl_household_details.id')
    					->leftjoin('hh_health_behaviors','hh_health_behaviors.house_id','=','tbl_household_details.id')
    					->where('del_flag',0)
    					->where('tbl_household_details.project_id',$value->id)
    					->get();


			$hhdatas = DB::table('tbl_household_details')
    					->leftjoin('hh_health_datas','hh_health_datas.house_id','=','tbl_household_details.id')
    					->select(DB::raw('count(hh_health_datas.death_cause) as count,tbl_household_details.project_id,hh_health_datas.death_cause'))
    					->where('del_flag',0)
    					->where('tbl_household_details.project_id',$value->id)
    					->groupBy('project_id','hh_health_datas.death_cause')
    					->get();
    		$max = '';
    		// dd($hhdatas);
			foreach ($hhdatas as $k => $v) {
				// var_dump(($v->count > @$hhdatas[$k]->count));
				// if($v->count > @$hhdatas[$k]->count){
				// 	$max = $v->death_cause;
				// }
				if($k == 0){
					$max = $v->death_cause;
				}else{
					if($v->count > $hhdatas[$k]->count){
						$max = $v->death_cause;
					}
				}
			}
			$value->max = $max;



    		foreach ($houseHold as $k => $v) {
    			//diarrhoea
    			$diarrhoea_between0and5_total += $v->diarrhoea_0to5_total;
				$diarrhoea_between0and5_frequency += $v->diarrhoea_0to5_frequency;
				$diarrhoea_above6_total += $v->diarrhoea_above6_total;
    			$diarrhoea_above6_frequency += $v->diarrhoea_above6_frequency;
    			//dysentry
    			$dysentry_between0and5_total += $v->dysentry_0to5_total;
				$dysentry_between0and5_frequency += $v->dysentry_0to5_frequency;
				$dysentry_above6_total += $v->dysentry_above6_total;
    			$dysentry_above6_frequency += $v->dysentry_above6_frequency;
    			//jaundice
    			$jaundice_between0and5_total += $v->jaundice_0to5_total;
				$jaundice_between0and5_frequency += $v->jaundice_0to5_frequency;
				$jaundice_above6_total += $v->jaundice_above6_total;
    			$jaundice_above6_frequency += $v->jaundice_above6_frequency;
    			//colera
    			$colera_between0and5_total += $v->colera_0to5_total;
    			$colera_between0and5_frequency += $v->colera_0to5_frequency;
				$colera_above6_total += $v->colera_above6_total;
    			$colera_above6_frequency += $v->colera_above6_frequency;
    			//worms
    			$worms_between0and5_total += $v->worm_infection_0to5_total;
				$worms_between0and5_frequency += $v->worm_infection_0to5_frequency;
				$worms_above6_total += $v->worm_infection_above6_total;
    			$worms_above6_frequency += $v->worm_infection_above6_frequency;
    			//scabies
    			$scabies_between0and5_total += $v->scabies_0to5_total;
				$scabies_between0and5_frequency += $v->scabies_0to5_frequency;
				$scabies_above6_total += $v->scabies_above6_total;
    			$scabies_above6_frequency += $v->scabies_above6_frequency;

	    		$max_absence = $houseHold->max('sickness_absence_school');
	    		$max_sick = $houseHold->max('max_sick_days');
	    		$max_annual_expense = $houseHold->max('annual_expenses');

	    		//preference
	    		if($v->healthpost_preference == "Yes" ){
	    			$healthpost_preference += 1;
	    		}
	    		if($v->traditional_preference == "Yes" ){
					$traditional_preference += 1;
	    		}
	    		if($v->home_medecine_preference == "Yes" ){
					$home_medecine_preference += 1;
	    		}
	    		
    		}
			$value->max_absence = $max_absence;
			$value->max_sick = $max_sick;
			$value->max_annual_expense = $max_annual_expense;
			//diarrhoea
			$value->diarrhoea_between0and5_total = $diarrhoea_between0and5_total;
			$value->diarrhoea_between0and5_frequency = $diarrhoea_between0and5_frequency;
			$value->diarrhoea_above6_total = $diarrhoea_above6_total;
			$value->diarrhoea_above6_frequency = $diarrhoea_above6_frequency;
			//dysentry
			$value->dysentry_between0and5_total = $dysentry_between0and5_total;
			$value->dysentry_between0and5_frequency = $dysentry_between0and5_frequency;
			$value->dysentry_above6_total = $dysentry_above6_total;
			$value->dysentry_above6_frequency = $dysentry_above6_frequency;
			//jaundice
			$value->jaundice_between0and5_total = $jaundice_between0and5_total;
			$value->jaundice_between0and5_frequency = $jaundice_between0and5_frequency;
			$value->jaundice_above6_total = $jaundice_above6_total;
			$value->jaundice_above6_frequency = $jaundice_above6_frequency;
			//colera
			$value->colera_between0and5_total = $colera_between0and5_total;
			$value->colera_between0and5_frequency = $colera_between0and5_frequency;
			$value->colera_above6_total = $colera_above6_total;
			$value->colera_above6_frequency = $colera_above6_frequency;
			//worms
			$value->worms_between0and5_total = $worms_between0and5_total;
			$value->worms_between0and5_frequency = $worms_between0and5_frequency;
			$value->worms_above6_total = $worms_above6_total;
			$value->worms_above6_frequency = $worms_above6_frequency;
			//scabies
			$value->scabies_between0and5_total = $scabies_between0and5_total;
			$value->scabies_between0and5_frequency = $scabies_between0and5_frequency;
			$value->scabies_above6_total = $scabies_above6_total;
			$value->scabies_above6_frequency = $scabies_above6_frequency;

			$value->healthpost_preference = $healthpost_preference;
			$value->traditional_preference = $traditional_preference;
			$value->home_medecine_preference = $home_medecine_preference;
			
	    	$value->total=array(
					'diarrhoea'=>0,
					'dysentry'=>0,
					'jaundice'=>0,
					'cholera'=>0,
					'scabies'=>0,
					'worms'=>0
				);
    	}

    	
		// return view("admin.export.waterBorne_disease",compact('data'));
		libxml_use_internal_errors(true);

    	return Excel::download(new HouseHoldExport("admin.export.waterBorne_disease",$data), 'WRO_Baseline_Water_Borne_Disease_status Report.xlsx');
    }

    public function womenHousehold($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach ($data as $value) {
    		$houseHold = DB::table('tbl_household_details')
    					->where('del_flag',0)
    					->where('project_id',$value->id)
    					->get();
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();

    		$widow_number = 0;
    		$female_head_number = 0;

    		foreach ($houseHold as $k => $v) {
    			$widow_number += $v->total_female_widow;
    			if($v->head_gender == "female"){
    				$female_head_number += 1;
    			}
    		}
    		$value->widow_number = $widow_number;
    		$value->female_head_number = $female_head_number;
    	}

		return view('admin.export.women_household',compact('data'));
		libxml_use_internal_errors(true);

    	// return Excel::download(new HouseHoldExport("admin.export.women_household",$data), 'WRO_Baseline_Women_household_status Report.xlsx');
    }

    public function waterFetching($id)
    {
    	if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach ($data as $value) {
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		$houseHold = DB::table('tbl_household_details')
    					->leftjoin('hh_water_fetching_time','hh_water_fetching_time.house_id','=','tbl_household_details.id')
    					->where('del_flag',0)
    					->where('tbl_household_details.project_id',$value->id)
						->get();
		
			$value->source=DB:: table('mst_water_sources')
						->leftjoin('prj_project_water_sources','mst_water_sources.id','=','prj_project_water_sources.water_source_id')
						->where('del_flag',0)
						->where('prj_project_water_sources.project_id',$value->id)
						->get();

    		$total_hh = count($houseHold);
    		$value->hh = count($houseHold);
    		$total_going_second = 0;
    		$total_waiting_second = 0;
    		$total_filling_second = 0;
    		$total_return_second = 0;
    		$total_second = 0;
    		$total_second_8round = 0;
    		$total_minute_8round = 0;
    		$total_hour_8round = 0;
    		$total_water_colletion_pot = 0; 

    		foreach ($houseHold as $k => $v) {
    			$total_going_second +=$v->going_second;
	    		$total_waiting_second +=$v->waiting_second;
	    		$total_filling_second +=$v->filling_second;
	    		$total_return_second +=$v->return_second;
	    		$total_second +=$v->total_second;
	    		$total_second_8round +=$v->total_second_8round;
	    		$total_minute_8round +=$v->total_minute_8round;
	    		$total_hour_8round +=$v->total_hour_8round;
	    		$total_water_colletion_pot +=$v->water_collection_liter;
    		}
    		

    		$value->total_going_second = round($total_going_second/$total_hh);
    		$value->total_waiting_second = round($total_waiting_second/$total_hh);
    		$value->total_filling_second = round($total_filling_second/$total_hh);
    		$value->total_return_second = round($total_return_second/$total_hh);
    		$value->total_second = round($total_second/$total_hh);
    		$value->total_second_8round = round($total_second_8round/$total_hh);
    		$value->total_minute_8round = round($total_minute_8round/$total_hh);
    		$value->total_hour_8round = round($total_hour_8round/$total_hh);
    		$value->total_water_colletion_pot = round($total_water_colletion_pot/$total_hh);

    	}
    		// dd($data);
			return view('admin.export.water_fetching',compact('data'));
		libxml_use_internal_errors(true);

    	// return Excel::download(new HouseHoldExport("admin.export.water_fetching",$data), 'WRO_Time_Saving_Data Report.xlsx');
    }

   	public function CastWise($id)
   	{
   		if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
		$casts = Cast::where('del_flag',0)->orderBy('id')->pluck('name','id');
   		foreach ($data as &$value) {
   			$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
    		$value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();
    		$houseHold_count = [];
    		$male_count=[];
    		$female_count=[];
    		foreach ($casts as $k => $v) {
    			$houseHold_count[] = DB::table('tbl_household_details')
						->where('project_id',$value->id)
						->where('cast_id',$k)
						->where('del_flag',0)
						->count();
				$male_count[] = DB::table('tbl_household_details')
						->where('project_id',$value->id)
						->where('cast_id',$k)
						->where('del_flag',0)
						->sum('total_male');
				$female_count[] = DB::table('tbl_household_details')
						->where('project_id',$value->id)
						->where('cast_id',$k)
						->where('del_flag',0)
						->sum('total_female');
    		}
			$final_array = $this->get_full_final_array($houseHold_count,$male_count,$female_count);
    		// $value->hh_hold_cast = $houseHold_count;
    		// $value->male_count = $male_count;
    		// $value->female_count = $female_count;
    		$value->finals = $final_array;
   		}

   		$data->cast_divisions = Cast_divison::where('del_flag',0)->get();
   		// $cast_division = Cast_divison::where('del_flag',0)->get();
   		foreach ($data->cast_divisions as $k => &$v) {
   			$v->casts = Cast::where('del_flag',0)->where('cast_divison_id',$v->id)->pluck('name','id');

		   }
		//  return $data;
		   return view("admin.export.cast_wise",compact('data'));
		libxml_use_internal_errors(true);

   		return Excel::download(new HouseHoldExport("admin.export.cast_wise",$data), 'WRO_Cast_Wise_Data Report.xlsx');
   	}


   	private function get_full_final_array($houseHold_count,$male_count,$female_count)
   	{
		$return_array = [];
		foreach ($houseHold_count as $key => $value) {
			$return_array[] = $value;
			$return_array[] = $male_count[$key];
			$return_array[] = $female_count[$key];
		}
		return $return_array;
   	}

   	public function AverageCost($id)
   	{
   		if($id == 0){
    		$data = Project::where('del_flag',0)->get();
    	}
    	else{
    		$data = Project::where('del_flag',0)->where('fiscal_year',$id)->get();
    	}
    	foreach($data as $value)
    	{
    		$value->district = DB::table('mst_districts')->where('id',$value->district_id)->pluck('name')->first();
            $value->vdc = DB::table('mst_vdcs')->where('id',$value->vdc_id)->pluck('name')->first();

    		$latrine_cost = 0;
    		$hygiene_data= DB::table('hh_hygiene')
    					->leftjoin('tbl_household_details','hh_hygiene.house_id','=','tbl_household_details.id')
    					->where('hh_hygiene.project_id',$value->id)
    					->where('tbl_household_details.del_flag',0)
    					->get()->toArray();

    		$count_house = count($hygiene_data);
    		foreach ($hygiene_data as $k => $v) {
    			$latrine_cost += $v->latrine_cost;
    		}
    		// $value->average_cost = $latrine_cost/$count_house;
    		// dd($average_cost);
    		if (isset($latrine_cost)) {
    			# code...
    		$value->average_cost = $latrine_cost/$count_house;
    		}
    		else{
    			$value->average_cost = 0;
    		}
    	}
    	// dd($data);
    	// return view("admin.export.average_cost",compact('data'));
    	libxml_use_internal_errors(true);

        return Excel::download(new HouseHoldExport("admin.export.average_cost",$data), 'Average_latrine_Cost.xlsx');
   	}

   	public function logframe($id)
   	{
   		if($id == 0){
    		// $data = Project::where('del_flag',0)->get()->toArray();
    		$data = DB::table('tbl_projects')
    				->leftjoin('tbl_project_capacity','tbl_projects.id','=','tbl_project_capacity.project_id')
    				->leftjoin('tbl_project_disaster','tbl_projects.id','=','tbl_project_disaster.project_id')
    				->leftjoin('tbl_project_knowledge','tbl_projects.id','=','tbl_project_knowledge.project_id')
    				->select('tbl_project_capacity.*','tbl_projects.project_no','tbl_projects.project_name','tbl_project_disaster.*','tbl_project_knowledge.*')
    				->where('tbl_projects.del_flag',0)
    				->get()
    				->toArray();
    	}
    	else{
    		$data = DB::table('tbl_projects')
    				->leftjoin('tbl_project_capacity','tbl_projects.id','=','tbl_project_capacity.project_id')
    				->leftjoin('tbl_project_knowledge','tbl_projects.id','=','tbl_project_knowledge.project_id')
    				->leftjoin('tbl_project_disaster','tbl_projects.id','=','tbl_project_disaster.project_id')
    				->where('tbl_projects.del_flag',0)
    				->where('fiscal_year',$id)
    				->select('tbl_project_capacity.*','tbl_projects.project_no','tbl_projects.project_name','tbl_project_disaster.*','tbl_project_knowledge.*')
    				->get()
    				->toArray();
    	}

    	foreach($data as $value){
    		$household = DB::table('tbl_household_const')
    					->leftjoin('cons_hh_wash','cons_hh_wash.house_id','=','tbl_household_const.id')
    					->leftjoin('cons_hh_health_datas','cons_hh_health_datas.house_id','=','tbl_household_const.id')
    					->leftjoin('cons_hh_water_fetching_time','cons_hh_water_fetching_time.house_id','=','tbl_household_const.id')
    					->select('cons_hh_wash.*','cons_hh_health_datas.reported_below5','cons_hh_health_datas.infants_height','cons_hh_water_fetching_time.total_second')
    					->where('del_flag',0)
    					->where('tbl_household_const.project_id',$value->id)
    					->where('tbl_household_const.first_approve_status',1)
    					->where('tbl_household_const.second_approve_status',1)
    					->get()->toArray();
    		// dd($household);

    		$safety_management_12 = 0;
    		$safety_management_8 = 0;
    		$project_scheme = 0;
    		$school_safety_12 = 0;
    		$cleanliness_competitions = 0;
    		$handwashing_facility = 0;
    		$clean_toilet = 0;
    		$fchv_trained = 0;
    		$school_safe_haven = 0;
    		$grey_water = 0;
    		$report_below_5 = 0;
    		$infants_height = 0;
    		$supply_scheme = 0;
    		$exceeding_service = 0;
    		$affordable_wealth = 0;
    		$total_sanitation = 0;
    		$total_second = 0;

    		foreach ($household as $k => $v) {
    			$safety_management_12 += $v->safety_management_12;
    			$safety_management_8 += $v->safety_management_8;
    			$project_scheme += $v->project_scheme;
    			$school_safety_12 += $v->school_safety_12;
    			$cleanliness_competitions += $v->cleanliness_competitions;
    			$handwashing_facility += $v->handwashing_facility;
    			$clean_toilet += $v->clean_toilet;
    			$fchv_trained += $v->fchv_trained;
    			$school_safe_haven += $v->school_safe_haven;
    			$grey_water += $v->grey_water;
    			$report_below_5 += $v->reported_below5;
    			$infants_height += $v->infants_height;
    			$supply_scheme += $v->supply_scheme;
    			$exceeding_service += $v->exceeding_service;
    			$affordable_wealth += $v->affordable_wealth;
    			$total_sanitation += $v->total_sanitation;
    			$total_second += $v->total_second;
    		}
    		$value->total_safety_management_12 = $safety_management_12;
    		$value->total_safety_management_8 = $safety_management_8;
    		$value->total_project_scheme = $project_scheme;
    		$value->total_school_safety_12 = $school_safety_12;
    		$value->total_cleanliness_competitions = $cleanliness_competitions;
    		$value->total_handwashing_facility = $handwashing_facility;
    		$value->total_clean_toilet = $clean_toilet;
    		$value->total_fchv_trained = $fchv_trained;
    		$value->total_school_safe_haven = $school_safe_haven;
    		$value->total_grey_water = $grey_water;
    		$value->report_below_5 = $report_below_5;
    		$value->infants_height = $infants_height;
    		$value->supply_scheme = $supply_scheme;
    		$value->exceeding_service = $exceeding_service;
    		$value->affordable_wealth = $affordable_wealth;
    		$value->total_sanitation = $total_sanitation;
    		$value->total_second = $total_second;
    		// dd($value);
    	}
    	libxml_use_internal_errors(true);
		// return view("admin.export.logframe",compact('data'));
        return Excel::download(new HouseHoldExport("admin.export.logframe",$data), 'Logframe_Report.xlsx');
   	}
}
