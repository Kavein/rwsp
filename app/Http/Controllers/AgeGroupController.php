<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modules\Fiscal_year\Model\Fiscal_year;
use App\Modules\Project\Model\Project;
use App\Model\AgeGroup;
use App\Modules\District\Model\District;
use DB;
use App\Model\CommunityInfo;

class AgeGroupController extends Controller
{
    public function ageGroupChart(){
    	$fiscal = Fiscal_year::where('del_flag',0)->where('status',1)->first();
        $age = AgeGroup::where('fiscal_year_id',$fiscal->id);
        $district_wise = DB::table('tbl_projects')
        			->leftjoin('mst_districts','mst_districts.id','=','tbl_projects.district_id')
        			->select(DB::raw('COUNT(*) as total_projects'),'mst_districts.name as district_name')
        			->groupBy('tbl_projects.district_id','mst_districts.name')
        			->get()->toArray();


        foreach ($district_wise as $key=>$value) {
	   	    	$districtWise[]=$value->district_name;
	        	$totalProject[]=$value->total_projects;
        }

        $district_pop = DB::table('tbl_projects')
        				->leftjoin('mst_districts','mst_districts.id','=','tbl_projects.district_id')
        				->leftjoin('tbl_household_details','tbl_projects.id','=','tbl_household_details.project_id')
        				->select(DB::raw('SUM(total_member) as total_pop,COUNT(*) as hh'),'mst_districts.name as district_name')
        				->groupBy('tbl_projects.district_id','mst_districts.name')
        				->get();

        foreach($district_pop as $key =>$value){
        	$district_pop_name[] = $value->district_name;
        	$district_pop_total[] = $value->total_pop;
        	$district_hh[] = $value->hh;
        }

        $geographical = DB::table('tbl_household_details')
        				->leftjoin('tbl_projects','tbl_projects.id','=','tbl_household_details.project_id')
        				->select(DB::raw('SUM(total_member) as population,COUNT(*) as hh'),'geographical_region')
        				->groupBy('geographical_region')
        				->get()->toArray();
        foreach ($geographical as $key => $value) {
        	$geo_pop[] = $value->population;
        	$geo_hh[] = $value->hh;
        	$geo_region[] = strtoupper($value->geographical_region);
        }
        
        $between_0to5_male = $age->sum('between_0to5_male');
        $between_0to5_female = $age->sum('between_0to5_female');

        $between_6to15_male = $age->sum('between_6to15_male');
        $between_6to15_female = $age->sum('between_6to15_female');

        $between_16to65_male = $age->sum('between_16to65_male');
        $between_16to65_female = $age->sum('between_16to65_female');

        $above65_male = $age->sum('above65_male');
        $above65_female = $age->sum('above65_female');

        $total_male = $between_0to5_male + $between_6to15_male + $between_16to65_male + $above65_male;
        $total_female = $between_0to5_female + $between_6to15_female + $between_16to65_female + $above65_female;
        // dd($total_female);

        $total_0_5 = $age->value(DB::raw("SUM(between_0to5_male + between_0to5_female)"));
        $total_6_15 = $age->value(DB::raw("SUM(between_6to15_male + between_6to15_female)"));
        $total_16_65 = $age->value(DB::raw("SUM(between_16to65_male + between_16to65_female)"));
        $total_65_above = $age->value(DB::raw("SUM(above65_male + above65_female)"));

        //Disable Chart
        $community_info = CommunityInfo::where('fiscal_year',$fiscal->id);

        $phy_dis_male = $community_info->sum('disabled_male');
        $phy_dis_female = $community_info->sum('disabled_female');

        $blind_dis_male = $community_info->sum('blind_male');
        $blind_dis_female = $community_info->sum('blind_female');

        $deaf_dis_male = $community_info->sum('deaf_male');
        $deaf_dis_female = $community_info->sum('deaf_female');

        $b_d_dis_male = $community_info->sum('blind_deaf_male');
        $b_d_dis_female = $community_info->sum('blind_deaf_female');

        $speech_dis_male = $community_info->sum('speech_male');
        $speech_dis_female = $community_info->sum('speech_female');

        $mental_dis_male = $community_info->sum('mental_male');
        $mental_dis_female = $community_info->sum('mental_female');

        $intel_dis_male = $community_info->sum('intel_male');
        $intel_dis_female = $community_info->sum('intel_female');

        $multiple_dis_male = $community_info->sum('multiple_male');
        $multiple_dis_female = $community_info->sum('multiple_female');

        $total_disable_male = $phy_dis_male + $blind_dis_male + $deaf_dis_male + $b_d_dis_male + $speech_dis_male + $mental_dis_male + $intel_dis_male + $multiple_dis_male;
        $total_disable_female = $phy_dis_female + $blind_dis_female + $deaf_dis_female + $b_d_dis_female + $speech_dis_female + $mental_dis_female + $intel_dis_female + $multiple_dis_female;
        //disable End

        //Project number by Province
        $provinces = DB::table('tbl_projects')
        				->leftjoin('mst_provinces','tbl_projects.province_id','=','mst_provinces.id')
        				->select(DB::raw('count(*) as total_project'),'mst_provinces.name')
        				->groupBy('mst_provinces.name')
        				->get();
        foreach ($provinces as $key => $value) {
        	$provience_name[] = $value->name;
        	$provience_project[] = $value->total_project;
        }
        //End
		//Provice Household and population

        $provience_population = DB::table('tbl_projects')
                                ->leftjoin('mst_provinces','mst_provinces.id','=','tbl_projects.province_id')
                                ->leftjoin('tbl_household_details','tbl_projects.id','=','tbl_household_details.project_id')
                                ->select(DB::raw('Count(tbl_household_details.id) AS total_hh, SUM(total_member) as population'),'mst_provinces.name')
                                ->groupBy('mst_provinces.name')
                                ->get();

        foreach ($provience_population as $key => $value) {
            $prov_pop[] = $value->population; 
            $prov_hh[] = $value->total_hh; 
            $prov_name[] = $value->name; 
        }
        // dd($prov_name);

        //end


        $male = [$between_0to5_male,$between_6to15_male,$between_16to65_male,$above65_male,$total_male];

        $female = [$between_0to5_female,$between_6to15_female,$between_16to65_female,$above65_female,$total_female];

        $age_total = [$total_0_5,$total_6_15,$total_16_65,$total_65_above];

        $district_wise_pro =[$districtWise,$totalProject];

        $disabled_male =[$phy_dis_male,$blind_dis_male,$deaf_dis_male,$b_d_dis_male,$speech_dis_male,$mental_dis_male,
        				$intel_dis_male,$multiple_dis_male,$total_disable_male];
		$disabled_female =[$phy_dis_female,$blind_dis_female,$deaf_dis_female,$b_d_dis_female,$speech_dis_female,$mental_dis_female,
        				$intel_dis_female,$multiple_dis_female,$total_disable_female];
        $disable_gender =[$total_disable_male,$total_disable_female];

        $array = [
        	'male' =>$male,
        	'female' =>$female,
        	'age_total' =>$age_total,
        	'district_wise_name' =>$districtWise,
        	'district_wise_project' =>$totalProject,
        	'district_pop_name' => $district_pop_name,
        	'district_pop' => $district_pop_total,
        	'district_hh' => $district_hh,
        	'disable_male' => $disabled_male,
        	'disable_female' => $disabled_female,
        	'disable_gender' => $disable_gender,
			'geo_pop'=>$geo_pop,
			'geo_hh'=>$geo_hh,
			'geo_region'=>$geo_region,
			'provience_name'=>$provience_name,
			'provience_project'=>$provience_project,
            'prov_pop' => $prov_pop,
            'prov_hh' => $prov_hh,
            'prov_name' => $prov_name,

        ];
        return response()->json($array);

    }
}
