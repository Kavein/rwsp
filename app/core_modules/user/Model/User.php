<?php

namespace App\Core_modules\User\Model;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    public  $table = 'users';

    protected $fillable = ['id','username','control','last_visit','status','activation_key','email','password','remember_token','created_at','updated_at',];

    public function roles(){
        return $this->belongsTo('App\Core_modules\Role_user\Model\Role_user', 'role_user','user_id', 'role_id');
    }
}
