<?php
return [
    'modules'=>array(
        "developer_tool", 	//Do Not Change These Files      
        "permission",	//Do Not Change These Files
        "permission_role", 	//Do Not Change These Files      
        "role",	//Do Not Change These Files
        "role_user",  	//Do Not Change These Files     
        "user",	//Do Not Change These Files\

        'demo',
        'project_phase',
        'system_type',
        'service_type',
        'water_source',
        'cast_divison',
        'province',
        'district',
        'vdc','fiscal_year','funding_partner','occupation','awc','project','cast','religion','household_detail','household_const',
        'hygiene','literacy','income_source','prep_household_detail','position','prep_construction','community_info'

    ),
];